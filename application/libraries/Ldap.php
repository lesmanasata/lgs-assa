<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Ldap Class
 *
 * Build yaur app login with LDAP
 *
 * @category		Libraries
 * @author			Sata Lesamna

 */
class Ldap
{
	private $url = "103.90.250.241/rest/index.php"; //"103.90.250.241/rest/index.php"; //192.168.98.2/rest/index.php/ldap_browse

	function browse(){
		$curl = curl_init();
		$header = array("Accept: application/json");
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_ENCODING, "gzip");
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_URL, $this->url."/ldap_browse");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

		$resp = curl_exec($curl);
		curl_close($curl);

		return $resp;
	}

	function login($username,$password){
		$curl = curl_init();
		curl_setopt_array($curl, 
			array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_USERAGENT=> 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)',
				CURLOPT_URL => $this->url."/ldap_login",
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => array(
			        'username' => $username,
			        'password' => $password
			    )
			)
		);
		$resp = curl_exec($curl);
		curl_close($curl);
		return $resp;
	}

}
