<?php
/*
* This is laguage configuration asset for english language.
* make sure your load default language $this->lod->laguage('language','english');
* used : echo lang('language_key');
* ex   : echo lang('title_cst');
*@lesmanasata
* lesmana@gmail.com
*/


/*
* Title language
*/
$lang["titile_pwd"] 				= "Change Password";
$lang["titile_dist"] 				= "Cost Distribution";
$lang["titile_ldist"] 				= "List Of Cost Distribution";
$lang["titile_ldist2"] 				= "Cost Distribution Uploaded";
$lang["titile_confirm_dialog"] 	    = "Confirmation";
$lang["titile_ar_clearing"] 		= "List Of Bank";
$lang["titile_bp_clearing"] 		= "List Of Bank";
$lang["titile_ar_form"] 		    = "AR Clearing";
$lang["titile_sspt_form"] 		    = "PPN SSP Clearing";
$lang["titile_ssptint_form"] 		= "PPN SSP Clearing Inter Branch";
$lang["titile_ar_form_int"] 		= "AR Clearing Inter Branch";
$lang["titile_bp_form"] 		    = "BP Clearing";
$lang["titile_bp_form_unt"] 		= "BP Clearing Inter Branch";
$lang["titile_arint_clearing"] 		= "List Of Bank";
$lang["titile_bpint_clearing"] 		= "List Of Bank";

$lang["titile_arint_form"] 		    = "AR Clearing Inter Branch";
$lang["titile_bpint_form"] 		    = "BP Clearing Inter Branch";
$lang["titile_operators"] 		    = "List Of Operators";
$lang["titile_log_session"] 		= "List Of Loged Operators";

$lang['titile_dah_user_aktfif']		= 'Loged User';

/*
* Form label
*/
$lang["pwd_new"]    			= "New Password";
$lang["pwd_retype"] 			= "Retype New Password";
$lang["pwd_old"]    			= "Olad Password";

$lang["dist_cmp_code"]      	= "Company"; //Company Code
$lang["dist_binis_area"]    	= "Business Area";
$lang["dist_cost_center"]		= "Cost Center";
$lang["dist_gl_acc"]			= "GL Account";
$lang["dist_curency"]			= "Currency";
$lang["dist_amount"]			= "Amount";
$lang["dist_post_date"]			= "Posting Date";
$lang["dist_doc_date"]			= "Document Date";
$lang["dist_remark"]			= "Remark";
$lang["dist_internal_order"]	= "Internal Order";
$lang["dist_text"]				= "Text";
$lang["dist_hider_text"]		= "Doc. Header Text";
$lang["dist_billing_doc"]		= "Billing Document";
$lang["dist_yer_doc"]			= "Year";

$lang['rpt_odr_id']				= "Order Id";
$lang['rpt_odr_notif']			= "Notif Id";
$lang['rpt_odr_type']			= "Order Type";
$lang['rpt_odr_funclock']		= "Funct Loc";
$lang['rpt_odr_equipment']		= "Equipment Id";
$lang['rpt_odr_equipment_desc']	= "Equipment Name";
$lang['rpt_odr_status']			= "Status";
$lang['rpt_odr_soldtoprty']		= "Sold Toparty";
$lang['rpt_odr_date_start']		= "Date Start";
$lang['rpt_odr_date_end']		= "Date End";

$lang['ar_doc_no']				= "Doc Number";
$lang['ar_doc_yer']				= "Doc Year";
$lang['ar_doc_type']			= "Doc Type";
$lang['ar_post_key']			= "Posting Key";
$lang["ar_outs"]				= "Outstanding";
$lang["ar_advance"]				= "Advance";
$lang["ar_tax"]					= "Ar Tax";
$lang["ar_amount"]				= "Ar Amount";
$lang["ar_total"]				= "Ar Total";
$lang["ar_cleare_amount"]		= "Clear Amount";
$lang["ar_posted"]				= "Posted Value";
$lang["ar_pph_indicator"]		= "PPH Indicator";
$lang["ar_pph_amount"]			= "PPH Amount";
$lang["ar_banalance"]			= "Balance";
$lang["ar_status"]				= "Status";
$lang['ar_cus_name']			= "Customer";

$lang['ar_tbl_posted']			= 'POSTED';
$lang['ar_tbl_clear']			= 'CLEAR';
$lang['ar_tbl_balance']			= 'BALANCE';
$lang['ar_tbl_pph_ind']			= 'PPH IND (%)';
$lang['ar_tbl_doc_numb']		= 'DOC NUMB';
$lang['ar_tbl_doc_date']		= 'DOC DATE';
$lang['ar_tbl_pst_date']		= 'POST DATE';
$lang['ar_tbl_bist_area']		= 'AREA';
$lang['ar_tbl_amount']			= 'AR AMOUNT';
$lang['ar_tbl_tax']				= 'AR TAX';
$lang['ar_tbl_total']			= 'AR TOTAL';
$lang['ar_tbl_pph_amount'] 		= 'PPH';
$lang['ar_tbl_balance']			= 'BALANCE';
$lang['ar_tbl_stat']			= 'STATUS';
$lang['ar_wnd_cpcode']			= 'CP CODE';
$lang['ar_wnd_dcno']			= 'DOC NUMB';
$lang['ar_wnd_dcyer']			= 'DOC YER';
$lang['cs_bis']         		= 'Area'; //Bisnis Area
$lang['cos_ar']         		= 'Costcenter';
$lang['cos_ket']        		= 'Keterangan';

$lang['cos_id']                 = 'Customer ID';
$lang['ar_cust']        		= 'CUSTOMER';
$lang['cos_name']               = 'Customer Name';
$lang['dash_view_dtl']			= 'View Detail Item';

$lang['dash_view_dtl']			= "View Detail Item";
$lang['bp_tbl_tgl_bukpot']			= "TGL BUKPOT";
$lang['bp_tbl_numb_bukpot']			= "NUMBER BUKPOT";


$lang['cmb_select_cust'] ="Select Customer...";
$lang['cmb_select_ptg'] ="Select Petugas...";
$lang['rpt_df_none']	="type report is undefined";
/*
* Main menu label
*/
$lang["mn_dash"] 				= "Dashboard";
$lang["mn_master"] 				= "Master Data";
$lang["mn_clearing_bp"] 		= "BP Clearing";
$lang["mn_clearing"] 			= "Clearing";
$lang["mn_clearing_ar"]			= "AR Clearing";
$lang["mn_clearing_finance"]	= "Clearing Finance";
$lang["mn_distribute_main"]		= "Cost Distribution";
$lang["mn_distribute_form"]		= "Add New Distribution";
$lang["mn_distribute_list"]		= "List Of Distribution";
$lang["mn_costcenter"]          = "Cost Center";
$lang["mn_clearing_ar_int"]     = "AR Clearing Inter Branc";
$lang["mn_clearing_bp_int"]     = "BP Clearing Inter Branc";
$lang["mn_petugas"]          	= "Operators";
$lang["mn_log_session"]         = "Log Session";
$lang["mn_rpt_detail"]          = "Report Detail";
$lang["mn_rpt_araging"]          = "AR Aging";
$lang["mn_rpt_log"]          	= "Log Trans SAP";
$lang["mn_rpt"]          		= "Report Clearing";
$lang["mn_rpt_detail_form"]          = "Report Detail Form";
$lang["mn_customer"]            = "Customer";
$lang['mn_jv']					= "Journal Voucher";
$lang['mn_jv_add']				= "Add New JV";
$lang['mn_jv_list']				= "Show List Of JV";
$lang['mn_jv_print']			= "Print JV";
$lang['mn_billing']				= "Billing";
$lang['mn_billing_add']			= "Add New Billing";
$lang['mn_billing_list']		= "Show LIst Of Billing";
$lang['mn_order']				= "Order/SPK";
$lang['mn_order_main']			= "Create SPK";
$lang['mn_order_zbak']			= "Create SPK OR01";
$lang['mn_order_list']			= "List SPK";
$lang['mn_order_mnt']			= "Maintenance Plan";
$lang['mn_order_rpt']			= "Report SPK";

$lang['mn_notifikasi']			= "Notification";
$lang['mn_nf_disposal']			= "Disposal Unit";
$lang['mn_nf_mutasi']			= "Mutasi Unit";
$lang['mn_nf_ganti']			= "Temporary Replacement";
$lang['mn_nf_tgskantor']		= "Office tasks";
$lang['mn_nf_compli']			= "Compliment";
$lang['mn_nf_other']			= "Other Gatepass";
$lang['mn_notifikasi_list']		= "List Of Notification";
$lang['mn_file_manajer']		= "File Manager";
$lang['mn_sspt']				= "PPN SSP";
$lang['mn_sspt_int']			= "PPN SSP Inter Branch";


$lang['btn_aktif']          = 'Active';
$lang['btn_nonaktif']       = 'Nonactive';
$lang['nfc_aktif_confirm']          = "Are you sure to active this record Id ";
$lang['nfc_nonaktif_confirm']       = "Are you sure to nonactive this record Id ";
/*
* Button and link action
*/

$lang["pwd_btn_submit"] 		= "Change Password";
$lang["pwd_btn_cancel"] 		= "Cancel";
$lang['btn_save']				= "Save Record";
$lang['btn_cancel']				= "Cancel";
$lang['label_action']			= "Action";
$lang['btn_edit_record']		= "Edit Record";
$lang['btn_delete']				= "Delete Record";
$lang['btn_proses']				= "Proses";
$lang['btn_find']				= "Find";
$lang['btn_add_item']       	= "Add Item";
$lang['lbl_code']				= "Code";
$lang['lbl_text']				= "Description Code";
$lang['btn_select']				= "Select";
$lang['btn_hitung']				= "Calculate";
$lang['btn_reload']				= "Reload";
$lang["btn_ptg_clear"]      = "Clear Logged Session";
$lang['tbl_ptg_id']        = 'User Id';
$lang['tbl_ptg_limit']     = 'User Limitation';
$lang['tbl_ptg_name']      = 'Name';
$lang['tbl_ptg_email']     = 'E-Mail Adress';
$lang['tbl_ptg_type']      = 'User Type';
$lang['tbl_ptg_sts']       = 'Status';
$lang['tbl_ptg_logtime']       = 'Login Time';
$lang['tbl_ptg_uagent']       = 'User Agent';

$lang['form_ptg_title']     = 'User Form';
$lang['form_ptg_type']      = 'User Type';
$lang['form_ptg_sal']       = 'Salutation';
$lang['form_ptg_first']     = 'First Name';
$lang['form_ptg_limit']     = 'User Limitation';
$lang['form_ptg_last']      = 'Last Name';
$lang['form_ptg_email']     = 'E-Mail Adress';
$lang['form_ptg_uname']     = 'Username';
$lang['form_ptg_pass']      = 'Password';

$lang['frm_balance_label']	= 'Balance';
$lang['form_balance_amout']	= 'Amount';
$lang['frm_balance_type']	= 'Type';

$lang['frm_balance_cust']	= 'Customer';

/*
* Order label
*/
$lang['ord_notif_no']        	= "Notif Order";
$lang['ord_notif_type']        	= "Notif Type";
$lang['ord_description']        = "Description";
$lang['ord_sold_toparty']       = "Sold to Party";
$lang['ord_sold_toparty_desc']  = "Sold to party Desc";
$lang['ord_reported']        	= "Reported";
$lang['ord_date']        		= "Date";
$lang['ord_equipment']        	= "Equipment";
$lang['ord_functional_loc']     = "Functional Loc";
$lang['ord_busines_area']       = "Bunisness Area";
$lang['ord_voc_number']        	= "VOC Number";
$lang['ord_serca_number']       = "Serca Number";

$lang['ord_opr_number']       	= "Operation Number";
$lang['ord_plan']       		= "Plant";
$lang['ord_workcenter']       	= "Work Center";
$lang['ord_type']       		= "Type";

$lang['ord_part_number']       	= "Part Number";
$lang['ord_part_description']   = "Part Description";
$lang['ord_uom']       			= "UOM";
$lang['ord_qty']       			= "Qty";
$lang['ord_price']       		= "Price";
$lang['ord_purch_group']       	= "Purch Group";

$lang['ord_sort_text']       	= "Activity";




/*
* Notification label
*/

$lang['nfc_blank_table_row']        = "Data is empty";
$lang['nfc_dialog']                 = "Dialog Window";
$lang['nfc_confirm']                 = "Dialog Confirmation";
$lang['nfc_del_confirm']            = "Are you sure delete record Id ";
$lang['msg_failed_del_record'] 	    = "Failed to delete record";
$lang['msg_success_del_record']     = "Record successfully deleted";
$lang['msg_success_save_record']    = "Record successfully saved";
$lang['msg_failed_save_record']    = "Failed to Saved Record";
$lang['msg_no_selected'] 		 	= "No data Selected";
$lang['msg_one_select_only']		= "Select Only One data";
$lang['msg_uname_is_taken']			= "Username is already used";

$lang['rpt_find_id']				= "Filter by id";
$lang['rpt_find_custome']			= "Use Custome Filter";
$lang['rpt_find_yer']				= "Document Year";
$lang['rpt_find_peride']			= "Periode";
$lang['rpt_find_peride_one']		= "Daily";
$lang['rpt_find_peride_two']		= "Monthly";
$lang['rpt_find_date']				= "Date";
$lang['rpt_find_month']				= "Month";
$lang['rpt_find_user']				= "User Processing";
$lang['rpt_find_trans_type']		= "Transaction Type";

$lang['msg_null_company_code'] 		= "Company Code can not be empty";
$lang['msg_null_bisnis_area'] 		= "Business Area can not be empty";
$lang['msg_null_cost_center'] 		= "Cost Center can not be empty";
$lang['msg_null_gl_account'] 		= "GL Account can not be empty";
$lang['msg_null_amount'] 			= "Amount can not be empty and Number Only";
$lang['msg_null_item_detail']		= "Please <b>Add New Record</b> First";
$lang['msg_ammount_difrn']			= "Amount is not balanced";
$lang['msg_wht_type_null']          = "WHT Type cannot be empty!";
$lang['msg_wht_code_null']          = "WHT Code cannot be empty!";
$lang['msg_vendor_null']            = "Vendor cannot be empty!";
$lang['msg_gl_vendor_null']         = "GL Account Vendor cannot be empty!";
$lang['msg_payterm_null']           = "Pay Term cannot be null!";

$lang['msg_dist_ok']				= "Cost distribution has been successfully processed";
$lang['msg_jv_ok']					= "Jurnal Voucher has been successfully processed";
$lang['msg_validate_cost_center'] 	= "Cost Center can not be the same";
$lang['msg_calculated'] 			= "data successfully calculated";
$lang['msg_amount_invalid'] 		= "The Number <b>Amount</b> and Sum of <b>Clear</b> is does not match";
$lang['msg_balance_minus'] 			= "Balance should not be less than zero (0)";
$lang['msg_clear_minus'] 			= "Clear should not be less than zero (0)";
$lang['msg_amount_zero_titipan']	= "Amount in balance should not be less than zero (0)";
$lang['mgs_balance_null']			= "Balance cannot be empty";
$lang['mgs_balance_amount_limit']	= "Amount is invalid";
$lang['msg_info_blank_pass']  		= 'Be sure to remain empty if you do not want to change password';
$lang['msg_empty_balance_amount']	= 'The amout balance can not be empty or zero (0)';
$lang['msg_validate_empty_cost_center']	= 'Cost Center on item cannot be empty';
$lang['msg_validate_empty_internal_order']	= 'Internal Order cannot be empty';
$lang['msg_difrn_bisnis_area']		= 'Item Detail should be different area';
$lang['msg_validate_empty_internal_order']	= 'Internal Order can not be empty';
