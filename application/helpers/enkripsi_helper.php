<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('my_encrypt'))
{
	function my_encrypt($pure_string) {
	    $dirty = array("+", "/", "=");
	    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $_SESSION['iv'] = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, "lamsolusi", utf8_encode($pure_string), MCRYPT_MODE_ECB, $_SESSION['iv']);
	    $encrypted_string = base64_encode($encrypted_string);
	    return str_replace($dirty, $clean, $encrypted_string);
	}
}

if ( ! function_exists('my_decrypt'))
{
	function my_decrypt($encrypted_string) { 
	    $dirty = array("+", "/", "=");
	    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");

	    $string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

	    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, "lamsolusi",$string, MCRYPT_MODE_ECB, $_SESSION['iv']);
	    return $decrypted_string;
	}
}