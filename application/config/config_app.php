<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

/*
|--------------------------------------------------------------------------
| User Permission
|--------------------------------------------------------------------------
|
| Jenis user yang dapat digunakan untk mengakses aplikasi, USER_UNDEFINE 
| merupakan user yang diambil dari LDAP dan harus dirubah oleh USER_ADMIN
|
*/
define('ALLOWED_USER','10,11,12');
define('USER_ADMIN',10);
define('USER_AREA',11);
define('USER_UNDEFINE',12);


/*
|--------------------------------------------------------------------------
| Informasi Sistem
|--------------------------------------------------------------------------
|
| default "app_theme" adalah mouldifi adapun untuk tema lainnya adalah "default"
|
*/
$config['version'] 		= '1.0.0';
$config['app'] 			= 'ASSA';
$config['app_theme'] 	= 'mouldifi';
$config['df_curency'] 	= 'IDR'; 
$config['gl_tax'] 		= array('1171000000','2162000000','2162000001','2162000002','2162000003','2162000004','2162000005','2162000006');
$config['gl_materai'] 	= array('6102000015');


/*
|--------------------------------------------------------------------------
| Configurasi Module Titipan
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
*/
$config['titipan']['exseption_key']			= 'ARL,ARI,BPL,BPI';
$config['titipan']['initial_key']			= 'T';
$config['titipan']['type_doc_in']			= 'DZ,IB';
$config['titipan']['max_value']				= 5000;
$config['titipan']['min_value']				= -5000;
$config['titipan']['min_bank_charge']		= 30000;
$config['titipan']['type_for_ar']			= 'Titipan,Bank Charge,Selisih';
$config['titipan']['type_for_bp']			= 'Other Income,Selisih';
$config['titipan']['posting_key_titipan']	= '19,11';
$config['titipan']['posting_key_bank_chrg']	= 50;
$config['titipan']['posting_key_other_income']= 50;
$config['titipan']['posting_key_slisih_pls']= 50;
$config['titipan']['posting_key_slisih_mns']= 40;
$config['titipan']['gl_titipan']			= 2131000000;
$config['titipan']['gl_selisih']			= 6102000033;
$config['titipan']['gl_charge']				= 6102000006;
$config['titipan']['gl_admlelang']			= 5000000101;
$config['titipan']['gl_other_income']		= 4200999999;



/*
|--------------------------------------------------------------------------
| Configurasi Module AR Clearing
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| Terdapat dua proses AR Clearing 
| 1. ARL = Ar Clearing Local
| 2. ARI = Ar Clearing Interbranch
|
*/
$config['ar_clearing']['type_doc_in'] 		= 'ZR,UE';
$config['ar_clearing']['type_doc_tag'] 		= 'RV,LL,DN,DP,SA';
$config['ar_clearing']['output_type'] 		= 'DZ';
$config['ar_clearing']['initial'] 			= 'ARL';
$config['ar_clearing']['pph'] 				= 2;
$config['ar_clearing']['tolerance']			= 1;
$config['ar_clearing']['get_postkey_in'] 	= 50;
$config['ar_clearing']['get_postkey_tag'] 	= 01;
$config['ar_clearing']['set_postkey_in'] 	= 40;
$config['ar_clearing']['set_postkey_tag'] 	= 15;

$config['ar_clearing_int']['df_pphind'] = 2;
$config['ar_clearing_int']['df_doc_type_ar'] = 'IB';
$config['ar_clearing_int']['initial_ar_clearing'] = 'ARI';
$config['ar_clearing_int']['cf_bank_key'] = 50;
$config['ar_clearing_int']['cf_bank_doc_type'] = 'ZR,UE';
$config['ar_clearing_int']['df_pph'] = 2;
$config['ar_clearing_int']['set_postkey_in'] = 40;
$config['ar_clearing_int']['set_postkey_tag'] = 15;



/*
|--------------------------------------------------------------------------
| Configurasi Module Clearing Bukti Potong (BP Clearing)
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| Berdasarkan dokument output yang dihasilkan proses BP Clearing terbagi
| menjadi tiga.
|
| 1. BPL = BP Clearing Local
| 2. BPI = BP Clearing Interbranch
| 3. ARB = BP Clearing dengan mengunakan doc Bank 'DZ','ZR','IB'
|
*/

$config['bp_clearing']['type_doc_in'] 		= 'BP,ZR,UE';
$config['bp_clearing']['type_doc_tag'] 		= 'RV,LL,DN,DP,SA';
$config['bp_clearing']['output_type'] 		= 'DZ';
$config['bp_clearing']['initial'] 			= 'BPL';
$config['bp_clearing']['pph'] 				= 2;
$config['bp_clearing']['tolerance']			= 1;
$config['bp_clearing']['get_postkey_in'] 	= 50;
$config['bp_clearing']['get_postkey_tag'] 	= 01;
$config['bp_clearing']['set_postkey_in'] 	= 40;
$config['bp_clearing']['set_postkey_tag'] 	= 15;

$config['bp_clearing_int']['cf_bank_doc_type'] 		= 'BP,ZR,UE';
$config['bp_clearing_int']['df_pphind'] 			= 2;
$config['bp_clearing_int']['output_type'] 			= 'IB';
$config['bp_clearing_int']['initial_ar_clearing'] 	= 'BPI';
$config['bp_clearing_int']['cf_bank_key'] 			= 50;
$config['bp_clearing_int']['set_postkey_tag'] 		= 15;



/*
|--------------------------------------------------------------------------
| Configurasi Module PPN SSP
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| Terdapat dua proses SP Clearing 
| 1. SPL = SP Clearing Local
| 2. SPI = SP Clearing Interbranch
|
*/
$config['ar_sspt']['type_doc_in'] 		= 'SP';
$config['ar_sspt']['type_doc_tag'] 		= 'RV,LL,DN,DP,SA';
$config['ar_sspt']['output_type'] 		= 'DZ';
$config['ar_sspt']['initial'] 			= 'SPL';
$config['ar_sspt']['pph'] 				= 2;
$config['ar_sspt']['tolerance']			= 1;
$config['ar_sspt']['get_postkey_in'] 	= 50;
$config['ar_sspt']['get_postkey_tag'] 	= 01;
$config['ar_sspt']['set_postkey_in'] 	= 40;

$config['ar_sspt_int']['df_pphind'] 			= 2;
$config['ar_sspt_int']['df_doc_type_ar'] 		= 'IB';
$config['ar_sspt_int']['initial_ar_clearing']	= 'SPI';
$config['ar_sspt_int']['cf_bank_key'] 			= '50';
$config['ar_sspt_int']['cf_bank_doc_type'] 		= 'SP';
$config['ar_sspt_int']['df_pph'] 				= 2;
$config['ar_sspt_int']['set_postkey_in'] 		= 40;
$config['ar_sspt_int']['set_postkey_tag'] 		= 15;


/*
|--------------------------------------------------------------------------
| Configurasi Module Journal Voucher (JV)
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk nenentukan penginisialan document JV
|
| 
|
*/
$config['jv']['initial'] = 'JV';


/*
|--------------------------------------------------------------------------
| Configurasi Module Cost Distribution
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk nenentukan penginisialan document Cost 
| Distribution
|
| 
|
*/
$config['cost_center']['initial'] = 'COS';



/*
|--------------------------------------------------------------------------
| Configurasi Module Notifikasi
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| 
|
*/
$config['notif_cfg']['disposal'] 			= "M1-00001";
$config['notif_cfg']['mutasi'] 				= "M2-00001";
$config['notif_cfg']['ganti_sementara']		= "M3-00001";
$config['notif_cfg']['tugas_kantor']		= "M4-00001";
$config['notif_cfg']['comliment']			= "M5-00001";
$config['notif_cfg']['other']				= "M6-00001";


/*
|--------------------------------------------------------------------------
| Configurasi Module Billing
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| Format date_cutof = d-M-Y
|
*/
$config['billing']['date_cutof']			= "01-01-2000";
$config['billing']['tmp_key']				= "BIL";
$config['billing']['initial_key']			= "BIL";
$config['billing']['partnerFunction']		= "AG,AP,RE,RG,WE,VE,Z1,Z2,Z3,Z4,Z5,Z6,ZA,ZB,ZC,ZD,ZE,ZF,ZG,ZP,ZS,Y1,Y2";



/*
|--------------------------------------------------------------------------
| Configurasi Module Order / SPK
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan document yang akan diproses dan 
| document yang menjadi perbandingan serta hasil output dari proses yang
| dilakukan.
|
| 
|
*/

$config['order']['order_type_sr'] = array( 
			array('key'=>'WO01', 'desc'=>'WO01 Breakdown Order', 'layout'=>'one'),
			array('key'=>'WO02', 'desc'=>'WO02 Corrective Order', 'layout'=>'two'),
			array('key'=>'WO03', 'desc'=>'WO03 Ekspedisi Order', 'layout'=>'two'),
			array('key'=>'WO05', 'desc'=>'WO05 Preventive Order (Perawatan Berkala)', 'layout'=>'two'),
			array('key'=>'ZBAK', 'desc'=>'ZBAK Berita Acara Kejadian', 'layout'=>'two')
		);

$config['order']['order_type_zbak'] = array( 
			array('key'=>'OR01', 'desc'=>'OR01 Own Risk', 'layout'=>'two')
		);


/*
|--------------------------------------------------------------------------
| Backup PSWD
|--------------------------------------------------------------------------
|
| Configurasi digunakan untuk menentukan password back end pada sistem,
| enkripsi menggunakan MD5
|
| 
|
*/

$config['psc'] = "8179ba23706b491958d856a40444a4b5";



/*
|--------------------------------------------------------------------------
| Configurasi Menu
|--------------------------------------------------------------------------
|
| Pengaturan menu yang ditampilkan pada sistem, sesuai dengan user permission
| yang dibuat.
|
| 
|
*/
$config['list_menu'] = array(
	'dashboard/index'=>array(
		'mn_dash',array(USER_ADMIN,USER_AREA,USER_UNDEFINE)
	),
	'clearing/index'=>array('mn_clearing',array(USER_ADMIN,USER_AREA),
		array(
			'clearing/ar'=>array('mn_clearing_ar',array(USER_ADMIN,USER_AREA)),
			'clearing/arint'=>array('mn_clearing_ar_int',array(USER_ADMIN,USER_AREA)),
			'clearing/bp'=>array('mn_clearing_bp',array(USER_ADMIN,USER_AREA)),
			'clearing/bpint'=>array('mn_clearing_bp_int',array(USER_ADMIN,USER_AREA)),
			'clearing/sspt'=>array('mn_sspt',array(USER_ADMIN,USER_AREA)),
			'clearing/ssptint'=>array('mn_sspt_int',array(USER_ADMIN,USER_AREA)),
		),'icon-doc-text',
	),
	'rpt/detail/index'=>array('mn_rpt',array(USER_ADMIN,USER_AREA),
		array(
			'rpt/detail'=>array('mn_rpt_detail',array(USER_ADMIN,USER_AREA)),
			'rpt/araging'=>array('mn_rpt_araging',array(USER_ADMIN,USER_AREA)),
			'rpt/log'=>array('mn_rpt_log',array(USER_ADMIN,USER_AREA)),
		),'icon-window',
	),'distribute/index'=>array('mn_distribute_main',array(USER_ADMIN,USER_AREA),
		array(
			'distribute/index'=>array('mn_distribute_form',array(USER_ADMIN,USER_AREA)),
			'distribute/table'=>array('mn_distribute_list',array(USER_ADMIN,USER_AREA)),
		),'icon-popup',
	),
	'jv/index'=>array('mn_jv',array(USER_ADMIN,USER_AREA),
		array(
			'jv/index'=>array('mn_jv_add',array(USER_ADMIN,USER_AREA)),
			'jv/jv_list'=>array('mn_jv_list',array(USER_ADMIN,USER_AREA)),
			'jv/printjv'=>array('mn_jv_print',array(USER_ADMIN,USER_AREA)),
		),'icon-list',
	),'billing/index'=>array('mn_billing',array(USER_ADMIN,USER_AREA),
		array(
			'billing/index'=>array('mn_billing_add',array(USER_ADMIN,USER_AREA)),
			'billing/list'=>array('mn_billing_list',array(USER_ADMIN,USER_AREA)),
		),'icon-folder',
	/*),'notif/index'=>array('mn_notifikasi',array(USER_ADMIN,USER_AREA),
		array(
			'notif/disposal'=>array('mn_nf_disposal',array(USER_ADMIN,USER_AREA)),
			'notif/mutasi'=>array('mn_nf_mutasi',array(USER_ADMIN,USER_AREA)),
			'notif/ganti'=>array('mn_nf_ganti',array(USER_ADMIN,USER_AREA)),
			'notif/tugas'=>array('mn_nf_tgskantor',array(USER_ADMIN,USER_AREA)),
			'notif/compli'=>array('mn_nf_compli',array(USER_ADMIN,USER_AREA)),
			'notif/other'=>array('mn_nf_other',array(USER_ADMIN,USER_AREA)),
			'notif/list'=>array('mn_notifikasi_list',array(USER_ADMIN,USER_AREA)),
		),'icon-bell',*/
	),
	'order/index'=>array('mn_order',array(USER_ADMIN,USER_AREA),
		array(
			'order/index/sr'=>array('mn_order_main',array(USER_ADMIN,USER_AREA)),
			'order/index/zbak'=>array('mn_order_zbak',array(USER_ADMIN,USER_AREA)),
			'order/list'=>array('mn_order_list',array(USER_ADMIN,USER_AREA)),
			'order/plan'=>array('mn_order_mnt',array(USER_ADMIN,USER_AREA)),
			'order/rpt'=>array('mn_order_rpt',array(USER_ADMIN,USER_AREA)),
		),'icon-window',
	),
	'costcenter/index'=>array('mn_master',array(USER_ADMIN),
		array(
			'costcenter/index'=>array('mn_costcenter',array(USER_ADMIN)),
			'customer/index'=>array('mn_customer',array(USER_ADMIN)),
			'user/petugas/index'=>array('mn_petugas',array(USER_ADMIN)),
			'user/session/index'=>array('mn_log_session',array(USER_ADMIN)),
			'app/filemanager/index'=>array('mn_file_manajer',array(USER_ADMIN)),
		),'icon-newspaper',
	),
);
