<h1 class="page-title">Report AR Aging</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Report Clearing</a></li>
  <li class="active"><strong>AR Aging</strong></li>
</ol>

<?php $timestamp = time();?>

<div class="containers">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#contact_01"  data-toggle="tab">Report AR Aging</a></li>
        <li ><a href="#contact_02"  data-toggle="tab">List Pembayaran Invoice</a></li>
        <li ><a href="#contact_03"  data-toggle="tab">Listing Invoice</a></li>
    </ul>
    <div class="tab-content grid-form1">
        <div class="tab-pane fade in active" id="contact_01">
        	<div class="padding_tab_body">
        		<form class="form-horizontal" id="frm_rpt_aging" target="_blank" action="<?php echo site_url('rpt/araging/requestdata'); ?>" method="POST">
        			<div class="form-group">
					    <label  class="control-label col-md-2">Doc Year	</label>
					    <div class="col-sm-2">
					      <input type="text" class="kendodropdown form-control" style="width:100%;" name="doc_year"  id="doc_year" placeholder="&mdash;&mdash;Doc Year&mdash;&mdash;">
					    </div>
					</div>
        			<div class="form-group">
						<label  class="col-sm-2 control-label hor-form"><?php echo lang('dist_cmp_code'); ?></label>
						<div class="col-sm-4">
							<input type="text" class="kendodropdown form-control" style="width:100%;" name="company_code"  id="company_code" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;">
						</div>
					</div>


        			<div class="form-group">
						<label class="control-label col-md-2" for="appendbutton">Customer</label>
						<div class="col-md-6">
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
										<input class="form-control" id="customerId" name="customer" type="text" readonly="">
										<div class="input-group-btn">
											<button class="btn btn-primary" type="button" id="btn_find"> Find Customer </button>
											<button class="btn btn-danger" type="button" id="btn_cancel">Reset</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
					    <label class="control-label col-md-2">&nbsp;</label>
					    <div class="col-sm-4">
					      <button type="button" id="btn_rpt_aging" class="btn btn-primary mb-2">Proses Data</button>
					      <button type="button" id="btn_rp_aging_excel" class="btn btn-success mb-2">Export to Excel</button>
					    </div>
					</div>
				</form>
				<div id="gridAraging"></div>
        	</div>
        </div>
        <div class="tab-pane fade in " id="contact_02">
        	<div class="padding_tab_body">
        		<?php $this->load->view('rpt/rpt_pembayaran_inv'); ?>
        	</div>
        </div>
        <div class="tab-pane fade in " id="contact_03">
        	<div class="padding_tab_body">
        		<?php $this->load->view('rpt/rpt_listing_inv'); ?>
        	</div>
        </div>
    </div>
</div>
<style type="text/css">
	
 /*#gridAraging tr td {
    overflow:visible;
}
*/
</style>
<script type="text/javascript">
	var counter = 0;
	var t_saldoAwal = 0;
	var t_pelunasan = 0;
	var t_saldoAkhir = 0;
	var t_jthTempo = 0;
	var t_lewatjt_one = 0;
	var t_lewatjt_two = 0;
	var t_lewatjt_thre = 0;
	var t_lewatjt_for = 0;
	<?php echo $js_cmb; ?>

	$(document).ready(function(){

		
		cmb['company_code'].value('-1');
		cmb['doc_year'].value('-1');

		kendoWindow = $("<div />").kendoWindow({
			title: "Dialog Pencarian",
			width: "500px",
			height: "450px",
			resizable: false,
			modal: true,
		});

		$('#btn_find').click(function(){
			kendoWindow.data("kendoWindow")
				.content($("#dialogCustomer").html())
				.center().open();

			var ds_find_cust = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('customer/view'); ?>'
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
				},	
				pageSize: 10,
			});

			kendoWindow.find('.gridDialogCustomer').kendoGrid({
				dataSource: ds_find_cust,
				filterable: true,
				sortable: true,
				pageable: true,
				height: 400,
				scrollable: false,
				filterable: {
					extra: false,
					operators: {
						string: {
							startswith: "Like",
							eq: "=",
							neq: "!="
						},
					}
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{
						title:"ACTION",
						width:20,
						template: '<button  onClick="$(this).selectItem(this.id);" id="#:VCH_KUNNR#-#:VCH_CSNAME#" class="pilih btn btn-xs btn-primary">PILIH</button>',
					},{field:"VCH_KUNNR",title:"CUSTOMER ID"},
					{field:"VCH_CSNAME",title:" CUSTOMER NAME"},
				]
			});	
		});

		$.fn.selectItem = function(id){
			$('#customerId').val(id);
			kendoWindow.data("kendoWindow").close();
		}

		$('#btn_cancel').click(function(){
			$('#customerId').val('');
		});

		var ds_araging = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('rpt/araging/requestdata'); ?>',
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							LEWATJT: { type: "number" },
							PELUNASAN: { type: "number" },
							NILAI_INVOICE: { type: "number" },
							SALDO_AKHIR: { type: "number" },
							SALDO_AWAL: { type: "number" },
							TGL_IVOICE: { type: "date" },
							TGL_JTEMPO: { type: "date" },
						}
					}
				},	
				pageSize: 10,
			});

		$('#gridAraging').kendoGrid({
			dataSource: ds_araging,
			filterable: false,
			sortable: true,
			resizable: true,
			pageable: true,
			height: "400px",
			scrollable: true,
			dataBinding: function(e) {
				t_saldoAwal = 0;
				t_pelunasan = 0;
				t_saldoAkhir = 0;
				t_jthTempo = 0;
				t_lewatjt_one = 0;
				t_lewatjt_two = 0;
				t_lewatjt_thre = 0;
				t_lewatjt_for = 0;
				var grid = e.sender;
				var row_data = grid.dataSource._data;
				$.each(row_data, function(i, item){
					t_saldoAwal += parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL);
					t_pelunasan += parseInt(row_data[i].PELUNASAN);
					t_saldoAkhir += parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN);
					t_jthTempo += (row_data[i].LEWATJT <= 0) ? parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN):0;
					t_lewatjt_one += (row_data[i].LEWATJT > 0 && row_data[i].LEWATJT <=30) ? parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN):0;
					t_lewatjt_two += (row_data[i].LEWATJT > 30 && row_data[i].LEWATJT <=60) ? parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN):0;
					t_lewatjt_thre += (row_data[i].LEWATJT > 60 && row_data[i].LEWATJT <=90) ? parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN):0;
					t_lewatjt_for += (row_data[i].LEWATJT > 90 ) ? parseInt(row_data[i].NILAI_INVOICE) - parseInt(row_data[i].SALDO_AWAL) - parseInt(row_data[i].PELUNASAN):0;
				});
			},
			columns: [
				{
					width:150,
					field:"NO_CMD",
					title:"NO CMD",
					footerAttributes: {"colspan": "9", "style":"text-align:right;"},
					footerTemplate: "TOTAL :"
				},{
					width:300,
					field:"NAMA_CUSTOMER",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					title:"NAMA CUSTOMER"
				},{
					width:200,
					field:"NO_INVOICE",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					title:"NO INVOICE"
				},{
					width:150,
					field:"DOC_SAP",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					title:"NO DOKUMEN SAP"
				},{
					width:150,
					field:"DOC_WEB",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					title:"NO DOKUMEN WEB"
				},{
					width:150,
					field:"TGL_IVOICE",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					template:"#: kendo.toString(kendo.parseDate(TGL_IVOICE, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"TGL INVOICE"
				},{
					width:150,
					field:"NILAI_INVOICE",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					template:'#= kendo.toString(NILAI_INVOICE, "n0")#',
					attributes: {'style':'text-align:right;'},
					title:"NILAI_INVOICE"
				},{
					width:150,
					field:"TGL_JTEMPO",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					template:"#= (TGL_JTEMPO != null) ? kendo.toString(kendo.parseDate(TGL_JTEMPO, 'yyyy-MM-dd'), 'dd-MM-yyyy'):'-' #",
					title:"TGL JT" 
				},{
					width:100,
					field:"LEWATJT",
					footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"},
					template:"#= (LEWATJT != null && LEWATJT > 0) ? LEWATJT : '0' # days",
					title:"LEWAT JT" 
				},{
					width:150,
					field:"SALDO_AWAL",
					template:'#= kendo.toString(NILAI_INVOICE-SALDO_AWAL, "n0")#',
					attributes: {'style':'text-align:right;'},
					title:"SALDO AWAL",
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_saldoAwal,'n0')#",
				},{
					width:150,
					field:"PELUNASAN",
					template:'#= kendo.toString(PELUNASAN, "n0")#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_pelunasan,'n0')#",
					title:"PELUNASAN" 
				},{
					width:150,
					template:'#= kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0")#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_saldoAkhir,'n0')#",
					title:"SALDO AKHIR"
				},{
					width:150, 
					template:'#= (LEWATJT <= 0)? kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0") : "-"#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_jthTempo,'n0')#",
					title:"BELUM JT"
				},{
					width:150, 
					template:'#= (LEWATJT > 0 && LEWATJT <=30)? kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0") : "-"#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_lewatjt_one,'n0')#",
					title:"1-30"
				},{
					width:150,
					template:'#= (LEWATJT > 30 && LEWATJT <=60)? kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0") : "-"#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_lewatjt_two,'n0')#",
					title:"31-60"
				},{
					width:150,
					template:'#= (LEWATJT > 60 && LEWATJT <=90)? kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0") : "-"#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_lewatjt_thre,'n0')#",
					title:"61 > 90"
				},{
					width:150,
					template:'#= (LEWATJT > 90)? kendo.toString(NILAI_INVOICE-SALDO_AWAL-PELUNASAN, "n0") : "-"#',
					attributes: {'style':'text-align:right;'},
					footerAttributes: {'style':'text-align:right;'},
					footerTemplate: "#: kendo.toString(t_lewatjt_for,'n0')#",
					title:"> 91"
				},
			]
		});
		
		$('#btn_rp_aging_excel').click(function(e){
			e.preventDefault();
			do{
				
				if(cmb['doc_year'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}

				$( "#frm_rpt_aging" ).submit();
			}while(false);
		});

		$('#btn_rpt_aging').click(function(e){
			e.preventDefault();
			do{
				
				if(cmb['doc_year'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}


				ds_araging.read({
					company_code: cmb['company_code'].value(),
					doc_year : cmb['doc_year'].value(),
					customer : $('#customerId').val()
				});
			}while(false);
		});

	});
</script>

<script id="dialogCustomer" type="text/x-kendo-template">
	<div class="gridDialogCustomer"></div>
</script>