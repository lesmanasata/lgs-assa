<form class="form-horizontal" id="frm_rpt_inv" method="POST" target="_blank" action="<?php echo site_url('rpt/araging/requestlv'); ?>"> 
	<div class="form-group">
	    <label  class="control-label col-md-2">Doc Year	</label> 
	    <div class="col-sm-2">
	      <input type="text" class="kendodropdown form-control" style="width:100%;" name="doc_year_inv"  id="doc_year_listinv" placeholder="&mdash;&mdash;Doc Year&mdash;&mdash;">
	    </div>
	</div>
	<div class="form-group">
		<label  class="col-sm-2 control-label hor-form"><?php echo lang('dist_cmp_code'); ?></label>
		<div class="col-sm-4">
			<input type="text" class="kendodropdown form-control" style="width:100%;" name="company_code_inv"  id="company_code_listinv" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-2" for="appendbutton">Customer</label>
		<div class="col-md-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group">
						<input class="form-control" id="customerId_inv" name="customer" type="text" readonly="">
						<div class="input-group-btn">
							<button class="btn btn-primary" type="button" id="btn_find_inv"> Find Customer </button>
							<button class="btn btn-danger" type="button" id="btn_cancel_inv">Reset</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
	    <label class="control-label col-md-2">&nbsp;</label>
	    <div class="col-sm-4">
	      <button type="button" id="btn_inv_proses" class="btn btn-primary mb-2">Proses Data</button>
	      <button type="button" id="btn_inv_excel" class="btn btn-success mb-2">Export to Excel</button>
	    </div>
	</div>
</form>			
<div style="clear: both;">&nbsp;</div>
					
<div id="gridListInv"></div>
<script type="text/javascript">
	$(document).ready(function(){
		cmb['doc_year_inv'].value('-1');
		cmb['company_code_inv'].value('-1');

		$('#btn_find_inv').click(function(){
			kendoWindow.data("kendoWindow")
				.content($("#dialogCustomer").html())
				.center().open();

			var ds_find_cust = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('customer/view'); ?>'
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
				},	
				pageSize: 10,
			});

			kendoWindow.find('.gridDialogCustomer').kendoGrid({
				dataSource: ds_find_cust,
				filterable: true,
				sortable: true,
				pageable: true,
				height: 400,
				scrollable: false,
				filterable: {
					extra: false,
					operators: {
						string: {
							startswith: "Like",
							eq: "=",
							neq: "!="
						},
					}
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{
						title:"ACTION",
						width:20,
						template: '<button  onClick="$(this).selectItem_inv(this.id);" id="#:VCH_KUNNR#-#:VCH_CSNAME#" class="pilih btn btn-xs btn-primary">PILIH</button>',
					},{field:"VCH_KUNNR",title:"CUSTOMER ID"},
					{field:"VCH_CSNAME",title:" CUSTOMER NAME"},
				]
			});	
		});

		$.fn.selectItem_inv = function(id){
			$('#customerId_inv').val(id);
			kendoWindow.data("kendoWindow").close();
		}

		$('#btn_cancel_inv').click(function(){
			$('#customerId_inv').val('');
		});

		var ds_lstinv = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('rpt/araging/requestlv'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						TGL_JTEMPO: { type: "date" },
						NILAI_INVOICE: { type: "number" },
					}
				}
			},	
			pageSize: 10,
		});

		$('#gridListInv').kendoGrid({
			dataSource: ds_lstinv,
			filterable: false,
			sortable: true,
			resizable: true,
			pageable: true,
			height: "400px",
			scrollable: true,
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					width:150,
					field:"NO_CMD",
					title:"NO CMD"
				},{
					width:300,
					field:"NAMA_CMD",
					title:"NAMA CUSTOMER"
				},{
					width:150,
					//field:"NO_POLISI",
					title:"NO POLISI"
				},{
					width:150,
					//field:"PERIOD_SEWA",
					title:"PERIODE SEWA"
				},{
					width:150,	
					field:"TGL_IVOICE",
					title:"TGL INVOICE"
				},{
					width:250,
					field:"NO_INVOICE",
					title:"NO INVOICE"
				},{
					width:150,
					field:"NO_DOC_SAP",
					title:"NO DOKUMEN"
				},{
					width:150,
					field:"TGL_JTEMPO",
					template:"#= (TGL_JTEMPO != null) ? kendo.toString(kendo.parseDate(TGL_JTEMPO, 'yyyy-MM-dd'), 'dd-MM-yyyy'):'-' #",
					title:"TGL JTH TEMPO"
				},{
					width:150,
					field:"NILAI_INVOICE",
					template:'#= kendo.toString(NILAI_INVOICE, "n0")#',
					attributes: {'style':'text-align:right;'},
					title:"NILAI INV"
				},{
					field:"NO_FP",
					width:150,
					title:"NOMOR FP"
				}
			]
		});

		$('#btn_inv_excel').click(function(e){
			e.preventDefault();
			do{  
				if(cmb['doc_year_inv'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code_inv'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}
				$('#frm_rpt_inv').submit();
			}while(false);
		});

		$('#btn_inv_proses').click(function(e){
			e.preventDefault();
			do{  
				if(cmb['doc_year_inv'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code_inv'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}

				ds_lstinv.read({
					company_code_i: cmb['company_code_inv'].value(),
					doc_year_i : cmb['doc_year_inv'].value(),
					customer : $('#customerId_inv').val()
				});

			}while(false);
		});
	});
</script>