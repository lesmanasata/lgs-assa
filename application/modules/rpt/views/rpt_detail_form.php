<h1 class="page-title">Report Detail Clearing & Cost Distribution</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Report Clearing</a></li>
  <li class="active"><strong>Report Detail</strong></li>
</ol>

<?php $timestamp = time();?>

<div class="containers">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#contact_01"  data-toggle="tab"><?php echo lang('mn_rpt_detail_form'); ?></a></li>

    </ul>
    <div class="tab-content grid-form1">
        <div class="tab-pane fade in active" id="contact_01">
        	<div class="padding_tab_body">
        		<form class="form-horizontal" id="frm_trx<?php echo $timestamp; ?>" >
        			<div class="form-group">
				      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_id'); ?></label>
				      <div class="col-sm-3">
				        <input type="text" id="find_btn_id" name="find_id" class="form-control"  placeholder="<?php echo lang('rpt_find_id'); ?>" >
				      </div>
				    </div>

				    <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				        <div class="checkbox">
				          <label><input type="checkbox" id="custome_filter" name="custome_filter"> <?php echo lang('rpt_find_custome'); ?></label>
				        </div>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_peride'); ?></label>
				      <div class="col-sm-10">
				        <label><input type="radio" id="rb_date" value="rb_date"checked name="find_periode"> <?php echo lang('rpt_find_peride_one'); ?></label>&nbsp; &nbsp;
				        <label><input type="radio" id="rb_month" value="rb_month" name="find_periode"> <?php echo lang('rpt_find_peride_two'); ?></label>
				      </div>
				    </div>

				    <div class="form-group" id="dv_date">
				      <div class="col-sm-offset-2 col-sm-10">
				        <input type="text" id="find_tanggal" name="find_tanggal" value="<?php echo date('d-m-Y'); ?>" class="form-control"  placeholder="<?php echo lang('rpt_find_date'); ?>" >
				      </div>

				    </div>

				    <div class="form-group" id="dv_month">
				    	<div class="col-sm-offset-2 col-sm-2">
				        	<input type="number" onkeypress='numberOnly(event)' name="find_year" class="form-control"  placeholder="<?php echo lang('rpt_find_yer'); ?>" >
				      	</div>
				      	<div class=" col-sm-4">
				        	<input type="text" id="find_bulan" name="find_bulan" style="width: 100%" class="form-control"  placeholder="<?php echo lang('rpt_find_month'); ?>" >
				      	</div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_user'); ?></label>
				      <div class="col-sm-4">
				        <input type="text" id="find_user" name="find_user" style="width: 100% " class="form-control"  placeholder="<?php echo lang('rpt_find_user'); ?>" >
				      </div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_trans_type'); ?></label>
				      <div class="col-sm-4">
				        <input type="text" id="find_trx_type" name="find_trx_type" style="width: 100%" class="form-control"  placeholder="<?php echo lang('rpt_find_trans_type'); ?>" >
				      </div>
				    </div>

				    <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				        <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
				        <button type="button" id="ref<?php echo $timestamp; ?>" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
				      </div>
				    </div>
				</form>
        		<!-- <a href="#" class="add-contact" data-id="Sample" data-url="<?php //echo site_url("rpt/detail/getDetial"); ?>">+ Add Contact</a> -->
        	</div>

        	<div class="padding_tab_body">
        		<div id="item_row<?php echo $timestamp; ?>"></div>
        	</div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#ref<?php echo $timestamp; ?>').click(function() {
            location.reload();
            $('#frm_trx<?php echo $timestamp; ?> input[name=find_id]').val('');
            $('#frm_trx<?php echo $timestamp; ?> input[name=custome_filter]')[0].checked = false;
        });
		var cmbTrx_type = $('#frm_trx<?php echo $timestamp; ?> input[name=find_trx_type]')
          .kendoDropDownList({
            autoBind: true,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/getcmbTrx'); ?>",
                }
              }
            }
        });

        var cmbBulan = $('#frm_trx<?php echo $timestamp; ?> input[name=find_bulan]')
          .kendoDropDownList({
            autoBind: true,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/getcmbBulan'); ?>",
                }
              }
            }
        });

        var cmbUser = $('#find_user')
          .kendoDropDownList({
            autoBind: true,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_petugas'); ?>",
                }
              }
            }
        });


        $("#frm_trx<?php echo $timestamp; ?>").submit(function(e){
        	e.preventDefault();
        	$.ajax({
				url:'<?php echo site_url('rpt/detail/getDetial'); ?>',
				type:'POST',
				data:$(this).serialize(),
				dataType: 'html',
				beforeSend:function(){
					$('#item_row<?php echo $timestamp; ?>').addClass('loader');
				},success:function(res){
					$('#item_row<?php echo $timestamp; ?>').html(res);
				}
			}).done(function(){
				$('#item_row<?php echo $timestamp; ?>').removeClass('loader');
			});
        });

        $("#dv_month").hide();
        $("#dv_year").hide();
        $("#dv_date").show();
        $("#rb_date, #rb_month").click(function(){
         	var curId = $(this).attr('id');
         	if(curId=="rb_date"){
         		$("#dv_month").hide();
         		$("#dv_year").hide();
         		$("#dv_date").show();
         	}else{
         		$("#dv_month").show();
         		$("#dv_year").show();
         		$("#dv_date").hide();
         	}
         });

 		$("#find_tanggal").kendoDatePicker({format: "dd-MM-yyyy"});
		var datepicker = $("#find_tanggal").data("kendoDatePicker");

        $('#frm_trx<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', true);
       	$('#rb_date').prop('disabled', true);
        $('#rb_month').prop('disabled', true);
        datepicker.readonly(true);
        $("#find_user").data("kendoDropDownList").enable(false);
        $("#find_trx_type").data("kendoDropDownList").enable(false);
        $("#find_bulan").data("kendoDropDownList").enable(false);
        $("#custome_filter").click(function(){
        	var curStat = $(this).is(":checked");
        	if(curStat){
        		$('#frm_trx<?php echo $timestamp; ?> input[name=find_id]').val('');
        		$('#frm_trx<?php echo $timestamp; ?> input[name=find_id').prop('readonly', true);
        		$('#frm_trx<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', false);
        		$('#rb_date').prop('disabled', false);
        		$('#rb_month').prop('disabled', false);
        		datepicker.readonly(false);
        		$("#find_user").data("kendoDropDownList").enable(true);
        		$("#find_trx_type").data("kendoDropDownList").enable(true);
        		$("#find_bulan").data("kendoDropDownList").enable(true);

        	}else{
        		$('#frm_trx<?php echo $timestamp; ?> input[name=find_id]').prop('readonly', false);
        		$('#frm_trx<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', true);
        		$('#rb_date').prop('disabled', true);
        		$('#rb_month').prop('disabled', true);
        		datepicker.readonly(true);
        		$("#find_user").data("kendoDropDownList").enable(false);
        		$("#find_trx_type").data("kendoDropDownList").enable(false);
        		$("#find_bulan").data("kendoDropDownList").enable(false);
        	}
        });

		$(".nav-tabs").on("click", "a", function(e){
		      e.preventDefault();
		      $(this).tab('show');
		}).on("click", "span", function () {
		        var anchor = $(this).siblings('a');
		        $(anchor.attr('href')).remove();
		        $(this).parent().remove();
		        $(".nav-tabs li").children('a').first().click();
		});

	    $('.add-contact').click(function(e) {
	        e.preventDefault();
	        var url = $(this).data("url");
	        var title = $(this).data("id");
	        var id = $(".nav-tabs").children().length;
	        $('.nav-tabs').append('<li><a href="#contact_'+id+'">'+title+'  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;</a> <span class="close">x</span> </li>');
	        $('.tab-content').append('<div class="tab-pane" id="contact_'+id+'"></div>');

	        $.ajax({
				url:url,
				type:'POST',
				dataType: 'html',
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},success:function(res){
					$("#contact_"+id).html(res);
				}
			}).done(function(){
				$('#loading-mask').removeClass('loader-mask');
			});

	        $('.nav-tabs a[href="#contact_' + id + '"]').trigger('click');
		});
	});

</script>
<style type="text/css">

.container {
    margin-top: 10px;

}

.nav-tabs > li {
    position:relative;
}

.nav-tabs > li > a {
    display:inline-block;
}

.nav-tabs > li > span {
    font-size: 16px;
    box-sizing:border-box;
    border: 1px solid #505050;
   	background: #ddd;
    font-weight: bold;
    cursor:pointer;
    position:absolute;
    right: 6px;
    top: 8px;

    padding-left: 4px;
    padding-right: 4px;
    padding-bottom: 3px;
    border-radius: 4px;
}

.nav-tabs > li:hover > span {
    display: inline-block;
}
</style>
