<h1 class="page-title">Log Trans SAP</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Report Clearing</a></li>
  <li class="active"><strong>Log Trans SAP</strong></li>
</ol>

<?php $timestamp = time();?>

<div class="containers">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#contact_01"  data-toggle="tab">Report Clearing Log</a></li>
        <li ><a href="#contact_02"  data-toggle="tab">Report PPH & PPN</a></li>
    </ul>
    <div class="tab-content grid-form1">
        <div class="tab-pane fade in active" id="contact_01">
        	<div class="padding_tab_body">
				<div id="gridLogClearing"></div>
        	</div>
        </div>
        <div class="tab-pane fade in " id="contact_02">
        	<div class="padding_tab_body" style="min-height: 500px;">
        		<div id="gridLogPpn"></div>
        	</div>
        </div>
    </div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		var ds_logtrans = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('rpt/log/getcleringlog'); ?>'
					}
				},
				schema: {
					data: "data",
					total: "total",
					model: {
						fields: {
							INT_id: { type: "string" },
							DAT_createon: { type: "string" ,},
							TME_createat: { type: "string"},
							VCH_reference: { type: "string" },
							CHR_BUKRS: { type: "string" },
							DAT_postingdate: { type: "string",},
							CHR_year: { type: "string" },
							VCH_msgtext: { type: "string" },
						}
					}
				},
				batch: true,
				serverFiltering: true, 
				serverPaging: true,
				pageSize: 10,
			});

		$('#gridLogClearing').kendoGrid({
			dataSource: ds_logtrans,
			sortable: true,
			pageable: {
		        refresh: true,
		        pageSizes: true,
		        buttonCount: 10
		    },
			height: "400px",
			scrollable: true,
			resizable:true,
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like", 
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					field:"DAT_createon", 
					width: 130, 
					title:"CREATE ON",
					template:"#: kendo.toString(kendo.parseDate(DAT_createon, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					filterable : {
					  ui: function (element) {
					    element.kendoDatePicker({
					      format: kendo.toString(new Date(), "dd-MM-yyyy")
					    });
					  }
					},
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["0:dd-MM-yyyy"],
				},{
					width:150,
					field:"TME_createat",
					title:"CREATE AT"
				},{
					width:200,
					field:"VCH_reference",
					title:"REFERENCE"
				},{
					width:120,
					field:"CHR_BUKRS",
					title:"COMP. CODE"
				},{
					width:150,
					field:"DAT_postingdate",
					template:"#: kendo.toString(kendo.parseDate(DAT_postingdate, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"POSTING DATE"
				},{
					width:150,
					field:"CHR_year",
					title:"YEAR"
				},{
					width:350,
					field:"VCH_msgtext",
					title:"MESSAGES TEXT" 
				}
			]
		});


		var ds_logppn = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('rpt/log/getppnlog'); ?>'
					}
				},
				schema: {
					data: "data",
					total: "total",
					model: {
						fields: {
							DAT_createon: { type: "string" },
							TME_createat: { type: "string" },
							VCH_BELNR: { type: "string" },
							VCH_BUZEI: { type: "string" ,},
							CHR_BUKRS: { type: "string"},
							DYR_GJAHR: { type: "string" },
							VCH_BELNR_SAP: { type: "string" },
							VCH_BUZEI_SAP: { type: "string",},
							VCH_SAPJV: { type: "string" },
							VCH_SAPPAYMENT: { type: "string" },
							VCH_SAPREKON: { type: "string" },
						}
					}
				},
				batch: true,
				serverFiltering: true, 
				serverPaging: true,
				pageSize: 10,
			});


		$('#gridLogPpn').kendoGrid({
			dataSource: ds_logppn,
			sortable: true,
			pageable: {
		        refresh: true,
		        pageSizes: true,
		        buttonCount: 10
		    },
			height: 400,
			scrollable: true,
			resizable:true,
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like", 
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					field:"DAT_createon", 
					width: 130, 
					title:"CREATE ON",
					template:"#: kendo.toString(kendo.parseDate(DAT_createon, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					filterable : {
					  ui: function (element) {
					    element.kendoDatePicker({
					      format: kendo.toString(new Date(), "dd-MM-yyyy")
					    });
					  }
					},
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["0:dd-MM-yyyy"],
				},{
					width:150,
					field:"TME_createat",
					title:"CREATE AT"
				},{
					width:200,
					field:"VCH_BELNR",
					title:"REFERENCE"
				},{
					width:120,
					field:"CHR_BUKRS",
					title:"COMP. CODE"
				},{
					width:100,
					field:"DYR_GJAHR",
					title:"YEAR"
				},{
					width:150,
					field:"VCH_BELNR_SAP",
					title:"DOCUMENT NO" 
				},{
					width:150,
					field:"VCH_SAPJV",
					title:"DOCUMENT JV" 
				},{
					width:180,
					field:"VCH_SAPPAYMENT",
					title:"DOCUMENT PAYMENT" 
				},{
					width:180,
					field:"VCH_SAPREKON",
					title:"DOCUMENT REKON" 
				},{
					width:350,
					field:"VCH_msgtext",
					title:"MESSAGES TEXT" 
				}
			]
		});
	});
</script>

