<?php 
header("Content-Disposition: attachment; filename=ReportDetail".date('is').".xls");
header("Content-Type: application/vnd.ms-excel");
?>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>

<table>
  <tr>
    <th>DOC NUMBER</th>
    <th>COMPANY CODE</th>
    <th>DOC TYPE</th>
    <th>TAHUN</th>
    <th>POSTING DATE</th>
    <th>CURENCY</th>
    <th>STATUS SAP</th>
    <th>DOC SAP</th>
    <th>TGL ENTRY</th>
    <th>CREATED BY</th>
  </tr>
  <?php foreach($data as $row){ ?>
  <tr>
    <td><?php echo $row->doc_number; ?></td>
    <td><?php echo $row->company_code; ?></td>
    <td><?php echo $row->doc_type; ?></td>
    <td><?php echo $row->doc_yer; ?></td>
    <td><?php echo $row->posting_date; ?></td>
    <td><?php echo $row->curency; ?></td>
    <td><?php 
    	$status ='';
		if($row->flag==0)
			$status = 'Open';
		else if($row->flag==1 && $row->doc_sap==' ')
			$status ='Waiting';
		else if($row->flag==1 && $row->doc_sap !=' ')
			$status ='Success';

		echo $status;
	?></td>
	<td><?php echo $row->doc_sap; ?></td>
	<td><?php echo $row->last_insert; ?></td>
	<td><?php echo $row->petugas_id; ?></td>
  </tr>
  <?php } ?>
</table>