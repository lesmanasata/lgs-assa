<form class="form-horizontal" id="frm_rpt_listinv" action="<?php echo site_url('rpt/araging/requestlvbayar'); ?>" method="POST" target="_blank">
	<div class="form-group">
	    <label  class="control-label col-md-2">Doc Year	</label>
	    <div class="col-sm-2">
	      <input type="text" class="kendodropdown form-control" style="width:100%;" name="doc_year_listinv"  id="doc_year_listinv" placeholder="&mdash;&mdash;Doc Year&mdash;&mdash;">
	    </div>
	</div>
	<div class="form-group">
		<label  class="col-sm-2 control-label hor-form"><?php echo lang('dist_cmp_code'); ?></label>
		<div class="col-sm-4">
			<input type="text" class="kendodropdown form-control" style="width:100%;" name="company_code_listinv"  id="company_code_listinv" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-2" for="appendbutton">Customer</label>
		<div class="col-md-6">
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group">
						<input class="form-control" id="customerId_byr" name="customer" type="text" readonly="">
						<div class="input-group-btn">
							<button class="btn btn-primary" type="button" id="btn_find_byr"> Find Customer </button>
							<button class="btn btn-danger" type="button" id="btn_cancel_byr">Reset</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
	    <label class="control-label col-md-2">&nbsp;</label>
	    <div class="col-sm-4">
	      <button type="button" id="btn_proses_bayar" class="btn btn-primary mb-2">Proses Data</button>
	      <button type="button" id="btn_proses_bayar_excel" class="btn btn-success mb-2">Export To Excel</button>
	    </div>
	</div>
</form>			
<div style="clear: both;">&nbsp;</div>

<div id="gridListPembayaran"></div>
<script type="text/javascript">
	$(document).ready(function(){
		cmb['company_code_listinv'].value('-1');
		cmb['doc_year_listinv'].value('-1');


		$('#btn_find_byr').click(function(){
			kendoWindow.data("kendoWindow")
				.content($("#dialogCustomer").html())
				.center().open();

			var ds_find_cust = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						url: '<?php echo site_url('customer/view'); ?>'
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
				},	
				pageSize: 10,
			});

			kendoWindow.find('.gridDialogCustomer').kendoGrid({
				dataSource: ds_find_cust,
				filterable: true,
				sortable: true,
				pageable: true,
				height: 400,
				scrollable: false,
				filterable: {
					extra: false,
					operators: {
						string: {
							startswith: "Like",
							eq: "=",
							neq: "!="
						},
					}
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{
						title:"ACTION",
						width:20,
						template: '<button  onClick="$(this).selectItem_byr(this.id);" id="#:VCH_KUNNR#-#:VCH_CSNAME#" class="pilih btn btn-xs btn-primary">PILIH</button>',
					},{field:"VCH_KUNNR",title:"CUSTOMER ID"},
					{field:"VCH_CSNAME",title:" CUSTOMER NAME"},
				]
			});	
		});

		$.fn.selectItem_byr = function(id){
			$('#customerId_byr').val(id);
			kendoWindow.data("kendoWindow").close();
		}

		$('#btn_cancel_byr').click(function(){
			$('#customerId_byr').val('');
		});

		var ds_lpbayar = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('rpt/araging/requestlvbayar'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						NILAI_INV: { type: "number" },
						INT_BAYAR: { type: "number" },
						DAT_INV: { type: "date" },
						TGL_BAYAR: { type: "date" },
						TGL_JTEMPO: { type: "date" },
						TGL_CLEARPPH: { type: "date" },
						TGL_AR: { type: "date" }
					}
				}
			},	
			pageSize: 10,
		});

		$('#gridListPembayaran').kendoGrid({
			dataSource: ds_lpbayar,
			filterable: false,
			sortable: true,
			resizable: true,
			pageable: true,
			height: "400px",
			scrollable: true,
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					width:150,
					field:"NO_CMD",
					title:"NO CMD"
				},{
					width:350,
					field:"NAMA_CMD",
					title:"NAMA CUSTOMER"
				},{
					width:150,
					field:"NO_INVOICE",
					title:"NO INVOICE"
				},{
					width:150,
					field:"NO_DOC",
					title:"NO DOKUMEN"
				},{
					width:150,
					field:"DAT_INV",
					template:"#: kendo.toString(kendo.parseDate(DAT_INV, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"TGL INVOICE"
				},{
					width:150,
					field:"TGL_JTEMPO",
					template:"#: kendo.toString(kendo.parseDate(TGL_JTEMPO, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"TGL JTP"
				},{
					width:150,
					field:"NILAI_INV",
					template:'#= kendo.toString(NILAI_INV, "n0")#',
					attributes: {'style':'text-align:right;'},
					title:"NILAI INVOICE"
				},{
					width:150,
					field:"INT_BAYAR",
					template:'#= kendo.toString(INT_BAYAR, "n0")#',
					attributes: {'style':'text-align:right;'},
					title:"NILAI BAYAR"
				},{
					width:150,
					field:"TGL_BAYAR",
					template:"#: kendo.toString(kendo.parseDate(TGL_BAYAR, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"TGL BAYAR"
				},{
					width:150,
					field:"TGL_AR",
					template:"#: kendo.toString(kendo.parseDate(TGL_AR, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
					title:"TGL CLEAR AR"
				},{
					width:150,
					field:"NO_AR",
					title:"NO DOKUMEN"
				},{
					width:150,
					field:"TGL_CLEARPPH",
					template:"#= (TGL_CLEARPPH != null) ? kendo.toString(kendo.parseDate(TGL_CLEARPPH, 'yyyy-MM-dd'), 'dd-MM-yyyy') : '-' #",
					title:"TGL CLEAR PPH 23"
				},{
					width:150,
					field:"NO_DOCPPH",
					template:"#= (NO_DOCPPH != null) ? NO_DOCPPH : '-' #",
					title:"NO DOKUMEN"
				}
			]
		});

		$('#btn_proses_bayar_excel').click(function(e){
			e.preventDefault();
			do{
				if(cmb['doc_year_listinv'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code_listinv'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}

				$( "#frm_rpt_listinv" ).submit();
			}while(false);
		});

		$('#btn_proses_bayar').click(function(e){
			e.preventDefault();
			do{
				if(cmb['doc_year_listinv'].value() =='-1'){
					msg_box('Please Select One of Doc Year ',['btnOK'],'Info!');
					break;
				}

				if(cmb['company_code_listinv'].value() =='-1'){
					msg_box('Please Select One of Company',['btnOK'],'Info!');
					break;
				}

				ds_lpbayar.read({
					company_code_inv: cmb['company_code_listinv'].value(),
					doc_year_inv : cmb['doc_year_listinv'].value(),
					customer : $('#customerId_byr').val()
				});

			}while(false);
		});
	});
</script>