<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report List Invoice | Excel</title>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                var blob = new Blob([format(template, ctx)]);
                var blobURL = window.URL.createObjectURL(blob);

                if (ifIE()) {
                    csvData = table.innerHTML;
                    if (window.navigator.msSaveBlob) {
                        var blob = new Blob([format(template, ctx)], {
                            type: "text/html"
                        });
                        navigator.msSaveBlob(blob, '' + name + '.xls');
                    }
                }
                else
                window.location.href = uri + base64(format(template, ctx))
            window.close();
            }
        })()

        function ifIE() {
            var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
            var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
            return isIE11orLess;
        }
    </script>
</head>
<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Ar_list_pembayaran.xls"); 
?>
<body>
<!-- <body onload="tableToExcel('testTable', 'List Pembayaran')">  -->
    <table id="testTable" summary="Summary" rules="groups" frame="hsides" border="2">
        <caption>
            <h2>Report List Pembayaran Invoice </h2>
        </caption>
        <caption>&nbsp;</caption>
        <colgroup align="center"></colgroup>
        <colgroup align="left"></colgroup>
        <colgroup span="2" align="center"></colgroup>
        <colgroup span="3" align="center"></colgroup>
        <thead valign="top">
            <tr style="background-color: #cbc0c0; height: 40px;">
                <th>NO</th>
                <th>NO CMD</th>
                <th>NAMA CUSTOMER</th>
                <th>NO INVOICE</th>
                <th>NO DOKUMEN</th>
                <th>TGL INVOICE</th>
                <th>TGL JT TEMPO</th>
                <th>NILAI INVOICE</th>
                <th>NILAI BAYAR</th>
                <th>TGL BAYAR</th>
                <th>TGL CLEAR AR</th>
                <th>NO DOKUMEN AR</th>
                <th>TGL CLEAR PPH 23</th>
                <th>NO DOKUMEN PPH</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $no=1; 
                $total_inv=0;
                foreach($data as $row){ 
                    $total_inv += $row->NILAI_INV;
            ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->NO_CMD; ?></td>
                    <td><?php echo $row->NAMA_CMD; ?></td>
                    <td><?php echo $row->NO_INVOICE; ?></td>
                    <td><?php echo $row->NO_DOC; ?></td>
                    <td><?php echo $row->DAT_INV; ?></td>
                    <td><?php echo $row->TGL_JTEMPO; ?></td>
                    <td><?php echo $row->NILAI_INV; ?></td>
                    <td><?php echo $row->INT_BAYAR; ?></td>
                    <td><?php echo $row->TGL_BAYAR; ?></td>
                    <td><?php echo $row->TGL_AR; ?></td>
                    <td><?php echo $row->NO_AR; ?></td>
                    <td><?php echo $row->TGL_CLEARPPH; ?></td>
                    <td><?php echo $row->NO_DOCPPH; ?></td>
                </tr>
            <?php $no++;  } ?>
             <tr style="background-color: #ff6a00; color: #fff; height: 35px;">
                <td colspan="7" style="text-align: right;"><b>Total :</b></td>
                <td><?php echo $total_inv; ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr> 
        </tbody>
    </table>
</body>
</html>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>