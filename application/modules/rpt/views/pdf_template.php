<?php $timestamp = time();?>
<div id="example" class="col-lg-12">
    <div id="modal-body" >
        <form class="form-horizontal">
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label col-sm-4" ><?php echo lang('dist_cmp_code'); ?>:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id = "company_code<?php echo $timestamp;?>" placeholder="<?php echo lang('dist_cmp_code'); ?>" readonly>
              </div>
            </div>
              <div class="form-group">
              <label class="control-label col-sm-4" ><?php echo lang('ar_doc_no'); ?>:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="doc_number<?php echo $timestamp;?>" readonly>
              </div>
            </div>

              <div class="form-group">
                  <label class="control-label col-sm-4" ><?php echo lang('ar_doc_yer'); ?>:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="doc_yer<?php echo $timestamp;?>" placeholder="<?php echo lang('ar_doc_yer'); ?>" readonly>
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-4" >Synchronize:</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="sync<?php echo $timestamp;?>" readonly>
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-4" >Doc SAP:</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="sap<?php echo $timestamp;?>" readonly>
                  </div>
              </div>

              <div class="form-group">
                  <label class="control-label col-sm-4" >Amount:</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="amount<?php echo $timestamp;?>" readonly>
                  </div>
              </div>
          </div>

          <div class="col-sm-6">
              <div class="form-group">
              <label class="control-label col-sm-4" ><?php echo lang('dist_post_date'); ?>:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="posting_date<?php echo $timestamp;?>" placeholder="<?php echo lang('dist_post_date'); ?>" readonly>
              </div>
            </div>

           <div class="form-group">
              <label class="control-label col-sm-4" ><?php echo lang('ar_doc_type'); ?>:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="doc_type<?php echo $timestamp;?>" placeholder="<?php echo lang('ar_doc_type'); ?>" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-4" >Proses by:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="insert_by<?php echo $timestamp;?>" readonly>
              </div>
            </div>

              <div class="form-group">
              <label class="control-label col-sm-4" >Insert Date:</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="lastins<?php echo $timestamp;?>" readonly>
              </div>
            </div>

          </div>
      </form>
    </div>
    <div  style="clear:both;"></div>
          <p>&nbsp;</p>
    <div id="grid<?php echo $timestamp; ?>"></div>
</div>
            <script>
                $(document).ready(function() {

                    var ds_report = new kendo.data.DataSource({
                                        transport: {
                                            read: {
                                                type:"POST",
                                                dataType: "json",
                                                url: "<?php echo site_url('rpt/detail/report/'.$this->uri->segment(4));?>",
                                                complete: function(res) {
                                                    var res_msg = JSON.parse(res.responseText);
                                                    $('#company_code<?php echo $timestamp; ?>').val(res_msg.data[0].CHR_BUKRS);
                                                    $('#doc_yer<?php echo $timestamp; ?>').val(res_msg.data[0].DYR_GJAHR);
                                                    $('#doc_number<?php echo $timestamp; ?>').val(res_msg.data[0].VCH_BELNR);
                                                    $('#posting_date<?php echo $timestamp; ?>').val(res_msg.data[0].DAT_BUDAT);
                                                    $('#doc_type<?php echo $timestamp; ?>').val(res_msg.data[0].ENM_BLART);
                                                    $('#insert_by<?php echo $timestamp; ?>').val(res_msg.data[0].petugas_id);
                                                    $('#lastins<?php echo $timestamp; ?>').val(res_msg.data[0].last_insert);
                                                    $('#sap<?php echo $timestamp; ?>').val(res_msg.data[0].VCH_BELNR_SAP);
                                                    $('#amount<?php echo $timestamp; ?>').val(numberWithCommas(res_msg.data[0].INT_WRBTR));
                                                    var test = res_msg.data[0].CHR_FLAG;
                                                    if (test=='1') {
                                                        $('#sync<?php echo $timestamp; ?>').val('True');
                                                    }else {
                                                        $('#sync<?php echo $timestamp; ?>').val('False');
                                                    }


                                                }
                                            }
                                        },
                                        batch: true,
                                        pageSize: 20,
                                        schema: {
                                            parse: function(res) {
                                                    return res.data;
                                                },
                                            model: {
                                                id: "VCH_BELNR",
                                                fields: {
                                                    VCH_BELNR: {type: "text"},
                                                    CHR_BUKRS: { type: "text" },
                                                    INT_WRBTR: {type: "number"}
                                                }
                                            }
                                        },

                                    });
                    $("#grid<?php echo $timestamp; ?>").kendoGrid({
                        dataSource: ds_report,
                        scrollable: true,
                        sortable: true,
                        pageable: true,
                        resizable: true,
                        pageSize: 50,
                        columns: [
                            {
                                field: "CHR_BUKRS",
                                width: 60,
                                title: "Code"
                            },{
                                field:"VCH_BELNR",
                                template:"#if(VCH_BSCHL =='40' || VCH_BSCHL =='15'){# #:VCH_REBZG# #}else{# #:VCH_BELNR# #}#",
                                width: 130,
                                title: "Doc Number"
                            },{
                                field:"DYR_GJAHR",
                                width: 60,
                                title: "Year"
                            },{
                                field:"VCH_GSBER",
                                width: 80,
                                title: "Bis Area"
                            },{
                                field:"VCH_BSCHL",
                                width: 80,
                                title: "Post Key"
                            },{
                                field:"ENM_SHKZG",
                                template:"#= (ENM_SHKZG=='S')?'D':'C' #",
                                width: 100,
                                attributes:{
                                    style: "text-align: center;"
                                },
                                title: "Indicator"
                            },{
                                field:"VCH_HKONT",
                                width: 130,
                                title: "GL Account"
                            },{
                                field:"VCH_KUNNR",
                                width: 150,
                                title: "Customer"
                            },{
                                field:"INT_WRBTR",
                                width: 100,
                                attributes:{
                                    style: "text-align: right;"
                                },
                                template:'#= kendo.toString(INT_WRBTR, "n0")#',
                                title: "Amount"
                            },{
                                field:"DAT_BUKPOT",
                                width: 150,
                                title: "TGL BUKPOT"
                            },{
                                field:"VCH_NUMBPOT",
                                width: 150,
                                title: "NUMBER BUKPOT"
                            },{
                                field:"TEX_SGTXT",
                                width: 200,
                                title: "Text"
                            }
                        ]
                    });
                });
            </script>
</div>
