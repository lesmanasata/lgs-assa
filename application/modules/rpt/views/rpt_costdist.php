<?php $timestamp = time();?>

<button class="btn btn-warning navbar-btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing..." id="excel-<?php echo $timestamp; ?>" ><i class="fa fa-file-excel-o"></i> Download Excel</button>

<div class="padding_tab_body">
	<div id="distribute_detail_sub<?php echo $timestamp; ?>"></div>
</div>


<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {
		$('#excel-<?php echo $timestamp; ?>').click(function(e){
			e.preventDefault();

			var $this = $(this);
			$.ajax({
				url:'<?php echo site_url('rpt/detail/setexcel'); ?>',
				type:'POST',
				data:<?php echo json_encode($res); ?>,
				dataType: 'html',
				beforeSend:function(){
					$this.button('loading');
				},success:function(res){
					$this.button('reset');
					window.open('<?php echo site_url('rpt/detail/getData'); ?>','_blank' );
				}
			}).done(function(){
				$this.button('reset');
			});
		});

		var ds_distribute_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('rpt/detail/getData'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 amount_item: { type: "number"},
						 posting_date: {type: "date"}
					 }
				}
			},
			pageSize: 50,
		});

		$.fn.show_detail=function(){
			var url = $(this).data("url");
	        var title = $(this).data("id");
	        var id = $(".nav-tabs").children().length;
	        $('.nav-tabs').append('<li><a href="#contact_'+id+'">'+title+'  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;</a> <span class="close">x</span> </li>');
	        $('.tab-content').append('<div class="tab-pane" id="contact_'+id+'"></div>');

	        $.ajax({
				url:url,
				type:'POST',
				dataType: 'html',
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},success:function(res){
					$("#contact_"+id).html(res);
				}
			}).done(function(){
				$('#loading-mask').removeClass('loader-mask');
			});

	        $('.nav-tabs a[href="#contact_' + id + '"]').trigger('click');
		}

		$("#distribute_detail_sub<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_distribute_detail,
			pageable: true,
			resizable: true,
			height: 550,
			scrollable: true,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					title:"ACTION",
					filterable: false,
					template: "<a href='javascript:void(0)'  onclick='$(this).show_detail();' data-url='<?php echo site_url('rpt/detail/itemdetail'); ?>/#: url #' data-id='#: doc_number #'><button class='btn btn-xs btn-info'> <i class='glyphicon glyphicon-pencil'></i> Detail</button></a>",
					width: 70,
				},{
					field:"doc_number",
					width: 100,
					title:"<?php echo lang('ar_tbl_doc_numb'); ?>",
					filterable: false
				},{
					field:"company_code",
					width: 70, 
					title:"<?php echo lang('dist_cmp_code'); ?>",
					filterable: false
				},{
					field:"doc_type",
					width: 70, 
					title:"<?php echo lang('ar_doc_type'); ?>",
					filterable: false
				},{
					field:"doc_yer",
					width: 60, 
					title:"Tahun",
					filterable: false
				},{
					field:"posting_date",
					width: 90, 
					title:"Posting Date",
					filterable: false,
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["MM/dd/yyyy"]
				},{
					field:"curency",
					width: 80,
					title:"Currency",
					filterable: false
				},{
					field:"flag",
					width: 80,
					title:"Status Sync",
					filterable: false,
					template: "#if(flag ==0){# <span class='label label-success'>Open</span> #}else if(flag ==1 && doc_sap ==' '){# <span class='label label-info'>Waiting</span> #}else if(flag ==1 && doc_sap !=' '){# <span class='label label-success'>Success</span> #}#",
				},{
					field:"doc_sap",
					width: 100,
					title:"Doc SAP",
					filterable: false,

				},{
					field:"doc_sapjv",
					width: 100,
					title:"Doc SAP JV",
					filterable: false,

				},{
					field:"doc_sappayment",
					width: 150,
					title:"Doc SAP PAYMENT",
					filterable: false,

				},{
					field:"doc_sapprekon",
					width: 150,
					title:"Doc SAP REKON",
					filterable: false,

				},{
					field:"last_insert",
					width: 100,
					title:"Tgl Entry",
					filterable: false
				},{
					field:"petugas_id",
					width: 100,
					title:"Created By",
					filterable: false
				}
			]
		});

	});
</script>
