<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report AR Aging | Excel</title>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                var blob = new Blob([format(template, ctx)]);
                var blobURL = window.URL.createObjectURL(blob);

                if (ifIE()) {
                    csvData = table.innerHTML;
                    if (window.navigator.msSaveBlob) {
                        var blob = new Blob([format(template, ctx)], {
                            type: "text/html"
                        });
                        navigator.msSaveBlob(blob, '' + name + '.xls');
                    }
                }
                else
                window.location.href = uri + base64(format(template, ctx))
            window.close();
            }
        })()

        function ifIE() {
            var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
            var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
            return isIE11orLess;
        }
    </script>
</head>

<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Ar_Aging.xls"); 
?>
<!-- <body onload="tableToExcel('testTable', 'Ar Aging')"> -->
<body >
    <table id="testTable" summary="Summary" rules="groups" frame="hsides" border="2">
        <caption>
            <h2>Report Ar Aging </h2>
        </caption>
        <caption>&nbsp;</caption>
        <colgroup align="center"></colgroup>
        <colgroup align="left"></colgroup>
        <colgroup span="2" align="center"></colgroup>
        <colgroup span="3" align="center"></colgroup>
        <thead valign="top">
            <tr style="background-color: #cbc0c0; height: 40px;">
                <th>NO</th>
                <th>NO CMD</th>
                <th>NAMA CUSTOMER</th>
                <th>NO INVOICE</th>
                <th>NO DOKUMEN SAP</th>
                <th>NO DOKUMEN WEB</th>
                <th>TGL INVOICE</th>
                <th>NILAI INVOICE</th>
                <th>TGL JT</th>
                <th>LEWAT JT</th>
                <th>SALDO AWAL</th>
                <th>PELUNASAN</th>
                <th>SALDO AKHIR</th>
                <th>BELUM JT</th>
                <th>1 s/d 30</th>
                <th>31 s/d 60</th>
                <th>61 s/d 90</th>
                <th> > 91</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $no=1; 
                $sub_inv =0;
                $sub_pelunasan =0;
                $sub_sakhir =0;
                $sub_lewatjt = 0;
                $sub_first = 0;
                $sub_second = 0;
                $sub_third = 0;
                $sub_fourth = 0;
                foreach($data as $row){ 

            ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $row->NO_CMD; ?></td>
                    <td style="text-align: left;"><?php echo $row->NAMA_CUSTOMER; ?></td>
                    <td><?php echo $row->NO_INVOICE; ?></td>
                    <td><?php echo $row->DOC_SAP; ?></td>
                    <td><?php echo $row->DOC_WEB; ?></td>
                    <td><?php echo $row->TGL_IVOICE; ?></td>
                    <td><?php echo $row->NILAI_INVOICE; ?></td>
                    <td><?php echo $row->TGL_JTEMPO; ?></td>
                    <td><?php echo $row->LEWATJT; ?> days</td>
                    <td style="text-align: right;">
                        <?php 
                            $sub_inv +=$row->NILAI_INVOICE-$row->SALDO_AWAL;
                            echo ($row->NILAI_INVOICE-$row->SALDO_AWAL); 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_pelunasan += $row->PELUNASAN;
                            echo ($row->PELUNASAN); 
                        ?>    
                    </td>
                    <td>
                        <?php
                            $sub_sakhir +=  $row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN;
                            echo ($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN); 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_lewatjt += ($row->LEWATJT <= 0)?$row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN:0; 
                            echo ($row->LEWATJT <= 0)?($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN):0; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_first += ($row->LEWATJT > 0 && $row->LEWATJT <=30)?$row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN:0;
                            echo ($row->LEWATJT > 0 && $row->LEWATJT <=30)?($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN):0; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_second += ($row->LEWATJT > 30 && $row->LEWATJT <=60)?$row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN:0;
                            echo ($row->LEWATJT > 30 && $row->LEWATJT <=60)?($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN):0; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_third += ($row->LEWATJT > 60 && $row->LEWATJT <=90)?$row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN:0;
                            echo ($row->LEWATJT > 60 && $row->LEWATJT <=90)?($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN):0; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $sub_fourth += ($row->LEWATJT > 90)?$row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN:0;
                            echo ($row->LEWATJT > 90)?($row->NILAI_INVOICE-$row->SALDO_AWAL-$row->PELUNASAN):0; 
                        ?>
                    </td>
                </tr>
            <?php $no++; } ?>
             <tr style="background-color: #ff6a00; color: #fff; height: 35px;">
                <td colspan="10" style="text-align: right;"><b>Total :</b></td>
                <td style="text-align: right;"><?php echo ($sub_inv); ?></td>
                <td style="text-align: right;"><?php echo ($sub_pelunasan); ?></td>
                <td style="text-align: right;"><?php echo ($sub_sakhir); ?></td>
                <td style="text-align: right;"><?php echo ($sub_lewatjt); ?></td>
                <td style="text-align: right;"><?php echo ($sub_first); ?></td>
                <td style="text-align: right;"><?php echo ($sub_second); ?></td>
                <td style="text-align: right;"><?php echo ($sub_third); ?></td>
                <td style="text-align: right;"><?php echo ($sub_fourth); ?></td>
            </tr> 
        </tbody>
    </table>
</body>
</html>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>