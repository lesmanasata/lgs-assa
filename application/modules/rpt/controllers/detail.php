<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Detail extends MY_Controller
{
    public $tbl_hider              = 'TF_BKPF';
    public $tbl_detail             = 'TF_BSEG';

    public $fild_comp_code         = 'CHR_BUKRS';
    public $fild_bisnis_area       = 'VCH_GSBER';
    public $fild_cost_center       = 'VCH_KOSTL';
    public $fild_gl_account        = 'VCH_HKONT';
    public $fild_curency           = 'CHR_CURENCY';
    public $fild_amount            = 'INT_WRBTR';
    public $fild_posting_date      = 'DAT_BUDAT';
    public $fild_doc_date          = 'DAT_BLDAT';


    public $fild_reverence         = 'CHR_XBLNR';
    public $fild_doc_type          = 'ENM_BLART';
    public $fild_acounting_no      = 'VCH_BELNR';
    public $fild_fiscal_year       = 'DYR_GJAHR';
    public $fild_text              = 'TEX_SGTXT';
    public $fild_internal_order    = 'VCH_AUFNR';
    public $fild_line_item         = 'VCH_BUZEI';
    public $fild_posting_key       = 'VCH_BSCHL';
    public $fild_indicator_saldo   = 'ENM_SHKZG';
    public $fild_group_id          = 'CHR_GROUP';
    public $fild_flag              = 'CHR_FLAG';
    public $cfg_Clearing           = array();
    public $fild_doc_hider         = 'CHR_BKTXT';
    public $fild_clearing_date     = 'DAT_AUGCP';
    public $fild_clearing_doc_no   = 'VCH_AUGBL';
    public $fild_doc_number_alias  = 'VCH_REBZG';
    public $fild_doc_yer_alias     = 'DYR_REBZJ';
    public $fild_doc_item_alias    = 'INT_REBZZ';
    public $fild_customer          = 'VCH_KUNNR';
    public $fild_special           = 'VCH_IND_JURNAL';
    public $fild_billing_doc       = 'CHR_AWKEY';
    public $petugas                = 'petugas_id';
    public $last_insert            = 'last_insert';
    public $date_clearing          = 'DAT_AUGDT';
    public $sap_doc                = 'VCH_BELNR_SAP';
    public $flag                   = 'CHR_FLAG';

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->template->title('Report Detail');
        $this->template->build('rpt/rpt_detail_form');
    }

    public function getDetial()
    {
        $custom_filter  = $this->input->post("custome_filter");
        $filter_id      = $this->input->post("find_id");
        $filter_year    = $this->input->post("find_year");
        $filter_tgl     = $this->input->post("find_tanggal");
        $custom_periode = $this->input->post("find_periode");
        $filter_moth    = $this->input->post("find_bulan");
        $filter_user    = $this->input->post("find_user");
        $filter_trx     = $this->input->post("find_trx_type");
        $group_trx_clr  = array("ARL","BPL","ARI","BPI","ARB","SPI","SPL");
        $initial        = substr($filter_id, 0, 3);

        $postData['res'] = array(
            'custom_filter'=>$custom_filter,
            'filter_id'=>$filter_id,
            'filter_year'=>$filter_year,
            'filter_tgl'=>$filter_tgl,
            'custom_periode'=>$custom_periode,
            'filter_moth'=>$filter_moth,
            'filter_user'=>$filter_user,
            'filter_trx'=>$filter_trx
        );

        if (in_array($filter_trx, $group_trx_clr) && $filter_trx !='') {
            $this->load->view('rpt/rpt_costdist', $postData);
        } elseif (!in_array($filter_trx, $group_trx_clr) && $filter_trx !='') {
            $this->load->view('rpt/rpt_costdist', $postData);
        } else {
            if (!$custom_filter && in_array($initial, $group_trx_clr)) {
                $this->load->view('rpt/rpt_costdist', $postData);
            } else {
                $this->load->view('rpt/rpt_costdist', $postData);
            }
        }
    }

    public function getData()
    {
        $this->load->model('master/master_model', 'mod_mstr');
        if(!$this->session->userdata('setexcel')){
            $custom_filter  = $this->input->post("custom_filter");
            $custom_per     = $this->input->post("custom_periode");
            $filter_tgl     = date('Y-m-d', strtotime($this->input->post("filter_tgl")));
            $filter_month   = $this->input->post("filter_moth");
            $filter_year    = $this->input->post("filter_year");
            $filter_user    = $this->input->post("filter_user");
            $filter_trx     = $this->input->post("filter_trx");
            $filter_tr     = $this->input->post("filter_id");
        }else{
            $custom_filter  = $this->session->userdata('custom_filter');
            $custom_per     = $this->session->userdata('custom_periode');
            $filter_tgl     = $this->session->userdata('filter_tgl');
            $filter_month   = $this->session->userdata('filter_moth');
            $filter_year    = $this->session->userdata('filter_year');
            $filter_user    = $this->session->userdata('filter_user');
            $filter_trx     = $this->session->userdata('filter_trx');
            $filter_tr      = $this->session->userdata('filter_id'); 
        }
        $group          = "";
        $filter         = array();

        $filds = "h.".$this->fild_comp_code." as company_code,";
        $filds .= "h.".$this->fild_acounting_no." as doc_number,";
        $filds .= "h.".$this->fild_doc_type." as doc_type,";
        $filds .= "h.".$this->fild_posting_date." as posting_date,";
        $filds .= "h.".$this->fild_doc_date." as doc_date,";
        $filds .= "h.".$this->fild_fiscal_year." as doc_yer,";
        $filds .= "h.".$this->fild_reverence." as rev,";
        $filds .= "h.".$this->fild_doc_hider." as doc_hider,";
        $filds .= "h.".$this->fild_billing_doc." as doc_bil,";
        $filds .= "h.".$this->fild_curency." as curency,";
        $filds .= "h.".$this->flag." as flag,";
        $filds .= "IFNULL(bs.VCH_BELNR_SAP,' ') as doc_sap,";
        $filds .= "IFNULL(bs.VCH_SAPJV,' ') as doc_sapjv,";
        $filds .= "IFNULL(bs.VCH_SAPPAYMENT,' ') as doc_sappayment,";
        $filds .= "IFNULL(bs.VCH_SAPREKON,' ') as doc_sapprekon,";
        $filds .= "h.petugas_id,";
        $filds .= "date_format(h.last_insert,'%d-%m-%Y') as last_insert,";
        $filds .= "'' as url";
        $group = 'h.'.$this->fild_acounting_no;
        $cust = array('COS','ARL','BPI','BPL','ARI','ARB','SPI','SPL');
        $test = substr($filter_tr,0,3);
        if ($custom_filter=="on") {
            if ($custom_per=='rb_date' and $filter_user !='-1') {
                $filter['LEFT(h.'.$this->fild_acounting_no.',3)']=$filter_trx;
                $filter['DATE(h.'.$this->last_insert.')']=$filter_tgl;
                $filter['h.'.$this->petugas]=$filter_user;
            }elseif ($custom_per=='rb_date') {
                $filter['LEFT(h.'.$this->fild_acounting_no.',3)']=$filter_trx;
                $filter['DATE(h.'.$this->last_insert.')']=$filter_tgl;
            }
            if ($custom_per=='rb_month' and $filter_user !='-1') {
                $filter['LEFT(h.'.$this->fild_acounting_no.',3)']=$filter_trx;
                $filter['MONTH(h.'.$this->last_insert.')']=$filter_month;
                $filter['YEAR(h.'.$this->last_insert.')']=$filter_year;
                $filter['h.'.$this->petugas]=$filter_user;
            }elseif ($custom_per=='rb_month') {
                $filter['LEFT(h.'.$this->fild_acounting_no.',3)']=$filter_trx;
                $filter['MONTH(h.'.$this->last_insert.')']=$filter_month;
                $filter['YEAR(h.'.$this->last_insert.')']=$filter_year;
            }
            $filter['LEFT(h.'.$this->fild_acounting_no.',3)']=$filter_trx;

        } else {
            if (!in_array($test, $cust)) {
                $filter['(bs.'.$this->sap_doc.')']=$filter_tr;
            }else {
                $filter['(h.'.$this->fild_acounting_no.')']=$filter_tr;
            }
        }

        $res = $this->mod_mstr->getBK($filds, $filter, $group);

        foreach ($res as $key => $value) {
            $res[$key]->url = $value->doc_number;
        }

        if(!$this->session->userdata('setexcel')){
            echo json_encode(array("data"=>$res));
        }
        else{
            $this->session->unset_userdata('setexcel');
            $this->load->view('rpt/excel',array("data"=>$res));
        }
    }

    public function itemdetail()
    {
        $this->load->view('rpt/pdf_template');
    }
    public function report()
    {
        $this->load->model('master/master_model', 'mod_mstr');
        $where = $this->uri->segment(4);

        $filds = "bk.".$this->fild_comp_code.",";
        $filds .= "bk.".$this->fild_acounting_no.",";
        $filds .= "bk.".$this->fild_fiscal_year.",";
        $filds .= "DATE_FORMAT(bk.".$this->fild_posting_date.",'%d-%m-%Y') as DAT_BUDAT,";
        $filds .= "bk.".$this->fild_doc_date.",";
        $filds .= "bk.".$this->fild_doc_type.",";
        $filds .= "DATE_FORMAT(bs.".$this->date_clearing.",'%d-%m-%Y') as DAT_AUGDT,";
        $filds .= "bs.".$this->fild_posting_key.",";
        $filds .= "bs.".$this->fild_indicator_saldo.",";
        $filds .= "bs.".$this->fild_gl_account.",";
        $filds .= "bs.".$this->fild_customer.",";
        $filds .= "bs.".$this->fild_bisnis_area.",";
        $filds .= "bs.".$this->fild_text.",";
        $filds .= "bs.".$this->fild_doc_number_alias.",";
        $filds .= "bs.".$this->fild_amount.",";
        $filds .= "bs.".$this->fild_doc_yer_alias.",";
        $filds .= "DATE_FORMAT(bk.".$this->last_insert.", '%d-%m-%Y %H:%i:%S') as last_insert,";
        $filds .= "si.".$this->sap_doc.",";
        $filds .= "bk.".$this->flag.",";
        $filds .= "bs.DAT_BUKPOT,";
        $filds .= "bs.VCH_NUMBPOT,";
        $filds .= "bk.".$this->petugas;

        $filter["bk.".$this->fild_acounting_no]=$where;

        $res = $this->mod_mstr->reportBS($filds, $filter);
        echo json_encode(array("data"=>$res));
    }

    function setexcel(){
        $sesdata = array(
            'setexcel'=>TRUE,
            'custom_filter'=>$this->input->post("custom_filter"),
            'filter_id'=>$this->input->post("filter_id"),
            'filter_year'=>$this->input->post("filter_year"),
            'filter_tgl'=>date('Y-m-d', strtotime($this->input->post("filter_tgl"))),
            'custom_periode'=>$this->input->post("custom_periode"),
            'filter_moth'=>$this->input->post("filter_moth"),
            'filter_user'=>$this->input->post("filter_user"),
            'filter_trx'=>$this->input->post("filter_trx")
        );
        $this->session->set_userdata($sesdata);
    }

}
