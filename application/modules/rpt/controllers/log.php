<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Log extends MY_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }
    
	public function index()
    {
        $this->template->title('Report Detail');
        $this->template->build('rpt/rpt_log_trans');
    }

    function getcleringlog(){
        $this->load->model('rpt/log_model','mod_loging');
        $take   = $this->input->post('take');
        $skip   = $this->input->post('skip');
        $filds  = '*';
        $filter =array();

        if (isset($_POST['filter']['filters'])) {
            $setFilter = $_POST['filter']['filters'];
            foreach ($setFilter as $key => $value) {
                $fild       = $setFilter[$key]['field'];
                $keyword    = $setFilter[$key]['value'];
                $operator   = $setFilter[$key]['operator'];

                if($fild=='DAT_postingdate' || $fild=='DAT_createon'){
                    $c_date     = explode(" ",$keyword);
                    $ar_bln     = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
                    
                    if(count($c_date)>2)
                        $keyword    = $c_date[3].'-'.$ar_bln[$c_date[1]].'-'.$c_date[2];
                
                }


                if($operator=='contains')
                    $filter["`".$fild."` LIKE '%".$keyword."%'"]=NULL;

                if($operator=='eq')
                    $filter["`".$fild."` = '".$keyword."'"]=NULL;

                if($operator=='neq')
                    $filter["`".$fild."` != '".$keyword."'"]=NULL;
            }
        }
        
        $out['data']    = $this->mod_loging->getCleringLog('*',
            $filter, array(
                'take'=>$take,
                'skip'=>$skip,
                'total'=>FALSE,
            )
        );

        $out['total']    = $this->mod_loging->getCleringLog('INT_id',
            $filter, array(
                'take'=>$take,
                'skip'=>$skip,
                'total'=>TRUE,
            )
        );

        echo json_encode($out);
    }


    function getppnlog(){
        $this->load->model('rpt/log_model','mod_loging');
        $take   = $this->input->post('take');
        $skip   = $this->input->post('skip');
        $filds  = '*';
        $filter =array();

        if (isset($_POST['filter']['filters'])) {
            $setFilter = $_POST['filter']['filters'];
            foreach ($setFilter as $key => $value) {
                $fild       = $setFilter[$key]['field'];
                $keyword    = $setFilter[$key]['value'];
                $operator   = $setFilter[$key]['operator'];

                if($fild=='DAT_createon'){
                    $c_date     = explode(" ",$keyword);
                    $ar_bln     = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
                    
                    if(count($c_date)>2)
                        $keyword    = $c_date[3].'-'.$ar_bln[$c_date[1]].'-'.$c_date[2];
                
                }

                if($operator=='contains')
                    $filter["`".$fild."` LIKE '%".$keyword."%'"]=NULL;

                if($operator=='eq')
                    $filter["`".$fild."` = '".$keyword."'"]=NULL;

                if($operator=='neq')
                    $filter["`".$fild."` != '".$keyword."'"]=NULL;
            }
        }
        
        $out['data']    = $this->mod_loging->getppnLog('*',
            $filter, array(
                'take'=>$take,
                'skip'=>$skip,
                'total'=>FALSE,
            )
        );

        $out['total']    = $this->mod_loging->getppnLog('VCH_BELNR',
            $filter, array(
                'take'=>$take,
                'skip'=>$skip,
                'total'=>TRUE,
            )
        );

        echo json_encode($out);
    }
}