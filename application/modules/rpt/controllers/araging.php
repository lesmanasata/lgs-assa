<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Araging extends MY_Controller
{   
    public $tbl_hider = 'TF_BKPF';
    public $tbl_detail = 'TF_BSEG';

	public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->cfg_Clearing = json_decode(json_encode($this->config->item('ar_clearing')));
        $this->app->cek_permit();
    }
    
	public function index() 
    {
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => ''),
            array('name' => 'company_code_listinv', 'url' => 'master/cmb_company', 'flow' => ''),
            array('name' => 'doc_year', 'url' => 'rpt/araging/getdoc_yer', 'flow' => ''),
            array('name' => 'doc_year_listinv', 'url' => 'rpt/araging/getdoc_yer', 'flow' => ''),
            array('name' => 'doc_year_inv', 'url' => 'rpt/araging/getdoc_yer', 'flow' => ''),
            array('name' => 'company_code_inv', 'url' => 'master/cmb_company', 'flow' => ''),
        );
        
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);

        $this->template->title('Report Detail');
        $this->template->build('rpt/rpt_aging_form',$data);
    }

    function getdoc_yer(){
        $this->load->model('rpt/rpt_model','mod_rpt');
        $fild = "DYR_GJAHR as id, DYR_GJAHR as text";
        $filter["LEFT(VCH_BELNR,3) NOT IN('ARI','ARL','BPL','BPI','SPL','SPI','ARB')"]=NULL;
        $res = $this->mod_rpt->get_cmb($fild, $this->tbl_hider, $filter,'DYR_GJAHR');

        echo json_encode(array('data'=>$res));
    }


    function requestdata(){
        $this->load->model('rpt/rpt_model','mod_rpt');
        $out        = array('status'=>true, 'data'=>array(),'msg'=>'');
        $doc_type   = explode(",", $this->cfg_Clearing->type_doc_tag);
        $postid     = explode('-',$this->input->post('customer'));
        $company    = $this->input->post('company_code');
        $doc_yer    = $this->input->post('doc_year');
        $bis_area   = $this->input->post('area');

        $filds   ='d.VCH_KUNNR AS NO_CMD,';
        $filds  .='c.VCH_Name1 AS NAMA_CUSTOMER,';
        $filds  .='h.CHR_BKTXT AS NO_INVOICE,';
        $filds  .='h.VCH_BELNR AS DOC_SAP,';
        $filds  .="(SELECT  GROUP_CONCAT(DISTINCT VCH_BELNR) FROM  TF_BSEG WHERE  CHR_BUKRS = h.CHR_BUKRS AND  VCH_REBZG = h.VCH_BELNR AND DATE_FORMAT(DAT_AUGCP,'%Y%m') <  DATE_FORMAT(curdate(),'%Y%m') ) AS  DOC_WEB,";
        $filds  .='h.DAT_BUDAT AS  TGL_IVOICE,';
        $filds  .='SUM(d.INT_WRBTR) AS  NILAI_INVOICE,';
        $filds  .='CASE WHEN d.DATE_BASE IS NULL THEN h.DAT_BUDAT WHEN DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY)  < h.DAT_BUDAT THEN h.DAT_BUDAT ELSE DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY) END AS  TGL_JTEMPO,';
        //$filds  .='CASE WHEN DATE_ADD(DATE_BASE, INTERVAL INT_TERMS DAY)  < DAT_BUDAT THEN curdate() - DAT_BUDAT ELSE curdate() - DATE_ADD(DATE_BASE, INTERVAL INT_TERMS DAY) END AS  LEWATJT,';
        $filds  .='DATEDIFF(curdate(), (CASE WHEN d.DATE_BASE IS NULL THEN h.DAT_BUDAT WHEN DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY) < h.DAT_BUDAT THEN h.DAT_BUDAT ELSE DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY) END)) AS  LEWATJT,';
        $filds  .="(SELECT IFNULL(SUM(INT_WRBTR),0) FROM  TF_BSEG WHERE CHR_BUKRS = h.CHR_BUKRS AND  VCH_REBZG = h.VCH_BELNR AND DATE_FORMAT(DAT_AUGCP,'%Y%m') <  DATE_FORMAT(curdate(),'%Y%m') ) AS  SALDO_AWAL,";
        $filds  .="(SELECT IFNULL(SUM(INT_WRBTR),0) FROM  TF_BSEG WHERE CHR_BUKRS = h.CHR_BUKRS AND  VCH_REBZG = h.VCH_BELNR AND DATE_FORMAT(DAT_AUGCP,'%Y%m') =  DATE_FORMAT(curdate(),'%Y%m') ) AS  PELUNASAN";


        do{
            if($doc_yer =='-1'){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Doc Year';
                break;
            }

            if($company=='-1'){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Company';
                break;
            }

            if(count($postid) > 1){
                $belner_cus = $this->mod_rpt->get_detail('DISTINCT VCH_BELNR',
                    array(
                        'CHR_BUKRS' =>$company,
                        'DYR_GJAHR' =>$doc_yer,
                        'VCH_KUNNR' =>$postid[0]
                    )
                );

                $belner_ar=array();
                if(count($belner_cus)> 1){
                    foreach ($belner_cus as $row)
                        $belner_ar[] = $row->VCH_BELNR;

                    if(count($belner_ar)> 0)
                        $filter["d.VCH_BELNR IN('".implode("','", $belner_ar)."')"] =NULL;
                }
            }


            //$filter['d.VCH_AUGBL IS NULL'] = NULL;
            $filter['CONCAT(d.bp_status,d.ar_status,d.ssp_status) !="111"'] = NULL;
            $filter['d.CHR_BUKRS'] = $company;
            $filter['d.DYR_GJAHR'] = $doc_yer;
            $filter["d.VCH_BSCHL IN ('01')"] = NULL;
            $filter["h.ENM_BLART IN('".implode("','", $doc_type)."')"] =NULL;

            $out['data'] = $this->mod_rpt->getAgingData($filds, $filter);
        }while(FALSE);

        if($this->input->is_ajax_request())
            echo json_encode($out);
        else
            $this->load->view('rpt/rpt_aging_excel',$out);
    }

    function requestlvbayar(){
        $this->load->model('rpt/rpt_model','mod_rpt');
        $out        = array('status'=>true, 'data'=>array(),'msg'=>'');
        $company    = ($this->input->is_ajax_request())?$this->input->post('company_code_inv'):$this->input->post('company_code_listinv');
        $doc_yer    = ($this->input->is_ajax_request())?$this->input->post('doc_year_inv'):$this->input->post('doc_year_listinv');
        $doc_type   = explode(",", $this->cfg_Clearing->type_doc_tag);
        $postid     = explode('-',$this->input->post('customer'));
        $doc_all    = array();
        $doc_out    = array();
        $doc_inv   = array();
        $doc_bank   = array();

        
        
         do{

            if($doc_yer =='-1' || $doc_yer==false){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Doc Year';
                break;
            }

            if($company=='-1' || $company==false){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Company';
                break;
            }

            $filter=array(
                    //"DATE_FORMAT(h.DAT_BUDAT, '%Y%m') = DATE_FORMAT(CURDATE(), '%Y%m')"=>NULL,
                    "DATE_FORMAT(d.last_insert, '%Y%m') = DATE_FORMAT(CURDATE(), '%Y%m')"=>NULL,
                    "LEFT(d.VCH_BELNR,3) IN ('ARI','ARL')"=>NULL,
                    "d.VCH_REBZG IS NOT NULL"=>NULL
                );

            
            if(count($postid) > 1){
                $belner_cus = $this->mod_rpt->get_detail('DISTINCT VCH_BELNR',
                    array(
                        'CHR_BUKRS' =>$company,
                        'DYR_REBZJ' =>$doc_yer,
                        //'DYR_GJAHR' =>$doc_yer,
                        'VCH_KUNNR' =>$postid[0]
                    )
                );


                $belner_ar=array();
                if(count($belner_cus)> 1){
                    foreach ($belner_cus as $row)
                        $belner_ar[] = $row->VCH_BELNR;

                    if(count($belner_ar)> 0){
                        $filter["d.VCH_BELNR IN('".implode("','", $belner_ar)."')"] =NULL;
                    }
                }else
                  $filter["d.VCH_KUNNR "] ='blank';  
            }

            
            $doc_proses = $this->mod_rpt->get_join(
                'd.CHR_BUKRS,d.VCH_REBZG,d.DYR_REBZJ,d.VCH_BELNR,d.INT_WRBTR,h.DAT_BUDAT',
                $filter
            );

            foreach ($doc_proses as $key => $value){
                $doc_all[] = $value->CHR_BUKRS.$value->VCH_REBZG.$value->DYR_REBZJ;
            }

            $filds   ='d.VCH_KUNNR AS NO_CMD,';
            $filds  .='c.VCH_Name1 AS NAMA_CMD,';
            $filds  .='h.VCH_BELNR AS NO_INVOICE,';
            $filds  .='h.DAT_BUDAT AS DAT_INV,';
            $filds  .='CASE WHEN d.DATE_BASE IS NULL THEN h.DAT_BUDAT WHEN DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY)  < h.DAT_BUDAT THEN h.DAT_BUDAT ELSE DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY) END AS  TGL_JTEMPO,';
            $filds  .='SUM(d.INT_WRBTR) AS NILAI_INV,';
            $filds  .='d.CHR_BUKRS,';
            $filds  .='d.DYR_GJAHR';

            if(count($doc_all)>0){
                $doc_out = $this->mod_rpt->getAgingData($filds,
                    array(
                       "CONCAT(d.CHR_BUKRS,d.VCH_BELNR,d.DYR_GJAHR) IN ('".implode("','", $doc_all)."')"=> NULL,
                       "h.ENM_BLART IN('".implode("','", $doc_type)."')"=>NULL
                    )
                );
            }

            

            $i=0;
            foreach ($doc_proses as $key => $value) {
                foreach ($doc_out as $key_out => $value_out) {
                    if($value->VCH_REBZG == $value_out->NO_INVOICE){
                        $out['data'][$i]['NO_CMD']      = $value_out->NO_CMD;
                        $out['data'][$i]['NAMA_CMD']    = $value_out->NAMA_CMD;
                        $out['data'][$i]['NO_INVOICE']  = $value_out->NO_INVOICE;
                        $out['data'][$i]['DAT_INV']     = $value_out->DAT_INV;
                        $out['data'][$i]['TGL_JTEMPO']  = $value_out->TGL_JTEMPO;
                        $out['data'][$i]['NILAI_INV']   = $value_out->NILAI_INV;
                        $out['data'][$i]['INT_BAYAR']   = $value->INT_WRBTR;
                        $out['data'][$i]['TGL_BAYAR']   = $value->DAT_BUDAT;
                        $out['data'][$i]['NO_AR']      = $value->VCH_BELNR;
                        $out['data'][$i]['TGL_AR']      = $value->DAT_BUDAT;

                        $ubelner = $value->VCH_BELNR;
                        foreach ($doc_proses as $key_b => $value_b) {
                            if($value_b->VCH_BELNR == $ubelner && $value_b->VCH_REBZG !=$value_out->NO_INVOICE ){
                                $out['data'][$i]['NO_DOC']   = $value_b->VCH_REBZG;
                            }
                        }

                        $doc_bp = $this->mod_rpt->get_detail(
                            'DAT_AUGCP, VCH_BELNR',
                            array(
                                'CHR_BUKRS'=> $value->CHR_BUKRS,
                                'VCH_REBZG'=> $value->VCH_REBZG,
                                "left(VCH_BELNR,3) IN ('BPL','BPI','ARB')"=>NULL
                            )
                        );

                        $out['data'][$i]['TGL_CLEARPPH'] = $doc_bp[0]->DAT_AUGCP;
                        $out['data'][$i]['NO_DOCPPH']    = $doc_bp[0]->VCH_BELNR;

                        $i++;
                    }
                }
            }
            
            
        }while (false);
        
        if($this->input->is_ajax_request())
            echo json_encode($out);
        else{
            $this->load->view('rpt/rpt_pembayaran_excel',json_decode(json_encode($out)));
        }
    }


    function requestlv(){
        $this->load->model('rpt/rpt_model','mod_rpt');
        $out        = array('status'=>true, 'data'=>array(),'msg'=>'');
        $doc_type   = explode(",", $this->cfg_Clearing->type_doc_tag);
        $postid     = explode('-',$this->input->post('customer'));
        $company    = ($this->input->is_ajax_request())?$this->input->post('company_code_i'):$this->input->post('company_code_inv'); 
        $doc_yer    = ($this->input->is_ajax_request())?$this->input->post('doc_year_i'):$this->input->post('doc_year_inv');

        do{

            if($doc_yer =='-1'){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Doc Year';
                break;
            }

            if($company=='-1'){
                $out['status'] = false;
                $out['msg'] ='Please select One Of Company';
                break;
            }

            $filds   ='d.VCH_KUNNR AS NO_CMD,';
            $filds  .='c.VCH_Name1 AS NAMA_CMD,';
            $filds  .='h.DAT_BUDAT AS  TGL_IVOICE,';
            $filds  .='h.CHR_BKTXT AS NO_INVOICE,';
            $filds  .='h.VCH_BELNR AS NO_DOC_SAP,';
            $filds  .='CASE WHEN d.DATE_BASE IS NULL THEN h.DAT_BUDAT WHEN DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY)  < h.DAT_BUDAT THEN h.DAT_BUDAT ELSE DATE_ADD(d.DATE_BASE, INTERVAL INT_TERMS DAY) END AS  TGL_JTEMPO,';
            $filds  .='SUM(d.INT_WRBTR) AS  NILAI_INVOICE,';
            
            $filter = array(
                    'CONCAT(d.bp_status,d.ar_status,d.ssp_status) !="111"'=>NULL,
                    "d.CHR_BUKRS"=>$company,
                    "d.DYR_GJAHR"=>$doc_yer,
                    "d.VCH_BSCHL IN ('01')"=>NULL,
                    "h.ENM_BLART IN('".implode("','", $doc_type)."')"=>NULL
                );

            if(count($postid) > 1){
                $belner_cus = $this->mod_rpt->get_detail('DISTINCT VCH_BELNR',
                    array(
                        'CHR_BUKRS' =>$company,
                        'DYR_GJAHR' =>$doc_yer,
                        'VCH_KUNNR' =>$postid[0]
                    )
                );

                $belner_ar=array();
                if(count($belner_cus)> 0){
                    foreach ($belner_cus as $row)
                        $belner_ar[] = $row->VCH_BELNR;

                    if(count($belner_ar)> 0)
                        $filter["d.VCH_BELNR IN('".implode("','", $belner_ar)."')"] =NULL;
                }
            }


            $res = $this->mod_rpt->getAgingData( $filds, $filter );
            $out['data']=$res;

        }while(false);

        if($this->input->is_ajax_request())
            echo json_encode($out);
        else{
            $this->load->view('rpt/rpt_listing_excel',$out);
        }
    }
    
}