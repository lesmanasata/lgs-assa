<?php
class Log_model extends CI_Model
{
    public $tbl_log_clearing = 'TF_LOG_CLEARING';
	public $tbl_log_ppn = 'TF_SINC';

	public function __construct()
	{
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }


    function getCleringLog($custom_fild, $filter=array(),$mode=array()){ 
        $use = json_decode(json_encode($mode));

        $this->db_main->select($custom_fild,FALSE)
            ->from($this->tbl_log_clearing);
            
        if($filter !=FALSE && count($filter)>0)
            $this->db_main->where($filter);

        if(count($mode)>0){
            if(!$use->total)
                $this->db_main->limit($use->take,$use->skip);
        }
        $this->db_main->order_by('INT_id','DESC');
        $res=$this->db_main->get();
        if($use->total)
            return $res->num_rows();
        else
            return $res->result();
        
    }

    function getppnLog($custom_fild, $filter=array(),$mode=array()){ 
        $use = json_decode(json_encode($mode));

        $this->db_main->select($custom_fild,FALSE)
            ->from($this->tbl_log_ppn);
            
        if($filter !=FALSE && count($filter)>0)
            $this->db_main->where($filter);

        if(count($mode)>0){
            if(!$use->total)
                $this->db_main->limit($use->take,$use->skip);
        }
        $this->db_main->order_by('DAT_createon','DESC');

        $res=$this->db_main->get();
        if($use->total)
            return $res->num_rows();
        else
            return $res->result();
        
    }


}