<?php
class Rpt_model extends CI_Model
{
	public $tbl_hider = 'TF_BKPF';
    public $tbl_detail = 'TF_BSEG';
    public $tbl_customer = 'MF_CUSTOMER';

	public function __construct()
	{
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    function getAgingData($filds='*', $filter=array()){
    	$this->db_main->select($filds,FALSE)
    		->from($this->tbl_hider.' h')
    		->join($this->tbl_detail.' d','h.CHR_BUKRS=d.CHR_BUKRS AND h.VCH_BELNR=d.VCH_BELNR AND h.DYR_GJAHR=d.DYR_GJAHR')
    		->join($this->tbl_customer.' c', 'd.VCH_KUNNR=c.VCH_CustomerId','LEFT');

    	if(count($filter)>0){
    		$this->db_main->where($filter);
    	}
        $this->db_main->group_by('h.CHR_BUKRS,h.VCH_BELNR,h.DYR_GJAHR');
    	$res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }

    public function get_cmb($custom_fild, $tbl, $filter=array(),$group='')
    {
        $this->db_main->select($custom_fild, false)
            ->from($tbl);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        if($group !='')
            $this->db_main->group_by($group);

        $this->db_main->order_by('DYR_GJAHR','DESC');
        $res=$this->db_main->get();

        return $res->result();
    }

    public function get_join($custom_fild, $filter=array(), $group=false)
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider.' h')
            ->join($this->tbl_detail.' d', 'h.CHR_BUKRS=d.CHR_BUKRS and h.VCH_BELNR=d.VCH_BELNR and h.DYR_GJAHR=d.DYR_GJAHR');

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        if ($group !=false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }

    public function get_detail($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_detail);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }
}