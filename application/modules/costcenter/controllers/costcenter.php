<?php
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Costcenter extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->lang->load('language', 'english2');
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => 'bisnis_area'),
            array('name' => 'bisnis_area', 'url' => 'master/cmb_bisnis_area', 'flow' => '')
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);
        $this->template->build('costcenter/costcenter_view', $data);
    }
    public function view_cost()
    {
        $this->load->model('costcenter/costcenter_model', 'cost_mod');
        $data = $this->cost_mod->costCenter();

        echo json_encode(array('data' => $data, ));
    }
    public function getBisCode()
    {
        $this->load->model('costcenter/costcenter_model', 'cost_mod');
        $id = $this->input->post('id');
        $data = $this->cost_mod->getID($id);

        echo json_encode(array('data'=>$data));
    }

    public function submit()
    {
        $mode = $this->input->post('models');
        $con = current(json_decode($mode));
        $this->load->model('costcenter/costcenter_model', 'cost_mod');
        $this->cost_mod->updateKet($con);
    }
    public function save()
    {
        $this->load->model('costcenter/costcenter_model', 'cost_mod');
        $this->cost_mod->savecustomer();
    }
}
