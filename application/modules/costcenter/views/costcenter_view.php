<div class="banner">
    <div class="btn-group">
            <?php
                echo '<button class="btn btn-success navbar-btn" id="add">
                <i class="glyphicon glyphicon-plus"></i> Add record </button> ';
            ?>
    </div>
</div>

<div class="blank">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Cost Center</a></li>
    </ul>

    <div class="tab-content grid-form1" style="border-top:0px;">
          <div id="home" class="tab-pane fade in active ">
            <div class="padding_tab_body">
                <div id="grid"></div>
            </div>
          </div>
    </div>
</div>
<script>
    var record	= 0;
    var company_code_global ='';

    $(document).ready(function () {
        <?php echo $js_cmb; ?>
		cmb['company_code'].value('-1');
		cmb['bisnis_area'].value('-1');

		cmb['company_code'].bind('change',function(){

		cmb['bisnis_area'].value('-1');
		company_code_global = cmb['company_code'].value();
		ds_costcenter.read();
		});

        var  ds_costcenter = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    type:"POST",
                                    dataType: "json",
                                    url: "<?php echo site_url('costcenter/view_cost');?>"
                                },
                                update: {
                                    url: "<?php echo site_url('costcenter/submit'); ?>",
                                    dataType: "json",
                                    type:'POST'
                                },
                                parameterMap: function(options, operation){
                                    if (operation !== "read" && options.models) {
                                      return {
                                          models: kendo.stringify(options.models)
                                      };
                                    }
                              }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                parse: function(res) {
                                        return res.data;
                                    },
                                model: {
                                    id: "CHR_CODE",
                                    fields: {
                                        CHR_CODE: {type: "text", editable: false, nullable: true },
                                        VCH_BUSINESS_CODE: { type: "text", editable: false, nullable: true },
                                        VCH_DESCRIPTION: { type: "text" },
                                        status: { type: "boolean"}
                                    }
                                }
                            },

                        });
        $("#grid").kendoGrid({
            dataSource: ds_costcenter,
            height: 550,
            sortable: true,
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
            {field: "CHR_CODE", title: "Bisnis Area", width: 150, editable: false},
            {field: "VCH_BUSINESS_CODE", title: "Cost Center", width: 150},
            {field: "VCH_DESCRIPTION", title: "Keterangan"},
            {field: "status", title: "Status",template: "#if(status==true){#Aktif#}else{#Non Aktif#}#"},
            { command: ["edit"], title: "&nbsp;", width: "250px"}],
            editable: "inline",
            save:function(){
           this.refresh();
        }
        });
        $('#add').click(function(){
            $('#form_dialog_costcenter input[type=text]').val('');
            cmb['company_code'].value('-1');
    		cmb['bisnis_area'].value('-1');
            $('#dialog_costcenter').modal('show');
        })

        $("#dialog_costcenter").submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'<?php echo site_url('costcenter/save');?>',
                type:'POST',
                data:$("#form_dialog_costcenter").serialize(),
                beferosend:function() {
                    $('#loading-mask').addClass('loader-mask');
                },
                success:function(res) {
                    ds_costcenter.read();
                    $('#dialog_costcenter input[name=company_code]').val('');
                    $('#dialog_costcenter input[name=bisnis_area]').val('');
                    $('#dialog_costcenter input[name=cost_ket]').val('');
                    $('#dialog_costcenter').modal('toggle');
                }
            });
        });
    });

</script>

<div id="dialog_costcenter" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" id="form_dialog_costcenter" action="" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cost Center</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label col-sm-4" ><?php echo lang('cos_ar'); ?>:</label>
                            <div class="col-sm-8">
                            <input type="text" class="kendodropdown form-control" name="company_code" id="company_code" style="width:100%;" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" ><?php echo lang('cs_bis'); ?>:</label>
                            <div class="col-sm-8">
                            <input type="text" class="kendodropdown form-control" name="bisnis_area" id="bisnis_area" style="width:100%;" placeholder="&mdash;&mdash;<?php echo lang('dist_binis_area'); ?>&mdash;&mdash;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" ><?php echo lang('cos_ket'); ?>:</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="cost_ket" >
                            </div>
                        </div>
                    </div>
                <div style="clear:both"></div>
                <div class="modal-footer">
                    <button type="submit" id='simpan' class="btn btn-primary"  ><?php echo lang('btn_save'); ?></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('pwd_btn_cancel'); ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
