<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Costcenter_model extends CI_Model
{
    public $tbl_costcenter = 'MF_COST_CENTER';

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    public function costCenter()
    {
        $this->db_main->select('CHR_CODE, VCH_BUSINESS_CODE, VCH_DESCRIPTION,IF(status="1","true","false") as status', false)
                  ->from($this->tbl_costcenter);
        $res = $this->db_main->get();
        return $res->result();
    }

    public function updateKet($con)
    {
        $status = $con->status;
        if ($status==true) {
            $sta = '1';
        }else {
            $sta = '0';
        }

        $data = array(
            'VCH_DESCRIPTION' => $con->VCH_DESCRIPTION,
            'status'=>$sta
        );
        $this->db_main->where('CHR_CODE',$con->CHR_CODE);
        $this->db_main->update($this->tbl_costcenter, $data);

    }
    public function saveKet()
    {
        $bisnisarea = $this->input->post('bisnis_area');
        $desc = $this->input->post('cost_ket');

        $this->db_main->select('max(CHR_CODE)+1 AS NEW_TEST', false)
                        ->from($this->tbl_costcenter)
                        ->where('VCH_BUSINESS_CODE',$bisnisarea);
                        $dat = $this->db_main->get();
                        $data = current($dat->result());

        $data = array(
            'CHR_CODE' => '0000'.$data->NEW_TEST,
            'VCH_BUSINESS_CODE' => $bisnisarea,
            'VCH_DESCRIPTION' => $desc
        );

        $this->db_main->insert($this->tbl_costcenter,$data);

    }
}
