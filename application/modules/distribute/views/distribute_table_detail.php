<?php $timestamp = time();?>
<div id="distribute_detail_sub<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {

		var ds_distribute_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('distribute/table_get_detail_data'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 amount_item: { type: "number"},
					 }
				}
			},
		});

		$("#distribute_detail_sub<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_distribute_detail,
			pageable: false,
			scrollable: true,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				// {
				// 	filterable: false,
				// 	template: "#: ++no_urut #",
				// 	width: 50,
				// 	title:"NO"
				// },
				{field:"bisnis_area",width: 150, title:"<?php echo lang('dist_binis_area'); ?>",filterable: false},
				{field:"cost_center",width: 150, title:"<?php echo lang('dist_cost_center'); ?>",filterable: false},
				{field:"internal_order",width: 200, title:"<?php echo lang('dist_internal_order'); ?>",filterable: false},
				{field:"text_item",width: 200, title:"<?php echo lang('dist_text'); ?>",filterable: false},
				{field:"amount_item",width: 100, title:"<?php echo lang('dist_amount'); ?>",filterable: false, template:'#= kendo.toString(amount_item, "n0")#'},
			]
		});

	});
</script>
