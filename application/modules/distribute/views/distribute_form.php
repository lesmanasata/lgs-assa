<h1 class="page-title"><?php echo lang('titile_dist'); ?></h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Cost Distribution </a></li>
  <li class="active"><strong>Add New <?php echo lang('titile_dist'); ?></strong></li>
</ol>

<?php $timestamp = time();?>

<div class="grid-form">
	<div class="grid-form1">
			<form class="form-horizontal" id="Frm_costdist_<?php echo $timestamp; ?>" method="POST" action="<?php echo site_url("master/distribution_submit"); ?>">
				<div class="col-md-6">
					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_cmp_code'); ?></label>
						<div class="col-sm-9">
							<input type="text" class="kendodropdown form-control" style="width:100%;" name="company_code"  id="company_code" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;">
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_binis_area'); ?></label>
						<div class="col-sm-9">
							<input type="text" class="kendodropdown form-control" style="width:100%;" name="bisnis_area"  id="bisnis_area" placeholder="&mdash;&mdash;<?php echo lang('dist_binis_area'); ?>&mdash;&mdash;">
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_cost_center'); ?></label>
						<div class="col-sm-9">
							<div class="input-group">
								<input type="text" name="cost_center" id="cost_center" class="form-control" placeholder="<?php echo lang('dist_cost_center'); ?>" readonly>
								<span class="input-group-btn">
									<button type="button" id="find_cost_center" class="find btn btn-primary"><?php echo lang('btn_find'); ?></button>
								</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_gl_acc'); ?></label>
						<div class="col-sm-9">
							<div class="input-group">
								<input type="text" name="gl_acc" id="gl_acc" class="form-control" placeholder="<?php echo lang('dist_gl_acc'); ?>" readonly>
								<span class="input-group-btn">
									<button type="button" id="find_gl_acc" class="find btn btn-primary"><?php echo lang('btn_find'); ?></button>
								</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_curency'); ?></label>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="curency" value="IDR" readonly placeholder="<?php echo lang('dist_curency'); ?>">
						</div>

						<label  class="col-sm-2 control-label hor-form"><?php echo lang('dist_amount'); ?></label>
						<div class="col-sm-5 staticParent">
							<input type="text"   class="form-control numberOnly" name="amount" placeholder="<?php echo lang('dist_amount'); ?>">
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_post_date'); ?></label>
						<div class="col-sm-9">
							<input type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" name="psting_date" placeholder="<?php echo lang('dist_post_date'); ?>">
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_doc_date'); ?></label>
						<div class="col-sm-9">
							<input type="text" class="form-control datepicker" value="<?php echo date('Y-m-d'); ?>" name="doc_date" placeholder="<?php echo lang('dist_doc_date'); ?>">
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label hor-form"><?php echo lang('dist_remark'); ?></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="remark" placeholder="<?php echo lang('dist_remark'); ?>">
						</div>
					</div>
				</div>
				<div style="clear:both; ">&nbsp;</div>

				<div class="form-group">
					<label  class="col-sm-1 control-label hor-form">&nbsp;</label>
					<div class="col-sm-11">
						<div id="distribute_detail<?php echo $timestamp; ?>"></div>
					</div>
				</div>

				<div class="panel-footer">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
						<button type="submit" class="btn btn-primary"><?php echo lang('btn_save'); ?></button>
						<?php echo anchor('distribute','<button type="button" class="btn btn-danger">'.lang('btn_cancel').'</button>'); ?>
						
						</div>
					</div>
				</div>
			</form>
	</div>
</div>


<script type="text/javascript">
	var record	= 0;
	var company_code_global ='';
	$(document).ready(function () {
		<?php echo $js_cmb; ?>
		cmb['company_code'].value('-1');
		cmb['bisnis_area'].value('-1');

		cmb['bisnis_area'].bind('change',function(){
			$('input[name=cost_center]').val('');
		});

		cmb['company_code'].bind('change',function(){
			$('input[name=cost_center]').val('');
			cmb['bisnis_area'].value('-1');
			company_code_global = cmb['company_code'].value();
			ds_distribute.read();
		});

		var ds_distribute = new kendo.data.DataSource({
			transport: {
				read:  {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('distribute/diestribute_tbl'); ?>',
					//data: function() { return JSON.parse(JSON.stringify(viewModel.param)); }
				},
				create: {
					url: '<?php echo site_url('distribute/diestribute_submit'); ?>',
					type:"POST",
					dataType: "json",
					data: function() {
						return JSON.parse(JSON.stringify( $(this).hiderForm() ));
					},complete: function(res) {
						var res_msg = JSON.parse(res.responseText);
						$('#loading-mask').removeClass('loader-mask');
						if(res_msg.status){
							$(this).ClearhiderForm();
							ds_distribute.read();
						}
						msg_box(res_msg.messages,['btnOK'],'Info!');
					}
				}
			},
			batch: true,
			pageSize: 50,
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						bisnis_area: {  type: "text", validation: { required: true},defaultValue: { id: '', bisnis_area: ""} },
						cost_center: {  type: "text", validation: { required: true},defaultValue: { id: '', cost_center: ""} },
						internal_order: {  type: "text", validation: { required: true},defaultValue: { id: '', internal_order: ""} },
						item_amount: { type: "number", validation: { required: true} },
					}
				}
			}
		});

		$("#distribute_detail<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_distribute,
			pageable: false,
			scrollable: true,
			height: 300,
			selectable: "multiple",
			toolbar: ["create"],
			dataBinding: function() {
			  record = (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			editable: {
				createAt: 'bottom'
			},
			edit: function onEdit(e) {
			  var grid = this;
			  var cellIndex = e.container.index();
			  var nextCell = e.container.closest("tr").next("tr[role='row']").find("td").eq(cellIndex);

			  if (nextCell) {
				  e.container.find("input").on("keydown", function (e) {
					  if (e.which === 9) {
						  e.preventDefault();
						  e.stopImmediatePropagation();

							$(this).trigger("change");
						  grid.closeCell();
							grid.editCell(nextCell);
					  }
				  });
			  }
		  },
			columns: [
				{
					filterable: false,
					template: "#: ++record #",
					width: 50,
					title:"NO"
				},
				{field:"bisnis_area.bisnis_area",width: 250, title:"<?php echo lang('dist_binis_area'); ?>",filterable: false, 
					editor: function cmb_bisnis_area_box(container, options){
						$('<input required name="' + options.field + '" style=" font-size: 16px;" />')
							.appendTo(container)
							.kendoDropDownList({
								autoBind: false,
								filter: "contains",
								dataTextField: "text",
								dataValueField: "id",
								dataSource: {
									serverFiltering: true,
									transport: {
										read: {
											dataType: "jsonp",
											data:{'company_code':company_code_global},
											url: "<?php echo site_url('master/get_bisnis_area'); ?>",
										}
									}
								},
								change:function(){
									var grid = $("#distribute_detail<?php echo $timestamp; ?>").data("kendoGrid");
									var selectedItem = grid.dataItem(grid.select());
									var dtBisArea = selectedItem.cost_center.cost_center;
									selectedItem.set('cost_center.cost_center',' ');
								}
						});
					}
				},
				{field:"cost_center.cost_center",width: 250, title:"<?php echo lang('dist_cost_center'); ?>",filterable: false,
					editor:function(container, options){
						var grid = $("#distribute_detail<?php echo $timestamp; ?>").data("kendoGrid");
						var selectedItem = grid.dataItem(grid.select());
						var dtBisArea = selectedItem.bisnis_area.bisnis_area;
						$('<input  required name="' + options.field + '" style=" font-size: 14px;" />')
			              .appendTo(container)
			              .kendoDropDownList({
			                autoBind: false,
			                dataTextField: "text",
			                dataValueField: "id",
			                dataSource: {
			                  serverFiltering: true,
			                  transport: {
			                    read: {
			                      dataType: "jsonp",
			                      data:{'id':dtBisArea},
			                      url: "<?php echo site_url('master/cmb_cost_center2'); ?>",
			                    }
			                  }
			                }
			            });
					}
				},
				{
					field:"internal_order.internal_order",
					width: 200, 
					title:"<?php echo lang('dist_internal_order'); ?>",
					filterable: false,
					editor:function(container, options){
						var cmp_code_select = cmb['company_code'].value();
						var bis_area_select = options.model.bisnis_area.bisnis_area;

						$('<input  required name="' + options.field + '" style=" font-size: 14px;" />')
			              .appendTo(container)
			              .kendoDropDownList({
			                autoBind: true,
			                dataTextField: "text",
			                dataValueField: "id",
			                filter: "contains",
			                dataSource: {
			                  serverFiltering: true,
			                  transport: {
			                    read: {
			                    	data:{'company_code':cmp_code_select, 'bisarea':bis_area_select},
			                       	dataType: "jsonp",
			                       	url: "<?php echo site_url('distribute/internal_order'); ?>",
			                    }
			                  }
			                },
			                dataBound: adjustDropDownWidth, 
			            });
					}
				},
				{field:"item_amount",width: 100, title:"<?php echo lang('dist_amount'); ?>",filterable: false},
				{field:"description",width: 200, title:"<?php echo lang('dist_text'); ?>",filterable: false},
				{ 
					command: [
						{
							name: "destroy",
							template: "<a class='k-button k-grid-delete'><i class='glyphicon glyphicon-trash'></i></a>",
						}
					], 
					title: "<?php echo lang('label_action'); ?>", 
					text: "View Details",	
					width: 120, 
				},
			]
		});

		$("#Frm_costdist_<?php echo $timestamp; ?>").submit(function(e){
			e.preventDefault();
			$('#loading-mask').addClass('loader-mask');
			var cekData = $('#distribute_detail<?php echo $timestamp; ?>').data('kendoGrid').dataSource.total();
			if(cekData > 0)
				ds_distribute.sync();
			else{
				$('#loading-mask').removeClass('loader-mask');
				msg_box("<?php echo lang('msg_null_item_detail'); ?>",['btnOK'],'Info!');
			}
		});


		kendoWindow = $("<div />").kendoWindow({
			title: "<?php echo lang('nfc_dialog'); ?>",
			width: "500px",
			height: "450px",
			resizable: false,
			modal: true,
		});

		$('.find').click(function(){
			 kendoWindow.data("kendoWindow")
				.content($("#dialog_templates").html())
				.center().open();
			var ceksum = $(this).attr('id'), curUrl = '',titleId='',fieldCstm='',type='',sendItem={};

			if(ceksum=='find_cost_center'){
				curUrl 		= '<?php echo site_url('master/cmb_cost_center'); ?>';
				sendItem	= {"id":cmb['bisnis_area'].value()};
				fieldCstm 	= 'cost_center';
			}

			if(ceksum=='find_gl_acc'){
				curUrl 		= '<?php echo site_url('master/cmb_gl_acc'); ?>';
				fieldCstm 	= 'gl_acc';
			}

			var ds_find = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						dataType: "json",
						data:sendItem,
						url: curUrl,
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
				},
				pageSize: 100,
			});

			kendoWindow.find('.myGrid').kendoGrid({
				dataSource: ds_find,
				filterable: true,
				sortable: true,
				pageable: true,
				height: "400px",
				scrollable: false,
				filterable: {
					extra: false,
					operators: {
						string: {
							contains: "Like",
							eq: "=",
							neq: "!="
						},
					}
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{
						title:"<?php echo lang('label_action'); ?>",
						width:20,
						template: '<button  onClick="selectItem(this.id);" id="'+fieldCstm+'-#:id#-#:text#" class="pilih btn btn-xs btn-primary"><?php echo lang('btn_select'); ?></button>',
					},
					{field:"id",title:"<?php echo lang('lbl_code'); ?>"},
					{field:'text',title:"<?php echo lang('lbl_text'); ?>"},
				]
			});
		});

		$.fn.hiderForm = function() {
			out = {
				'company_code': cmb['company_code'].value(),
				'bisnis_area':cmb['bisnis_area'].value(),
				'cost_center': $('#Frm_costdist_<?php echo $timestamp; ?> input[name=cost_center]').val(),
				'gl_acc':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=gl_acc]').val(),
				'curency':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=curency]').val(),
				'amount':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=amount]').val(),
				'remark':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=remark]').val(),
				'doc_date':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=doc_date]').val(),
				'psting_date':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=psting_date]').val()
			}
			return out;
		}

		$.fn.ClearhiderForm = function() {
			out = {
				'company_code': cmb['company_code'].value('-1'),
				'bisnis_area':cmb['bisnis_area'].value('-1'),
				'cost_center': $('#Frm_costdist_<?php echo $timestamp; ?> input[name=cost_center]').val(''),
				'gl_acc':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=gl_acc]').val(''),
				'curency':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=curency]').val('IDR'),
				'amount':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=amount]').val(''),
				'remark':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=remark]').val(''),
				'doc_date':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=doc_date]').val('<?php echo date('Y-m-d'); ?>'),
				'psting_date':  $('#Frm_costdist_<?php echo $timestamp; ?> input[name=psting_date]').val('<?php echo date('Y-m-d'); ?>')
			}
			return out;
		}
	});



function selectItem(id){
	var record = id.split('-');
	var valls = record[1]+'-'+record[2];
	$(' input[name='+record[0]+']').val(valls);
	kendoWindow.data("kendoWindow").close();
}

 function adjustDropDownWidth(e) { 
      var listContainer = e.sender.list.closest(".k-list-container"); 
      listContainer.width('300px'); 
    } 
</script>

<script id="dialog_templates" type="text/x-kendo-template">
	<div class="myGrid"></div>
</script>
