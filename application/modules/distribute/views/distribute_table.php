<h1 class="page-title"><?php echo lang('titile_dist'); ?></h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Cost Distribution </a></li>
  <li class="active"><strong>List Of <?php echo lang('titile_dist'); ?></strong></li>
</ol>

<?php $timestamp = time();?>
<div class="banner">
	<div class="btn-group">
			<?php
				echo '<button class="btn btn-danger navbar-btn " id="del'.$timestamp.'"><i class="glyphicon glyphicon-trash"></i> '.lang('btn_delete').' </button> ';
				echo '<button class="btn btn-warning navbar-btn " id="reload'.$timestamp.'"><i class="fa fa-refresh"></i> '.lang('btn_reload').' </button> ';
			?>

	</div>
</div>

<div class="blank">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link"><?php echo lang('titile_ldist'); ?></a></li>
	  <li class=""><a data-toggle="tab" href="#home2" id="padding_tab_link"><?php echo lang('titile_ldist2'); ?></a></li>
	</ul>

	<div class="tab-content grid-form1" style="border-top:0px;">
		  <div id="home" class="tab-pane fade in active ">
		  	<div class="padding_tab_body">
		  		<div id="distribute_table<?php echo $timestamp; ?>"></div>
			</div>
		  </div>
		  <div id="home2" class="tab-pane fade in ">
		  	<div class="padding_tab_body">
		  		<div id="distribute_table2<?php echo $timestamp; ?>"></div>
			</div>
		  </div>
	</div>
</div>


<script id="distribute_table-row-<?php echo $timestamp; ?>" type="text/x-kendo-tmpl">
	<tr id="#: company_code #-#: acc_doc_no #-#: th #">
		<td style=" padding: 5px; text-align:center;">
			<input type="checkbox" class='unixID' name="unixID[]" value="#: company_code #-#: acc_doc_no #-#: th #">
		</td>
		<td>
			<a href="javascript:void(0)" onclick="$(this).getitem();" data-id="#: company_code #-#: acc_doc_no #-#: th #">
				<strong>#: company_code #</strong>
			</a>
		</td>
		<td>#: gl_account #</td>
		<td>#: kendo.toString(kendo.parseDate(posting_date, 'yyyy-MM-dd'), 'dd-MM-yyyy') #</td>
		<td>#: reference #</td>
		<td>#: curency #</td>
		<td>#: kendo.toString(amount, "n0") #</td>
	</tr>
</script>

<script id="distribute_table2-row-<?php echo $timestamp; ?>" type="text/x-kendo-tmpl">
	<tr id="#: company_code #-#: acc_doc_no #-#: th #">
		<td>
			<a href="javascript:void(0)" onclick="$(this).getitem();" data-id="#: company_code #-#: acc_doc_no #-#: th #">
				<strong>#: company_code #</strong>
			</a>
		</td>
		<td>#: gl_account #</td>
		<td>#: kendo.toString(kendo.parseDate(posting_date, 'yyyy-MM-dd'), 'dd-MM-yyyy') #</td>
		<td>#: reference #</td>
		<td>#: curency #</td>
		<td>#: kendo.toString(amount, "n0") #</td>
	</tr>
</script>

<script type="text/javascript">
	var record=0;
	$(document).ready(function () {
		var ds_distribute_table_first = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('distribute/table_get'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				 model: {
					 fields: {
						 	amount: { type: "number"},
						}
				 }
			},
			pageSize: 100,
		});

		$('#distribute_table<?php echo $timestamp; ?>').kendoGrid({
			dataSource: ds_distribute_table_first,
			filterable: true,
			sortable: true,
			pageable: true,
			scrollable: false,
			rowTemplate: kendo.template($("#distribute_table-row-<?php echo $timestamp; ?>").html()),
			filterable: {
				extra: false,
				operators: {
					string: {
						startswith: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{title:"<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",width:20},
				{field:"company_code",title:"<?php echo lang('dist_cmp_code'); ?>"},
				{field:"gl_account",title:"<?php echo lang('dist_gl_acc'); ?>" },
				{field:"posting_date",title:"<?php echo lang('dist_post_date'); ?>"},
				{field:"reference",title:"<?php echo lang('dist_remark'); ?>"},
				{field:"curency",title:"<?php echo lang('dist_curency'); ?>" },
				{field:"amount",title:"<?php echo lang('dist_amount'); ?>" },
			]
		});

		var ds_distribute_table = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('distribute/table_get2'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				 model: {
					 fields: {
						 	amount: { type: "number"},
						}
				 }
			},
			pageSize: 100,
		});


		$('#distribute_table2<?php echo $timestamp; ?>').kendoGrid({
			dataSource: ds_distribute_table,
			filterable: true,
			sortable: true,
			pageable: true,
			scrollable: false,
			rowTemplate: kendo.template($("#distribute_table2-row-<?php echo $timestamp; ?>").html()),
			filterable: {
				extra: false,
				operators: {
					string: {
						startswith: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				//{title:"<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",width:20},
				{field:"company_code",title:"<?php echo lang('dist_cmp_code'); ?>"},
				{field:"gl_account",title:"<?php echo lang('dist_gl_acc'); ?>" },
				{field:"posting_date",title:"<?php echo lang('dist_post_date'); ?>"},
				{field:"reference",title:"<?php echo lang('dist_remark'); ?>"},
				{field:"curency",title:"<?php echo lang('dist_curency'); ?>" },
				{field:"amount",title:"<?php echo lang('dist_amount'); ?>" },
			]
		});

		$('#reload<?php echo $timestamp; ?>').click(function(){
			ds_distribute_table.read();
			ds_distribute_table_first.read();
		});

		$.fn.getitem=function(){
			var reqID = $(this).data('id');
			var tr_parent = $(this).parent('td').parent('tr');
			var elmid = tr_parent.data('uid');
			if(tr_parent.hasClass('open')){
				tr_parent.removeClass('open');
				$('tr.sub_'+elmid).remove();
			}else{
				tr_parent.addClass('open');
				$.ajax({
					url: '<?php echo site_url('distribute/table_get_detail'); ?>',
					data: 'id='+reqID,
					type:'POST',
					dataType: 'html',
					beforeSend: function(){
						$(tr_parent).after('<tr class="sub_'+elmid+'"><td colspan="7" class="loader"></td><td style="display:none">&mdash;|</td></tr>');
					},
					success:function(result){
						$('tr.sub_'+elmid).addClass('alert-success');
						$('tr.sub_'+elmid+'>td:eq(0)').html(result);
					}
				}).done(function(){
					$('tr.sub_'+elmid+'>:eq(0)').removeClass('loader');
				});
			}
		}


		$('#del<?php echo $timestamp; ?>').click(function(){
		var txt;
		var checkedVals = $('.unixID:checkbox:checked').map(function() {
			return this.value;
		}).get();


		if(checkedVals !=''){
			//msg_box("<?php //echo lang('nfc_del_confirm'); ?> ("+checkedVals+")...?",[{'btnYES':function(){
			msg_box("<?php echo lang('nfc_del_confirm'); ?> ...?",[{'btnYES':function(){
				var customerId= checkedVals.join(",");
				$(this).trigger('closeWindow');
				$.ajax({
					url:'<?php echo site_url('distribute/delete_record'); ?>',
					data:'id='+customerId,
					type:'POST',
					dataType:'json',tail:1,
					beforeSend:function(){
						$('#loading-mask').addClass('loader-mask');
					},
					success:function(res){
						if(res.status){
							ds_distribute_table.read();
							ds_distribute_table_first.read();
						}
						msg_box(res.messages,['btnOK'],'Info!');
					}
				}).done(function(){
					$('#loading-mask').removeClass('loader-mask');
				});

			}},'btnNO'],'<?php echo lang('titile_confirm_dialog'); ?>');

		}else
			msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
	});

	});

function toggle(source) {
  checkboxes = document.getElementsByClassName('unixID');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>
