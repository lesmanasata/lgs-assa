<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Distribute extends MY_Controller
{
    public $tbl_hider = 'TF_BKPF';
    public $tbl_detail = 'TF_BSEG';
    public $fild_comp_code = 'CHR_BUKRS';
    public $fild_bisnis_area = 'VCH_GSBER';
    public $fild_cost_center = 'VCH_KOSTL';
    public $fild_gl_account = 'VCH_HKONT';
    public $fild_curency = 'CHR_CURENCY';
    public $fild_amount = 'INT_WRBTR';
    public $fild_posting_date = 'DAT_BUDAT';
    public $fild_doc_date = 'DAT_BLDAT';
    public $fild_reverence = 'CHR_XBLNR';
    public $fild_doc_type = 'ENM_BLART';
    public $fild_acounting_no = 'VCH_BELNR'; 
    public $fild_fiscal_year = 'DYR_GJAHR';
    public $fild_text = 'TEX_SGTXT';
    public $fild_internal_order = 'VCH_AUFNR';
    public $fild_line_item = 'VCH_BUZEI';
    public $fild_posting_key = 'VCH_BSCHL';
    public $fild_indicator_saldo = 'ENM_SHKZG';
    public $fild_flag = 'CHR_FLAG';
    public $df_val_key_debit = "50"; //for credit
    public $df_val_key_credit = "40"; //for debit
    public $re_val_key_debit = "31";
    public $re_val_key_credit = "01";
    public $initial_key = 'COS';

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    /*
     * PT. Lamjaya Global Solusi
     * this is modeule for  input distribution cost
     * By. Sata Lesmana - lesmana@lamsolusi.com
     */

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => 'bisnis_area'),
            array('name' => 'bisnis_area', 'url' => 'master/cmb_bisnis_area', 'flow' => '')
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);
        $this->template->title('Request Creation');
        $this->template->build('distribute/distribute_form', $data);
    }
    public function diestribute_submit()
    {
        $this->load->model('master/master_model', 'mod_mstr');
        $out['status'] = false;
        $item_detail = json_decode(json_encode(($this->input->post('models'))));
        $credit_foother = array();
        $cos_center_split = explode("-", $this->input->post('cost_center'));
        $gl_acc = explode("-", $this->input->post('gl_acc'));
        $row_addx = array();
        $row_addy = array();
        $row_addz = array();
        $account_number = $this->app->getAutoId($this->fild_acounting_no, $this->tbl_hider, $this->initial_key);
        $index = 1;
        //-----------VALIDATION ----------------------//
        do {
            if ($this->input->post('company_code') == '-1' || $this->input->post('company_code') == '') {
                $out['messages'] = lang('msg_null_company_code');
                break;
            }

            if ($this->input->post('bisnis_area') == '-1' || $this->input->post('bisnis_area') == '') {
                $out['messages'] = lang('msg_null_bisnis_area');
                break;
            }

            if (count($cos_center_split) <= 1) {
                $out['messages'] = lang('msg_null_cost_center');
                break;
            }
            
            if (count($gl_acc) <= 1) {
                $out['messages'] = lang('msg_null_gl_account');
                break;
            }

            if ($this->input->post('amount') == '' || $this->input->post('amount') <= 0) {
                $out['messages'] = lang('msg_null_amount').' and Amount > 0';
                break;
            }

            $posting_date   = $this->input->post('psting_date');
            $documentDate   = $this->input->post('doc_date');

            $debit_hider = array(
                $this->fild_comp_code => $this->input->post('company_code'),
                $this->fild_acounting_no => $account_number,
                $this->fild_fiscal_year => date('Y'),
                $this->fild_doc_type => 'IB',
                $this->fild_doc_date => date('Y-m-d', strtotime($posting_date)),
                $this->fild_posting_date => date('Y-m-d', strtotime($documentDate)),
                $this->fild_reverence => $this->input->post('remark'),
                $this->fild_curency => $this->input->post('curency'),
                $this->fild_flag => 0,
            );
            $debit_foother = array(
                $this->fild_comp_code => $this->input->post('company_code'),
                $this->fild_acounting_no => $account_number,
                $this->fild_fiscal_year => date('Y'),
                $this->fild_line_item => $index,
                $this->fild_cost_center => $cos_center_split[0],
                $this->fild_posting_key => $this->df_val_key_debit,
                $this->fild_indicator_saldo => 'H', //Credit
                $this->fild_amount => $this->input->post('amount'),
                $this->fild_gl_account => $gl_acc[0],
                $this->fild_bisnis_area => $this->input->post('bisnis_area'),
                $this->fild_text => $this->input->post('remark'),
                $this->fild_flag => 0,
            );
            $this->mod_mstr->add_hider($debit_hider);
            $this->mod_mstr->add_detail(array($debit_foother));

            //---------------*for input credit-------------------//

            $total_amount_item              = 0;
            $validate_cost_center           = false;
            $validate_empty_cost_center     = false;
            $validate_empty_internal_order  = false;
            $fiscal_yer = date('Y');

            foreach ($item_detail as $k => $row) {
                $index++;
                $total_amount_item += isset($row->item_amount) ? $row->item_amount : 0;
                $cost_center        = isset($row->cost_center->cost_center) ? $row->cost_center->cost_center : '';
                $internal_order     = isset($row->internal_order->internal_order) ? $row->internal_order->internal_order : '';
                $bisnis_area        = isset($row->bisnis_area->bisnis_area) ? $row->bisnis_area->bisnis_area : '';
                $cek_gl             = substr($gl_acc[0], 0, 1);

                if (trim($cost_center) =='' && $cek_gl=='5' || trim($cost_center) =='' && $cek_gl=='6') {
                    $validate_empty_cost_center = true;
                }

                if (trim($internal_order =='') && $cek_gl=='5') {
                    $validate_empty_internal_order = true;
                }

                if ($cost_center == $cos_center_split[0]) {
                    $validate_cost_center = true;
                }

                $row_addx[$this->fild_comp_code]        = $this->input->post('company_code');
                $row_addx[$this->fild_acounting_no]     = $account_number;
                $row_addx[$this->fild_fiscal_year]      = $fiscal_yer;
                $row_addx[$this->fild_line_item]        = $index;

                $cost_item_x = array();
                if(isset($row->cost_center->cost_center))
                    $cost_item_x= explode('-', $row->cost_center->cost_center);

                $row_addx[$this->fild_cost_center]      =  (count($cost_item_x) > 0)? $cost_item_x[0] : '';
                $row_addx[$this->fild_posting_key]      = $this->df_val_key_credit;
                $row_addx[$this->fild_indicator_saldo]  = 'S'; //Debit
                $row_addx[$this->fild_amount]           = isset($row->item_amount) ? $row->item_amount : 0;
                $row_addx[$this->fild_gl_account]       = $gl_acc[0];
                $row_addx[$this->fild_bisnis_area]      = $bisnis_area;
                $row_addx[$this->fild_text]             = isset($row->description) ? $row->description : '';
                
                $internal_order_x = array();
                if(isset($row->internal_order->internal_order))
                    $internal_order_x= explode('-', $row->internal_order->internal_order);

                $row_addx[$this->fild_internal_order]   = (count($internal_order_x) > 0)? $internal_order_x[0] : ''; 
                $row_addx[$this->fild_flag]             = 0;

                array_push($credit_foother, $row_addx);
            }

            $filter_delete[$this->fild_comp_code]       = $this->input->post('company_code');
            $filter_delete[$this->fild_acounting_no]    = $account_number;
            $filter_delete[$this->fild_fiscal_year]     = $fiscal_yer;
            if ($validate_cost_center) {
                $this->mod_mstr->delete_hider($filter_delete);
                $this->mod_mstr->delete_detail($filter_delete);
                $out['messages'] = lang('msg_validate_cost_center');
                break;
            }

            if ($validate_empty_cost_center) {
                $this->mod_mstr->delete_hider($filter_delete);
                $this->mod_mstr->delete_detail($filter_delete);
                $out['messages'] = lang('msg_validate_empty_cost_center');
                break;
            }

            if ($validate_empty_internal_order) {
                $this->mod_mstr->delete_hider($filter_delete);
                $this->mod_mstr->delete_detail($filter_delete);
                $out['messages'] = lang('msg_validate_empty_internal_order');
                break;
            }


            if ($total_amount_item != $this->input->post('amount')) {
                $this->mod_mstr->delete_hider($filter_delete);
                $this->mod_mstr->delete_detail($filter_delete);
                $out['messages'] = lang('msg_ammount_difrn');
                break;
            }

            if (count($credit_foother) > 0) {
                $this->mod_mstr->add_detail($credit_foother);
            }


            //---------------BALANCING value of record--------------------
            foreach ($item_detail as $k => $row) {
                $dc = array('H', 'S');
                $bisnis_area2 = isset($row->bisnis_area) ? $row->bisnis_area->bisnis_area : '';
               // echo $this->input->post('bisnis_area').$bisnis_area2;

                if(trim($this->input->post('bisnis_area')) != trim($bisnis_area2)){
                    foreach ($dc as $rdc) {
                        $index++;
                        //$balance_id = $this->app->getAutoId($this->fild_acounting_no, $this->tbl_hider, $this->input->post('company_code'));
                        $key = ($rdc == 'H') ? $this->re_val_key_debit : $this->re_val_key_credit;
                        $bis_area = ($rdc == 'H') ? $bisnis_area2 : $this->input->post('bisnis_area');

                        $filter_vecus['CHR_COMPANY_CODE'] = $this->input->post('company_code');
                        if ($rdc == 'H') {
                            $filds_vecus = 'CHR_VCODE AS account';
                            $filter_vecus['CHR_BA'] = $this->input->post('bisnis_area');
                        } else {
                            $filds_vecus = 'CHR_CUST_CODE AS account';
                            $filter_vecus['CHR_BA'] = $bisnis_area2;
                        }
                        $arAccount = $this->mod_mstr->get_vecus($filds_vecus, $filter_vecus);
                        unset($filter_vecus['CHR_BA']);
                        unset($filter_vecus['CHR_COMPANY_CODE']);

                        $balance_amount = isset($row->item_amount) ? $row->item_amount : 0;
                        $balance_text = isset($row->description) ? $row->description : '';

                        $internal_order_xx = array();
                        if(isset($row->internal_order->internal_order))
                            $internal_order_xx= explode('-', $row->cost_center->cost_center);

                        $balance_io = (count($internal_order_x) > 0)? $internal_order_x[0] : '';  //isset($row->internal_order->internal_order) ? $row->internal_order->internal_order : '';

                        $balancing_foother = array(
                            $this->fild_comp_code => $this->input->post('company_code'),
                            $this->fild_acounting_no => $account_number,
                            $this->fild_fiscal_year => date('Y'),
                            $this->fild_line_item => $index,
                            $this->fild_posting_key => $key,
                            $this->fild_indicator_saldo => $rdc, //Credit
                            $this->fild_amount => $balance_amount,
                            $this->fild_gl_account => isset($arAccount[0]->account) ? $arAccount[0]->account : '',
                            $this->fild_bisnis_area => $bis_area,
                            $this->fild_text => $balance_text,
                            $this->fild_internal_order => $balance_io,
                            $this->fild_flag => 0,
                        );
                        $this->mod_mstr->add_detail(array($balancing_foother));
                    }
                }
            }


            $out['status'] = true;
            $out['messages'] = lang('msg_dist_ok').", Number : <b>".$account_number."</b>";
        } while (false);
        echo json_encode($out);
    }

    public function diestribute_tbl()
    {
        $row = array();
        echo json_encode(array('data' => $row));
    }

    /*
     * PT. Lamjaya Global Solusi
     * this is modeule for list of distribute table
     * By. Sata Lesmana - lesmana@lamsolusi.com
     */

    public function table()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));

        $this->template->build('distribute/distribute_table');
    }

    public function table_get()
    {
        $this->load->model('master/master_model', 'mod_dist');

        $filds = "h.CHR_BUKRS as company_code,";
        $filds .= "d.VCH_HKONT as gl_account,";
        $filds .= "h.DAT_BUDAT as posting_date,";
        $filds .= "h.CHR_CURENCY as curency,";
        $filds .= " sum(d.INT_WRBTR)  as amount,";
        $filds .= " h.CHR_XBLNR as reference,";
        $filds .= " h.DYR_GJAHR as th,";
        $filds .= " h.VCH_BELNR as acc_doc_no";

        $group = 'h.VCH_BELNR';

        $filter['h.VCH_BELNR like"%COS%"'] = NULL;
        //$filter['h.ENM_BLART'] = 'IB';
        //$filter['d.ENM_SHKZG'] = 'S';
        $filter['d.CHR_FLAG'] = '0';
        $filter['d.VCH_KOSTL IS NOT NULL'] = null;

        $row = $this->mod_dist->get_join($filds, $filter, $group);
        echo json_encode(array('data' => $row));
    }

    public function table_get2()
    {
        $this->load->model('master/master_model', 'mod_dist');

        $filds = "h.CHR_BUKRS as company_code,";
        $filds .= "d.VCH_HKONT as gl_account,";
        $filds .= "h.DAT_BUDAT as posting_date,";
        $filds .= "h.CHR_CURENCY as curency,";
        $filds .= " sum(d.INT_WRBTR)  as amount,";
        $filds .= " h.CHR_XBLNR as reference,";
        $filds .= " h.DYR_GJAHR as th,";
        $filds .= " h.VCH_BELNR as acc_doc_no";

        $group = 'h.VCH_BELNR';

        $filter['h.VCH_BELNR like"%COS%"'] = NULL;
        //$filter['h.ENM_BLART'] = 'IB';
        //$filter['d.ENM_SHKZG'] = 'S';
        $filter['d.CHR_FLAG'] = '1';
        $filter['d.VCH_KOSTL IS NOT NULL'] = null;

        $row = $this->mod_dist->get_join($filds, $filter, $group);
        echo json_encode(array('data' => $row));
    }

    public function table_get_detail()
    {
        $unixId = $this->input->post('id');
        $unixId_split = explode("-", $unixId);
        $res['res'] = array(
            'company_code' => $unixId_split[0],
            'acc_doc_no' => $unixId_split[1],
            'fiscal_year' => $unixId_split[2],
        );
        $this->load->view('distribute/distribute_table_detail', $res);
    }

    public function table_get_detail_data()
    {
        $company_code = $this->input->post('company_code');
        $acc_doc_no = $this->input->post('acc_doc_no');
        $fiscal_yer = $this->input->post('fiscal_year');
        $this->load->model('master/master_model', 'mod_mstr');
        $out['data'] = array();

        $filds = $this->fild_bisnis_area . ' as bisnis_area,';
        $filds .= $this->fild_cost_center . ' as cost_center,';
        $filds .= $this->fild_internal_order . ' as internal_order,';
        $filds .= $this->fild_amount . ' as amount_item,';
        $filds .= $this->fild_text . ' as text_item';


        $filter[$this->fild_comp_code] = $company_code;
        $filter[$this->fild_acounting_no] = $acc_doc_no;
        $filter[$this->fild_fiscal_year] = $fiscal_yer;
        $filter[$this->fild_line_item . ' != "1" '] = null;
        $filter[$this->fild_cost_center . ' IS NOT NULL '] = null;

        $res = $this->mod_mstr->get_detail($filds, $filter);

        if (count($res) > 0) {
            $out['data'] = $res;
        }

        echo json_encode($out);
    }

    public function delete_record()
    {
        $this->load->model('master/master_model', 'mod_dist');
        $out['status'] = false;
        $out['messages'] = lang('msg_failed_del_record');
        $unixId = explode(",", $this->input->post('id'));
         
        do {
            foreach ($unixId as $key => $value) {
                $filter['CONCAT('.$this->fild_comp_code.',"-",'.$this->fild_acounting_no.',"-",'.$this->fild_fiscal_year.')'] = $value;
                $filter[$this->fild_flag] = 0;

                $res = $this->mod_dist->del_hider($filter);
                $res = $this->mod_dist->del_detail($filter);
                unset($filter);
            }
            if ($res) {
                $out['messages'] = lang('msg_success_del_record');
            }
        } while (false);
        $out['status'] = $res;
        echo json_encode($out);
    }


    public function internal_order()
    {
        $this->db_main = $this->load->database('default', true);
        $company_code   = $_GET['company_code'];
        $bisarea        = $_GET['bisarea'];
        
        $calback= $_GET['callback'];
        $text ='';
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];

            if (isset($_GET['filter']['filters'][0]['value'])) {
                $text = $filter['filters'][0]['value'];
            }
        }
        $res=array();
        $this->db_main->select('CONCAT(VCH_OrderID,"-",VCH_OrderName) as id, CONCAT(VCH_OrderID,"-",VCH_OrderName) as text', false)
            ->from('MF_INTERNAL_ORDER');

        $this->db_main->where('VCH_CompanyCode',$company_code);
        $this->db_main->where('VCH_bis_area',$bisarea);
        if ($text !='') {
            $this->db_main->like('CONCAT(VCH_OrderID,VCH_OrderName)', $text,false);
        }

        $this->db_main->order_by('VCH_OrderID', 'ASC');
        $this->db_main->limit(50);
        $resx = $this->db_main->get();
        $res = $resx->result();
        echo $calback."(".json_encode($res).")";
    }


}
