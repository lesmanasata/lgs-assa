<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jv extends MY_Controller
{
    public $tbl_header 			= 'TF_JVHEADER';
    public $tbl_detail 			= 'TF_JVITEM';
    public $tbl_bisnis_area 	= "MF_AREA";
    public $tbl_gl_alter        = "MF_ALTERNATIF_GL";
    public $cfg_Clearing        = array();
    public $field_jvid          = 'CHR_jv_id';
    public $field_jvth          = 'CHR_jv_th';
    public $field_comp          = 'CHR_company_code';
    public $field_ven           = 'CHR_vendor_id';
    public $field_name          = 'CHR_vendor_name';
    public $field_add           = 'CHR_vendor_addres';
    public $field_phone         = 'CHR_vendor_phone';
    public $field_npwp          = 'CHR_vendor_npwp';
    public $field_inv           = 'CHR_invoice_number';
    public $field_faktur_date   = 'DAT_faktur_date';
    public $field_fak           = 'CHR_faktur_pajak';
    public $field_pos           = 'DAT_date_posting';
    public $field_amount        = 'INT_amount';
    public $field_tax           = 'INT_tax';
    public $field_desc          = 'VCH_description';
    public $field_pay           = 'INT_payment_term';
    public $field_date_base     = 'DAT_date_base_line';
    public $field_due           = 'DAT_date_due_on';
    public $field_inv_date      = 'DAT_invoice_date';
    public $field_wht_type      = 'CHR_wht_type';
    public $field_wht_code      = 'CHR_wht_code';
    public $field_wht_amount    = 'INT_wht_amount';
    public $field_wht_base      = 'INT_wht_base_amount';
    public $field_glvendor      = 'CHR_gl_account';
    public $field_bis           = 'CHR_bisnis_area';
    public $field_country       = 'CHR_country';
    public $field_ins           = 'lastInsert';
    public $field_cost 			= 'CHR_cost_center';
    public $field_kota			= 'CHR_vendor_kota';
    public $field_amountDe 		= 'INT_amount';
    public $field_ppnty 		= 'CHR_ppn_type';
    public $field_ppnAmo 		= 'INT_ppn_amount';
    public $field_pphAmo 		= 'INT_pph_amount';
    public $field_text 			= 'VCH_text_desc';
    public $field_textAs 		= 'VCH_text_asign';
    public $field_internal 		= 'VCH_internal_order';
    public $parking 			= 'parking';
    public $vendor_gl 			= 'VCH_vendor_gl';
    public $field_index 		= 'INT_no_item';
    public $field_payment_code 	= 'CHR_payment_code';
    public $field_gl 			= 'CHR_gl_account';
    public $field_post 			= 'CHR_PostCode';

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->cfg_Clearing = json_decode(json_encode($this->config->item('jv')));
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $hari = date('d-m-Y');
        

        if ($this->uri->segment(3) !='') {
            $this->load->helper('enkripsi');
            $this->load->model('jv/jv_model', 'jv_mod');

            $this->load->library('encrypt');
            $enc_id     = $this->url_back($this->uri->segment(3));
            $jv_id 		= $this->encrypt->decrypt($enc_id);
            $filter['CONCAT(CHR_jv_id,CHR_jv_th)'] = trim($jv_id);
            $res_jv = $this->jv_mod->get_jv_header('*', $filter);

            $faktur_date    = date('d-m-Y', strtotime($res_jv[0]->DAT_faktur_date));
            $invoice_date   = date('d-m-Y', strtotime($res_jv[0]->DAT_invoice_date));
            $posting_date   = date('d-m-Y', strtotime($res_jv[0]->DAT_date_posting));
            $baseline_date  = date('d-m-Y', strtotime($res_jv[0]->DAT_date_base_line));
            $dueon_date     = date('d-m-Y', strtotime($res_jv[0]->DAT_date_due_on));
            $amount         = number_format($res_jv[0]->INT_amount);
            $ppn            = number_format($res_jv[0]->INT_tax);
            $wht_base       = number_format($res_jv[0]->INT_wht_base_amount);
            $amount_amo     = number_format($res_jv[0]->INT_wht_amount);

            $cp_code 				= (isset($res_jv[0]->CHR_company_code))?$res_jv[0]->CHR_company_code:'';
            $data['item']			= array('jvid'=>trim($jv_id),'cpCode'=>$cp_code);
            $data['tag']			= $jv_id;
            $data['vendor_name'] 	= (isset($res_jv[0]->CHR_vendor_name))?$res_jv[0]->CHR_vendor_name:'';
            $data['date_posting'] 	= (isset($posting_date))?$posting_date:'';
            $data['vendor_detail']	= (isset($res_jv[0]->CHR_gl_account))?$res_jv[0]->CHR_gl_account:'';
            $data['vendor_gl'] 		= ($res_jv[0]->VCH_vendor_gl !='')?$res_jv[0]->VCH_vendor_gl:'-1';
            
            $data['vendor_id']      = $res_jv[0]->CHR_vendor_id;
            if($res_jv[0]->VCH_vendor_gl !='')
                $data['vendor_id'] 	= $res_jv[0]->CHR_vendor_id.'|'.$res_jv[0]->VCH_vendor_gl;
            
            $data['date_inv']		= (isset($invoice_date))?$invoice_date:'';
            $data['invoice_number'] = (isset($res_jv[0]->CHR_invoice_number))?$res_jv[0]->CHR_invoice_number:'';
            $data['company_code'] 	= (isset($res_jv[0]->CHR_company_code))?$res_jv[0]->CHR_company_code:'';
            $data['vendor_addres'] 	= (isset($res_jv[0]->CHR_vendor_addres))?$res_jv[0]->CHR_vendor_addres:'';
            $data['vendor_phone'] 	= (isset($res_jv[0]->CHR_vendor_phone))?$res_jv[0]->CHR_vendor_phone:'';
            $data['vendor_npwp'] 	= (isset($res_jv[0]->CHR_vendor_npwp))?$res_jv[0]->CHR_vendor_npwp:'';
            $data['faktur_pajak'] 	= (isset($res_jv[0]->CHR_faktur_pajak))?$res_jv[0]->CHR_faktur_pajak:'';
            $data['faktur_date']	= (isset($faktur_date))?$faktur_date:'';
            $data['amount'] 		= (isset($amount))?$amount:'';
            $data['tax'] 			= (isset($ppn))?$ppn:'';
            $data['description'] 	= (isset($res_jv[0]->VCH_description))?$res_jv[0]->VCH_description:'';
            $data['payment_code'] 	= (isset($res_jv[0]->INT_payment_term))?$res_jv[0]->INT_payment_term:'';
            $data['days'] 			= (isset($res_jv[0]->INT_payment_term))?$res_jv[0]->INT_payment_term:'';
            $data['date_base_line'] = (isset($baseline_date))?$baseline_date:'';
            $data['vendor_kota']	= (isset($res_jv[0]->CHR_vendor_kota))?$res_jv[0]->CHR_vendor_kota:'';
            $data['wht_type']		= ($res_jv[0]->CHR_wht_type !='')?$res_jv[0]->CHR_wht_type:'-1';
            $data['wht_code']		= ($res_jv[0]->CHR_wht_code !='')?$res_jv[0]->CHR_wht_code:'-1';
            $data['wht_base_amount']= (isset($wht_base))?$wht_base:'';
            $data['wht_amount']		= (isset($amount_amo))?$amount_amo:'';
            $data['due_on']			= (isset($dueon_date))?$dueon_date:'';
            $data['bisnis_area']	= ($res_jv[0]->CHR_bisnis_area !='')?$res_jv[0]->CHR_bisnis_area:'-1';
            $data['vendor_kodepost']= (isset($res_jv[0]->CHR_PostCode))?$res_jv[0]->CHR_PostCode:'';
        } else {
            $data['vendor_kodepost']= '';
            $data['vendor_detail'] 	= '-1';
            $data['bisnis_area']	= '-1';
            $data['payment_code'] 	= '-1';
            $data['item']			= array();
            $data['wht_type']		= '-1';
            $data['wht_code']		= '-1';
            $data['wht_base_amount']= '';
            $data['wht_amount']		= '';
            $data['due_on']			= '';
            $data['faktur_date']	= $hari;
            $data['vendor_name'] 	= '';
            $data['date_posting'] 	= $hari;
            $data['vendor_gl'] 		= '-1';
            $data['vendor_id'] 		= '-1';
            $data['date_inv']		= $hari;
            $data['invoice_number'] = '';
            $data['company_code'] 	= '-1';
            $data['vendor_addres'] 	= '';
            $data['vendor_phone'] 	= '';
            $data['vendor_npwp'] 	= '';
            $data['faktur_pajak'] 	= '';
            $data['amount'] 		= '';
            $data['tax'] 			= '';
            $data['description'] 	= '';
            $data['days'] 			= '';
            $data['date_base_line']	= $hari;
            $data['vendor_kota']	= '';
            $data['tag']			= '';
        }

        //echo "<pre>",print_r($data),"</pre>";
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => 'bisnis_area'),
            array('name' => 'bisnis_area', 'url' => 'jv/cmb_bisnis_area', 'flow' => ''),
            array('name' => 'vendor', 'url' => 'master/cmb_vendor', 'flow' => 'vendor_detail'),
            array('name' => 'vendor_detail', 'url'=>'jv/cmb_alternate_gl', 'flow' =>''),
            array('name' => 'pay_term', 'url' => 'master/cmb_payment', 'flow' => ''),
            array('name' => 'wht_type', 'url' => 'master/wht', 'flow' => 'wht_code'),
            array('name' => 'wht_code', 'url' => 'master/wht_code', 'flow' => '')
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);
        $this->template->title('Jurnal Voucer');
        
        $this->template->build('jv/jv_view', $data);
    }
	public function cmb_bisnis_area()
    {
        $filter = array();
        $this->load->model("jv/jv_model", "jv_mod");
        $id = $this->input->post('id');
        $fild = "CHR_AREAID as id, CONCAT(CHR_AREAID,' - ',VCH_AREANAME) as text";

        if($id != false)
		  $filter['CHR_COMPANY_CODE'] = $id;

        $res = $this->jv_mod->get_cmb($fild, $this->tbl_bisnis_area, $filter);
        echo json_encode(array('data'=>$res));
    }
    public function jv_list()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $this->template->title('List of Jurnal Voucer');
        $this->template->build('jv/jv_list');
    }

    public function jurnal_submit()
    {
        $this->load->helper('enkripsi');
        $this->load->model('jv/jv_model', 'jv_mod');
        $signature 		= $this->input->post('parking');
        $code 			= $this->input->post('tag');
        if ($code=='') {
            $id 			= $this->app->getAutoId($this->field_jvid, $this->tbl_header, $this->cfg_Clearing->initial);
        } else {
            $id = substr($code, 0, -4);
        }

        $year 			= date('Y');
        $out['status']  = false;
        $item_post		= json_decode(json_encode($this->input->post('models')));
        $data_row 		= array();
        $data_detail 	= array();
        $vendor 		= $this->input->post('vendor');
        $bisniss_area	= $this->input->post('bisnis_area');
        $save_vendor 	= explode("|", $vendor);
        $index 			= 0;
        $company		= $this->input->post('company_code');
        $wht_type		= $this->input->post('wht_type');
        $wht_code		= $this->input->post('wht_code');
        $gl_vendor		= $this->input->post('vendor_detail');
        $payment_term 	= (strlen($this->input->post('pay_term')) >5 )?'-1':$this->input->post('pay_term');
        $park           = ($signature=='parking')?'1':'0';
        $invoicedate    = $this->input->post('date_inv');
        $faktur_date    = $this->input->post('date_faktur');
        $posting_date   = $this->input->post('posting_date');
        $due_on         = $this->input->post('due_on');
        $baseLine       = $this->input->post('base_line');
        $amount         = $this->input->post('amount')+0;
        $tax_amo        = $this->input->post('tax_amount')+0;
        $wht_amo        = $this->input->post('wht_amount')+0;
        $wht_base       = $this->input->post('wht_base')+0;
        $payment_trm_int= $this->input->post('days')+0;
        $vendor_alamat  = $this->input->post('alamat');
        $faktur         = $this->input->post('faktur');
        $no_invoice     = $this->input->post('no_inv');
        $text_desc      = $this->input->post('text_head');

        do {
            if ($company == '-1' and $park=='0') {
                $out['messages'] = lang('msg_null_company_code');
                break;
            }

            if ($vendor == '-1' and $park=='0') {
                $out['messages'] = lang('msg_vendor_null');
                break;
            }

            if( $amount <= 0 and $park=='0'){
                $out['messages'] = 'Amount harus lebih besar dari 0 (NOL)';
                break;
            }

            if( $vendor_alamat =='' and $park=='0'){
                $out['messages'] = 'Alamat Vendor harus diisi';
                break;
            }

            if( $faktur =='' and $park=='0'){
                $out['messages'] = 'Faktur pajak harus diisi';
                break;
            }

            if ($wht_type == '-1') {
                $whtType_val = '';
            } else {
                $whtType_val = $wht_type;
            }

            if ($wht_code == '-1') {
                $whtCode_val = '';
            } else {
                $whtCode_val = $wht_code;
            }

            if ($gl_vendor == '-1' and $park=='0' || $gl_vendor == '' and $park=='0') {
                $out['messages'] = lang('msg_gl_vendor_null');
                break;
            }

            if ($bisniss_area == '-1' and $park=='0' || $bisniss_area == '' and $park=='0') {
                $out['messages'] = lang('msg_null_bisnis_area');
                break;
            }

            if ($payment_term == '-1' and $park=='0') {
                $out['messages'] = lang('msg_payterm_null');
                break;
            }

            if( $no_invoice =='' and $park=='0'){
                $out['messages'] = 'Nomor Invoice Harus diisi';
                break;
            }

            if( $text_desc =='' and $park=='0'){
                $out['messages'] = 'Text Harus diisi';
                break;
            }


            $validate_empty_internal_order  = false;
            foreach ($item_post as $key => $row) {
                $gl 			= $row->gl_account->gl_account;
                $save_gl 		= explode("-", $gl);
                $cost			= $row->cost_center->cost_center;
                $cost_save		= explode("-", $cost);
                $internal 		= $row->internal_order->internal_order;
                $internal_save	= explode("-", $internal);
                $index++;
                $cek_gl         = substr($gl, 0, 1);

                if ($cek_gl == '5' and $internal=='' and $park=='0') {
                    $validate_empty_internal_order = true;
                }
                $amountDet =str_replace(',','',$row->amount);
                $ppn_amount= str_replace(',','',$row->ppn_amount);
                $pph_amount = str_replace(',','',$row->pph_amount);
                $data_row[$this->field_jvid] 		= $id;
                $data_row[$this->field_jvth] 		= $year;
                $data_row[$this->field_gl]			= isset($save_gl[0])?$save_gl[0]:'';
                $data_row[$this->field_cost]		= isset($cost_save[0])?$cost_save[0]:'';
                $data_row[$this->field_amountDe]	= isset($amountDet)?$amountDet:'0';
                $data_row[$this->field_ppnty]		= isset($row->ppn_type->ppn_type)?$row->ppn_type->ppn_type:'';
                $data_row[$this->field_ppnAmo]		= isset($ppn_amount)?$ppn_amount:'';
                $data_row[$this->field_pphAmo]		= isset($pph_amount)?$pph_amount:'';
                $data_row[$this->field_textAs]		= isset($row->text_asign)?$row->text_asign:'';
                $data_row[$this->field_text]		= isset($row->text_desc)?$row->text_desc:'';
                $data_row[$this->field_index]		= $index;
                $data_row[$this->field_internal]	= isset($internal_save[0])?$internal_save[0]:'';

                array_push($data_detail, $data_row);
            }

            if ($validate_empty_internal_order) {
                $out['messages'] = lang('msg_validate_empty_internal_order');
                break;
            }
            

            $tbHeader = array(
                $this->field_jvid 		=> $id,
                $this->field_jvth 		=> $year,
                $this->field_comp 		=> $company,
                $this->field_ven 		=> (isset($save_vendor[0]))?$save_vendor[0]:'',
                $this->vendor_gl		=> (isset($save_vendor[1]))?$save_vendor[1]:'',
                $this->field_name 		=> $this->input->post('nama'),
                $this->field_payment_code => $payment_term,
                $this->field_add 		=> $vendor_alamat,
                $this->field_post		=> $this->input->post('kd_pos'),
                $this->field_phone 		=> $this->input->post('telp'),
                $this->field_kota		=> $this->input->post('kota'),
                $this->field_npwp 		=> $this->input->post('npwp'),
                $this->field_inv 		=> $no_invoice,
                $this->field_inv_date	=> date('Y-m-d', strtotime($invoicedate)),
                $this->field_due		=> date('Y-m-d', strtotime($due_on)),
                $this->field_fak	 	=> $faktur,
                $this->field_faktur_date => date('Y-m-d', strtotime($faktur_date)),
                $this->field_pos 		=> date('Y-m-d', strtotime($posting_date)),
                $this->field_amount 	=> str_replace(',','',$amount),
                $this->field_tax 		=> str_replace(',','',$tax_amo),
                $this->field_desc 		=> $this->input->post('text_head'),
                $this->field_pay 		=> $payment_trm_int,
                $this->field_date_base 	=> date('Y-m-d', strtotime($baseLine)),
                $this->field_wht_type 	=> $whtType_val,
                $this->field_wht_code 	=> $whtCode_val,
                $this->field_wht_amount => str_replace(',','',$wht_amo),
                $this->field_wht_base	=> str_replace(',','',$wht_base),
                $this->field_glvendor	=> $gl_vendor,
                $this->field_bis 		=> $bisniss_area,
                $this->field_country 	=> $this->input->post('country'),
                $this->field_ins 		=> date('Y-m-d H:i:s'),
                $this->parking          => $park
            );
            $this->jv_mod->addHeader($tbHeader, $code);

            if (count($data_detail)>0) {
                $this->jv_mod->add_detail($data_detail, $code);
            }

            $out['status']=true;
            $out['messages'] = lang('msg_jv_ok').", Number : <b>".$id."</b>";
        } while (false);

        echo json_encode($out);
    }

    public function tabel_load()
    {
        $row 	= array();
        $jv_id 	= $this->input->post('jvid');

        if ($jv_id !='' && $jv_id != false) {
            $this->load->model('jv/jv_model', 'jv_mod');
            $filter['CONCAT(CHR_jv_id,CHR_jv_th)'] = trim($jv_id);

            $filds = "CONCAT(d.CHR_gl_account,'-',g.VCH_DESCRIPTION) as gl_account,";
            $filds .= "CONCAT(d.CHR_cost_center,'-',c.VCH_DESCRIPTION) AS costCenter,";
            $filds .= "d.VCH_internal_order as internalOreder,";
            $filds .= "d.INT_amount as amount,";
            $filds .= "d.INT_ppn_amount as ppnAmmount,";
            $filds .= "d.INT_pph_amount as pphAmmount,";
            $filds .= "d.VCH_text_desc as textDesc,";
            $filds .= "d.VCH_text_asign as textAssign,";
            $filds .= "d.CHR_ppn_type as ppnType";
            $res_x = $this->jv_mod->get_jv_item_grid($filds, $filter);
            //print_r($res_x);
            foreach ($res_x as $res) {
                $res_y['gl_account']['gl_account'] 			= isset($res->gl_account)?$res->gl_account:'';
                $res_y['cost_center']['cost_center'] 		= isset($res->costCenter)?$res->costCenter:'';
                $res_y['internal_order']['internal_order'] 	= isset($res->internalOreder)?$res->internalOreder:'';
                $res_y['amount'] 							= $res->amount;
                $res_y['ppn_amount'] 						= $res->ppnAmmount;
                //$res_y['pph'] 								= $res->VCH_internal_order;
                $res_y['pph_amount'] 						= $res->pphAmmount;
                $res_y['text_desc'] 						= $res->textDesc;
                $res_y['text_asign'] 						= $res->textAssign;
                $res_y['ppn_type']['ppn_type'] 				= isset($res->ppnType)? $res->ppnType:'';
                array_push($row, $res_y);
            }
        }


        echo json_encode(array('data' => $row));
    }
    public function GetJvParking()
    {
        $this->load->model('jv/jv_model', 'j_mod');
        $this->load->library('encrypt');
        $out['data'] = array();

        $res = $this->j_mod->JvParking();
        foreach ($res as $key => $value) {
            $enc_id = $this->encrypt->encrypt($res[$key]->CHR_jv_id.$res[$key]->CHR_jv_th);
            $res[$key]->encriptid= $this->url_save($enc_id);
        }
        if (count($res)>0) {
            $out['data'] = $res;
        }
        echo json_encode($out);
    }
    public function GetJvcomplate()
    {
        $out['data'] = array();
        $this->load->model('jv/jv_model', 'jv_mod');

        $filter[$this->parking]='0';
        $res = $this->jv_mod->get_jv_header('*', $filter);
        if (count($res)>0) {
            $out['data'] = $res;
        }

        echo json_encode($out);
    }
    public function table_get_detail()
    {
        $unixId = $this->input->post('id');
        $res['res'] = array(
            'document_number' => $unixId,
        );
        $this->load->view('jv/table_detail', $res);
    }
    public function getdetail()
    {
        $id = $this->input->post('document_number');
        $out['data'] = array();
        $this->load->model('jv/jv_model', 'jv_mod');
        $filter[$this->field_jvid] = $id;

        $res = $this->jv_mod->get_jv_item('*', $filter);

        if (count($res)>0) {
            $out['data'] = $res;
        }

        echo json_encode($out);
    }
    public function getSegment()
    {
        $this->load->model('jv/jv_model', 'j_mod');
        $where = $this->uri->segment(3);

        $this->db_main->select('*')
                ->from($this->tbl_header)
                ->where($where);
        $res = $this->db_main->get();
        return $res->result();
    }

    function url_save($value){
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        return str_replace($dirty, $clean, $value);
    }

    function url_back($value){
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        return str_replace($clean, $dirty, $value);
    }

    public function cmb_internal_order()
    {
        $this->db_main = $this->load->database('default', true);

        $calback= $_GET['callback'];
        $text ='';
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];

            if (isset($_GET['filter']['filters'][0]['value'])) {
                $text = $filter['filters'][0]['value'];
            }
        }
        $res=array();
        $this->db_main->select('CONCAT(VCH_OrderID,"-",VCH_OrderName) as id, CONCAT(VCH_OrderID,"-",VCH_OrderName) as text', false)
            ->from('MF_INTERNAL_ORDER');
        if ($text !='') {
            $this->db_main->like('VCH_OrderID', $text);
            $this->db_main->or_like('VCH_OrderName', $text);
        }
        $this->db_main->order_by('VCH_OrderID', 'ASC');
        $this->db_main->limit(50);
        $resx = $this->db_main->get();
        $res = $resx->result();
        echo $calback."(".json_encode($res).")";
    }

     public function cmb_alternate_gl()
    {
        $this->load->model("jv/jv_model", "jv_mod");
        $filter = array();
        $test   = $this->input->post('id');
        $id     = explode("|",$test);
        $idx    = (isset($id[1]))?$id[1]:'-1';

        $fild = "VCH_AlternatifAccount as id, VCH_AlternatifAccount as text";
        if($test !=false)
            $filter["VCH_Account"]= $idx;
        
        $res = $this->jv_mod->get_cmb($fild, $this->tbl_gl_alter, $filter);
        if(count($res)>0)
            echo json_encode(array('data'=>$res));
        elseif(count($res) == 0 && $test !=false && $test !='-1')
            echo json_encode(array('data'=>array(array('id'=>$idx,'text'=>$idx))));
        else
            echo json_encode(array('data'=>array()));
    }

    function tes(){
        var_dump( $this->app->is_date('31-02-2017'));
    }

}
