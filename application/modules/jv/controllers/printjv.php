<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Printjv extends MY_Controller
{
    public $tbl_printJv_item 			= 'TF_PRINT_JV_ITEM';
    public $tbl_printJv_header			= 'TF_PRINT_JV_HEADER';

    public $field_doc 					= 'CHR_no_document';
    public $field_user					= 'CHR_user_create';
    public $field_add					= 'INT_document_print';
    public $field_doc_date              = 'Dat_document_date';

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => 'bisnis_area'),
            array('name' => 'bisnis_area', 'url' => 'master/cmb_bisnis_area', 'flow' => ''),
            array('name' => 'vendor', 'url' => 'master/cmb_vendor', 'flow' => ''),
            array('name' => 'document_number', 'url'=> 'master/cmb_docnum','flow'=>''),
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);
        $this->template->title('Print Jurnal Voucer');
        $this->template->build('jv/printjv_view', $data);
    }
    public function getrePrint()
    {
        $document_number	= $this->input->post('document_number');
        $postData['res'] = array(
            'document_number'=>$document_number,
        );
        $this->load->view('jv/reprint',$postData);
    }
    public function getdataReprint()
    {
        $document_number = $this->input->post('document_number');

        $this->load->model('jv/jv_model', 'jv_mod');

        $filds = "h.CHR_no_document,";
        $filds .= "d.CHR_id,";
        $filds .= "d.CHR_no_web,";
        $filds .= "d.DAT_posting_date,";
        $filds .= "d.CHR_ref,";
        $filds .= "d.INT_service_none,";
        $filds .= "d.INT_service,";
        $filds .= "d.INT_ppn,";
        $filds .= "d.INT_nilai_invoice,";

        $filter['h.CHR_no_document']= $document_number;
        $filter['h.INT_document_print'] ='1';
        $res = $this->jv_mod->get_rePrint($filds, $filter);
        echo json_encode(array("data"=>$res));
    }
    public function getPrint()
    {
        $company_code	= $this->input->post('company_code');
        $bisnis_area	= $this->input->post('bisnis_area');
        $tgl 			= $this->input->post('tgl_bayar');
        $vendor 		= $this->input->post('vendor');

        $postData['msg'] = '';
        $postData['res'] = array(
            'company_code'=>$company_code,
            'bisnis_area'=>$bisnis_area,
            'tgl'=>$tgl,
            'vendor'=>$vendor,
        );

        do{

            if($company_code == '-1'){
                $postData['msg'] = 'Please select one of Company Code';
                break;
            }

            if($bisnis_area == '-1'){
                $postData['msg'] = 'Please select one of Bisnis Area';
                break;
            }

            $tgl_validate = $this->app->is_date($tgl);
            if(!$tgl_validate || $tgl==''){
                $postData['msg'] = 'Format Tanggal Pembayaran tidak sesuai (dd-mm-yyyy)';
                break;
            }

            if($vendor == '-1'){
                $postData['msg'] = 'Please select one of Vendor';
                break;
            }

        } while (false);

        $this->load->view('jv/pdf_print', $postData);
    }

    public function getData()
    {
        $company_code	= $this->input->post('company_code');
        $bisnis_area	= $this->input->post('bisnis_area');
        $tgl 			= $this->input->post('tgl');
        $vendor 		= $this->input->post('vendor');
        $vendor_send 	= explode('|', $vendor);


        $this->load->model('jv/jv_model', 'jv_mod');
        $filter['CHR_company_code']= $company_code;
        $filter['CHR_bisnis_area']= $bisnis_area;
        $filter['CHR_vendor']= $vendor_send[0];
       
        $filter['CHR_no_document IS NULL']=null;
        $res = $this->jv_mod->get_printjv_header('*', $filter);
        echo json_encode(array("data"=>$res));
    }

    public function pdf_reprint()
    {
        $this->load->model('jv/jv_model', 'jv_mod');
        $doc_num = $this->input->post('id');
        $user =$this->session->userdata('user');

        $filds = "d.CHR_ref as Keterangan,";
        $filds .= "d.CHR_no_faktur as Reff,";
        $filds .= "d.INT_service_none as Nilai_Non,";
        $filds .= "d.INT_service as Nilai_service,";
        $filds .= "d.INT_ppn as PPN,";
        $filds .= "d.INT_nilai_invoice as Nilaiinvoice,";
        $filds .= "d.INT_PPh as PPh,";
        $filds .= "d.INT_amount as Jumlah,";
        $filds .= "c.VCH_Name as vendor,";
        $filds .= "DATE_FORMAT(h.Dat_document_date, '%d-%b-%Y') as date,";
        $filter['h.CHR_no_document']= $doc_num;
        $filter['h.INT_document_print']= '1';
        $res = $this->jv_mod->get_rePrint($filds, $filter);
        $data['item'] = $res;
        $data['docnum'] = $doc_num;
        $data['date'] = $res[0]->date;
        $data['vendor']= $res[0]->vendor;
        $this->jv_mod->insertReprint($doc_num,$user);

        $nomor = substr($doc_num, 6, 9);
        $this->dompdf->load_html($this->load->view('jv/mypdf',$data,true));
        $this->dompdf->render();
        file_put_contents(BASEPATH.'../tmp/Print'.$nomor.'.pdf',$this->dompdf->output());
        echo json_encode(array('filename' => base_url().'tmp/Print'.$nomor.'.pdf' ));
    }
    public function pdf_jv()
    {
        $this->load->model('jv/jv_model', 'jv_mod');
        $post_id = $this->input->post('id');
        $prinID = $this->app->getAutoId_print($this->field_doc, $this->tbl_printJv_item);
        $useID = implode(',', $post_id);
        $bisnis = $this->input->post('bisnis_area');
        $date = date('d M Y', strtotime($this->input->post('tgl')));
        $year = date('y');

        $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $bulan = $array_bulan[date('n')];
        $doc_num= $bisnis.'.'.$prinID.'/JV/'.$bulan.'/'.$year;
        $filds  = 'd.CHR_ref as Keterangan,';
        $filds .= 'd.CHR_no_faktur as Reff,';
        $filds .= 'd.INT_service_none as Nilai_Non,';
        $filds .= 'd.INT_service as Nilai_service,';
        $filds .= 'd.INT_ppn as PPN,';
        $filds .= 'd.INT_nilai_invoice as Nilaiinvoice,';
        $filds .= 'd.INT_PPh as PPh,';
        $filds .= 'c.VCH_Name as vendor,';
        $filds .= 'd.INT_amount as Jumlah,';

        $filter = "d.CHR_id IN($useID)";
        $res = $this->jv_mod->printSelected($filds, $filter);
        $data['item'] = $res;
        $data['docnum']=$doc_num;
        $data['date']=$date;
        $data['vendor']= $res[0]->vendor;
        $tbitem = array($this->field_doc => $doc_num );
        $tbheader = array(
            $this->field_doc 	=> $doc_num,
            $this->field_add 	=> '1',
            $this->field_doc_date => date('Y-m-d', strtotime($date)),
            $this->field_user	=> $this->session->userdata('user')
        );
        $this->jv_mod->insPrintheader($tbheader);
        $this->jv_mod->UpPrintitem($tbitem, $useID);

        $nomor = substr($doc_num, 6, 9);
        $this->dompdf->load_html($this->load->view('jv/mypdf',$data,true));
        $this->dompdf->render();
        file_put_contents(BASEPATH.'../tmp/Print'.$nomor.'.pdf',$this->dompdf->output());
        echo json_encode(array('filename' => base_url().'tmp/Print'.$nomor.'.pdf' ));
    }

}
