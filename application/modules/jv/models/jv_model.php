<?php
class Jv_model extends CI_Model
{
    public $tbl_header  = 'TF_JVHEADER';
    public $tbl_detail  = 'TF_JVITEM';
    public $parking     = 'parking';
    public $tbl_vendor  = 'MF_VENDOR';
    public $tbl_gl      = 'MF_GLACCOUNT';
    public $tbl_costCenter=  'MF_COST_CENTER';
    public $tbl_printJv_item = 'TF_PRINT_JV_ITEM';
    public $tbl_printJv_header			= 'TF_PRINT_JV_HEADER';

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    public function addHeader($data, $id)
    {
        if ($id == '') {
            $this->db_main->insert(
                $this->tbl_header,
                $data
            );
        } else {
            $this->db_main->where('CONCAT(CHR_jv_id,CHR_jv_th)', $id);
            $this->db_main->update($this->tbl_header, $data);
        }

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insPrintheader($data)
    {
        $this->db_main->insert(
            $this->tbl_printJv_header,
            $data
        );

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insertReprint($document_number, $user)
    {
        $this->db_main->query("INSERT INTO TF_PRINT_JV_HEADER (CHR_no_document,INT_document_print,CHR_user_create)
            SELECT CHR_no_document,max(INT_document_print) + 1 as INT_document_print,'$user' AS CHR_user_create
            FROM TF_PRINT_JV_HEADER
            WHERE CHR_no_document ='$document_number'");
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }

    }

    public function get_rePrint($filds, $filter=array())
    {
        $this->db_main->select($filds, false)
            ->from($this->tbl_printJv_header.' h')
            ->join($this->tbl_printJv_item.' d', 'h.CHR_no_document=d.CHR_no_document', 'LEFT')
            ->join($this->tbl_vendor.' c', 'd.CHR_vendor=c.VCH_VendordID', 'LEFT');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        $res=$this->db_main->get();
        return $res->result();
    }
    public function UpPrintitem($tbitem, $useID)
    {
        $this->db_main->where("CHR_id IN($useID)");
        $this->db_main->update($this->tbl_printJv_item, $tbitem);
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }
    public function add_detail($data, $id)
    {
        if ($id=='') {
            $this->db_main->insert_batch(
                $this->tbl_detail,
                $data
            );
        } else {
            $this->db_main->delete(
                $this->tbl_detail, array('CONCAT(CHR_jv_id,CHR_jv_th)' => $id)
            );

            $this->db_main->insert_batch(
                $this->tbl_detail,
                $data
            );
        }

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    public function joinJv_com($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_header.' h')
            ->join($this->tbl_detail.' d', 'h.CHR_jv_id=d.CHR_jv_id');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        $res=$this->db_main->get();
        return $res->result();
    }
    public function JvParking()
    {
        $this->db_main->select('"" as encriptid,'.$this->tbl_header.'.*', false)
                ->from($this->tbl_header)
                ->where($this->parking, '1');
        $res = $this->db_main->get();
        return $res->result();
    }

    public function get_jv_header($fild='*', $filter=array(), $group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_header);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter, false);
        }

        if ($group != false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        return $res->result();
    }
    public function get_printjv_header($fild='*', $filter=array(), $group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_printJv_item);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter, false);
        }

        if ($group != false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        return $res->result();
    }
    public function get_jv_item($fild='*', $filter=array(), $group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_detail);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter, false);
        }

        if ($group != false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        return $res->result();
    }

    public function get_jv_item_grid($fild='*', $filter=array(), $group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_detail.' d')
            ->join($this->tbl_gl.' g', 'g.CHR_CODE=d.CHR_gl_account', 'LEFT')
            ->join($this->tbl_costCenter.' c', 'd.CHR_cost_center=c.CHR_CODE', 'LEFT');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter, false);
        }

        if ($group != false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function get_cmb($custom_fild, $tbl, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($tbl);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }
    public function printSelected($fields, $filter)
    {
        $this->db_main->select($fields, false)
                    ->from($this->tbl_printJv_item.' d', false)
                    ->join($this->tbl_vendor.' c', 'd.CHR_vendor=c.VCH_VendordID', 'LEFT')
                    ->where($filter);
        $res=$this->db_main->get();

        return $res->result();
    }
}
