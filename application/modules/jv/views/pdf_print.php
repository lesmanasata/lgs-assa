<?php if($msg !=''){?>
	<div class="alert alert-danger" role="alert">
		<strong>Info !</strong> <?php echo $msg; ?> 
	</div>
<?php } else { ?>

<?php $timestamp = time();?>
<button type="button" name="button" id="print" class="btn btn-primary">PRINT</button>

<div id="detailprintjv<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	var checkedVals = [];
	$(document).ready(function () {

		var ds_distribute_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('jv/printjv/getData'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 INT_amount: { type: "number"},
						 DAT_posting_date: {  type: "date" },
					 }
				}
			},
		});

		$("#detailprintjv<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_distribute_detail,
			pageable: true,
			pageSize: 50,
			resizable: true,
			scrollable: true,
            dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
                {
					field:"",
    				title:"<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",
    				width:25,
    				template: "<input type='checkbox' class='id' name='id[]' value='#: CHR_id #'>"
    			},
				{field:"CHR_id",width: 100, title:"ID",filterable: false},
                {field:"CHR_no_web",width: 100, title:"No Web",filterable: false},
                {field:"DAT_posting_date",width: 100, title:"Posting Date",filterable: false,format: "{0:dd-MM-yyyy}",parseFormats: ["MM/dd/yyyy"]},
				{field:"CHR_ref",width: 150, title:"No Referensi",filterable: false},
				{field:"INT_service_none",width: 150, title:"Nilai Non Service",filterable: false},
				{field:"INT_service",width: 150, title:"Nilai Service",filterable: false},
				{field:"INT_ppn",width: 150, title:"PPN",filterable: false},
				{field:"INT_nilai_invoice",width: 150, title:"Nilai Invoice",filterable: false},
                {field:"INT_PPh",width: 150, title:"PPh",filterable: false},
                {field:"INT_amount",width: 150, title:"Jumlah",filterable: false}

			]
		});
		$('#print').click(function() {
			var checkedVals = $('.id:checkbox:checked').map(function() {
				return this.value;
			}).get();
			var bisnis = $('#form_print input[name=bisnis_area]').val();
			var tgl_bayar = $('#form_print input[name=tgl_bayar]').val();
			if(checkedVals !=''){
					$.ajax({
						url:'<?php echo site_url('jv/printjv/pdf_jv'); ?>',
						data:{'id':checkedVals,
							  'bisnis_area':bisnis,
							  'tgl': tgl_bayar
						  },
						type:'POST',
						dataType:'json',tail:1, success: function(result) {
							window.open(result.filename, '_blank');
							window.setTimeout('window.location.href="<?php echo site_url('jv/printjv/index');?>"', 1000);
						}
					});
			}else {
				msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
			}
        });

	});
    function toggle(source) {
      checkboxes = document.getElementsByClassName('id');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
    }
</script>

<?php } ?>
