<?php $timestamp = time();?>
<h1 class="page-title">Journal Voucher - Form</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Journal Voucher </a></li>
  <li class="active"><strong>Add New JV</strong></li>
</ol>

<form class="form-horizontal" id="jv_form" action="" method="POST">
  <div class="col-md-12 animatedParent animateOnce z-index-47">
      <ul class="nav nav-tabs">
        <li class="active" id="heder_link"><a aria-expanded="true" href="#heder" data-toggle="tab">Header</a></li>
        <li id="detail_link"><a aria-expanded="false" href="#detail" data-toggle="tab">Detail</a></li>
        <li id="simulate_link"><a aria-expanded="false" href="#simulate_tab" data-toggle="tab">Simulate</a></li>
      </ul>
      <div class="tab-content">
          <div class="tab-pane active" id="heder">
            <div class="panel-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Company Code:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="company_code" name="company_code" style="width:100%;" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Vendor:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="vendor" name="vendor" style="width:100%;" placeholder="&mdash;&mdash;Vendor&mdash;&mdash;" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">No Invoice:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="no_inv" value='<?php echo $invoice_number; ?>' name="no_inv">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Invoice Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker1" id="date_inv" name="date_inv" value="<?php echo $date_posting; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Faktur Pajak:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="faktur" name="faktur" value='<?php echo $faktur_pajak; ?>'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Faktur Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker1" id="date_faktur" name="date_faktur" value="<?php echo $faktur_date;?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Posting Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker1" id="posting_date" name="posting_date" value="<?php echo $date_posting;?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Amount:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $amount; ?>" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Tax Amount:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $tax; ?>" id="tax_amount" name="tax_amount" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Text:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="text_head" value="<?php echo $description; ?>" name="text_head" value="<?php echo $description; ?>" >
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                  <div class="panel panel-info ">
                    <div class="panel-heading clearfix"> 
                      <div class="panel-title">Detail Vendor Data</div>  
                    </div> 
                    <div class="panel-body"> 
                        <div class="form-group">
                          <label class="control-label col-sm-3">Name:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $vendor_name; ?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3">Kota:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="kota" name="kota" value="<?php echo $vendor_kota; ?>" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3">Telpon:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="telp" name="telp" value="<?php echo $vendor_phone; ?>" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3">NPWP:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="npwp" name="npwp" value="<?php echo $vendor_npwp; ?>" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3">Alamat:</label>
                          <div class="col-sm-9">
                              <textarea name="alamat" class="form-control" id="alamat" ><?php echo $vendor_addres;?></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-sm-3">Kode Pos:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="kd_pos" name="kd_pos" value="<?php echo $vendor_kodepost; ?>" >
                          </div>
                      </div>
                    </div> 
                  </div> 
                  <div class="alert alert-success" role="alert"><strong>Info :</strong> Detail Vendor Data, Terisi ketika Vendor terpilih.</div>
                </div>
            </div>
          </div>

          <div class="tab-pane" id="detail">
            <div class="panel-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Base Line Date:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker1" id="base_line" name="base_line" value="<?php echo $date_base_line; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Payment Term:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control kendodropdown" id="pay_term" name="pay_term" style="width:100%;" placeholder="&mdash;&mdash;Payment Term&mdash;&mdash;">
                        </div>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="days" name="days" value="<?php echo $days; ?>" readonly>
                        </div> <label class="control-label">Days</label>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Bisnis area:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="bisnis_area" name="bisnis_area" style="width:100%;" placeholder="&mdash;&mdash;Bisnis Area&mdash;&mdash;" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">GL Account:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="vendor_detail" name="vendor_detail" style="width:100%;" placeholder="&mdash;&mdash;GL Vendor&mdash;&mdash;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Rep Country:</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="counrty" name="country" value="ID" readonly >
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Due On:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="due_on" name="due_on"  value="<?php echo $due_on; ?>" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">WHT Type:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="wht_type" name="wht_type" style="width:100%;" placeholder="&mdash;&mdash;WHT Type&mdash;&mdash;" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">WHT Code:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control kendodropdown" id="wht_code" name="wht_code" style="width:100%;" placeholder="&mdash;&mdash;WHT Code&mdash;&mdash;" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">WHT Base Amount:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="wht_base" name="wht_base" value="<?php echo $wht_base_amount; ?>" readonly >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">WHT Amount:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="wht_amount" name="wht_amount" value="<?php echo $wht_amount; ?>" readonly >
                        </div>
                    </div>
                </div>
            </div>
          </div>

          <div class="tab-pane" id="simulate_tab">
              <div class="panel-body">
                  <table class="table">
                    <tr>
                      <td colspan="3">Doc. Type : KR (Vendor Invoice) Normal Document</td>
                    </tr>
                    <tr>
                      <td>Doc. Number</td>
                      <td>Company Code: <span id="company"></span></td>
                      <td>Fiscal Year: <?php echo date('Y')?></td>
                    </tr>
                    <tr>
                      <td>Doc. Date: <span id="date_doc"></span></td>
                      <td>Posting  Date: <span id="posting"></span> </td>
                      <td>Period: <span id="period"></span></td>
                    </tr>
                    <tr>

                      <td>Tax Report Date: <span id="fak_dat"></span></td>
                      <td>Doc Currency: IDR</td>
                      <td></td>
                    </tr>
                  </table>

                  <div class="col-sm-12">
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr class="bg-grey">
                            <th>ITEM</th>
                            <th>ACCOUNT</th>
                            <th>ACCOUNT SHORT TEXT</th>
                            <th>ASSIGMENT</th>
                            <th>TAX</th>
                            <th>AMOUNT</th>
                          </tr>
                        </thead>
                        <tbody id="trans"></tbody>
                      </table>
                    </div>
                </div>
              </div>
          </div>
      </div>
  </div>

  <div class="col-md-12" id="item_panel" >
    <fieldset>
        <legend>Item Journal Voucher</legend>
        <div id="tax_detail"></div>
    </fieldset>
  </div>

  <div style="clear: both;">&nbsp;</div>

  <div class="panel-footer modal-footer">
      <button type="button" id='calculate' class="btn btn-info">Calculate</button>
      <button type="button" id="save_p" class="btn btn-default">Save Parking</button>
      <button type="button" id='simulate' class="btn btn-warning">Simulate</button>
      <button type="submit" id='simpan' class="btn btn-success">Save and Finish</button>
      <button type="button" id="back" class="btn btn-primary">Back</button>
  </div>

  <input type="hidden" id="sig" name="sig" class="sig">
  <input type="hidden" name="tag" value="<?php echo $tag ;?>">
</form>

<script>
var record	            = 0;
var company_code_global = '';
var wht_head_global     = '';
var coba                = '';
var checkedIds          = {};
var gl_glo              = '';
var bisnis              = '';
$(document).ready(function() {

    $('#simpan').attr('disabled', true);
    $('#simulate').attr('disabled', true);

	  <?php echo $js_cmb; ?>
    cmb['company_code'].value('<?php echo $company_code; ?>');
    cmb['pay_term'].value('<?php echo $payment_code; ?>');
    cmb['wht_code'].value('<?php echo $wht_code; ?>');
    cmb['wht_type'].value('<?php echo $wht_type; ?>');
    cmb['vendor'].value('<?php echo $vendor_id; ?>');

    cmb['bisnis_area'].dataSource.read({id:'<?php  echo $company_code; ?>'});
    cmb['bisnis_area'].value("<?php echo trim($bisnis_area); ?>");

    cmb['vendor_detail'].dataSource.read({id:'<?php  echo $vendor_id; ?>'});
    cmb['vendor_detail'].value('<?php echo $vendor_gl; ?>'); //===> gl account vendor in detail tab


    cmb['bisnis_area'].bind('change',function(){
      bisnis = cmb['bisnis_area'].value();
      ds_jv.read();
    });

    cmb['company_code'].bind('change',function(){
      cmb['bisnis_area'].value('-1');
      ds_jv.read();
    });

    cmb['vendor'].bind('change', function(){
      var id_vendor = cmb['vendor'].value();
      $.ajax({
        url:'<?php echo site_url('master/cmb_vendor_detail');?>',
        type:'POST',
        data:'id='+id_vendor,
        dataType: 'json',
        success:function(html){
          cmb['vendor_detail'].value('-1');
          if(html.data.length > 0){
            $('#nama').val(html.data[0].VCH_Name);
            $('#kota').val(html.data[0].VCH_City);
            $('#telp').val(html.data[0].VCH_Telephone);
            $('#npwp').val(html.data[0].VCH_Stceg);
            $('#alamat').val(html.data[0].VCH_Address);
            $('#kd_pos').val(html.data[0].CHR_PostCode);
            if(html.data[0].VCH_ControllAccount !='' || html.data[0].VCH_ControllAccount !='null')
              cmb['vendor_detail'].value(html.data[0].VCH_ControllAccount);
          }else{
            $('#nama').val('');
            $('#kota').val('');
            $('#telp').val('');
            $('#npwp').val('');
            $('#alamat').val('');
            $('#kd_pos').val('');
          }
        }
      });
      gl_glo = id_vendor;
    });

    $(".datepicker1").kendoDatePicker({
        format: "dd-MM-yyyy",
    });

    cmb['pay_term'].bind('change', function(){
      var jml_day = cmb['pay_term'].value();
      $('input[name=days]').val(jml_day);
      var tgl = $('input[name=base_line]').val();
      var tgl_t = tgl.split('-');
      var due = getJmlHari(tgl_t[0],tgl_t[1],tgl_t[2],jml_day);
      $('input[name=due_on]').val(due);
    });

    var ds_jv = new kendo.data.DataSource({
      transport: {
        read:  {
          type:"POST",
          dataType: "json",
          data:<?php echo json_encode($item); ?>,
          url: '<?php echo site_url('jv/tabel_load'); ?>',
        },
        create: {
          url: '<?php echo site_url('jv/jurnal_submit'); ?>',
          type:"POST",
          dataType: "json",
          data: function() {
              return JSON.parse(JSON.stringify( $(this).headerForm() ));
          },complete:function(res){
              var res_msg = JSON.parse(res.responseText);
              $('#loading-mask').removeClass('loader-mask');

              msg_box(res_msg.messages,[{'btnOK':function(){
                  $(this).trigger('closeWindow');
                  if(res_msg.status){
                    <?php if($this->uri->segment(3) == ''){ ?>
                      window.location.href="<?php echo site_url('/jv/index');?>";
                    <?php }else{ ?>
                      window.location.href="<?php echo site_url('/jv/jv_list');?>";
                    <?php } ?>
                  }
                }
              }],'Info!');
          }
        },
      },
      batch: true,
      pageSize: 50,
      schema: {
        parse: function(response){
          return response.data;
        },
        model: {
          fields: {
            gl_account: {  type: "text", validation: { required: true},defaultValue: { gl_account: ""} },
            cost_center: {  type: "text", validation: { required: true},defaultValue: { cost_center: ""} },
            internal_order: {  type: "text",defaultValue: { internal_order: ""} },
            ppn_type: {  type: "text", validation: { required: true},defaultValue: { ppn_type: "M0"} },
            amount: {  type: "number", validation: { required: true},defaultValue: 0 },
            ppn_amount: {  type: "number"},
            pph_amount: { type:"number",defaultValue:0},
            pph: { type: "boolean",editable:true },
            text_asign:{type:'text', defaultValue:''}
          }
        },
      }
    });

    $("#base_line").change(function(){
      var jml_day = cmb['pay_term'].value();
      $('input[name=days]').val(jml_day);
      var tgl = $('input[name=base_line]').val();
      var tgl_t = tgl.split('-');
      var due = getJmlHari(tgl_t[0],tgl_t[1],tgl_t[2],jml_day);
      $('input[name=due_on]').val(due);
    });

    $("#tax_detail").kendoGrid({
      dataSource: ds_jv,
      pageable: false,
      scrollable: true,
      selectable: "multiple",
      height: 350,
      toolbar: ["create"],
      dataBinding: function() {
        record = (this.dataSource.page() -1) * this.dataSource.pageSize();
      },
      change: function(){
        $('#simpan').attr("disabled", true);
        $('#simulate').attr("disabled", true);
      },
      edit: function(e) {
          $(e.container).find("input[name='ppn_amount']").prop('disabled', true);
          $(e.container).find("input[name='pph_amount']").prop('disabled', true);
      },
      editable: {
        createAt: 'bottom',
      },
      columns: [
        {
          filterable: false,
          template: "#: ++record #",
          width: 50,
          title:"NO",
        },{
          title: "<?php echo lang('label_action'); ?>",
          width: 60,
          command: [
            {
              name: "destroy",
              template: "<a style='cursor: pointer;' class='k-buttons k-grid-delete'><i class='glyphicon glyphicon-trash'></i></a>"
            }
          ],
        },{ 
          field:"gl_account.gl_account",
          title:"GL Accountss", 
          width: 270,
          editor: function cmb_gl_acc2(container, options){
            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
              .appendTo(container)
              .kendoDropDownList({
                autoBind: false,
                filter: "contains",
                dataTextField: "text",
                dataValueField: "id",
                dataSource: {
                  serverFiltering: false,
                  transport: {
                    read: {
                      dataType: "jsonp",
                      data:'id',
                      url: "<?php echo site_url('master/cmb_gl_acc2'); ?>",
                    }
                  }
                },
            });
          }
        },{
          field: "cost_center.cost_center", 
          title: "Cost Center", 
          width: 300,
          editor: function cost_cent_cmb(container, options){
            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
              .appendTo(container)
              .kendoDropDownList({
                autoBind: false,
                filter: "contains",
                dataTextField: "text",
                dataValueField: "id",
                optionLabel   : "Select your option",
                dataSource: {
                  serverFiltering: false,
                  transport: {
                    read: {
                      dataType: "jsonp",
                      data:{'id':bisnis},
                      url: "<?php echo site_url('master/cmb_cost_center2'); ?>",
                    }
                  }
                },
            });
          }
        },{
          field: "internal_order.internal_order", 
          title:"Internal Order", 
          width: 270,
          editor: function internal_order_cmb(container, options){
            $('<input name="' + options.field + '" style=" font-size: 14px;" />')
              .appendTo(container)
              .kendoDropDownList({
                autoBind: false,
                filter: "contains",
                dataTextField : "text",
                dataValueField: "id",
                optionLabel   : "Select your option",
                dataSource: {
                  serverFiltering: false,
                  transport: {
                    read: {
                      dataType: "jsonp",
                      url: "<?php echo site_url('jv/cmb_internal_order'); ?>",
                    }
                  }
                },
            });
          }
        },{
          field: "amount", 
          title:"Amount", 
          width: 100,
          template:'#= kendo.toString(amount, "n0")#'
        },{ 
          field: "ppn_type.ppn_type", 
          title:"PPNType", 
          width: 80,
          editor: function cmb_ppntype(container, options){
            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
              .appendTo(container)
              .kendoDropDownList({
              autoBind: false,
              dataTextField: "text",
              dataValueField: "id",
              dataSource: {
                serverFiltering: true,
                transport: {
                  read: {
                    dataType: "jsonp",
                    url: "<?php echo site_url('app/get_cmb_ppntype'); ?>",
                  }
                }
              },change: function (){
                var grid = $("#tax_detail").data("kendoGrid");
                var selectedItem = grid.dataItem(grid.select());
                var al_amount = selectedItem.amount;
                var select_drop = selectedItem.ppn_type.ppn_type;
                if (select_drop==='M4') {
                  amo = Math.round(al_amount * 10/100);
                }else {
                  amo = 0;
                }
                selectedItem.set('ppn_amount',amo);
              }
            });
          }
        },{
          field: "ppn_amount", 
          title:"PPN Amount", 
          width: 100,
          template:'#= kendo.toString(ppn_amount, "n0")#'
        },{ 
          field:"pph", 
          title:"PPh Indc", 
          width: 90,
          template: "<input type='checkbox' id= 'head' class='checkbox row-checkbox' #= pph ? \"checked='checked'\" : ''# />",
          attributes: { 
            "align":"center" 
          }
        },{ 
          field: "pph_amount", 
          title:"PPH Amount", 
          width: "120px",
          template:'#= kendo.toString(pph_amount, "n0")#'
        },{ 
          field: "text_desc", 
          title:"Text", width: "100px" 
        },{ 
          field: "text_asign", 
          title:"Assigment", 
          width: "100px" 
        }
      ]
    });
    
    
    $.fn.headerForm = function() {
      out = {
        'kd_pos'        : $('#jv_form input[name=kd_pos]').val(),
        'company_code'  : cmb['company_code'].value(),
        'vendor'        : cmb['vendor'].value(),
        'no_inv'        : $('#jv_form input[name=no_inv]').val(),
        'date_inv'      : $('#jv_form input[name=date_inv]').val(),
        'faktur'        : $('#jv_form input[name=faktur]').val(),
        'date_faktur'   : $('#jv_form input[name=date_faktur]').val(),
        'posting_date'  : $('#jv_form input[name=posting_date]').val(),
        'amount'        : $('#jv_form input[name=amount]').val(),
        'tax_amount'    : $('#jv_form input[name=tax_amount]').val(),
        'text_head'     : $('#jv_form input[name=text_head]').val(),
        'pay_term'      : cmb['pay_term'].text(),
        'days'          : $('#jv_form input[name=days]').val(),
        'base_line'     : $('#jv_form input[name=base_line]').val(),
        'vendor_detail' : cmb['vendor_detail'].value(),
        'bisnis_area'   : cmb['bisnis_area'].value(),
        'country'       : $('#jv_form input[name=country]').val(),
        'due_on'        : $('#jv_form input[name=due_on]').val(),
        'wht_type'      : cmb['wht_type'].value(),
        'wht_code'      : cmb['wht_code'].value(),
        'wht_base'      : $('#jv_form input[name=wht_base]').val(),
        'wht_amount'    : $('#jv_form input[name=wht_amount]').val(),
        'nama'          : $('#jv_form input[name=nama]').val(),
        'kota'          : $('#jv_form input[name=kota]').val(),
        'telp'          : $('#jv_form input[name=telp]').val(),
        'parking'       : $('#jv_form input[name=sig]').val(),
        'alamat'        : $('#jv_form textarea[name=alamat]').val(),
        'npwp'          : $('#jv_form input[name=npwp]').val(),
        'tag'           : $('#jv_form input[name=tag]').val()

      }
      return out;
    }


    $.fn.Formclear = function() {
      out = {
                'kd_post'       : $('#jv_form input[name=kd_pos]').val(''),
        'alamat'        : $('#jv_form textarea[name=alamat]').val(''),
        'hidden'        : $('#jv_form input[name=sig]').val(''),
        'company_code'  : cmb['company_code'].value('-1'),
        'vendor'        : cmb['vendor'].value('-1'),
        'no_inv'        : $('#jv_form input[name=no_inv]').val(''),
        'date_inv'      : $('#jv_form input[name=date_inv]').val('<?php echo date('Y-m-d'); ?>'),
        'faktur'        : $('#jv_form input[name=faktur]').val(''),
        'date_faktur'   : $('#jv_form input[name=date_faktur]').val('<?php echo date('Y-m-d'); ?>'),
        'posting_date'  : $('#jv_form input[name=posting_date]').val('<?php echo date('Y-m-d'); ?>'),
        'amount'        : $('#jv_form input[name=amount]').val(''),
        'tax_amount'    : $('#jv_form input[name=tax_amount]').val(''),
        'text_head'     : $('#jv_form input[name=text_head]').val(''),
        'pay_term'      : cmb['pay_term'].value('-1'),
        'days'          : $('#jv_form input[name=days]').val(''),
        'base_line'     : $('#jv_form input[name=base_line]').val('<?php echo date('Y-m-d'); ?>'),
        'gl_account'    : cmb['vendor_detail'].value('-1'),
        'bisnis_area'   : cmb['bisnis_area'].value('-1'),
        'country'       : $('#jv_form input[name=country]').val('ID'),
        'due_on'        : $('#jv_form input[name=due_on]').val(''),
        'wht_type'      : cmb['wht_type'].value('-1'),
        'wht_code'      : cmb['wht_code'].value('-1'),
        'wht_base'      : $('#jv_form input[name=wht_base]').val(''),
        'wht_amount'    : $('#jv_form input[name=wht_amount]').val(''),
        'nama'          : $('#jv_form input[name=nama]').val(''),
        'kota'          : $('#jv_form input[name=kota]').val(''),
        'telp'          : $('#jv_form input[name=telp]').val(''),
        'npwp'          : $('#jv_form input[name=npwp]').val(''),
        'tag'           : $('#jv_form input[name=tag]').val('')
      }
      return out;
    }

    var grid = $("#tax_detail").data("kendoGrid");
    grid.table.on("click", ".checkbox" , selectRow);
    function selectRow() {
      var checked = this.checked;
      var row = $(this).closest("tr"),
      grid = $("#tax_detail").data("kendoGrid");
      dataItem = grid.dataItem(row);
      var wht_t = cmb['wht_code'].value();

      if (checked && wht_t === '-1' ) {
          msg_box("Please fill WHT first!!",['btnOK'],'Info!');
          $(".checkbox").prop("checked", false);
      }else if (checked) {
            var al_amount = dataItem.amount;
            var wht = cmb['wht_code'].text();
            var wht_per = wht.split('-');
            var wht_send = wht_per[2].replace('%','');
            var wht_sendt = wht_send.replace(',','.');
            pph = al_amount * wht_sendt / 100;
            dataItem.set('pph_amount',pph);
      }else {
        dataItem.set('pph_amount','0');
      }
      dataItem.set("pph", this.checked);
      $(this).amount_calculate();
    }

  $("#save_p").click(function(){
      var cekData = $('#tax_detail').data('kendoGrid').dataSource.total();
      if(cekData > 0 ){
          $('#jv_form input[name=sig]').val('parking');
          $('#jv_form input[name=tag]').val();
          ds_jv.sync();
          $(this).Formclear();
      }else{
          $('#loading-mask').removeClass('loader-mask');
          msg_box("<?php echo lang('msg_null_item_detail'); ?>",['btnOK'],'Info!');
      }
  });

  $("#jv_form").submit(function(e) {
        e.preventDefault();
        $('#loading-mask').addClass('loader-mask');
        var cekData = $('#tax_detail').data('kendoGrid').dataSource.total();
        if(cekData > 0){
          $('#jv_form input[name=sig]').val('submit');
          ds_jv.sync();
        }
        else{
          $('#loading-mask').removeClass('loader-mask');
          msg_box("<?php echo lang('msg_null_item_detail'); ?>",['btnOK'],'Info!');
        }
  });

  $("#calculate").click(function(){
     $(this).amount_calculate();
  });

  $.fn.amount_calculate = function(){
     $('#simpan').attr('disabled', false);
      $('#simulate').attr('disabled', false);
      var data = $('#tax_detail').data().kendoGrid.dataSource.data();
      var amo = 0;
      var wht = 0;
      var Ppn = 0;
      var Pph = 0;
      var wht = 0;

      for (var i = 0; i < data.length; i++) {
        var amount = data[i].amount;
        var ppn = data[i].ppn_amount;
        var pph = data[i].pph_amount;
        amo += amount ;
        Ppn += ppn;
        Pph += pph;
        if(pph>0){
          wht += amount;
        }

      }
      var total = amo + Ppn;
      $('#jv_form input[name=amount]').val(numberWithCommas(total));
      $('#jv_form input[name=tax_amount]').val(numberWithCommas(Ppn));
      $('#jv_form input[name=wht_amount]').val(numberWithCommas(Pph));
      $('#jv_form input[name=wht_base]').val(numberWithCommas(wht));
  }


  $("#simulate_link").hide();
  $('#back').hide();
  $('#trans').html('');
  $("#simulate").click(function(){
      var company     = cmb['company_code'].value();
      var dateTo      = $('#jv_form input[name=date_inv]').val();
      var posting     = $('#jv_form input[name=posting_date]').val();
      var fak_date    = $('#jv_form input[name=date_faktur]').val();
      var amo         = $('#jv_form input[name=amount]').val();
      var tax_amount  = $('#jv_form input[name=tax_amount]').val();
      var wht_amount  = $('#jv_form input[name=wht_amount]').val();
      var ven         = cmb['vendor'].text();
      var vendor      = ven.split("-")
      var period      = dateTo.split("-");
      var amo_hit     = amo.replace(/,/g, '');
      var wht_amountHit = wht_amount.replace(/,/g, '');
      var tax_amountHit = tax_amount.replace(/,/g, '');
      var fix_amo     = parseInt(amo_hit) - parseInt(wht_amountHit);
      var data        = $('#tax_detail').data().kendoGrid.dataSource.data();
      var no          = 1;
      var tr          = '';
      var cek_vendor  = vendor[1];

      $('#posting').html(posting);
      $('#date_doc').html(dateTo);
      $('#fak_dat').html(fak_date);
      $('#company').html(company);
      $('#period').html(period[1]);
      $('#trans').html('');
      
      if(cek_vendor != undefined){ 
        tr = $('<tr/>');
        tr.append("<td>" + no+ "</td>");
        tr.append("<td>" + vendor[0] + "</td>");
        tr.append("<td>" +vendor[1] + "</td>")
        tr.append("<td></td>");
        tr.append("<td></td>");
        tr.append("<td style='text-align: right;'>" +numberWithCommas(fix_amo)+ " - </td>");
        $('#trans').append(tr);
      }

      no++;
      for (var i = 0; i < data.length; i++) {
           var gl = data[i].gl_account.gl_account;
           var send = gl.split("-");
            var internal = data[i].internal_order.internal_order;
            var internal_save = internal.split("-");
            var total = data[i].amount;
            if (total>0) {
                tr = $('<tr/>');
                tr.append("<td>" + no + "</td>");
                tr.append("<td>" + send[0] + "</td>");
                tr.append("<td>" + send[1] +"</td>")
          tr.append("<td>" + data[i].text_asign +"</td>");
          tr.append("<td>" + data[i].ppn_type.ppn_type +"</td>");
          tr.append("<td style='text-align: right;'>" + numberWithCommas(total) +"</td>");
          $('#trans').append(tr);
           no++;
            }
        }

        var num = 1;
        if (tax_amountHit!=='0') {
            tr = $('<tr/>');
            tr.append("<td>" + no+ "</td>");
            tr.append("<td>1171000000</td>");
            tr.append("<td>PPN Masukan</td>")
            tr.append("<td></td>");
            tr.append("<td></td>");
            tr.append("<td style='text-align: right;'>" +numberWithCommas(tax_amountHit)+ "</td>");

            $('#trans').append(tr);
        }

        if (wht_amountHit!== '0') {
            if(tax_amountHit=='0'){
                var test = no
            }else {
                var test = no+=num
            }
            tr = $('<tr/>');
            tr.append("<td>" + test + "</td>");
            tr.append("<td>2161000002</td>");
            tr.append("<td>Hutang PPH</td>")
            tr.append("<td></td>");
            tr.append("<td></td>");
            tr.append("<td style='text-align: right;'>" + numberWithCommas(wht_amountHit) +" - </td>");
            $('#trans').append(tr);
        }

      $('.simu').show();
      $('.head').hide();
      $('.deta').hide();
      $('#save_p').hide();
      $('#simpan').hide();
      $('#simulate').hide();
      $('#back').show();
      $('#calculate').hide();
      $('#item_panel').hide();
      $("#simulate_link").show();
      $("#heder_link").hide();
      $("#detail_link").hide();
      $('.nav-tabs a[href="#simulate_tab"]').tab('show');
    });

  $("#back").click(function(){
      $('.simu').hide();
      $('.head').show();
      $('.deta').show()
      $('#save_p').show();
      $('#simpan').show();
      $('#item_panel').show();
      $('#calculate').show();
      $('#back').hide();
      $('#simulate').show();
      $("#simulate_link").hide();
      $("#heder_link").show();
      $("#detail_link").show();
      $('.nav-tabs a[href="#heder"]').tab('show');
  });

});


function getJmlHari(hari,bulan,tahun,jmlhari){
  var day;
  var jml = parseFloat(hari) + parseFloat(jmlhari);
  do
     {
         day = getLastDayOfMonth(parseFloat(bulan),parseFloat(tahun));
         if (jml > getLastDayOfMonth(parseFloat(bulan),parseFloat(tahun)) ){
             jml = jml - getLastDayOfMonth(parseFloat(bulan),parseFloat(tahun));
             if (bulan > 12) {
                 bulan = 1;
                 tahun = parseFloat(tahun) + 1;
             }else {
                 bulan = parseFloat(bulan) + 1;
             }
         }


     }
  while (jml > getLastDayOfMonth(parseFloat(bulan),parseFloat(tahun)) );
  jml = (jml.toString().length==1)?"0"+jml.toString():jml.toString();
  bulan = (bulan.toString().length==1)?"0"+bulan.toString():bulan.toString();
  day = jml+'-'+bulan+'-'+tahun;
  return day;

}


function getLastDayOfMonth(month,year){
  var day;
  switch(month)
     {
     case 1 :
     case 3 :
     case 5 :amount
     case 7 :
     case 8 :
     case 10:
     case 12:
         day = 31;
         break;
     case 4 :
     case 6 :
     case 9 :
     case 11:
            day = 30;
         break;
     case 2 :
         if( ( (year % 4 == 0) && ( year % 100 != 0) ) || (year % 400 == 0) )
             day = 29;
         else
             day = 28;
         break;

  }

  return day;
}
</script>
