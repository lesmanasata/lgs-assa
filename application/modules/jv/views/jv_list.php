<?php $timestamp = time();?>
<h1 class="page-title">Journal Voucher - List</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Journal Voucher </a></li>
  <li class="active"><strong>Shor List Of JV</strong></li>
</ol>

<div class="col-md-12 animatedParent animateOnce z-index-47">
      <ul class="nav nav-tabs">
        <li class="active" id="heder_link"><a aria-expanded="true" href="#heder" data-toggle="tab">Complete</a></li>
        <li id="detail_link"><a aria-expanded="false" href="#detail" data-toggle="tab">Parking</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="heder">
            <div class="panel-body" style="padding-left: 0px; padding-right: 0px;">
                <div id="complate_table"></div>
            </div>
        </div>
        <div class="tab-pane" id="detail">
            <div class="panel-body" style="padding-left: 0px; padding-right: 0px;">
                <div id="table_parking"></div>
            </div>
        </div>
    </div>
</div>


<script id="detailComplete" type="text/x-kendo-tmpl">
	<tr id="#: CHR_jv_id #">
		<td>
			<a href="javascript:void(0)" onclick="$(this).getitem();" data-id="#: CHR_jv_id #">
				<strong>#: CHR_jv_id #</strong>
			</a>
		</td>
		<td>#: CHR_company_code #</td>
		<td>#: CHR_vendor_id #</td>
		<td>#: CHR_vendor_name #</td>
		<td>#: kendo.toString(kendo.parseDate(DAT_date_posting, 'yyyy-MM-dd'), 'dd-MM-yyyy') #</td>
        <td>#: CHR_invoice_number #</td>
		<td>#: kendo.toString(INT_amount, "n0") #</td>
        <td>#: kendo.toString(INT_tax, "n0") #</td>

	</tr>
</script>

<script>
	$(document).ready(function() {
		var ds_jv = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('jv/GetJvcomplate'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						DAT_date_posting: {  type: "date" },
					}
				}
			},
			pageSize: 14,

		});

        var element = $("#complate_table").kendoGrid({
                dataSource: ds_jv,
                height: 600,
                sortable: true,
                pageable: true,
                detailInit: detailInit,
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Like",
                            eq: "=",
                            neq: "!="
                        },
                    }
                },
                dataBound: function() {
                    //this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [
                    {field:"CHR_jv_id",title:"DOC NUUMBER",width:130},
                    {field:"CHR_company_code",title:"COMPANY",width:100},
                    {field:"CHR_vendor_id",title:"VENDOR ID",width:100},
                    {field:"CHR_vendor_name",title:"VENDOR NAME",width:300},
                    {field:"DAT_date_posting",title:"POST DATE",width:100,
                        format: "{0:dd-MM-yyyy}",
                        parseFormats: ["MM/dd/yyyy"],
                        filterable: {
                            ui: function (e) {
                                e.kendoDatePicker({
                                    format: "dd-MM-yyyy"
                                });
                            }
                        }
                    },
                    {field:"CHR_invoice_number",title:"INVOICE",width:100},
                    {field:"INT_amount",title:"AMOUNT",width:100},
                    {field:"INT_tax",title:"TAX",width:100},
                ]
            });


        var ds_parking = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('jv/GetJvParking'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;

				},
				model: {
					fields: {
						INT_amount: {  type: "number" },
                        DAT_date_posting: {type:"date"},
					}
				}
			},
			pageSize: 14,

		});

        $('#table_parking').kendoGrid({
            dataSource: ds_parking,
            pageable: true,
            scrollable: true,
            height: 550,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Like",
                        eq: "=",
                        neq: "!="
                    },
                }
            },
        columns: [
            {
                title:"<?php echo lang('label_action'); ?>",
                width:80,
                template:'<a href="<?php echo site_url('jv/index');?>/#: encriptid #" class="btn btn-info btn-sm" id="test" role="button"> <span class="glyphicon glyphicon-pencil"></span> Edit</a>',
                editable:false
            },
            {field:"CHR_jv_id",title:"DOC NUMBER",width:120},
            {
                field:"CHR_company_code",
                title:"COMPANY",
                template:'#if(CHR_company_code=="-1"){# - #}else{# #CHR_company_code# #}#',
                width:100
            },{
                field:"CHR_vendor_id",
                title:"VENDOR",
                template:'#if(CHR_vendor_id=="-1"){# - #}else{# #CHR_vendor_id# #}#',
                width:100
            },
            {field:"CHR_vendor_name",title:"VENDOR NAME",width:300},
            {field:"CHR_invoice_number",title:"INVOICE",width:150},
            {field:"DAT_date_posting",title:"POST DATE",width:100,format: "{0:dd-MM-yyyy}",parseFormats: ["MM/dd/yyyy"]},
            {field:"INT_amount",title:"AMOUNT",width:100},
            {field:"INT_tax",title:"TAX",width:100},
        ]
    });

    $("#test").click(function(e) {
        e.preventDefault();

        $.ajax({
			url:'<?php echo site_url('jv/getSegment');?>',
			type:'POST',
			dataType: 'html',
			beforeSend:function(){
				$('#loading-mask').addClass('loader-mask');
			},
		}).done(function(){
			$('#loading-mask').removeClass('loader-mask');
		});

    });

});

function activaTab(tab){
	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
};

function detailInit(e) {
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: {
                    type:"POST",
                    dataType: "json",
                    data:{'document_number':e.data.CHR_jv_id},
                    url: '<?php echo site_url('jv/getdetail'); ?>',
                }
            },
            schema: {
                parse: function(response){
                    return response.data;
                },
                model: {
                    fields: {
                         INT_amount: { type: "number"},
                         INT_pph_amount: { type: "number"},
                         INT_pph_amount: { type: "number"},
                     }
                }
            },
        },
        pageable: false,
        scrollable: true,
        resizable: true,
        columns: [
            {field:"CHR_gl_account",width: 120, title:"GL Account",filterable: false},
            {field:"CHR_cost_center",width: 120, title:"Cost Center",filterable: false},
            {field:"VCH_internal_order",width: 120, title:"Internal Order",filterable: false,},
            {field:"INT_amount",width: 100, title:"Amount",filterable: false, template:'#= kendo.toString(INT_amount, "n0")#'},
            {field:"CHR_ppn_type",width: 100, title:"PPN Type",filterable: false, },
            {field:"INT_ppn_amount",width: 100, title:"PPN Amount",filterable: false, template:'#= kendo.toString(INT_ppn_amount, "n0")#'},
            {field:"INT_pph_amount",width: 100, title:"PPH Amount",filterable: false, template:'#= kendo.toString(INT_pph_amount, "n0")#'},
            {field:"VCH_text_desc",width: 100, title:"Description",filterable: false,},
            {field:"VCH_text_asign",width: 100, title:"Text Asign",filterable: false,},
        ]
    });
}
</script>


