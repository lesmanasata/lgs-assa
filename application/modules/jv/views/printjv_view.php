<?php $timestamp = time();?>
<h1 class="page-title">Journal Voucher - Print</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Journal Voucher </a></li>
  <li class="active"><strong>Print JV</strong></li>
</ol>

<div class="containers">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#contact_01"  data-toggle="tab">Print Jurnal Voucher</a></li>
        <li><a href="#contact_02" data-toggle="tab">Reprint Jurnal Voucher</a></li>
    </ul>
    <div class="tab-content grid-form1">
        <div class="tab-pane fade in active" id="contact_01">
        	<div class="padding_tab_body">
        		<form class="form-horizontal" id="form_print" >
        			<div class="form-group">
				      <label class="control-label col-sm-2" >Company Code:</label>
				      <div class="col-sm-3">
				        <input type="text" id="company_code" name="company_code" class="form-control kendodropdown" style="width:100%;" placeholder="&mdash;&mdash;<?php echo lang('dist_cmp_code'); ?>&mdash;&mdash;" >
				      </div>
				    </div>
                    <div class="form-group">
				      <label class="control-label col-sm-2" >Bisnis Area:</label>
				      <div class="col-sm-3">
				        <input type="text" id="bisnis_area" name="bisnis_area" class="form-control kendodropdown"  style="width:100%;" placeholder="&mdash;&mdash;Bisnis Area&mdash;&mdash;" >
				      </div>
				    </div>
                    <div class="form-group">
				      <label class="control-label col-sm-2" >Tanggal Pembayaran:</label>
				      <div class="col-sm-3">
				        <input type="text" id="tgl_bayar" name="tgl_bayar" class="form-control datepicker"  value="<?php echo date('Y-m-d');?>">
				      </div>
				    </div>
                    <div class="form-group">
				      <label class="control-label col-sm-2" >Vendor:</label>
				      <div class="col-sm-3">
				        <input type="text" id="vendor" name="vendor" class="form-control kendodropdown" style="width:100%;" placeholder="&mdash;&mdash;Vendor&mdash;&mdash;">
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-sm-offset-2 col-sm-10">
				        <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
				        <button type="button" id="ref" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
				      </div>
				    </div>
                    <div class="padding_tab_body">
                        <div id="item_print"></div>
        	        </div>
        </form>
        </div>
    </div>
    <div class="tab-pane fade" id="contact_02">
        <div class="padding_tab_body">
            <form class="form-horizontal" id="form_reprint">
                <div class="form-group">
                  <label class="control-label col-sm-2" >Document Print Number:</label>
                  <div class="col-sm-3">
                    <input type="text" id="document_number" name="document_number" class="form-control kendodropdown" style="width:100%;" placeholder="&mdash;&mdash;Document Print Number&mdash;&mdash;">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
                    <button type="button" id="ref" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
                  </div>
                </div>
                <div class="padding_tab_body">
                    <div id="item_reprint"></div>
                </div>
            </form>
    </div>
</div>

</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        <?php echo $js_cmb; ?>
        cmb['company_code'].value('-1');
		cmb['bisnis_area'].value('-1');
		cmb['vendor'].value('-1');
        cmb['document_number'].value('-1');
        $("#form_print").submit(function(e){
        	e.preventDefault();
        	$.ajax({
				url:'<?php echo site_url('jv/printjv/getPrint'); ?>',
				type:'POST',
				data:$(this).serialize(),
				dataType: 'html',
				beforeSend:function(){
					$('#item_print').addClass('loader');
				},success:function(res){
					$('#item_print').html(res);
				}
			}).done(function(){
				$('#item_print').removeClass('loader');
			});
        });
        $("#form_reprint").submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'<?php echo site_url('jv/printjv/getrePrint'); ?>',
                type:'POST',
                data:$(this).serialize(),
                dataType: 'html',
                beforeSend:function(){
                    $('#item_reprint').addClass('loader');
                },success:function(res){
                    $('#item_reprint').html(res);
                }
            }).done(function(){
                $('#item_reprint').removeClass('loader');
            });
        });
    });
</script>
<style type="text/css">

.container {
    margin-top: 10px;

}

.nav-tabs > li {
    position:relative;
}

.nav-tabs > li > a {
    display:inline-block;
}

.nav-tabs > li > span {
    font-size: 16px;
    box-sizing:border-box;
    border: 1px solid #505050;
   	background: #ddd;
    font-weight: bold;
    cursor:pointer;
    position:absolute;
    right: 6px;
    top: 8px;

    padding-left: 4px;
    padding-right: 4px;
    padding-bottom: 3px;
    border-radius: 4px;
}

.nav-tabs > li:hover > span {
    display: inline-block;
}
</style>
