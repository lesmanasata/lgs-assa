<html>
    <head>
      <title>Print JV</title>
    </head>
    <body>
      <table width="100%" border='0' style="border-collapse: collapse; ">
        <thead>
          <tr>
            <td colspan="9">
              <table width="100%" style="border-bottom: 1px solid #000; ">
                <tr>
                  <td>
                    <div id="logo">
                      <img src="<?php echo BASEPATH.'..'.DIRECTORY_SEPARATOR; ?>assets/images/logo.png">
                    </div>
                  </td>
                  <td>
                    <div id="company">
                      <h2 class="name">PT Adi Sarana Armada, Tbk</h2>
                      <div>Jln. Walisongo KM 9 RT 01 RW 02, Kel. Tambak Aji, Kec.Ngaliyan,</div>
                      <div>Semarang Barat. Tel: 024 7612333 Fax: 024 7611777</div>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="9" align="center"><h3><b><u>PERMOHONAN PEMBAYARAN<u></u></h3></td>
          </tr>
        </thead>
        <tbody>
          <tr>
             <td colspan="9"><b>Kepada :</b> <?php echo $vendor; ?></td>
          </tr>
        
                  <tr style="text-align: center; border-bottom: 1px solid #000; font-weight: bold; ">
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">No</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Keterangan</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">No Reff</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Nilai Non Service</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Nilai Service</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">PPN</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Nilai Invoice</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">PPh</td>
                    <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Jumlah</td>
                  </tr>
                  <?php $no =1; $NiNon=0; $NiSev =0; $niPpn =0; $nilInv=0; $nilPph=0; $amount=0; ?>
                  <?php  foreach ($item as $value){ ?>
                  <tr >
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; "><?php echo $no; ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; "><?php echo $value->Keterangan; ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; "><?php echo $value->Reff; ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $valnilNo= $value->Nilai_Non; $NiNon += $valnilNo; echo number_format($valnilNo); ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $amoNilsev=$value->Nilai_service; $NiSev += $amoNilsev; echo number_format($amoNilsev); ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $amoPpn = $value->PPN; $niPpn += $amoPpn; echo number_format($amoPpn); ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $amoInv = $value->Nilaiinvoice; $nilInv += $amoInv; echo number_format($amoInv); ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $amoPPh = $value->PPh; $nilPph += $amoPPh; echo number_format($amoPPh); ?></td>
                    <td style="padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php $amoJum = $value->Jumlah; $amount += $amoJum; echo number_format($amoJum); ?></td>
                  </tr>
                  <?php $no++; } ?>

                  <tr>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " colspan="3"><b>Total :</b> </td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($NiNon); ?></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($NiSev); ?></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($niPpn); ?></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($nilInv); ?></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($nilPph); ?></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($amount); ?></td>
                  </tr>
                  <tr>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " colspan="8"><b>Total Nilai Invoice :</b> </td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right"><?php echo number_format($amount); ?></td>
                  </tr>
                  <tr>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " colspan="8"><b>Materai</b></td>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " align="right">0</td>
                  </tr>
                  <tr>
                      <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #ddd; " colspan="9"><b>Terbilang :</b> <center># <?php echo terbilang($amount); ?>#</center></td>
                  </tr>

          <tr>
             <td colspan="9">
              <table width="100%">
                <tr>
                    <td>Pemohon,<br><br><br><br><br><br><br></td>
                    <td>Diperiksa,<br><br><br><br><br><br><br></td>
                    <td>Disetujui,<br><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td style="padding:5px;  border-bottom: 1px solid #000; " >Nama:</td>
                    <td style="padding:5px;  border-bottom: 1px solid #000; ">Nama:</td>
                    <td style="padding:5px;  border-bottom: 1px solid #000; ">Nama:</td>
                </tr>
                <tr>
                    <td style="padding:5px;  border-bottom: 0px solid #ddd; ">Jabatan: </td>
                    <td style="padding:5px;  border-bottom: 0px solid #ddd; ">Jabatan: </td>
                    <td style="padding:5px;  border-bottom: 0px solid #ddd; ">Jabatan: </td>
                </tr>
              </table>
            </td>            
          </tr>
        </tbody>
    </table>
  </body>
</html>




<style type="text/css">
  @font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  /*color: #555555;*/
  color: #000;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: SourceSansPro;
}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
float: left;
 margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
  float: right;
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}  
</style>