<?php $timestamp = time();?>
<div id="detail_sub<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {

		var ds_jv_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('jv/getdetail'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 INT_pph_amount: { type: "number"},
					 }
				}
			},
		});

		$("#detail_sub<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_jv_detail,
			pageable: false,
			scrollable: true,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{field:"CHR_gl_account",width: 150, title:"GL Account",filterable: false},
				{field:"CHR_cost_center",width: 200, title:"Cost Center",filterable: false},
				{field:"VCH_internal_order",width: 100, title:"Internal Order",filterable: false,},
				{field:"INT_amount",width: 200, title:"Amount",filterable: false},
				{field:"CHR_ppn_type",width: 100, title:"PPN Type",filterable: false, },
                {field:"INT_ppn_amount",width: 100, title:"PPN Amount",filterable: false, template:'#= kendo.toString(INT_ppn_amount, "n0")#'},
                {field:"INT_pph_amount",width: 100, title:"PPH Amount",filterable: false, template:'#= kendo.toString(INT_pph_amount, "n0")#'},
                {field:"VCH_text_desc",width: 100, title:"Description",filterable: false,},
                {field:"VCH_text_asign",width: 100, title:"Text Asign",filterable: false,},



            ]
		});

	});
</script>
