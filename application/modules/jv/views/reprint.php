<button type="button" name="button" id="reprint" class="btn btn-primary">REPRINT</button>
<div id="detailreprintjv<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	var checkedVals = [];
	$(document).ready(function () {

		var ds_reprint = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('jv/printjv/getdataReprint'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 INT_amount: { type: "number"},
						 DAT_posting_date: {  type: "date" },
					 }
				}
			},
		});

		$("#detailreprintjv<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_reprint,
			pageable: true,
			pageSize: 50,
			resizable: true,
			scrollable: true,
            dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{field:"CHR_id",width: 100, title:"ID",filterable: false},
                {field:"CHR_no_web",width: 100, title:"No Web",filterable: false},
                {field:"DAT_posting_date",width: 100, title:"Posting Date",filterable: false,format: "{0:dd-MM-yyyy}",parseFormats: ["MM/dd/yyyy"]},
				{field:"CHR_ref",width: 150, title:"No Referensi",filterable: false},
				{field:"INT_service_none",width: 150, title:"Nilai Non Service",filterable: false},
				{field:"INT_service",width: 150, title:"Nilai Service",filterable: false},
				{field:"INT_ppn",width: 150, title:"PPN",filterable: false},
				{field:"INT_nilai_invoice",width: 150, title:"Nilai Invoice",filterable: false},
                {field:"INT_PPh",width: 150, title:"PPh",filterable: false},
                {field:"INT_amount",width: 150, title:"Jumlah",filterable: false}

			]
		});
		$('#reprint').click(function() {
			var document_number = $('#form_reprint input[name=document_number]').val();
					$.ajax({
						url:'<?php echo site_url('jv/printjv/pdf_reprint'); ?>',
						data:'id='+document_number,
						type:'POST',
						dataType:'json',tail:1,
						success:function(res) {
							window.open(res.filename, '_blank');
							window.setTimeout('window.location.href="<?php echo site_url('jv/printjv/index');?>"', 1000);
						}
					});
        });

	});
    function toggle(source) {
      checkboxes = document.getElementsByClassName('id');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
    }
</script>
