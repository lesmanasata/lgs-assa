<div id="form" class="tab-pane fade in ">
  	<div class="padding_tab_body">
  		<form class="form-horizontal" id="billing_build<?php echo $time; ?>" method="POST">
			<div class="col-md-6">
				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Sales Org</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" readonly="readonly" value="<?php echo $header[0]->VCH_SalesOrg.'-'.$salesorg[0]->VCH_Description; ?>">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Payer</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" readonly="readonly" value="<?php echo $header[0]->VCH_Customer.'-'.$header[0]->VCH_Title.' '.$$header[0]->VCH_Name1; ?>">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Bill to Party</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" readonly="readonly" value="<?php echo $header[0]->VCH_Customer.'-'.$header[0]->VCH_Title.' '.$$header[0]->VCH_Name1; ?>">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Assignment Grp</label>
					<div class="col-sm-9">
						<input type="text" readonly="readonly" class="form-control" value="<?php echo $header[0]->VCH_AccountAssigment ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">NPWP</label>
					<div class="col-sm-9">
						<input type="text" readonly="readonly" class="form-control" value="<?php echo $header[0]->VCH_NPWP; ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Nett</label>
				    <div class="col-sm-4">
				      	<input type="text" readonly="readonly" class="form-control"  value="<?php echo number_format($header[0]->BIG_Net); ?>" >
				    </div>
					<label  class="col-sm-1 control-label hor-form">Tax</label>
				    <div class="col-sm-4">
				      <input type="text" readonly="readonly" class="form-control" value="<?php echo number_format($header[0]->BIG_Tax); ?>" >
				    </div>
			  	</div>

			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Tax Class C.</label>
					<div class="col-sm-9">
						<input type="text"  class="form-control " readonly value="<?php echo $tax_clas[0]->CHR_Type.'-'.$tax_clas[0]->INT_Amount.' '.$tax_clas[0]->CHR_Description; ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Payment Terms</label>
					<div class="col-sm-9">
						<input type="text"  readonly class="kendodropdown form-control" value="<?php echo $payment_term[0]->CHR_Type.'-'.$payment_term[0]->CHR_Description; ?>" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Billing Date</label>
					<div class="col-sm-9">
						<input type="text" readonly value="<?php echo $header[0]->DAT_BillingDate; ?>"  >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Materai Fee</label>
					<div class="col-sm-4">
						<input type="number"  class="form-control" value="<?php echo number_format($header[0]->BIG_MateraiFee); ?>" readonly>
					</div>
					<label  class="col-sm-4 materai_fee_label" id="materai" style="font-weight: bold; font-size: 16px;"></label>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Uraian</label>
					<div class="col-sm-9">
						<input type="text"  class="form-control" readonly="readonly" value="<?php echo $header[0]->VCH_Uraian; ?>">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Keterangan</label>
					<div class="col-sm-9">
						<textarea readonly="readonly" class="form-control" ><?php echo $header[0]->VCH_Uraian; ?></textarea>
					</div>
				</div>
			</div>

			<div style="clear:both; ">&nbsp;</div>
			<div class="grid-form">
				<div class="col-sm-12">
					<div id="bil_tbl_item<?php echo $time; ?>"></div>
				</div>
			</div>

			<div style="clear:both; ">&nbsp;</div>
	        <div class="panel-footer">
	          <div class="row">
	            <div style="float: right; padding-right: 15px;">
	            	<button type="button" id="cancel" class="batal btn btn-danger">Close</button>
	            </div>
	          </div>
	        </div>
		</form>


	</div>
  </div>

  <script type="text/javascript">
  	$(document).ready(function(){
  		var ds_distribute_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode(array('id'=>$header[0]->VCH_BillingDocumentWeb)); ?>,
					url: '<?php echo site_url('billing/get_billing_detail'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 INT_amount: { type: "number"},
					 }
				}
			},
		});

		$("#bil_tbl_item<?php echo $time; ?>").kendoGrid({
			dataSource: ds_distribute_detail,
			pageable: false,
			scrollable: true,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{field:"CHR_SalesDocument",width: 150, title:"Lampiran LT",filterable: false},
				{field:"CHR_ItemSD",width: 150, title:"Item SD",filterable: false},
				{field:"CHR_BillingPlanNumber",width: 150, title:"Billplan Number",filterable: false},
				{field:"CHR_Material",width: 200, title:"Material ID",filterable: false},
				{field:"DAT_SettlementDate",width: 150, title:"From",filterable: false},
				{field:"DAT_SettlemetDateDeadline",width: 150, title:"To",filterable: false},
				{field:"INT_amount",width: 100, title:"<?php echo lang('dist_amount'); ?>",filterable: false, template:'#= kendo.toString(INT_amount, "n0")#'},
			]
		});
  	});
  </script>