<?php $timestamp = time();?>

<div id="billing_tbl<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	$(document).ready(function(){
		var ds_billing = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($form_select); ?>,
					url: '<?php echo site_url('billing/get_tbl_select'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},model: {
					fields: {
						harga: { type: "number"},
						cp: { type: "text"},
					}
				},
			},
			pageSize: 100,
		});

		$("#billing_tbl<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_billing,
			pageable: true,
			sortable: true,
			height:500,
			resizable: true,
			scrollable: true,
			selectable:"multiple",
			dataBound: function(e) {
				for (var i=0; i<cekedId.length; i++) {
	            	var mId = cekedId[i];
	            	$("#"+mId).prop("checked", true);
	        	}
			},
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			columns: [
				{
					//title:"<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",
					title:"SELECT",
					width:80,
					template: '<input type="checkbox" class="id" name="id[]" id="#:id#" data-marketing="#:cp#" data-id="#:id#" data-asggroup="#:asg_group#" data-lt="#:lampiran_lt#" data-customer="#:customerId#" data-bildate="#:per_from#" onclick="$(this).cek_val(this.id);">',
				},{
					title:"Customer Id",
					width:150,
					field:"customerId"
				},{
					title:"Cust. Group",
					width:130,
					field:"asg_group"
				},{
					title:"Customer Name",
					width:300,
					field:"customer_name"
				},{
					title:"Lampiran LT",
					width:150,
					field:"lampiran_lt"
				},{
					title:"Material",
					width:300,
					field:"matrial"
				},{
					title:"Period From",
					width:150,
					field:"per_from"
				},{
					title:"Period To",
					width:150,
					field:"per_to"
				},{
					title:"Plat Nomor",
					width:150,
					field:"item_type"
				},{
					title:"Harga",
					width:150,
					field:"harga",
					attributes:{style:'text-align:right'},
					template:'#= kendo.toString(harga, "n0")#'
				},{
					title:"Marketing",
					width:150,
					field:"marketing",
				}
			]
		});




	});
</script>
