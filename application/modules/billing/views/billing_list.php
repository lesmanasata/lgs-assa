<?php $token = time('his'); ?> 
<h1 class="page-title">Billing - List</h1> 
<ol class="breadcrumb breadcrumb-2">  
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>  
  <li><a href="#">Billing</a></li>  
  <li class="active"><strong>List</strong></li>  
</ol>  
 
<div class="containers">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#contact_01"  data-toggle="tab">Selection  Biling List</a></li>

    </ul>
    <div class="tab-content grid-form1">
        <div class="tab-pane fade in active" id="contact_01">
          <div class="padding_tab_body">
              
              <div class="grid-form"> 
                <div class="grid-form1"> 
                    <form class="form-horizontal" id="list_billing_<?php echo $token; ?>" method="POST"> 
                       <div class="col-md-6"> 

                          <div class="form-group " >
                              <label class="control-label col-sm-3" >Customer Name :</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" style="width:100%;" name="customer"  id="cust_name_d">
                              </div>
                          </div>
             
                          <div class="form-group" > 
                            <label class="control-label col-lg-3" >Periode :</label> 
                            <div class="col-sm-4"> 
                              <input type="number" onkeypress='numberOnly(event)' name="tahun" class="form-control" value="<?php echo date('Y'); ?>" placeholder="<?php echo lang('rpt_find_yer'); ?>" > 
                            </div> 
                            <div class="col-sm-5"> 
                              <input type="text"  name="bulan" style="width: 100%" class=" form-control"  placeholder="<?php echo lang('rpt_find_month'); ?>" > 
                            </div> 
                          </div> 
             
                            <div class="form-group"> 
                              <label class="control-label col-sm-3" ></label> 
                              <div class="col-sm-5"> 
                                 <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button> 
                              </div> 
                            </div> 
                        </div>
                        <div class="col-md-6" style="padding-left: 80px;">
                          <div class="alert alert-success">
                            <h2>Info:</h2>
                            <span class="badge label-primary" >w</span> 
                            <strong>Waiting</strong> Doc Billing masih dalam antrian pengiriman.<br>

                            <span class="badge label-warning" >U</span>
                            <strong>Uploaded</strong> Pengiriman Doc. Billing Ke SAP Berhasil.<br>
                            
                             <span class="badge label-success" >C</span>
                             <strong>Complete</strong> Doc. Billing siap untuk proses selanjutnya.<br>
                          </div>
                        </div>
                     </form> 
                </div> 
            </div> 
             
            <div style="clear:both;">&nbsp;</div> 
            <div class="row"> 
              <div class="col-sm-12"> 
                <div class="padding_tab_body"> 
                  <div id="item_row<?php echo $token; ?>"></div> 
                </div> 
              </div> 
            </div> 

          </div>
        </div>
    </div>
</div>




 
<script type="text/javascript"> 
  $(document).ready(function(){ 
    <?php echo $js_cmb; ?> 
     //cmb['customer'].value('-1');
    $('#list_billing_<?php echo $token; ?>').submit(function(e){ 
      e.preventDefault(); 
      $.ajax({ 
             url:'<?php echo site_url('billing/show_item'); ?>', 
              type:'POST', 
              data:$(this).serialize()+'&token=<?php echo $token; ?>', 
              dataType: 'html', 
              beforeSend:function(){ 
                $('#item_row<?php echo $token; ?>').addClass('loader'); 
              },success:function(res){ 
                $('#item_row<?php echo $token; ?>').html(res); 
              } 
          }).done(function(){ 
              $('#item_row<?php echo $token; ?>').removeClass('loader'); 
          }); 
    }); 
 
     
    var cmbBulan = $('#list_billing_<?php echo $token; ?> input[name=bulan]') 
          .kendoDropDownList({ 
            autoBind: true, 
            dataTextField: "text", 
            dataValueField: "id", 
            dataSource: { 
              serverFiltering: true, 
              transport: { 
                read: { 
                  dataType: "jsonp", 
                  url: "<?php echo site_url('app/getcmbBulan'); ?>", 
                } 
              } 
            } 
        }); 


    var cmbCustomer = $('#cust_name_d')
          .kendoDropDownList({
            autoBind: false,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_customer'); ?>",
                }
              }
            }
        });
    var dropdownlist_cus_name = $("#cust_name_d").data("kendoDropDownList");
    dropdownlist_cus_name.value('-1');

    $.fn.show_detail=function(){
        var url = $(this).data("url");
        var title = $(this).data("id");
        var id = $(".nav-tabs").children().length;
        $('.nav-tabs').append('<li><a href="#contact_'+id+'">'+title+'  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;</a> <span class="close">x</span> </li>');
        $('.tab-content').append('<div class="tab-pane" id="contact_'+id+'"></div>');

        $.ajax({
          url:url,
          type:'POST',
          dataType: 'html',
          beforeSend:function(){
            $('#loading-mask').addClass('loader-mask');
          },success:function(res){
            $("#contact_"+id).html(res);
          }
        }).done(function(){
          $('#loading-mask').removeClass('loader-mask');
        });

        $('.nav-tabs a[href="#contact_' + id + '"]').trigger('click');
    }

    $(".nav-tabs").on("click", "a", function(e){
          e.preventDefault();
          $(this).tab('show');
    }).on("click", "span", function () {
            var anchor = $(this).siblings('a');
            $(anchor.attr('href')).remove();
            $(this).parent().remove();
            $(".nav-tabs li").children('a').first().click();
    });


  }); 
</script>

<style type="text/css">

.container {
    margin-top: 10px;

}

.nav-tabs > li {
    position:relative;
}

.nav-tabs > li > a {
    display:inline-block;
}

.nav-tabs > li > span {
    font-size: 16px;
    box-sizing:border-box;
    border: 1px solid #505050;
    background: #ddd;
    font-weight: bold;
    cursor:pointer;
    position:absolute;
    right: 6px;
    top: 8px;

    padding-left: 4px;
    padding-right: 4px;
    padding-bottom: 3px;
    border-radius: 4px;
}

.nav-tabs > li:hover > span {
    display: inline-block;
}
</style>