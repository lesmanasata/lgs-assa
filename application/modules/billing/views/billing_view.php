<?php $token = time('his'); ?>
<h1 class="page-title">Billing - Form</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Billing</a></li>
  <li class="active"><strong>Form</strong></li>
</ol>


<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Selection</a></li>
  <li id="form_link"><a data-toggle="tab" href="#form" id="padding_tab_link">Create Billing</a></li>
</ul>

<div class="tab-content grid-form1" style="border-top:0px;">
  <div id="home" class="tab-pane fade in active ">
  	<div class="padding_tab_body">
		<form class="form-horizontal" id="bilform<?php echo $token; ?>" method="POST">
			<div class="col-md-9">

				<div class="form-group " >
				    <label class="control-label col-sm-3" >Customer Name :</label>
				    <div class="col-sm-9">
				    	<input type="text" class="form-control" style="width:100%;" name="cust_name"  id="cust_name_d">
				    </div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Document Type</label>
					<div class="col-sm-9">
						<input type="text" class="kendodropdown form-control" style="width:100%;" name="doc_type"   placeholder="&mdash;&mdash;Select All Document &mdash;&mdash;">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Sales Org</label>
					<div class="col-sm-9">
						<input type="text" class="kendodropdown form-control" style="width:100%;" name="sales_org"   placeholder="&mdash;&mdash;Select All Sales Org&mdash;&mdash;">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Billing Period</label>
					<div class="col-sm-9">
						<div class="col-sm-5">
							<input class="startDate form-control" name='date_start' style="width: 100%;" value="<?php echo $date_cutof; ?>" />
						</div>
						<div class="col-sm-1"> <b>s/d</b></div>
						<div class="col-sm-6">
							<input class="endDate form-control" name='date_end' style="width: 100%;" value="<?php echo date('d-m-Y'); ?>"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form"></label>
					<div class="col-sm-9">
						<button type="submit" class="btn btn-primary">Proses Data</button>
					</div>
				</div>
			</div>
		</form>

		<div style="clear:both; ">&nbsp;</div>
		<div class="grid-form">
			<div class="col-sm-12">
				<div align="right">
					<button type="button" id="cbill" class="btn btn-primary">
						<i class="fa fa-upload" aria-hidden="true"></i> Create Billing
					</button>
				</div>
				<div style="clear:both; ">&nbsp;</div>

				<div id="bil_tbl<?php echo $token; ?>"></div>
			</div>
		</div>
	</div>
  </div>

  <div id="form" class="tab-pane fade in ">
  	<div class="padding_tab_body">
  		<form class="form-horizontal" id="billing_build<?php echo $token; ?>" method="POST">
			<div class="col-md-6">
				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Sales Org</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="sales_orgx" readonly="readonly">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Payer</label>
					<div class="col-sm-9">
						<input type="text" name="payer" class="form-control" readonly="readonly" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Bill to Party</label>
					<div class="col-sm-9">
						<input type="text" name="bill_to_party" class="form-control" readonly="readonly" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Up</label>
					<div class="col-sm-9">
						<input type="text" name="up" readonly="readonly" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Assignment Grp</label>
					<div class="col-sm-9">
						<input type="text" readonly="readonly" class="form-control" name="assign_group" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">NPWP</label>
					<div class="col-sm-9">
						<input type="text" readonly="readonly" class="form-control" name="npwp" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Nett</label>
				    <div class="col-sm-4">
				      	<input type="text" readonly="readonly" class="form-control" name="net" >
				    </div>
					<label  class="col-sm-1 control-label hor-form">Tax</label>
				    <div class="col-sm-4">
				      <input type="text" readonly="readonly" class="form-control" name="tax" >
				    </div>
			  	</div>

<!-- 			  	<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Lampiran LT</label>
					<div class="col-sm-9">
						<input type="text" readonly="readonly" class="form-control" name="lt" >
					</div>
				</div> -->
			</div>

			<div class="col-md-6">

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Tax Class C.</label>
					<div class="col-sm-9">
						<input type="text"  class="form-control kendodropdown" style="width: 100%;" name="tax_cust" >
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Payment Terms</label>
					<div class="col-sm-9">
						<input type="text"  class="kendodropdown form-control" name="payment_terms"  style="width: 100%;">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Billing Date</label>
					<div class="col-sm-9">
						<input type="text" name="billing_date"  class="billing_date form-control" value="<?php echo date('d-m-Y'); ?>">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Materai Fee</label>
					<div class="col-sm-4">
						<input type="number" name="materai_fee"  class="form-control">
					</div>
					<label  class="col-sm-4 materai_fee_label" id="materai" style="font-weight: bold; font-size: 16px;"></label>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Uraian</label>
					<div class="col-sm-9">
						<input type="text" name="uraian" id="uraian" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label hor-form">Keterangan</label>
					<div class="col-sm-9">
						<textarea name="keterangan" id="keterangan" class="form-control" > </textarea>
					</div>
				</div>
			</div>

			<div style="clear:both; ">&nbsp;</div>
			<div class="grid-form">
				<div class="col-sm-12">
					<div id="bil_tbl_item<?php echo $token; ?>"></div>
				</div>
			</div>

			<div style="clear:both; ">&nbsp;</div>
	        <div class="panel-footer">
	          <div class="row">
	            <div class="col-sm-8 col-sm-offset-2">
	            <button type="button" id='calculate' class="btn btn-warning">Calculate</button>
	            <button type="submit" class="btn btn-primary navbar-btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Billing" id="submitbtn-<?php echo $token; ?>"><?php echo lang('btn_save'); ?></button>
	            <button type="button" id="cancel" class="batal btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
	            </div>
	          </div>
	        </div>
		</form>


	</div>
  </div>
</div>


<script>
	var no_urut			= 0;
	var cekedId			= [];
	var dateceked 		= '';
	var customerceked 	= '';
	var merketing 		= '';
	var asgGroup 		= '';
	var grouplt 		= '';
	var muid 			= '';
$(document).ready(function(){
    $("#cancel").click(function(){
        $('.nav-tabs a[href="#home"]').tab('show');
        $('#billing_build<?php echo $token; ?> input[name=sales_orgx]').val('');
        $('#billing_build<?php echo $token; ?> input[name=payer]').val('');
        $('#billing_build<?php echo $token; ?> input[name=bill_to_party]').val('');
        $('#billing_build<?php echo $token; ?> input[name=up]').val('');
        $('#billing_build<?php echo $token; ?> input[name=assign_group]').val('');
        $('#billing_build<?php echo $token; ?> input[name=npwp]').val('');
        $('#billing_build<?php echo $token; ?> input[name=net]').val('');
        $('#billing_build<?php echo $token; ?> input[name=tax]').val('');
        $('#billing_build<?php echo $token; ?> input[name=tax_cust]').val('');
        $('#billing_build<?php echo $token; ?> input[name=payment_terms]').val('');
        $('#billing_build<?php echo $token; ?> input[name=materai_fee]').val('');
        $('#billing_build<?php echo $token; ?> input[name=uraian]').val('');
        $('#billing_build<?php echo $token; ?> textarea[name=keterangan]').val('');
        $('#materai').html('');
        $("#form_link").hide();
    });
	<?php echo $js_cmb; ?>
	//cmb['cust_name'].value('-1');
	cmb['doc_type'].value('-1');
	cmb['sales_org'].value('-1');
	$("#form_link").hide();
	$('#cbill').hide();
	$('#billing_build<?php echo $token; ?> button[type=submit]').attr('disabled', true);

	$('#billing_build<?php echo $token; ?> input[name=materai_fee]').keyup(function(){
		var val = $('#billing_build<?php echo $token; ?> input[name=materai_fee]').val();
		$('.materai_fee_label').html(numberWithCommas(val));
	})

	$('#bilform<?php echo $token; ?>').submit(function(e){
		e.preventDefault();
		cekedId			= [];
		dateceked 		= '';
		customerceked 	= '';
		merketing 		= '';
		asgGroup 		= '';
		grouplt 		= '';
		muid 			= '';
		$.ajax({
			url:'<?php echo site_url('billing/show_grid'); ?>',
			type:'POST',
			data:$(this).serialize(),
			dataType: 'html',
			beforeSend:function(){
				$('#bil_tbl<?php echo $token; ?>').addClass('loader');
			},success:function(res){
				$('#bil_tbl<?php echo $token; ?>').html(res);
				$('#cbill').show();
				$('.materai_fee_label').html('');
			}
		}).done(function(){
			$('#bil_tbl<?php echo $token; ?>').removeClass('loader');
		});
	});


	var ds_billing_item = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					url: '<?php echo site_url('billing/get_tobill_item'); ?>',
				},create: {
					url: '<?php echo site_url('billing/submitdata'); ?>',
					type:"POST",
					dataType: "json",
					data: function() {
						return JSON.parse(JSON.stringify( $(this).hiderForm() ));
					},complete: function(res) {
						var res_msg = JSON.parse(res.responseText);
						msg_box(res_msg.messages,['btnOK'],'Info!');
						$('#submitbtn-<?php echo $token; ?>').button('reset');
						if(res_msg.status){
							dateceked 		= '';
							customerceked 	= '';
							merketing 		= '';
							asgGroup 		= '';
							grouplt 		= '';
							muid 			= '';
							cekedId			= [];
							$('#bil_tbl<?php echo $token; ?>').html('');
							$('#cbill').hide();
							$(this).clear_form();
							$("#form_link").hide();
							$('.materai_fee_label').html('');
							$('.nav-tabs a[href="#home"]').tab('show');
							$('#billing_build<?php echo $token; ?> button[type=submit]').attr('disabled', true);
						}
					}
				}
			},
			batch: true,
			schema: {
				parse: function(response){
					return response.data;
				},model: {
					fields: {
						t_amount: { type: "number"},
						nomor_dokumen: { editable: false },
						item_sd: { editable: false },
						billplan_number: { editable: false },
						material: { editable: false },
						item_billing: { editable: false },
						date_from: { editable: false },
						date_to: { editable: false },
						plat_number: { editable: false },
					}
				},
			},
		});

	$.fn.hiderForm = function() {
		out = {
			'header_sales_orgx'		:  $('#billing_build<?php echo $token; ?> input[name=sales_orgx]').val(),
			'header_payer'			:  $('#billing_build<?php echo $token; ?> input[name=payer]').val(),
			'header_bill_to_party'	:  $('#billing_build<?php echo $token; ?> input[name=bill_to_party]').val(),
			'header_up'				:  $('#billing_build<?php echo $token; ?> input[name=up]').val(),
			'header_assign_group'	:  $('#billing_build<?php echo $token; ?> input[name=assign_group]').val(),
			'header_tax_cust'		:  $('#billing_build<?php echo $token; ?> input[name=tax_cust]').val(),
			'header_npwp'			:  $('#billing_build<?php echo $token; ?> input[name=npwp]').val(),
			'header_payment_terms' 	:  $('#billing_build<?php echo $token; ?> input[name=payment_terms]').val(),
			'header_net' 			:  $('#billing_build<?php echo $token; ?> input[name=net]').val(),
			'header_tax' 			:  $('#billing_build<?php echo $token; ?> input[name=tax]').val(),
			'header_billing_date' 	:  $('#billing_build<?php echo $token; ?> input[name=billing_date]').val(),
			'header_uraian' 		:  $('#billing_build<?php echo $token; ?> input[name=uraian]').val(),
			'header_keterangan' 	:  $('#billing_build<?php echo $token; ?> textarea[name=keterangan]').val(),
			'materai_fee' 			:  $('#billing_build<?php echo $token; ?> input[name=materai_fee]').val(),
			'lampiran_lt' 			:  $('#billing_build<?php echo $token; ?> input[name=lt]').val()
		}
		return out;
	}

	$.fn.clear_form = function() {
		$('#billing_build<?php echo $token; ?> input[name=sales_orgx]').val('');
		$('#billing_build<?php echo $token; ?> input[name=payer]').val('');
		$('#billing_build<?php echo $token; ?> input[name=bill_to_party]').val('');
		$('#billing_build<?php echo $token; ?> input[name=up]').val('');
		$('#billing_build<?php echo $token; ?> input[name=assign_group]').val('');
		$('#billing_build<?php echo $token; ?> input[name=npwp]').val('');
		$('#billing_build<?php echo $token; ?> input[name=net]').val('');
		$('#billing_build<?php echo $token; ?> input[name=tax]').val('');
		$('#billing_build<?php echo $token; ?> input[name=billing_date]').val('');
		$('#billing_build<?php echo $token; ?> input[name=uraian]').val('');
		$('#billing_build<?php echo $token; ?> textarea[name=keterangan]').val('');
		$('#billing_build<?php echo $token; ?> input[name=materai_fee]').val('');
		$('#billing_build<?php echo $token; ?> input[name=lt]').val('');
	}

	$("#bil_tbl_item<?php echo $token; ?>").kendoGrid({
			dataSource: ds_billing_item,
			pageable: {
                refresh: true,
                pageSizes: true,
            },
			sortable: true,
			resizable: true,
			scrollable: true,
			editable: true,
			dataBound: function(e) {
				$(this).countNetTax();
			},
			change : function(){
				$(this).countNetTax();
			},
			selectable:"multiple",
			filterable: false,
			columns: [
				{
					title:"Lampiran LT",
					width:80,
					field:"nomor_dokumen"
				},{
					title:"Item SD",
					width:80,
					field:"item_sd"
				},{
					title:"Billplan Number",
					width:110,
					field:"billplan_number"
				},{
					title:"Item Billing",
					width:100,
					field:"item_billing"
				},{
					title:"Material",
					width:250,
					field:"material"
				},{
					title:"Plat Nomor",
					width:100,
					field:"plat_number"
				},{
					title:"From",
					width:80,
					field:"date_from"
				},{
					title:"To",
					width:80,
					field:"date_to"
				},{
					title:"Total Amount",
					width:150,
					field:"t_amount",
					attributes:{class:'change_falue',style:'text-align:right'},
					template:'#= kendo.toString(t_amount, "n0")#'
				}
			]
		});


		$('#billing_build<?php echo $token; ?>').submit(function(e){
			e.preventDefault();
  			$('#submitbtn-<?php echo $token; ?>').button('loading');

			ds_billing_item.sync();
		});

		$.fn.countNetTax=function(){
			grid = $("#bil_tbl_item<?php echo $token; ?>").data("kendoGrid");
			var row_data = grid.dataSource._data;
			var tax    = 0;
			var amount = 0;
			var val_tax = cmb['tax_cust'].text();
			var ar_tax = val_tax.split('-');
			$.each(row_data,function(row_item){
				amount = parseInt(amount) + parseInt(row_data[row_item].t_amount);
			});

			if(ar_tax.length > 0)
				tax = Math.round(parseInt(ar_tax[0]) * parseInt(amount) / 100);
			else
				tax = Math.round(parseInt(0) * parseInt(amount) / 100);
			$('#billing_build<?php echo $token; ?> button[type=submit]').attr('disabled', true);
			$('#billing_build<?php echo $token; ?> input[name=net]').val(numberWithCommas(amount));
			$('#billing_build<?php echo $token; ?> input[name=tax]').val(numberWithCommas(tax));
		}
		cmb['tax_cust'].bind('change',function(){
			$('#billing_build<?php echo $token; ?> button[type=submit]').attr('disabled', true);
		});

		$('#calculate').click(function(){
			$(this).countNetTax();
			$('#billing_build<?php echo $token; ?> button[type=submit]').attr('disabled', false);

		});

		$.fn.cek_val=function(){
			var uid 		= $(this).data('id');
			var dateSelect	= $(this).data('bildate');
			var customerSelect	= $(this).data('customer');
			var merketingSelect	= $(this).data('marketing');
			var asgGroupSelect	= $(this).data('asggroup');
			var groupltSelect	= $(this).data('lt');

			if(document.getElementById(uid).checked) {
			    var addCekCustomer 	= true;
			    var addCekDate 		= true;
			    var addCekMarketing	= true;
			    var addAsgGroup		= true;
			    var addPartnerFunc	= true;

			    if(muid ==''){
			    	muid = uid;
			    }

			    if(dateceked ==''){
			    	dateceked = dateSelect;
			    }

			    if(grouplt==''){
			    	grouplt =groupltSelect;
			    }

			    if(asgGroup ==''){
			    	asgGroup = asgGroupSelect;
			    }

			    if(customerceked ==''){
			    	customerceked = customerSelect;
			    }

			    if(merketing ==''){
					merketing = merketingSelect;
			    }

			    do{
				    if(asgGroup !=''){
				    	if(asgGroupSelect != asgGroup){
				    		addAsgGroup = false;
				    		$("#"+uid).prop("checked", false);
				    		msg_box('Cust Group harus sama',['btnOK'],'Info!');
				    		break; 
				    	}
				    }

				    if(dateceked !=''){
				    	if(dateSelect != dateceked){
				    		addCekDate = false;
				    		$("#"+uid).prop("checked", false);
				    		msg_box('Tgl Periode From harus sama',['btnOK'],'Info!');
				    		break; 
				    	}
				    }

				    if(merketing !=''){
				    	if(merketingSelect != merketing){
				    		addCekMarketing = false;
				    		$("#"+uid).prop("checked", false);
				    		msg_box('Marketing harus sama',['btnOK'],'Info!');
				    	}
				    }

				    if(customerceked !=''){
				    	if(customerSelect != customerceked){
				    		$("#"+uid).prop("checked", false);
				    		addCekCustomer = false;
				    		msg_box('Customer harus sama',['btnOK'],'Info!');
				    		break; 
				    	}
				    }
				
				    if(muid !=''){
					    $.ajax({
							url:'<?php echo site_url('billing/biilcek_partner_function'); ?>',
							type:'POST',
							data:{'master':muid, 'compare':uid},
							dataType: 'json',
							success:function(res){
								if(res.status){
									addPartnerFunc=false;
									$("#"+uid).prop("checked", false);
									msg_box('Partner Function tidak sama',['btnOK'],'Info!');
								}
							}
						});
					}

			    }while(false);

			    if(addCekCustomer==true && addCekDate ==true && addAsgGroup==true && addPartnerFunc==true){
			    	cekedId.push(uid);
			    }

			} else {
			    var remove_index = cekedId.indexOf(uid);
			    cekedId.splice(remove_index);

			    if(cekedId.length < 1){
			    	dateceked 		= '';
			    	asgGroup 		= '';
			    	merketing 		= '';
			    	customerceked 	= '';
			    	grouplt 		= '';
			    	muid 			= '';
			    }
			}

		}

		$('#cbill').click(function(){
			if(cekedId.length > 0){
				$.ajax({
					url:'<?php echo site_url('billing/set_to_billing'); ?>',
					type:'POST',
					data:'id='+cekedId,
					dataType: 'json',
					beforeSend:function(){
						$('#loading-mask').addClass('loader-mask');
					},success:function(res){
						$('#loading-mask').removeClass('loader-mask');
						
						ds_billing_item.read();
						if(res.status){
							$('#billing_build<?php echo $token; ?> input[name=sales_orgx]').val(res.data[0].sales_org);
							$('#billing_build<?php echo $token; ?> input[name=payer]').val(res.data[0].party);
							$('#billing_build<?php echo $token; ?> input[name=bill_to_party]').val(res.data[0].party);
							$('#billing_build<?php echo $token; ?> input[name=up]').val(res.data[0].up);
							$('#billing_build<?php echo $token; ?> input[name=npwp]').val(res.data[0].npwp);

							$('#billing_build<?php echo $token; ?> input[name=net]').val(res.data[0].net);
							$('#billing_build<?php echo $token; ?> input[name=tax]').val(res.data[0].tax);
							$('#billing_build<?php echo $token; ?> input[name=assign_group]').val(res.data[0].chr_group);
							$('#billing_build<?php echo $token; ?> input[name=lt]').val(res.data[0].sales_documet);
							cmb['tax_cust'].value(res.data[0].classifikasi);
							cmb['payment_terms'].value(res.data[0].term);
							var dropdownlist = $('#billing_build<?php echo $token; ?> input[name=payment_terms]').data("kendoDropDownList");
							dropdownlist.readonly();
						}
					}
				}).done(function(){
					$('#loading-mask').removeClass('loader-mask');
				});

				$("#form_link").show();
				$('.nav-tabs a[href="#form"]').tab('show');

			}else{
				msg_box('No Data Selected',['btnOK'],'Info!');
			}
		});

	$(".billing_date").kendoDatePicker({
		format: "dd-MM-yyyy",
		min: new Date(<?php echo date('Y'); ?>, <?php echo date('m')-1; ?>, <?php echo date('d'); ?>)
	});



	var cmbCustomer = $('#cust_name_d')
          .kendoDropDownList({
            autoBind: false,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_customer'); ?>",
                }
              }
            }
        });
    var dropdownlist_cus_name = $("#cust_name_d").data("kendoDropDownList");
    dropdownlist_cus_name.value('-1');
    //$('#cust_name_d').data("kendoDropdownList").value("-1");
    //cmbCustomer.value('-1');

	function startChange() {
        var startDate = start.value(),
        endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
        startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var start = $(".startDate").kendoDatePicker({
        change: startChange,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    var end = $(".endDate").kendoDatePicker({
        change: endChange,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    start.max(end.value());
    end.min(start.value());
});

</script>
