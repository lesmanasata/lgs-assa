<?php $timestamp = time();?>
<div id="distribute_detail_sub<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {

		var ds_distribute_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('billing/get_billing_detail'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 INT_amount: { type: "number"},
					 }
				}
			},
		});

		$("#distribute_detail_sub<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_distribute_detail,
			pageable: false,
			scrollable: true,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{field:"CHR_SalesDocument",width: 150, title:"Lampiran LT",filterable: false},
				{field:"CHR_ItemSD",width: 150, title:"Item SD",filterable: false},
				{field:"CHR_BillingPlanNumber",width: 150, title:"Billplan Number",filterable: false},
				{field:"CHR_Material",width: 150, title:"Material ID",filterable: false},
				{field:"DAT_SettlementDate",width: 150, title:"From",filterable: false},
				{field:"DAT_SettlemetDateDeadline",width: 150, title:"To",filterable: false},
				{field:"INT_amount",width: 100, title:"<?php echo lang('dist_amount'); ?>",filterable: false, template:'#= kendo.toString(INT_amount, "n0")#'},
			]
		});

	});
</script>
