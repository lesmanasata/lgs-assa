<?php $timestamp = time();?>

<div id="billing_list_table<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	$(document).ready(function(){

		var ds_billing_list = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($filter); ?>,
					url: '<?php echo site_url('billing/get_billing_data'); ?>',
				}
			},
			serverPaging: true,
			schema: {
				data: "data",
				total: "total",
				model: {
					fields: {
						BIG_Net: { type: "number"},
						BIG_Tax: { type: "number"},
					}
				},
			},
			pageSize: 50,
		});

		$("#billing_list_table<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_billing_list,
			pageable: {
		        refresh: true,
		        pageSizes: true,
		        buttonCount: 5
		    },
			scrollable: true,
			height: 550,
			rowTemplate: kendo.template($("#distribute_table-row-<?php echo $timestamp; ?>").html()),
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			/*dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},*/
			columns: [
				{
					title:"ACTION",
					filterable: false,
					//template: "<a href='javascript:void(0)'  onclick='$(this).show_detail();' data-url='<?php echo site_url('rpt/detail/itemdetail'); ?>/#: VCH_BillingDocumentWeb #' data-id='#: VCH_BillingDocumentWeb #'><button class='btn btn-xs btn-info'> <i class='glyphicon glyphicon-pencil'></i> Detail</button></a>",
					width: 100,
				},{
					title:"BILL WEB",
					width:150,
					field:"VCH_BillingDocumentWeb"
				},{
					title:"Document SAP",
					width:150,
					field:"status_sap"
				},{
					title:"BILL DATE",
					width:130,
					field:"DAT_BillingDate"
				},{
					title:"ACC ASSIGMENT",
					width:170,
					field:"VCH_AccountAssigment"
				},{
					title:"SALES ORG",
					width:150,
					field:"VCH_SalesOrg"
				},{
					title:"CUSTOMER",
					width:300,
					field:"Customer" 
				},{
					title:"NPWP",
					width:100,
					field:"VCH_NPWP"
				},{
					title:"NET PRICE ",
					width:180,
					attributes:{style:'text-align:right'},
					//template:'#= kendo.toString(BIG_Net, "n0")#',
					field:"BIG_Net"
				},{
					title:"TAX AMMOUNT",
					width:180,
					attributes:{style:'text-align:right'},
					//template:'#= kendo.toString(BIG_Tax, "n0")#',
					field:"BIG_Tax"
				}
			] 
		});

		$.fn.getitem=function(){
			var reqID = $(this).data('id');
			var tr_parent = $(this).parent('td').parent('tr');
			var elmid = tr_parent.data('uid');
			if(tr_parent.hasClass('open')){
				tr_parent.removeClass('open');
				$('tr.sub_'+elmid).remove();
			}else{
				tr_parent.addClass('open');
				$.ajax({
					url: '<?php echo site_url('billing/table_get_detail'); ?>',
					data: 'id='+reqID,
					type:'POST',
					dataType: 'html',
					beforeSend: function(){
						$(tr_parent).after('<tr class="sub_'+elmid+'"><td colspan="12" class="loader"></td><td style="display:none">&mdash;|</td></tr>');
					},
					success:function(result){
						$('tr.sub_'+elmid).addClass('alert-success');
						$('tr.sub_'+elmid+'>td:eq(0)').html(result);
					}
				}).done(function(){
					$('tr.sub_'+elmid+'>:eq(0)').removeClass('loader');
				});
			}
		}
	});
</script>

<script id="distribute_table-row-<?php echo $timestamp; ?>" type="text/x-kendo-tmpl">
	<tr id="#: VCH_BillingDocumentWeb #">
		<td>
			<a href='javascript:void(0)'  onclick='$(this).show_detail();' data-url='<?php echo site_url('billing/rptdetail'); ?>/#: VCH_BillingDocumentWeb #' data-id='#: VCH_BillingDocumentWeb #'><button class='btn btn-xs btn-info'> <i class='glyphicon glyphicon-pencil'></i> Detail</button></a>
		</td>
		<td>
			#if(status_sap=='W' && doc_sap==''){#
				<span class="badge label-primary">W</span>
			#}else if(status_sap=='U' && doc_sap==''){#
				<span class="badge label-warning">U</span>
			#}else if(doc_sap !=''){#
				<span class="badge label-success">C</span>
			#}#

			<a href="javascript:void(0)" onclick="$(this).getitem();" data-id="#: VCH_BillingDocumentWeb #">
				<strong>#: VCH_BillingDocumentWeb #</strong>
			</a>
		</td>
		<td> #: doc_sap #</td>
		<td>#: DAT_BillingDate #</td>
		<td><center>#: VCH_AccountAssigment #</center></td>
		
		<td>#: VCH_SalesOrg #</td>
		<td>#: Customer #</td>
		<td>#: VCH_NPWP #</td>
		<td style='text-align:right'>#: kendo.toString(BIG_Net, "n0") #</td>
		<td style='text-align:right'>#: kendo.toString(BIG_Tax, "n0") #</td>
	</tr>
</script>

