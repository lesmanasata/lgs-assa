<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_billing extends CI_Model {
 	
 
	var $customer 			= 'MF_CUSTOMER';
	var $contract_bill 		= 'MF_CONTRACT_BILLING_PLAN';
	var $contract_header 	= 'MF_CONTRACT_HEADER';
	var $contract_item 		= 'MF_CONTRACT_ITEM';
	var $sales_doctype 		= 'MF_SALES_DOCUMENT_TYPE';
	var $tbl_partner 		= 'MF_CONTRACT_PARTNER';
	var $sales_org_tbl 		= 'MF_SALES_ORG';
	var $contract_condition = 'MF_CONTRACT_CONDITION';
	var $tf_contract_item 	= 'tf_contract_item';
	var $tf_billing_header 	= 'TF_BILLING_HEADER';
	var $tf_billing_item 	= 'TF_BILLING_ITEM';
	var $mf_assign_group 	= 'MF_ACCOUNT_ASSIGMENT_GROUP';
	var $opt_tax 			= 'mf_taxclassification';
	var $account_assignment_group = 'mf_accountassigmentgroup';
	var $tbl_tmp			= 'tmp_item_select';
	var $cfg_billing 		='';
 
	public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
        $this->cfg_billing = json_decode(json_encode($this->config->item('billing')));
    }
	 
	function get_cmb($custom_fild,$tbl, $filter=array()){
		$this->db_main->select($custom_fild,FALSE)
			->from($tbl);
			
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);
		$res=$this->db_main->get();
		return $res->result();
	}


	function get_filter_reason($custom_fild, $filter=array()){
		$this->db_main->select($custom_fild,FALSE)
			->from($this->contract_item);
			
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);

		$this->db_main->group_by('CHR_SalesDocument');
		
		$res=$this->db_main->get();
		return $res->result();
	}

	public function get_bill_data($custom_fild, $filter=array(),$group=FALSE)
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->contract_header.' h')
            ->join($this->contract_bill.' p','p.CHR_SalesDocument = h.CHR_SalesDocument')
            ->join($this->contract_item.' i','i.CHR_SalesDocument = p.CHR_SalesDocument and p.CHR_ItemSD = i.CHR_DocumentItem')
            ->join($this->customer.' c','h.CHR_SoldToParty = c.VCH_CustomerId')
            ->join($this->tbl_partner.' r','r.CHR_SalesDocument = h.CHR_SalesDocument AND r.CHR_PartnerFunction="Z1"','LEFT');

        if ($filter !=false && count($filter)>0){
            $this->db_main->where($filter);
        }

        if($group != FALSE)
        	$this->db_main->group_by('p.CHR_SalesDocument,p.CHR_ItemSD,p.CHR_BillingPlanNumber,p.CHR_ItemBilling');

        $this->db_main->order_by('p.DAT_SettlementDate','ASC'); 
        $res=$this->db_main->get();
        return $res->result();
    }

    function get_billing_plan($custom_fild, $filter=array()){ 
		$this->db_main->select($custom_fild,FALSE)
			->from($this->contract_bill.' p')
			->join($this->contract_item.' i', 'p.CHR_SalesDocument=i.CHR_SalesDocument AND p.CHR_ItemSD=i.CHR_DocumentItem','LEFT')
			->join($this->contract_header.' h','p.CHR_SalesDocument=h.CHR_SalesDocument');

		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);
		$res=$this->db_main->get();

		return $res->result();
	}

	function update_usetobill($update_fild,$where=array()){
		if(count($where) > 0){
			$this->db_main->where($where);

			$res = $this->db_main->update(
				$this->contract_bill,
				$update_fild
			);

			if($res)
				return TRUE;
			else
				return FALSE;
		}
		return FALSE;
	}

	function get_billing_header($custom_fild, $filter=array()){ 
		$this->db_main->select($custom_fild,FALSE)
			->from($this->contract_header.' h')
			->join($this->contract_item.' i','i.CHR_SalesDocument = h.CHR_SalesDocument')
			->join($this->customer.' c','h.CHR_SoldToParty = c.VCH_CustomerId')
			->join($this->sales_org_tbl.' a','a.VCH_SalesOrgId = h.CHR_SalesOrganization')
			->join($this->mf_assign_group.' g','g.CHR_AccountAssignGroup = i.CHR_AccountAssigmentGroup');
 
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);
		$res=$this->db_main->get();

		return $res->result();
	}

	function tmp_add($data)
    {
        $this->db_main->insert(
            $this->tbl_tmp,
            $data
        );

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function tmp_clear(){
    	$this->db_main->delete(
            $this->tbl_tmp,
            array(
            	'user_create' => $this->session->userdata('user'),
            	'tmp_for' 	  => $this->cfg_billing->tmp_key
            )
        );

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_tmp_billing($custom_fild, $filter=array()){ 
		$this->db_main->select($custom_fild,FALSE)
			->from($this->tbl_tmp);
			
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);
		$res=$this->db_main->get();
		return $res->result();
    }

    function get_billingdata_header($custom_fild, $filter=array(),$mode=array()){ 
    	$use = json_decode(json_encode($mode));

		$this->db_main->select($custom_fild,FALSE)
			->from($this->tf_billing_header.' h')
			->join($this->customer.' c','h.VCH_Customer = c.VCH_CustomerId');
			
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);

		if(count($mode)>0){
			if(!$use->total)
				$this->db_main->limit($use->take,$use->skip);
		}

		$res=$this->db_main->get();

		if($use->total)
			return $res->num_rows();
		else
			return $res->result();
		
	}

	function billing_item_add($data)
    {
        $this->db_main->insert(
            $this->tf_billing_item,
            $data
        );

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_billingdata_item($custom_fild, $filter=array()){ 
		$this->db_main->select($custom_fild,FALSE)
			->from($this->tf_billing_item.' h');
			//->join($this->customer.' c','h.VCH_Customer = c.VCH_CustomerId');
			
		if($filter !=FALSE && count($filter)>0)
			$this->db_main->where($filter);
		$res=$this->db_main->get();
		return $res->result();
	}

    function billing_header_add($data)
    {
        $this->db_main->insert(
            $this->tf_billing_header,
            $data
        );

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }





























	
	
	 
}