
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing extends MY_Controller {
	var $customer 			= 'MF_CUSTOMER';
	var $contract_header 	= 'MF_CONTRACT_HEADER';
	var $tf_contract_header = 'TF_BILLING_HEADER';
	var $tf_contract_item 	= 'TF_BILLING_ITEM';
	var $contract_bill 		= 'MF_CONTRACT_BILLING_PLAN';
	var $contract_item 		= 'MF_CONTRACT_ITEM';
	var $sales_doctype 		= 'MF_SALES_DOCUMENT_TYPE';
	var $sales_org_tbl 		= 'MF_SALES_ORG';
	var $contract_condition = 'MF_CONTRACT_CONDITION';
	var $tbl_payment_terms	= 'MF_PAYMENT_TERMS';
	var $opt_tax 			= 'MF_TAXCLASSIFICATION';
	var $account_assignment_group = 'mf_accountassigmentgroup';
	var $cfg_billing 		= '';

	public function __construct(){
        parent::__construct();
		$this->load->module('app');
		$this->app->cek_permit();
		$this->cfg_billing = json_decode(json_encode($this->config->item('billing')));
    }

    public function index()
	{
		$this->cek_build_access(explode(',',ALLOWED_USER));
		$this->template->title('Billing');

		$billing_cmb = array(
            array('name' => 'cust_name', 'url' => 'billing/cmb_company', 'flow' => ''),
            array('name' => 'doc_type', 'url' => 'billing/get_cmb_doctype', 'flow' => ''),
            array('name' => 'sales_org', 'url' => 'billing/cmb_sales_org', 'flow' => ''),
            array('name' => 'payment_terms', 'url' => 'billing/cmb_payment_terms', 'flow' => ''),
            array('name' => 'tax_cust', 'url' => 'billing/cmb_tax_classifikasi', 'flow' => '')
        );

		$data['date_cutof']  = $this->cfg_billing->date_cutof;
		$data['js_cmb']		 = $this->app->dropdown_kendo($billing_cmb);
		$this->template->build('billing/billing_view',$data);
	}

	public function show_grid(){
		$data['form_select'] =array(
			'cust_name' 	=> $this->input->post('cust_name'),
			'doc_type' 		=> $this->input->post('doc_type'),
			'sales_org' 	=> $this->input->post('sales_org'),
			'date_start' 	=> $this->input->post('date_start'),
			'date_end' 	 	=> $this->input->post('date_end')
		);
		$this->load->view('billing/billing_tbl',$data);
	}

	public function get_tbl_select(){
		$this->load->model("billing/m_billing","mod_bill");
		$cust_id 	= $this->input->post('cust_name');
		$doc_type 	= $this->input->post('doc_type');
		$sales_org 	= $this->input->post('sales_org');
		
		$date_start_sp = explode('-',$this->input->post('date_start'));
		$date_end_sp 	= explode('-',$this->input->post('date_end'));
		$date_start 	= $date_start_sp[2].'-'.$date_start_sp[1].'-'.$date_start_sp[0];
		$date_end 		= $date_end_sp[2].'-'.$date_end_sp[1].'-'.$date_end_sp[0];


		$custom_fild  = "CONCAT(p.CHR_SalesDocument,'-',p.CHR_ItemSD,'-',p.CHR_BillingPlanNumber,'-',p.CHR_ItemBilling) as id,";
		$custom_fild  .= "h.CHR_SoldToParty as customerId,";
		$custom_fild .= "concat(c.VCH_Title, IF(c.VCH_Name1 IS NOT NULL,c.VCH_Name1,c.VCH_Name2)) as customer_name,";
		$custom_fild .= "p.CHR_SalesDocument as lampiran_lt,";
		$custom_fild .= "h.CHR_CustomerAssignGroup as asg_group,";
		$custom_fild .= "i.CHR_LicensePlatNumber as license_number,";
		$custom_fild .= "CONCAT(i.CHR_Material,'-',i.CHR_Description) as matrial,";
		$custom_fild .= "DATE_FORMAT(p.DAT_SettlementDate,'%d-%m-%Y') as per_from,";
		$custom_fild .= "DATE_FORMAT(p.DAT_SettlemetDateDeadline,'%d-%m-%Y') as per_to,";
		$custom_fild .= "i.CHR_LicensePlatNumber as item_type,";
		$custom_fild .= "p.INT_BIllingValue as harga,";
		$custom_fild .= "r.CHR_Name as marketing,";//"IF(r.CHR_PartnerFunction='ZS',r.CHR_Name,r.CHR_PartnerFunction) as marketing";
		$custom_fild .= "r.CHR_ContactPerson as cp";//"IF(r.CHR_PartnerFunction='ZS',r.CHR_Name,r.CHR_PartnerFunction) as marketing";

		if($cust_id !='-1')
			$filter['h.CHR_SoldToParty'] = $cust_id;

		if($doc_type !='-1')
			$filter['h.CHR_SalesDocumentType'] = $doc_type;

		if($sales_org !='-1')
			$filter['h.CHR_SalesOrganization'] = $sales_org;

		$filter['p.CHR_BillingStatus !="C"'] = NULL;

		$filter['p.DAT_SettlementDate >="'.$date_start.'"'] = NULL;
		$filter['p.DAT_SettlemetDateDeadline <="'.$date_end.'"'] = NULL;
		$filter['p.INT_UsedToBilled'] = 0;
		$filter['p.CHR_BillingBlock =""'] =NULL;

		$reason_fild 	= 'CHR_SalesDocument';
		$reason_filter["CHR_ReasonRejection =''"]=NULL;
		$fres= $this->mod_bill->get_filter_reason($reason_fild,$reason_filter);
		$filter_x = array();
		foreach ($fres as $key => $value) {
			$filter_x[] = $value->CHR_SalesDocument;
		}
		if(count($filter_x)>0)
			$filter['p.CHR_SalesDocument IN("'.implode('","', $filter_x).'")'] = NULL;

		$res = $this->mod_bill->get_bill_data($custom_fild,$filter,TRUE);

		echo json_encode(array('data'=>$res));
	}


	public function get_cmb_doctype(){
		$this->load->model("billing/m_billing","mbill");
		$fild = "CHR_Type as id, CONCAT(CHR_Type,' - ',CHR_Description) as text";
		$res = $this->mbill->get_cmb($fild,$this->sales_doctype);

		echo json_encode(array('data'=>$res));
	}

	function cmb_company(){
		$this->load->model("billing/m_billing","mbill");
		$fild = "VCH_CustomerId as id, CONCAT(VCH_CustomerId,' - ',VCH_Name1) as text";
		$res = $this->mbill->get_cmb($fild,$this->customer);

		echo json_encode(array('data'=>$res));
	}

	function cmb_sales_org(){
		$this->load->model("billing/m_billing","mbill");
		$fild = "VCH_SalesOrgId as id, CONCAT(VCH_SalesOrgId,' - ',VCH_Description) as text";
		$res = $this->mbill->get_cmb($fild,$this->sales_org_tbl);

		echo json_encode(array('data'=>$res));
	}

	function cmb_payment_terms(){
		$this->load->model("billing/m_billing","mbill");
		$fild = "CHR_Type as id, CONCAT(CHR_Type,' - ',CHR_Description) as text";
		$res = $this->mbill->get_cmb($fild,$this->tbl_payment_terms);

		echo json_encode(array('data'=>$res));
	}

	function cmb_tax_classifikasi(){
		$this->load->model("billing/m_billing","mbill");
		$fild = "CHR_Type as id, CONCAT(INT_Amount,' - ',CHR_Description) as text";
		$res = $this->mbill->get_cmb($fild,$this->opt_tax);

		echo json_encode(array('data'=>$res));
	}


	function set_to_billing(){
		$this->load->model("billing/m_billing","mod_bill");
		$bilplan_id   	= explode(",",$this->input->post('id'));
		$out['data'] 	= array();
		$out['status'] 	= false;
		$getItemId    	= array();
		$hider_id		= '';
		foreach ($bilplan_id as $key => $value) {
			$spl_id = explode('-', $value);
			$getItemId[]	= $spl_id[0].$spl_id[1].$spl_id[2].$spl_id[3];
			$hider_id		= $spl_id[0];

		}


		$filds  = 'p.CHR_SalesDocument,'; //nomor_dokumen
		$filds .= 'p.CHR_ItemSD,'; //item_sd
		$filds .= 'p.CHR_BillingPlanNumber,'; //billplan_number
		$filds .= 'p.CHR_ItemBilling,'; //item_billing
		$filds .= "CONCAT(i.CHR_Material,'-',i.CHR_Description) as material,"; //material
		$filds .= "p.DAT_SettlementDate as date_from,"; //date_from
		$filds .= "p.DAT_SettlemetDateDeadline as date_to,"; //date_to
		$filds .= 'i.CHR_LicensePlatNumber as plat_number,';
		$filds .= 'p.INT_BIllingValue as amount,'; //amount
		$filds .= 'i.CHR_AccountAssigmentGroup as item_type,'; //item_type
		$filds .= 'h.CHR_SalesOrganization as bisnis_area,'; //bisnis_area
		$filds .= 'p.DAT_BillingDateIndex'; //tgl_bukpot

		$item_filter['CONCAT(p.CHR_SalesDocument,p.CHR_ItemSD,p.CHR_BillingPlanNumber,p.CHR_ItemBilling) IN("'.implode('","', $getItemId).'")'] = NULL;
		$res_item = $this->mod_bill->get_billing_plan($filds,$item_filter);

		$this->mod_bill->tmp_clear();
		if(count($res_item)>0){
			foreach($res_item as $row){
				$data=array(
					'user_create' 	=> $this->session->userdata('user'),
					'tmp_for' 	  	=> $this->cfg_billing->tmp_key,
					'customer'   	=> $row->CHR_SalesDocument, //nomor_dokumen
					'company_code'	=> $row->CHR_ItemSD, //item_sd
					'doc_number'	=> $row->CHR_BillingPlanNumber, //billplan_number
					'desc'	  		=> $row->material, //material
					'doc_type'		=> $row->CHR_ItemBilling, //item_billing
					'posting_date'	=> $row->date_from, //date_from
					'doc_date'		=> $row->date_to, //date_to
					'tgl_bukpot'	=> $row->DAT_BillingDateIndex, //date_to
					'bisnis_area'	=> $row->plat_number, //plat_number
					'posted_value'	=> $row->amount, //t_amount
					'status'		=> $row->item_type, //item_type
					'item'			=> $row->bisnis_area //bisnis_area
				);
				$res_tmp = $this->mod_bill->tmp_add($data);
			}
		}

		if($hider_id !=''){
			$filds_hider   = "h.CHR_SalesDocument as sales_documet,";
			$filds_hider .= "CONCAT(h.CHR_SalesOrganization,'-',a.VCH_Description) as sales_org,";
			$filds_hider .= "CONCAT(h.CHR_SoldToParty,'-',c.VCH_Title,' ',c.VCH_Name1) as party,";
			$filds_hider .= "h.CHR_Up as up,";
			$filds_hider .= "CONCAT(h.CHR_CustomerAssignGroup,'-',g.CHR_Description) as chr_group,";
			$filds_hider .= "h.CHR_Payment_term as term,";
			$filds_hider .= "c.VCH_TaxClassification as classifikasi,";
			$filds_hider .= "c.VCH_VatRegistrationNo as npwp,";
			$filds_hider .= "'0' as net,";
			$filds_hider .= "'0' as tax";

			$header_filter['h.CHR_SalesDocument'] = $hider_id;
			$res_header = $this->mod_bill->get_billing_header($filds_hider,$header_filter);

			if(count($res_header)>0){
				$out['status'] 	= true;
				$out['data'] = $res_header;
			}
		}


		echo json_encode($out);
	}

	function get_tobill_item(){
		$res = array();
		$this->load->model("billing/m_billing","mod_bill");

		$filds  = 'customer as nomor_dokumen,';
		$filds .= 'company_code as item_sd,';
		$filds .= 'doc_number as billplan_number,';
		$filds .= '`desc` as material,';
		$filds .= 'doc_type as item_billing,';
		$filds .= 'DATE_FORMAT(posting_date,"%d-%m-%Y") as date_from,';
		$filds .= 'DATE_FORMAT(doc_date,"%d-%m-%Y") as date_to,';
		//$filds .= 'DATE_FORMAT(doc_date,"%d-%m-%Y") as date_to,';
		$filds .= 'bisnis_area as plat_number,';
		$filds .= 'posted_value as t_amount,';
		$filds .= 'status as item_type,';
		$filds .= 'tgl_bukpot as DAT_BillingDateIndex,';
		$filds .= 'item as bisnis_area';


		$filter['user_create']	= $this->session->userdata('user');
		$filter['tmp_for']		= $this->cfg_billing->tmp_key;

		$res_tmp = $this->mod_bill->get_tmp_billing($filds,$filter);
		if(count($res_tmp)>0)
			$res = $res_tmp;

		echo json_encode(array('data'=>$res));
	}

	function submitdata(){
		$this->load->model("billing/m_billing","mod_bill");
		$out['status']  	= FALSE;
		$out['messages']  	= lang('msg_failed_save_record');
		$rowitem 			= json_decode(json_encode($this->input->post('models')));

		$ses_id 				= $this->session->userdata('user');
		$bilplan_id 			= $this->app->getAutoId('VCH_BillingDocumentWeb', $this->tf_contract_header, $this->cfg_billing->initial_key);
		$VCH_BillToParty_arr	= explode("-",$this->input->post('header_bill_to_party'));
		$VCH_BillToParty 		= (isset($VCH_BillToParty_arr[0]))?$VCH_BillToParty_arr[0]:'';
		$VCH_AccountAssigment_arr= explode("-", $this->input->post('header_assign_group'));
		$VCH_AccountAssigment 	= (isset($VCH_AccountAssigment_arr[0]))?$VCH_AccountAssigment_arr[0]:'';
		$DAT_BillingDate 		= $this->input->post('header_billing_date');
		$VCH_NPWP 				= $this->input->post('header_npwp');
		$VCH_Customer_arr		= explode("-", $this->input->post('header_payer'));
		$VCH_Customer			= (isset($VCH_Customer_arr[0]))?$VCH_Customer_arr[0]:'';
		$VCH_TermOfPayment		= $this->input->post('header_payment_terms');
		$VCH_SalesOrg_arr		= explode("-", $this->input->post('header_sales_orgx'));
		$VCH_SalesOrg			= (isset($VCH_SalesOrg_arr[0]))?$VCH_SalesOrg_arr[0]:'';
		$VCH_TaxClassCustomer	= $this->input->post('header_tax_cust');
		$VCH_Uraian				= $this->input->post('header_uraian');
		$VCH_Keterangan			= $this->input->post('header_keterangan');
		$BIG_Net 				= str_replace(",", "", $this->input->post('header_net'))+0;
		$BIG_MateraiFee			= str_replace(",", "", $this->input->post('materai_fee'))+0;
		$BIG_Tax				= str_replace(",", "", $this->input->post('header_tax'))+0;
		$lampiran_lt			= $this->input->post('lampiran_lt');

		do{
			$bis_stat = FALSE;
			$bis_fal  = '';
			foreach ($rowitem as $key => $value) {
				if($bis_fal=='')
					$bis_fal = $value->bisnis_area;

				if($bis_fal !='' and $bis_fal != $value->bisnis_area)
					$bis_stat = TRUE;
			}
			if($DAT_BillingDate ==''){
				$out['messages'] = 'Billing date can not be empty';
				break;
			}

			/*if($BIG_MateraiFee < 0 || $BIG_MateraiFee == ''){
				$out['messages'] = 'Biaya materai harus lebih besar dari nol (0)';
				break;
			}*/

			if($VCH_TaxClassCustomer == '-1' || $VCH_TaxClassCustomer == ''){
				$out['messages'] = 'Tax Class C. can not be empty';
				break;
			}

			if($bis_stat){
				$out['messages'] = 'Bisnis Area harus sama';
				break;
			}

			if($BIG_Net <=0){
				$out['messages'] = 'Net can not be empty';
				break;
			}

			if($BIG_Tax == '' || $BIG_Tax == 'NaN'){
				$out['messages'] = 'Tax canot can not be empty, Please select Tax Class C and calculate first';
				break;
			}

			$hider = array(
				'VCH_BillingDocumentWeb'=> $bilplan_id,
				'DAT_BillingDate' 		=> date('Y-m-d', strtotime($DAT_BillingDate)),
				'VCH_AccountAssigment'	=> $VCH_AccountAssigment,
				'VCH_Uraian' 			=> $VCH_Uraian,
				'VCH_Keterangan' 		=> $VCH_Keterangan,
				'VCH_SalesOrg' 			=> $VCH_SalesOrg,
				'VCH_Customer' 			=> $VCH_Customer,
				'VCH_NPWP' 				=> $VCH_NPWP,
				'VCH_TaxClassCustomer' 	=> $VCH_TaxClassCustomer,
				'VCH_TermOfPayment' 	=> $VCH_TermOfPayment,
				'VCH_BillToParty' 		=> $VCH_BillToParty,
				'BIG_MateraiFee' 		=> $BIG_MateraiFee+0,
				'BIG_Net' 				=> $BIG_Net+0,
				'BIG_Tax' 				=> $BIG_Tax+0,
				'created_by' 			=> $ses_id,
				'flag' 					=> '0'
			);

			$this->mod_bill->billing_header_add($hider);
			$no= 1;
			foreach ($rowitem as $key => $value){
				$material = explode('-', $value->material);
				$my_item = $material[0];
				$item= array(
					'BIG_BillingId' 		=> $bilplan_id,
					'NoUrutDoc' 			=> $no,
					'CHR_SalesDocument' 	=> $value->nomor_dokumen,
					'CHR_ItemSD' 			=> $value->item_sd,
					'CHR_BillingPlanNumber' => $value->billplan_number,
					'CHR_ItemBilling' 		=> $value->item_billing,
					'DAT_SettlementDate' 	=> date('Y-m-d',strtotime($value->date_from)),
					'DAT_SettlemetDateDeadline'	=> date('Y-m-d',strtotime($value->date_to)),
					'CHR_Material' 			=> $my_item,
					'INT_amount' 			=>  $value->t_amount,
					'CHR_AccountAssigmentGroup'=>  $value->item_type,
					'DAT_BillingDateIndex'=>  $value->DAT_BillingDateIndex
				);
				$no++;

				$where['CONCAT(CHR_SalesDocument,CHR_ItemSD,CHR_BillingPlanNumber,CHR_ItemBilling)'] = $value->nomor_dokumen.$value->item_sd.$value->billplan_number.$value->item_billing;
				$update['INT_UsedToBilled']='1';

				$this->mod_bill->billing_item_add($item);
				$this->mod_bill->update_usetobill($update,$where);
			}


			$out['status']  	= TRUE;
			$out['messages']  	= lang('msg_success_save_record').' <b>'.$bilplan_id.'</b>';
			$this->mod_bill->tmp_clear();
		}while(FALSE);


		echo json_encode($out);
	}

	function list(){
		$this->cek_build_access(explode(',',ALLOWED_USER));
		$this->template->title('Billing-List');
		$billing_cmb = array(
            array('name' => 'customer', 'url' => 'billing/cmb_company', 'flow' => '')
        );

		$data['js_cmb']		 = $this->app->dropdown_kendo($billing_cmb);

		$this->template->build('billing/billing_list',$data);
	}

	function show_item(){
		$data['filter'] = array(
			'customer' => $this->input->post('customer'),
			'tahun' => $this->input->post('tahun'),
			'bulan' => $this->input->post('bulan')
		);
		$this->load->view('billing/billing_table_list',$data);
	}

	function get_billing_data(){
		$this->load->model("billing/m_billing","mod_bill");
		$out['data'] =  array();
		$customer = $this->input->post('customer');
		$bulan = $this->input->post('bulan');
		$tahun	 = $this->input->post('tahun');
		$take = $this->input->post('take');
		$skip = $this->input->post('skip');

		$filds  = 'h.VCH_BillingDocumentWeb,';
		$filds .= 'h.DAT_BillingDate,';
		$filds .= 'h.VCH_AccountAssigment,';
		$filds .= 'h.VCH_Uraian,';
		$filds .= 'h.VCH_Keterangan,';
		$filds .= 'h.VCH_SalesOrg,';
		$filds .= 'h.VCH_NPWP,';
		//$filds .= 'h.MateraiFee,';
		$filds .= 'h.BIG_MateraiFee,';
		$filds .= 'h.BIG_Net,';
		$filds .= 'h.BIG_Tax,';
		$filds .= "IF(h.flag='0','W','U') as status_sap,";
		$filds .= " '' AS doc_sap,";
		$filds .= "CONCAT(c.VCH_Title,' ',c.VCH_Name1) as Customer";


		if($customer !='-1')
			$filter['h.VCH_Customer']= $customer;

		$filter['DATE_FORMAT(h.DAT_BillingDate,"%Y-%m")="'.$tahun."-".$bulan.'"']= NULL;

		$mode['take'] = $take;
		$mode['skip'] = $skip;
		$mode['total'] = FALSE;

		$res = $this->mod_bill->get_billingdata_header($filds,$filter,$mode);

		foreach ($res as $key => $value) {
			$res_bilweb = $this->mod_bill->get_cmb(
	    		'VCH_BELNR','TF_BKPF', array(
	    			'CHR_BILWEB'=> $value->VCH_BillingDocumentWeb
	    		)
	    	);

	    	if(count($res_bilweb) > 0){
	    		$res[$key]->doc_sap = $res_bilweb[0]->VCH_BELNR;
	    	}
		}

		$mode['total'] = TRUE;
		if(count($res) > 0)
			$out['data'] = $res;

		$out['total'] = $this->mod_bill->get_billingdata_header($filds,$filter,$mode);

		echo json_encode($out);
	}

	public function table_get_detail()
    {
        $unixId['res'] = array('id'=>$this->input->post('id'));
        $this->load->view('billing/billing_table_list_detail', $unixId);
    }

    function get_billing_detail(){
    	$this->load->model("billing/m_billing","mod_bill");
    	$id 			= $this->input->post('id');
    	$out['data'] 	= array();

    	$res = $this->mod_bill->get_billingdata_item(
    		'*', array(
    			'BIG_BillingId' => $id
    		)
    	);

    	if(count($res) > 0)
			$out['data'] = $res;

		echo json_encode($out);
    }

    function rptdetail(){
    	$this->load->model('billing/m_billing','mod_bill');
    	$bilwebid 	  = $this->uri->segment(3);
    	$data['time'] = date('his');

    	$filter['h.VCH_BillingDocumentWeb'] = $bilwebid;

    	$filds  = 'h.VCH_BillingDocumentWeb,';
    	$filds .= 'h.VCH_SalesOrg,';
    	$filds .= 'h.VCH_Customer,';
    	$filds .= 'h.VCH_NPWP,';
    	$filds .= 'h.VCH_AccountAssigment,';
    	$filds .= 'h.BIG_Tax,';
    	$filds .= 'h.BIG_Net,';
    	$filds .= 'h.VCH_TaxClassCustomer,';
    	$filds .= 'h.VCH_TermOfPayment,';
    	$filds .= 'h.DAT_BillingDate,';
    	$filds .= 'h.BIG_MateraiFee,';
    	$filds .= 'h.VCH_Uraian,';
    	$filds .= 'h.VCH_Keterangan,';
    	$filds .= 'c.VCH_Title,';
    	$filds .= 'c.VCH_Name1';

    	$res_header = $this->mod_bill->get_billingdata_header($filds,$filter);

    	$res_sorg = $this->mod_bill->get_cmb(
    		'*',$this->sales_org_tbl, array(
    			'VCH_SalesOrgId'=> $res_header[0]->VCH_SalesOrg
    		)
    	);

    	$res_tax_cus = $this->mod_bill->get_cmb(
    		'*',$this->opt_tax, array(
    			'CHR_Type'=> $res_header[0]->VCH_TaxClassCustomer
    		)
    	);


    	$res_term = $this->mod_bill->get_cmb(
    		'*',$this->tbl_payment_terms, array(
    			'CHR_Type'=> $res_header[0]->VCH_TermOfPayment
    		)
    	);

    	$data['header'] 		= $res_header;
    	$data['salesorg'] 		= $res_sorg;
    	$data['tax_clas'] 		= $res_tax_cus;
    	$data['payment_term'] 	= $res_term;

    	$this->load->view('billing/billing_rpt_detail',$data);
    }


    function biilcek_partner_function(){
    	$this->load->model("billing/m_billing","mod_bill");

    	$master 		= explode('-', $this->input->post('master'));
    	$compare 		= explode('-', $this->input->post('compare'));
    	$m_lt_sales 	= $master[0];
    	$m_item_sd  	= $master[1];
    	$m_bill_numb	= $master[2];
    	$m_bill_item	= $master[3];
    	$c_lt_sales 	= $compare[0];
    	$c_item_sd  	= $compare[1];
    	$c_bill_numb	= $compare[2];
    	$c_bill_item	= $compare[3];
    	$partner_fc 	= explode(',', $this->cfg_billing->partnerFunction);
    	$i 				= 0;
    	$out['status'] 	= FALSE;
    	$select_fild 	= 'CONCAT(CHR_SalesDocument,CHR_item,CHR_CustomerId,CHR_ContactPerson) AS cmp_fild';
    	$tbl_partner	= 'MF_CONTRACT_PARTNER';

    	do{
    		$use_pf = $partner_fc[$i];

    		$res_m = $this->mod_bill->get_cmb(
    			$select_fild, $tbl_partner,array(
    				'CHR_SalesDocument'=>$m_lt_sales,
    				'CHR_PartnerFunction'=>$use_pf,
    				'CHR_item'=> $m_item_sd
    			)
    		);

    		if(count($res_m) <=0){
    			$res_m = $this->mod_bill->get_cmb(
	    			$select_fild, $tbl_partner,array(
	    				'CHR_SalesDocument'=>$m_lt_sales,
	    				'CHR_PartnerFunction'=>$use_pf
	    			)
	    		);
    		}

    		$res_c = $this->mod_bill->get_cmb(
    			$select_fild, $tbl_partner,array(
    				'CHR_SalesDocument'=>$c_lt_sales,
    				'CHR_PartnerFunction'=>$use_pf,
    				'CHR_item'=> $c_item_sd
    			)
    		);

    		if(count($res_c) <=0){
    			$res_c = $this->mod_bill->get_cmb(
	    			$select_fild, $tbl_partner,array(
	    				'CHR_SalesDocument'=>$c_lt_sales,
	    				'CHR_PartnerFunction'=>$use_pf
	    			)
	    		);
    		}

    		if(count($res_m) > 0 && count($res_c)<= 0 || count($res_m) <= 0 && count($res_c) > 0){
    			$out['status'] 	= TRUE;
	    		break;
    		}
    		
    		if(count($res_m) > 0 && count($res_c)>0){
	    		if($res_m[0]->cmp_fild !=$res_c[0]->cmp_fild){
	    			$out['status'] 	= TRUE;
	    			break;
	    		}
    		}


    		$i++;
    	}while($i< count($partner_fc));

    	echo json_encode($out);
    }
























}
