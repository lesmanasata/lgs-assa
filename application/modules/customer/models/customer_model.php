<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    public $tbl_customer = 'MF_CUSTOMER';

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }
    public function viewcustomer()
    {
        $this->db_main->select('VCH_CustomerId AS VCH_KUNNR,
            VCH_Name1 AS VCH_CSNAME," " as status,VCH_Address,VCH_VatRegistrationNo', false)
              ->from($this->tbl_customer);
        $res = $this->db_main->get();
        return $res->result();
        //echo $this->db_main->last_query();
    }
    public function updatecustomer($con)
    {
        $status = $con->status;
        if ($status==true) {
            $data = '1';
        }else {
            $data = '0';
        }
        $data = array(
        'VCH_CSNAME' => $con->VCH_CSNAME,
        'status'=>$data
    );
        $this->db_main->where('VCH_KUNNR', $con->VCH_KUNNR);
        $this->db_main->update($this->tbl_customer, $data);

        //echo $this->db_main->last_query();
    }
    public function savecustomer()
    {
        $id = $this->input->post('cos_id');
        $name = $this->input->post('cos_name');

        $data = array(
            'VCH_KUNNR' => $id,
            'VCH_CSNAME' => $name
        );
        $this->db_main->insert($this->tbl_customer, $data);
        //echo $this->db_main->last_query();
    }
}
