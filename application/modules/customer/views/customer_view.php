<h1 class="page-title">Master - Customer</h1> 
<ol class="breadcrumb breadcrumb-2">  
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>  
  <li><a href="#">Master</a></li>  
  <li class="active"><strong>Customer</strong></li>  
</ol>  

<div class="banner">
    <div class="btn-group">
            <?php
                //echo '<button class="btn btn-success navbar-btn" id="add"><i class="glyphicon glyphicon-plus"></i> Add record </button> ';
            ?>
    </div>
</div>

<div class="blank">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Customer</a></li>
    </ul>

    <div class="tab-content grid-form1" style="border-top:0px;">
          <div id="home" class="tab-pane fade in active ">
            <div class="padding_tab_body">
                <div id="grid"></div>
            </div>
          </div>
    </div>
</div>
<script>

    $(document).ready(function () {

        var ds_customer = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    type:"POST",
                                    dataType: "json",
                                    url: "<?php echo site_url('customer/view');?>"
                                },
                                update: {
                                    url: "<?php echo site_url('customer/update'); ?>",
                                    dataType: "json",
                                    type:'POST'
                                },
                                parameterMap: function(options, operation){
                                    if (operation !== "read" && options.models) {
                                      return {
                                          models: kendo.stringify(options.models)
                                      };
                                    }
                              }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                parse: function(res) {
                                        return res.data;
                                    },
                                model: {
                                    id: "VCH_KUNNR",
                                    fields: {
                                        VCH_KUNNR: {type: "text", editable: false, nullable: true },
                                        VCH_CSNAME: { type: "text" },
                                        //status: { type: "boolean"}
                                    }
                                }
                            },

                        });
        $("#grid").kendoGrid({
            dataSource: ds_customer,
            height: 550,
            sortable: true,
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {field: "VCH_KUNNR", title: "Customer ID", width: 150, editable: false},
                {field: "VCH_VatRegistrationNo", title: "Registration Number"},
                {field: "VCH_CSNAME", title: "Nama Customer"},
                {field: "VCH_Address", title: "Address"},
                //{field: "status", title: "Status", template: "#if(status==true){#Aktif#}else{#Non Aktif#}#"},
                //{command: ["edit"], title: "&nbsp;", width: "250px"}
            ],
            editable: "inline",
            save:function(){
                this.refresh();
            }
        });
        $('#add').click(function(){
            $('#form_dialog_costcenter input[type=text]').val('');
            $('#dialog_costcenter').modal('show');
        })

        $("#dialog_costcenter").submit(function(e){
            e.preventDefault();
            $.ajax({
                url:'<?php echo site_url('customer/save');?>',
                type:'POST',
                data:$("#form_dialog_costcenter").serialize(),
                beferosend:function() {
                    $('#loading-mask').addClass('loader-mask');
                },
                success:function(res) {
                    ds_customer.read();
                    $('#dialog_costcenter input[name=cos_id]').val('');
                    $('#dialog_costcenter input[name=cos_name]').val('');
                    $('#dialog_costcenter').modal('toggle');
                }
            });
        });
    });

</script>

<div id="dialog_costcenter" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" id="form_dialog_costcenter" action="" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cost Center</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label col-sm-4" ><?php echo lang('cos_id'); ?>:</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="cos_id" id="cos_id" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" ><?php echo lang('cos_name'); ?>:</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" name="cos_name" >
                            </div>
                        </div>
                    </div>
                <div style="clear:both"></div>
                <div class="modal-footer">
                    <button type="submit" id='simpan' class="btn btn-primary"  ><?php echo lang('btn_save'); ?></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('pwd_btn_cancel'); ?></button>
                </div>
            </div>
        </form>
    </div>
</div>
