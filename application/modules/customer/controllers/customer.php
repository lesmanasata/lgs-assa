<?php
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customer extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->lang->load('language', 'english2');
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $arCmb = array(
            array('name' => 'company_code', 'url' => 'master/cmb_company', 'flow' => 'bisnis_area'),
            array('name' => 'bisnis_area', 'url' => 'master/cmb_bisnis_area', 'flow' => '')
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);
        $this->template->build('customer/customer_view', $data);
    }
    public function view()
    {
        $this->load->model('customer/customer_model', 'cus_mod');
        $data = $this->cus_mod->viewcustomer();

        echo json_encode(array('data' => $data, ));
    }
    public function update()
    {
        $mode = $this->input->post('models');
        $con = current(json_decode($mode));
        $this->load->model('customer/customer_model', 'cus_mod');
        $this->cus_mod->updatecustomer($con);
    }
    public function save()
    {
        $this->load->model('customer/customer_model', 'cus_mod');
        $this->cus_mod->savecustomer();
    }
}
