<h1 class="page-title">PPN SSP Inter Branch</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Clearing </a></li>
  <li class="active"><strong>PPN SSP Inter Branch</strong></li>
</ol>

 <style>
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>
<?php $timestamp = time(); ?>
<div class="banner">
    <div class="btn-group">
            <?php
                echo '<button class="btn btn-warning navbar-btn " id="reload'.$timestamp.'"><i class="fa fa-refresh"></i> '.lang('btn_reload').' </button> ';
            ?>

    </div>
</div>

<div class="blank">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link"><?php echo lang('titile_arint_clearing'); ?></a></li>
      <li id="form_link"><a data-toggle="tab" href="#form" id="padding_tab_link"><?php echo lang('titile_ssptint_form'); ?></a></li>
    </ul>

    <div class="tab-content grid-form1" style="border-top:0px;">
            <div id="home" class="tab-pane fade in active ">
                <div class="padding_tab_body">
                    <div id="arclearing_table<?php echo $timestamp; ?>"></div>
                </div>
            </div>
            <div id="form" class="tab-pane fade in ">
                <div class="padding_tab_body">
                     <form class="form-horizontal" id="form_dialog_proses<?php echo $timestamp; ?>" action="<?php echo site_url('clearing/ssptint/clearing_submit'); ?>" method="POST">
                        <input type="hidden" name="item">
                        <div class="modal-contents">
                            <div class="modal-bodys">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_cmp_code'); ?>:</label>
                                        <div class="col-sm-2">
                                          <input type="text" class="form-control" name="company_code" placeholder="<?php echo lang('dist_cmp_code'); ?>" readonly>
                                        </div>
                                        <label class="control-label col-sm-3" ><?php echo lang('dist_binis_area'); ?>:</label>
                                        <div class="col-sm-3">
                                          <input type="text" class="form-control" name="bisnis_area" placeholder="<?php echo lang('dist_binis_area'); ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('ar_doc_type'); ?>:</label>
                                        <div class="col-sm-2">
                                          <input type="text" class="form-control" name="doc_type" placeholder="<?php echo lang('ar_doc_type'); ?>" readonly>
                                        </div>
                                        <label class="control-label col-sm-3" ><?php echo lang('ar_doc_no'); ?>:</label>
                                        <div class="col-sm-3">
                                          <input type="text" class="form-control" name="doc_number" placeholder="<?php echo lang('ar_doc_no'); ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_post_date'); ?>:</label>
                                        <div class="col-sm-3">
                                          <input type="text" class="form-control" name="posting_date" placeholder="<?php echo lang('dist_post_date'); ?>" readonly>
                                        </div>
                                        <label class="control-label col-sm-3" ><?php echo lang('ar_doc_yer'); ?>:</label>
                                        <div class="col-sm-2">
                                          <input type="text" class="form-control" name="doc_yer" placeholder="<?php echo lang('ar_doc_yer'); ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_gl_acc'); ?>:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="gl_account" placeholder="<?php echo lang('dist_gl_acc'); ?>" readonly>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_post_date'); ?>:</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control inputDatate" name="posting_date_new" placeholder="<?php echo lang('dist_post_date'); ?>" style="width: 100%" value='<?php echo date('Y-m-d'); ?>'>
                                        </div>
                                     </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_text'); ?>:</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="text" placeholder="<?php echo lang('dist_text'); ?>"></textarea>
                                            <!-- <input type="text" class="form-control" name="text" placeholder="<?php echo lang('dist_text'); ?>"> -->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-4" ><?php echo lang('dist_curency'); ?>:</label>
                                        <div class="col-sm-2">
                                          <input type="text" class="form-control" name="curency" value="IDR" readonly placeholder="<?php echo lang('dist_curency'); ?>" readonly>
                                        </div>
                                        <label class="control-label col-sm-2" ><?php echo lang('dist_amount'); ?>:</label>
                                        <div class="col-sm-4">
                                            <strong><span class="form-control" style="background-color: #eee;" id="dv_amount"></span></strong>
                                          <input type="hidden" class="form-control" name="amount" placeholder="<?php echo lang('dist_amount'); ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                                <div  style="clear:both;"></div>
                                <div id="gridar<?php echo $timestamp; ?>"></div>

                                <p>&nbsp;</p>
                                <div  style="clear:both;"></div>
                                <div class="form-inline">
                                   <div class="form-group col-sm-2" style="display: none;">
                                        <label class="control-label col-sm-1" >&nbsp;</label>
                                        <div class="col-sm-8">
                                            <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                            <input type="checkbox" class="custom-control-input" name="balance_indicator" value='1' id="balance_indicator">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?php echo lang('frm_balance_label'); ?></span>
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4" style="display: none;">
                                        <label class="control-label col-sm-2" ><?php echo lang('frm_balance_type'); ?>:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" style="width:100%;" name="balance_type"  id="balance_type" placeholder="&mdash;&mdash;<?php echo lang('frm_balance_type'); ?>&mdash;&mdash;">

                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4" style="display: none;">
                                        <label class="control-label col-sm-2" ><?php echo lang('form_balance_amout'); ?>:</label>
                                        <div class="col-sm-10">
                                          <input type="number" class="form-control" id="balance_amount" name="balance_amount" placeholder="<?php echo lang('form_balance_amout'); ?>" >
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <div class="col-sm-12">
                                            <strong><span style="font-size: 25px;" id="dv_balance">0</span></strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-inline" id="dv_balance_cust">
                                    <div class="form-group col-sm-2">
                                        <label class="control-label col-sm-2" >&nbsp;</label>
                                        <div class="col-sm-10">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-5">
                                        <label class="control-label col-sm-2" ><?php echo lang('frm_balance_cust'); ?>:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" style="width:100%;" name="customer_balance"  id="customer_balance" placeholder="<?php echo lang('frm_balance_cust'); ?>">

                                        </div>
                                    </div>
                                </div>

                                <div  style="clear:both;"></div>
                                <p>&nbsp;</p>

                            </div>
                            <div class="modal-footer">
                                <input type="hidden" value="0" name="calculate" id="calculate">
                                <button type="button" id='hitung' class="btn btn-warning" ><?php echo lang('btn_hitung'); ?></button>
                                <button type="submit" id='simpan' class="btn btn-primary"  ><?php echo lang('btn_save'); ?></button>
                                <button type="button" id="cancel" class="btn btn-danger" data-dismiss="modal"><?php echo lang('pwd_btn_cancel'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>

<script id="dialog_paste" type="text/x-kendo-template">
    <div class="grid_paste" tabindex="0"></div>
    <p></p>
    <div align="right">
        <div class="panel-footer">
            <button type="button" class="paste_button btn btn-primary"> Proses </button>
        </div>
    </div>
</script>

<script id="arclearing_table-row-<?php echo $timestamp; ?>" type="text/x-kendo-tmpl">
    <tr id="#: company_code #-#: gl_account #-#: doc_yer #">
    <td>
    <a href="javascript:void(0)" onclick="$(this).dialog_proses();" data-id="#: company_code #-#: doc_number #-#: doc_yer #">
    <button class="btn btn-xs btn-info"> <i class="glyphicon glyphicon-pencil"></i> <?php echo lang('btn_proses'); ?></button>
    </a>
    </td>
    <td>#: bisnis_area #</td>
    <td>#: doc_number #</td>
    <td>#: doc_yer #</td>
    <td>#: doc_type #</td>
    <td>#: kendo.toString(kendo.parseDate(posting_date, 'yyyy-MM-dd'), 'dd-MM-yyyy') #</td>
    <td style='text-align:right'>#= kendo.toString(amount, "n0")#</td>
    <td>#: gl_account #</td>
    <td>#: text #</td>
    </tr>
</script>

<script type="text/javascript">
    var record = 0;
    var t_amount = 0;
    var t_tax = 0;
    var t_total = 0;
    var t_pph = 0;
    var dv_total = 0;
    var selectedCheked = [];
    $(document).ready(function() {
        var dateLimit = $('.inputDatate').kendoDatePicker({
                format: "dd-MM-yyyy",
        }).data("kendoDatePicker");

        var ds_arclearing = new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    dataType: "json",
                    url: '<?php echo site_url('clearing/ssptint/get'); ?>',
                }
            },
            schema: {
                parse: function(response) {
                    return response.data;
                },
                model: {
                    fields: {
                        amount: {type: "number"},
                        posting_date: {type: "date"},
                    }
                }
            },
            pageSize: 14,
        });

        $('#arclearing_table<?php echo $timestamp; ?>').kendoGrid({
            dataSource: ds_arclearing,
            pageable: true,
            scrollable: true,
            resizable: true,
            height: 550,
            rowTemplate: kendo.template($("#arclearing_table-row-<?php echo $timestamp; ?>").html()),
            dataBinding: function() {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function(e) {
                this.collapseRow(this.tbody.find("tr.k-master-row").first());
                var grid = e.sender;
                if (grid.dataSource.total() == 0) {
                    var colCount = grid.columns.length;
                    $(e.sender.wrapper)
                            .find('tbody')
                            .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
                }
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        contains: "Like",
                        eq: "=",
                        neq: "!="
                    },
                }
            },
            columns: [
                {title:"<?php echo lang('label_action'); ?>",width:100, filterable:false},
                //{field: "company_code", title: "<?php echo lang('dist_cmp_code'); ?>", width: 150},
                {field:"bisnis_area",title:"<?php echo lang('cs_bis'); ?>",width:80},
                {field:"doc_number",title:"<?php echo lang('ar_doc_no'); ?>",width:100},
                {field:"doc_yer",title:"<?php echo lang('ar_doc_yer'); ?>",width:90},
                {field:"doc_type",title:"<?php echo lang('ar_doc_type'); ?>",width:80},
                {field:"posting_date",
                    title:"<?php echo lang('dist_post_date'); ?>",
                    width:100,
                    filterable: {
                        ui: function (e) {
                            e.kendoDatePicker({
                                format: "dd-MM-yyyy"
                            });
                        }
                    }
                },
                {field:"amount",title:"<?php echo lang('dist_amount'); ?>",width:150},
                {field:"gl_account",title:"<?php echo lang('dist_gl_acc'); ?>",width:100},
                {field:"text",title:"<?php echo lang('dist_text'); ?>",width:500},
            ]
        });

        $('#reload<?php echo $timestamp; ?>').click(function() {
            ds_arclearing.read();
        });

        $.fn.dialog_proses = function() {
            $('#dv_balance').html('');
            var uid = $(this).data('id');

            $.ajax({
                url: '<?php echo site_url('clearing/ssptint/get_by_pk'); ?>',
                type: 'POST',
                data: 'id=' + uid,
                dataType: 'json', tail: 1,
                beforeSend: function() {
                    $('#loading-mask').addClass('loader-mask');
                },
                success: function(res) {
                    $.post('<?php echo site_url('master/clear_all_tmp'); ?>',{},function(){
                        ds_clearing_posted.read();
                    });

                    $.ajax({
                        url:'<?php echo site_url('app/get_date'); ?>',
                        type:'POST',
                        data:'company='+res.data[0].company_code,
                        dataType: 'json',tail:1,
                        success:function(res_date){
                            dateLimit.min(new Date(res_date.data[0].DYR_FROM_THN_TWO, res_date.data[0].VCH_FROM_BLN_TWO-1, 1));
                            dateLimit.max(new Date(res_date.data[0].DYR_TO_THN_TWO, res_date.data[0].VCH_TO_BLN_TWO-1, 31));
                        }
                    });

                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=company_code]').val(res.data[0].company_code);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val(res.data[0].bisnis_area);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_number]').val(res.data[0].doc_number);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_yer]').val(res.data[0].doc_yer);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val(res.data[0].amount);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=gl_account]').val(res.data[0].gl_account);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_type]').val(res.data[0].doc_type);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val(res.data[0].bisnis_area);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=item]').val(res.data[0].item);
                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date]').val(res.data[0].posting_date);

                    $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date_new]').val('');
                    $('#form_dialog_proses<?php echo $timestamp; ?> textarea[name=text]').val('');

                    $('#dv_amount').html(numberWithCommas(res.data[0].amount));
                   /* $('#dialog<?php echo $timestamp; ?>').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#dialog<?php echo $timestamp; ?>').modal('show');*/

                    $("#balance_type").data("kendoDropDownList").enable(false);
                    $('#balance_amount').prop('disabled', true);
                    $('#balance_amount').val('');

                    $("#form_link").show();
                    $('.nav-tabs a[href="#form"]').tab('show');

                }
            }).done(function() {
                $('#loading-mask').removeClass('loader-mask');
            });
        }

        $("#form_link").hide();
        $("#cancel").click(function(){
            $('.nav-tabs a[href="#home"]').tab('show');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=company_code]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_number]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_yer]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_type]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=gl_account]').val('');
            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=item]').val('');
            $("#form_link").hide();
        });

        var ds_clearing_posted = new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    dataType: "json",
                    url: '<?php echo site_url('clearing/ssptint/get_posted'); ?>',
                },
                create: {
                    url: '<?php echo site_url('clearing/ssptint/ar_clearing_submit'); ?>',
                    type: "POST",
                    dataType: "json",
                    data: function() {
                        return JSON.parse(JSON.stringify($(this).hiderForm()));
                    }, complete: function(res) {
                        var res_msg = JSON.parse(res.responseText);
                        $('#loading-mask').removeClass('loader-mask');

                        var cur_val = ($('#balance_amount').val().length >0)?$('#balance_amount').val():0;
                        var dv_balance = parseInt($('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val())- parseInt(dv_total);
                        var dv_balance_new = parseInt(dv_balance)- parseInt(cur_val);

                        $('#dv_balance').html(numberWithCommas(dv_balance_new));
                        if (res_msg.status) {
                            $('.nav-tabs a[href="#home"]').tab('show');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=company_code]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_number]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_yer]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_type]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=gl_account]').val('');
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=item]').val('');
                            $("#form_link").hide();
                            $('#simpan').attr("disabled", true);
                            $('#form_dialog_proses<?php echo $timestamp; ?> input[name=calculate]').val(0);
                            $('#hitung').attr('disabled',false);

                            //$('#dialog<?php echo $timestamp; ?>').modal('toggle');
                        }
                        msg_box(res_msg.messages, ['btnOK'], 'Info!');
                        ds_clearing_posted.read();
                        ds_arclearing.read();
                    }
                }
            },
            aggregate: [
                {field: "ar_tax", aggregate: "sum"},
                {field: "ar_total", aggregate: "sum"},
                {field: "posted_value", aggregate: "sum"},
                {field: "clear_amount", aggregate: "sum"}
            ],
            batch: true,
            schema: {
                parse: function(response) {
                    return response.data;
                },
                model: {
                    fields: {
                        doc_number: {editable: false},
                        posting_date: {editable: false},
                        bisnis_area: {editable: false},
                        ar_amount: {type: "number", editable: false},
                        ar_tax: {type: "number", editable: false},
                        ar_total: {type: "number", editable: false},
                        pph_indikator: {type: "number", editable: true},
                        sspt_ppnind: {type: "number", editable: false},
                        pph_amount: {type: "number", editable: false},
                        posted_value: {type: "number", editable: false},
                        clear_amount: {type: "number", editable: true},
                        total_balance: {type: "number", editable: false},
                        status: {editable: false},
                        customer: {editable: false}
                    }
                }
            },
            pageSize: 50,
        });


        $('#gridar<?php echo $timestamp; ?>').kendoGrid({
            dataSource: ds_clearing_posted,
            pageable: false,
            scrollable: true,
            editable: true,
            resizable: true,
            selectable: "multiple cell",
            change: function() {
                $('#simpan').attr("disabled", true);
                $('#form_dialog_proses<?php echo $timestamp; ?> input[name=calculate]').val(0);
                $('#hitung').attr('disabled', false);
            },
            toolbar: kendo.template($("#toolbar<?php echo $timestamp; ?>").html()),
            dataBinding: function() {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function(e) {
                this.collapseRow(this.tbody.find("tr.k-master-row").first());
                var grid = e.sender;
            },
            columns: [
                {title: "<?php echo lang('label_action'); ?>",
                    width: 80,
                    template: '<a href="javascript:void(0)" onclick="$(this).deleteItem();" data-id="#: no #"><i class="glyphicon glyphicon-trash"></i></a>',
                    editable: false,
                    footerAttributes: {"colspan": "5", "style": "text-align:right;"},
                    footerTemplate: "TOTAL :"
                }, {
                    field: "doc_number",
                    title: "<?php echo lang('ar_tbl_doc_numb'); ?>",
                    width: 130,
                    footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"}
                },{
                    field:'customer',
                    width:120,
                    title:"<?php echo lang('ar_cus_name'); ?>",
                    footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"}
                },{
                    field: "posting_date",
                    title: "<?php echo lang('ar_tbl_pst_date'); ?>",
                    width: 100,
                    footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"}
                }, {
                    field: "bisnis_area",
                    title: "<?php echo lang('ar_tbl_bist_area'); ?>",
                    width: 80,
                    footerAttributes: {"class": "hidden-md hidden-lg hidden-xs hidden-sm"}
                }, {
                    field: "",
                    title: "<?php echo lang('ar_tbl_amount'); ?>",
                    width: 150,
                    attributes:{style:'text-align:right'},
                    template: '#= kendo.toString(ar_total-ar_tax, "n0")#',
                    footerTemplate: "#t_tax = parseInt(data.ar_tax ? data.ar_tax.sum: 0 )#  #t_total=parseInt(data.ar_total ? data.ar_total.sum: 0  )# #: kendo.toString(parseInt(t_total-t_tax),'n0')#"
                }, {
                    field: "ar_tax",
                    title: "<?php echo lang('ar_tbl_tax'); ?>",
                    width: 95,
                    template: '#= kendo.toString(ar_tax, "n0")#',
                    footerTemplate: "#: kendo.toString(parseInt(data.ar_tax ? data.ar_tax.sum: 0  ),'n0')#"
                }, {
                    field: "ar_total",
                    title: "<?php echo lang('ar_tbl_total'); ?>",
                    width: 150,
                    template: '#= kendo.toString(ar_total, "n0")#',
                    footerTemplate: "#: kendo.toString(parseInt(data.ar_total ? data.ar_total.sum: 0  ),'n0')#"
                },{
                    field:"sspt_ppnind",
                    title:"PPN Ind",
                    width:120,
                    attributes:{style:'text-align:right'},
                },{
                    field: "posted_value",
                    title: "<?php echo lang('ar_tbl_posted'); ?>",
                    width: 150,
                    template: '#= kendo.toString(posted_value, "n0")#',
                    footerTemplate: "#= kendo.toString(data.posted_value?data.posted_value.sum : 0,'n0') #"
                },{
                    field:"numb_bukpot",
                    title:"NO SSP",
                    width:150,
                    attributes: {"class": "alert-success"},
                },{
                    field: "clear_amount",
                    title: "<?php echo lang('ar_tbl_clear'); ?>", width: 150,
                    attributes: {"class": "alert-success", style:'text-align:right'},
                    template: '#= kendo.toString(clear_amount, "n0")#',
                    footerTemplate: "#dv_total = data.clear_amount?data.clear_amount.sum : 0; # #= kendo.toString(data.clear_amount?data.clear_amount.sum : 0,'n0') #"
                },{
                    field: "",
                    title: "<?php echo lang('ar_tbl_balance'); ?>",
                    width: 150,
                    template:'#= kendo.toString(ar_tax - (clear_amount+posted_value), "n0")#'
                },
                {field: "status", title: "<?php echo lang('ar_tbl_stat'); ?>", width: 150},
            ]
        });

        $('#form_dialog_proses<?php echo $timestamp; ?>').submit(function(e) {
            e.preventDefault();
            $('#loading-mask').addClass('loader-mask');
            var cekData = $('#gridar<?php echo $timestamp; ?>').data('kendoGrid').dataSource.total();
            if (cekData > 0) {
                ds_clearing_posted.sync();
                ds_arclearing.read();
            }
            else {
                $('#loading-mask').removeClass('loader-mask');
                msg_box("<?php echo lang('msg_null_item_detail'); ?>", ['btnOK'], 'Info!');
            }
        });

        var cmbBalance = $('#balance_type')
          .kendoDropDownList({
            autoBind: false,
            dataTextField: "text",
            dataValueField: "id",
            optionLabel: "Please Select...",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_balance'); ?>",
                }
              }
            }
        });


        var ds_cmb_balance = new kendo.data.DataSource({
            serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_customer_bytmp'); ?>",
                }
              }
         });
        $('#customer_balance').kendoDropDownList({
            filter: "contains",
            autoBind: false,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: ds_cmb_balance

        });

        $('#dv_balance_cust').hide();
        cmbBalance.bind('change',function(){
            var cur_val = $("#balance_type").data("kendoDropDownList").value();
            if(cur_val=='Titipan'){
                $('#dv_balance_cust').show();
                ds_cmb_balance.read();
            }else{
                $('#dv_balance_cust').hide();
            }
        });

        $('#balance_amount').keyup(function(){
            var cur_val = ($(this).val().length >0)?$(this).val():0;
            var dv_balance = parseInt($('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val())- parseInt(dv_total);
            var dv_balance_new = parseInt(dv_balance)- parseInt(cur_val);
            $('#dv_balance').html(numberWithCommas(dv_balance_new));
        });

        $("#balance_type").data("kendoDropDownList").enable(false);
        $('#balance_amount').prop('disabled', true);
        $('#balance_indicator').change(function(){
                ds_cmb_balance.read();
            if ($(this).is(':checked')) {
                $('#balance_amount').prop('disabled', false);
                $("#balance_type").data("kendoDropDownList").enable(true);
            }else{
                $('#balance_amount').prop('disabled', true);
                $('#balance_amount').val('');
                $("#balance_type").data("kendoDropDownList").enable(false);

            }
        });

        $.fn.hiderForm = function() {
            out = {
                'company_code': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=company_code]').val(),
                'bisnis_area': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val(),
                'doc_number': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_number]').val(),
                'doc_yer': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_yer]').val(),
                'curency': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=curency]').val(),
                'amount': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=amount]').val(),
                'posting_date': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date]').val(),
                'doc_type': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=doc_type]').val(),
                'text': $('#form_dialog_proses<?php echo $timestamp; ?> textarea[name=text]').val(),
                'pcalculate': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=calculate]').val(),
                'gl_account': $('#form_dialog_proses<?php echo $timestamp; ?> input[name=gl_account]').val(),
                'balance_indicator':  ($('#balance_indicator').is(':checked'))?'1':'-1',
                'balance_amount':  $('#form_dialog_proses<?php echo $timestamp; ?> input[name=balance_amount]').val(),
                'balance_type':  $('#form_dialog_proses<?php echo $timestamp; ?> input[name=balance_type]').val(),
                'item':  $('#form_dialog_proses<?php echo $timestamp; ?> input[name=item]').val(),
                'posting_date_new':  $('#form_dialog_proses<?php echo $timestamp; ?> input[name=posting_date_new]').val(),
                'customer_balance':  $('#form_dialog_proses<?php echo $timestamp; ?> input[name=customer_balance]').val()
            }
            return out;
        }

        $.fn.deleteItem = function() {
            var uid = $(this).data('id');
            $.ajax({
                url: '<?php echo site_url('clearing/ssptint/posteditem_deltete'); ?>',
                type: 'POST',
                data: 'id=' + uid,
                dataType: 'json', tail: 1,
                success: function(res) {
                    ds_cmb_balance.read();
                    if (res.status)
                        ds_clearing_posted.read();
                }
            }).done(function() {
                ds_clearing_posted.read();//$('#loading-mask').removeClass('loader-mask');
            });
        }

        $.fn.reload_item = function() {
            ds_clearing_posted.read();
        }

        $.fn.get_data_filter = function(){
            kendoWindow.find("#param").val("1");
            ds_findItem.read();
        }

        var ds_findItem = new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    dataType: "json",
                    data: function() {
                        var filter_doc_number   = kendoWindow.find('#doc_number').val();
                        var filter_text         = kendoWindow.find('#text').val();
                        var filter_bis_area     = $('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val();
                        var filter_customer     = kendoWindow.find('#cus_name').val();
                        var filter_yer          = kendoWindow.find('#filter_tahun').val();
                        var filter_billing_doc  = kendoWindow.find('#filter_billing_doc').val();
                        var filter_param        = kendoWindow.find('#param').val();
                        var filter_comp_code    = kendoWindow.find('#curent_companicode').val();

                        var filter = {
                            'bisnis_area': filter_bis_area,
                            'doc_number': filter_doc_number,
                            'text': filter_text,
                            'customer': filter_customer,
                            'yer': filter_yer,
                            'billing_doc': filter_billing_doc,
                            'cmp_code': filter_comp_code,
                            'param': filter_param
                        }
                        kendoWindow.find("#param").val("");
                        return JSON.parse(JSON.stringify(filter));
                    },
                    url: '<?php echo site_url('clearing/ssptint/get_findItem'); ?>',
                },
            },
            schema: {
                parse: function(response) {
                    return response.data;
                }, model: {
                    fields: {
                        amount: {type: "number"},
                    }
                },
            },
            pageSize: 50,
            batch: true,
        });

        kendoWindow = $("<div />").kendoWindow({
            title: "<?php echo lang('nfc_dialog'); ?>",
            width: "1200px",
            height: "600px",
            resizable: false,
            modal: true,
        });

        $.fn.close_window=function(){
            kendoWindow.data("kendoWindow").close();
        }

        $.fn.add_coma=function(){
                var mydata = $(this).val().split(' ').join(",");
                $(this).val(mydata);
        }

        $.fn.getItem = function() {
            $('#simpan').prop('disabled', true);
            $('#calculate').val(0);

            kendoWindow.data("kendoWindow")
                    .content($("#dialog_templates").html())
                    .center().open();

            kendoWindow.find('.cus_name')
              .kendoDropDownList({
                filter: "contains",
                autoBind: false,
                dataTextField: "text",
                dataValueField: "id",

                dataSource: {
                  serverFiltering: true,
                  transport: {
                    read: {
                      dataType: "jsonp",
                      url: "<?php echo site_url('app/get_cmb_customer'); ?>",
                    }
                  }
                }
            });

            kendoWindow.find('.myGrid').kendoGrid({
                dataSource: ds_findItem,
                sortable: true,
                pageable: true,
                height: 410,
                scrollable: true,
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Like",
                            eq: "=",
                            neq: "!="
                        },
                    }
                },
                toolbar: kendo.template($("#toolbar-item<?php echo $timestamp; ?>").html()),
                dataBound: function(e) {
                    this.collapseRow(this.tbody.find("tr.k-master-row").first());
                    var grid = e.sender;
                    if (grid.dataSource.total() == 0) {
                        var colCount = grid.columns.length;
                        $(e.sender.wrapper)
                                .find('tbody')
                                .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
                    }

                    $("#myCheckbox").attr('checked', false);
                    for (var i=0; i<selectedCheked.length; i++) {
                        var mId = selectedCheked[i];
                        $("#"+mId).prop("checked", true);
                    }

                    $("#select_all").prop('checked', false);
                },
                columns: [
                    {
                        //title: "<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",
                        title:"<input type='checkbox' id='select_all'  onClick='$(this).cek_all();' /> All<br/>",
                        width:50,
                        template: '<input type="checkbox" class="id" name="id[]" onClick="$(this).chekedId();" id="#: company_code #-#:doc_number#-#:doc_yer#-#:item#" data-id="#: company_code #-#:doc_number#-#:doc_yer#-#:item#" value="#: company_code #-#:doc_number#-#:doc_yer#-#:item#">',
                        //template: '<input type="checkbox" class="id" name="id[]" value="#: company_code #-#:doc_number#-#:doc_yer#-#:item#">',
                    },{
                        field:"company_code",
                        width:100,
                        title:"<?php echo lang('ar_wnd_cpcode'); ?>"
                    }, {
                        field: "bisnis_area",
                        title: "<?php echo lang('ar_tbl_bist_area'); ?>",
                        width: 100,
                    },{
                        field:'customer',
                        width:120,
                        title:"<?php echo lang('ar_cust'); ?>"
                    },{
                        field:'doc_number',
                        width:150,
                        title:"<?php echo lang('ar_wnd_dcno'); ?>"
                    },{
                        field:'doc_yer',
                        width:100,
                        title:"<?php echo lang('ar_wnd_dcyer'); ?>"
                    },{
                        field:'billing_doc',
                        width:150,
                        title:"<?php echo lang('dist_billing_doc'); ?>"
                    },{
                        field:'text',
                        width:300,
                        title:"<?php echo lang('dist_hider_text'); ?>"
                    },{
                        field:'amount',
                        width:100,
                        title:"<?php echo lang('dist_amount'); ?>",
                        template:'#= kendo.toString(amount, "n0")#'
                    },
                ]
            });
            ds_findItem.read();
            kendoWindow.find('#curent_area').val($('#form_dialog_proses<?php echo $timestamp; ?> input[name=bisnis_area]').val());
            kendoWindow.find('#curent_companicode').val($('#form_dialog_proses<?php echo $timestamp; ?> input[name=company_code]').val());
        }

        $('#hitung').prop('disabled', false);
        $('#simpan').attr('disabled', true);
        $('#hitung').click(function() {
            $('#simpan').prop('disabled', false);
            $('#loading-mask').addClass('loader-mask');
            var cekData = $('#gridar<?php echo $timestamp; ?>').data('kendoGrid').dataSource.total();
            if (cekData > 0) {
                ds_clearing_posted.sync();
                $('#calculate').val('1');
                $(this).prop('disabled', true);

            } else {
                $('#loading-mask').removeClass('loader-mask');
                msg_box("<?php echo lang('msg_null_item_detail'); ?>", ['btnOK'], 'Info!');
            }
        });

        $.fn.cek_all=function(){
            var checkedVals ='';
            if($("#select_all").is(':checked')){
                $(".id").prop('checked', true);
                var checkedVals = $('.id:checkbox:checked').map(function() {
                    return this.value;
                }).get();

                var spl_val = checkedVals.toString().split(',');
                for(var i=0, n=spl_val.length;i<n;i++) {
                    selectedCheked.push(spl_val[i]);
                }
            }else{
                var checkedVals = $('.id:checkbox:checked').map(function() {
                    return this.value;
                }).get();
                spl_val = checkedVals.toString().split(',');
                for(var i=0, n=spl_val.length;i<n;i++) {
                    var remove_index = selectedCheked.indexOf(spl_val[i]);
                    selectedCheked.splice(remove_index);
                }
                $(".id").prop('checked', false);
            }
        }

        $.fn.chekedId=function(){
            var selecMeId = $(this).data('id');
            if(document.getElementById(selecMeId).checked) {
                selectedCheked.push(selecMeId);
            }else{
                var remove_index = selectedCheked.indexOf(selecMeId);
                selectedCheked.splice(remove_index);
            }
        }

        $.fn.selectItem = function() {
            var checkedVals = $('.id:checkbox:checked').map(function() {
                return this.value;
            }).get();

            if (checkedVals != '') {
                $.ajax({
                    url: '<?php echo site_url('clearing/ssptint/get_posted'); ?>',
                    data: 'id=' + checkedVals,
                    type: 'POST',
                    dataType: 'json', tail: 1,
                    success: function(res) {
                        kendoWindow.data("kendoWindow").close();
                        ds_clearing_posted.read();
                        ds_cmb_balance.read();
                    }
                });

            } else
                msg_box('<?php echo lang('msg_no_selected'); ?>', ['btnOK'], 'Info!');
        }

        pasteWindow = $("<div />").kendoWindow({
            title: "<?php echo lang('nfc_dialog'); ?>",
            width: "450px",
            height: "480px",
            resizable: false,
            modal: true,
        });

        $.fn.paste_value=function(){
            pasteWindow.data("kendoWindow")
             .content($("#dialog_paste").html())
             .center().open();


             var ds_paste=new kendo.data.DataSource({
                transport: {
                    read: {
                        type: "POST",
                        dataType: "json",
                        url: '<?php echo site_url('clearing/ssptint/get_posted'); ?>',
                    },create: {
                        url: '<?php echo site_url('app/set_paste_tmp'); ?>',
                        type:"POST",
                        dataType: "json",
                        complete: function(res) {
                            pasteWindow.data("kendoWindow").close();
                            ds_clearing_posted.read();
                        }
                    }
                },
                schema: {
                    parse: function(response) {
                        return response.data;
                    },
                    model: {
                        fields: {
                            clear_amount: {type: "number"},
                        }
                    }
                },
                batch: true,
            });

             ds_paste.read();

             pasteWindow.find('.grid_paste').kendoGrid({
                scrollable: true,
                height: 400,
                dataSource: ds_paste,
                columns: [
                  {
                    title:'DOC NUMB',
                    field: "doc_number"
                 },{
                    title:"CLEAR",
                    field: "clear_amount",
                    template: '#= kendo.toString(clear_amount, "n0")#',
                 }
                ]
              }).on('focusin', function(e) {
                // get the grid position
                var offset = $(this).offset();
                // crete a textarea element which will act as a clipboard
                var textarea = $("<textarea>");
                // position the textarea on top of the grid and make it transparent
                textarea.css({
                  position: 'absolute',
                  opacity: 0,
                  top: offset.top,
                  left: offset.left,
                  border: 'none',
                  width: $(this).width(),
                  height: $(this).height()
                })
                .appendTo('body')
                .on('paste', function() {
                  setTimeout(function() {
                    var value = $.trim(textarea.val());
                    var grid =  pasteWindow.find('.grid_paste').data("kendoGrid");
                    var rows = value.split('\n');
                    var row_data = grid.dataSource._data;
                    var data = [];

                    for (var i = 0; i < rows.length; i++) {
                      var cells = rows[i].split('\t');
                      data.push({
                        doc_number: row_data[i].doc_number,
                        clear_amount: cells[0]
                      });
                    };
                    grid.dataSource.data(data);
                  });
                }).on('focusout', function() {
                  $(this).remove();
                });
                setTimeout(function() {
                  textarea.focus();
                });
              });

            pasteWindow.find('.paste_button').click(function(){
                ds_paste.sync();
            });
        }


    });

    function toggle(source) {
        checkboxes = document.getElementsByClassName('id');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = source.checked;
        }
    }

    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1,$2");
        return x;
    }
</script>

<style type="text/css">
    .myInput {
        width: 100%;
        padding: 8px 15px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }
</style>

<script id="dialog_templates" type="text/x-kendo-template">
<div class="col-sm-12">
    <form class="form-horizontal">
        <div class="col-sm-5" style="float:left;">
            <input type="hidden" name="param" id ="param" value="">
            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="number"><?php echo lang('ar_doc_no'); ?> :</label>
                <div class="col-sm-7" >
                    <input type="text" class="myInput" id="doc_number" onkeyup="$(this).add_coma();" name='doc_number' placeholder="<?php echo lang('ar_doc_no'); ?>">
                </div>
            </div>

            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="text"><?php echo lang('dist_hider_text'); ?> :</label>
                <div class="col-sm-7" >
                    <input type="text" class="myInput" id="text" onkeyup="$(this).add_coma();" name='text' placeholder="<?php echo lang('dist_hider_text'); ?>">
                </div>
            </div>

            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="customer"><?php echo lang('ar_cus_name'); ?> :</label>
                <div class="col-sm-7" >
                    <input type="text" class=" cus_name" id="cus_name"  name='cus_name' style="width:100%">
                </div>
            </div>
        </div>

        <div class="col-sm-5" style="float:left;">

            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="customer"><?php echo lang('dist_yer_doc'); ?> :</label>
                <div class="col-sm-2" >
                    <input type="number" maxlength ="4" class="myInput" id="filter_tahun" value="<?php echo date('Y'); ?>" name='filter_tahun' placeholder="<?php echo lang('dist_yer_doc'); ?>">
                </div>
                <label class="control-label col-sm-1" style='padding-top:20px; ' for="customer">Area :</label>
                <div class="col-sm-3" >
                    <input type="text" class="myInput" readonly id="curent_area" value="">
                    <input type="hidden" class="myInput" readonly id="curent_companicode" value="">
                </div>
            </div>

            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="customer"><?php echo lang('dist_billing_doc'); ?> :</label>
                <div class="col-sm-7" >
                    <input type="text" class="myInput" id="filter_billing_doc" onkeyup="$(this).add_coma();" name='filter_billing_doc' placeholder="<?php echo lang('dist_billing_doc'); ?>">
                </div>
            </div>

            <div class="form-group" style='margin-bottom:3px; margin-top:0px;'>
                <label class="control-label col-sm-3" style='padding-top:20px; ' for="action">&nbsp;</label>
                <div class="col-sm-7" >
                     <button type="button" id='proses' onclick="$(this).get_data_filter();" class="btn btn-primary"  ><?php echo lang('btn_proses'); ?></button>
                     <button type="button" id='cancel' onclick="$(this).close_window();" class="btn btn-danger"  ><?php echo lang('pwd_btn_cancel'); ?></button>
                </div>
            </div>

        </div>
    </form>
</div>
    <div style="clear:both;"></div>
    <div class="myGrid"></div>
</script>

<script type="text/x-kendo-template" id="toolbar-item<?php echo $timestamp; ?>">
    <div class="toolbar">
    <button type="button" onclick="$(this).selectItem();" class="btnxx btn-infoxx k-button k-button-icontext " ><?php echo lang('btn_select'); ?></button>
    </div>
</script>

<script type="text/x-kendo-template" id="toolbar<?php echo $timestamp; ?>">
    <div class="refreshBtnContainer">
    <a href="javascript:void(0)" onclick="$(this).reload_item();" class="k-pager-refresh k-link k-button" title="Refresh"><span class="fa fa-refresh"></span></a>
    </div>
    <div class="toolbar">
        <button type="button" onclick="$(this).getItem();" class="btnxx btn-infoxx k-button k-button-icontext " ><?php echo lang('btn_add_item'); ?></button>
        <button type="button" onclick="$(this).paste_value();" class="btnxx btn-infoxx k-button k-button-icontext " >Paste Value</button>
    </div>
</script>

<div id="dialog<?php echo $timestamp; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">

    </div>
</div>
