<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ssptint extends MY_Controller {

    var $tbl_hider = 'TF_BKPF';
    var $tbl_detail = 'TF_BSEG';
    var $fild_comp_code = 'CHR_BUKRS';
    var $fild_bisnis_area = 'VCH_GSBER';
    var $fild_cost_center = 'VCH_KOSTL';
    var $fild_gl_account = 'VCH_HKONT';
    var $fild_curency = 'CHR_CURENCY';
    var $fild_amount = 'INT_WRBTR';
    var $fild_posting_date = 'DAT_BUDAT';
    var $fild_doc_date = 'DAT_BLDAT';
    var $fild_reverence = 'CHR_XBLNR';
    var $fild_doc_type = 'ENM_BLART';
    var $fild_acounting_no = 'VCH_BELNR';
    var $fild_fiscal_year = 'DYR_GJAHR';
    var $fild_text = 'TEX_SGTXT';
    var $fild_internal_order = 'VCH_AUFNR';
    var $fild_line_item = 'VCH_BUZEI';
    var $fild_posting_key = 'VCH_BSCHL';
    var $fild_indicator_saldo = 'ENM_SHKZG';
    var $fild_group_id = 'CHR_GROUP';
    var $fild_flag = 'CHR_FLAG';
    var $cfg_arClearing = array();
    var $fild_doc_hider = 'CHR_BKTXT';
    var $fild_clearing_date = 'DAT_AUGCP';
    var $fild_clearing_doc_no = 'VCH_AUGBL';
    var $fild_doc_number_alias = 'VCH_REBZG';
    var $fild_doc_yer_alias = 'DYR_REBZJ';
    var $fild_customer = 'VCH_KUNNR';
    var $fild_doc_item_alias = 'INT_REBZZ';
    var $key_bank = '40';
    var $key_tagihan = '15';
    var $fild_special           = 'VCH_IND_JURNAL';
    var $fild_billing_doc           = 'CHR_AWKEY';
    var $fild_petugas           = 'petugas_id';
    var $fild_sspt_ppnind            = 'sspt_ppnind';

    public function __construct() {
        parent::__construct();
        $this->load->module('app');
        $this->cfg_arClearing = json_decode(json_encode($this->config->item('ar_sspt_int')));
        $this->app->cek_permit();
    }

    public function index() {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $this->template->title(lang('titile_arint_form'));
        $this->template->build('clearing/ssptint_tbl');
    }

    function get() {
        $this->load->model('master/master_model', 'mod_mst');

        $fild = "h." . $this->fild_comp_code . " as company_code,";
        $fild .= "h." . $this->fild_acounting_no . " as doc_number,";
        $fild .= "h." . $this->fild_fiscal_year . " as doc_yer,";
        $fild .= "h." . $this->fild_doc_type . " as doc_type,";
        $fild .= "h." . $this->fild_posting_date . " as posting_date,";
        $fild .= "d." . $this->fild_posting_key . " as post_key,";
        $fild .= "d." . $this->fild_amount . " as amount,";
        $fild .= "d." . $this->fild_gl_account . " as gl_account,";
        $fild .= "d." . $this->fild_text . " as text,";
        $fild .= "d." . $this->fild_line_item . " as item,";
        $fild .= "d." . $this->fild_bisnis_area . " as bisnis_area";

        $filter['h.'.$this->fild_doc_type.' IN("'.implode('","',explode(',',$this->cfg_arClearing->cf_bank_doc_type)).'")']= NULL;
        //$filter['h.' . $this->fild_doc_type] = $this->cfg_arClearing->cf_bank_doc_type;
        $filter['d.' . $this->fild_clearing_doc_no . ' IS NULL '] = NULL;
        $filter['d.' . $this->fild_posting_key] = $this->cfg_arClearing->cf_bank_key;

        if($this->session->userdata('level') ==USER_AREA && $this->session->userdata('limit_id') !=""){
            $filter['d.'.$this->fild_bisnis_area.' IN("'.str_replace(',', '","', $this->session->userdata('limit_id')).'")'] = NULL;
        }

        $res = $this->mod_mst->get_join($fild,$filter);

        //$res = array_merge($res, $this->get_titipan());
        echo json_encode(array('data' => $res));
    }

    private function get_titipan(){
        $this->load->model('master/master_model','mod_mst');
        $cfg_titipan = json_decode(json_encode($this->config->item('titipan')));

        $fild  = "h.".$this->fild_comp_code." as company_code,";
        $fild .= "h.".$this->fild_acounting_no." as doc_number,";
        $fild .= "h.".$this->fild_fiscal_year." as doc_yer,";
        $fild .= "h.".$this->fild_doc_type." as doc_type,";
        $fild .= "h.".$this->fild_posting_date." as posting_date,";
        $fild .= "d.".$this->fild_posting_key." as post_key,";
        $fild .= "d.".$this->fild_amount." as amount,";
        $fild .= "d.".$this->fild_gl_account." as gl_account,";
        $fild .= "d.".$this->fild_text." as text,";
        $fild .= "d.".$this->fild_bisnis_area." as bisnis_area";

        $filter['h.'.$this->fild_doc_type." IN('".implode("','", explode(",", $cfg_titipan->type_doc_in))."')"]= NULL;
        $filter['d.'.$this->fild_clearing_doc_no.' IS NULL ']=NULL;
        $filter['d.'.$this->fild_posting_key." IN('".implode("','", explode(",", $cfg_titipan->posting_key_titipan))."')"]= NULL;
        //$filter['d.'.$this->fild_posting_key]=$cfg_titipan->posting_key_titipan;
        //$filter['d.'.$this->fild_special] = $cfg_titipan->initial_key;
        $filter['LEFT(d.'.$this->fild_acounting_no.', 3) NOT IN("'.implode('","',explode(',',$cfg_titipan->exseption_key)).'")'] = NULL;

        if($this->session->userdata('level') ==USER_AREA && $this->session->userdata('limit_id') !=""){
            $filter['d.'.$this->fild_bisnis_area.' IN("'.str_replace(',', '","', $this->session->userdata('limit_id')).'")'] = NULL;
        }

        $res = $this->mod_mst->get_join($fild,$filter);

        return $res;
    }

    function get_by_pk() {
        $this->load->model('master/master_model', 'mod_mstr');
        $out['data'] = array();
        $arrayId = explode('-', $this->input->post('id'));

        $this->session->unset_userdata('arClearing');

        $fild = "h." . $this->fild_comp_code . " as company_code,";
        $fild .= "h." . $this->fild_acounting_no . " as doc_number,";
        $fild .= "h." . $this->fild_fiscal_year . " as doc_yer,";
        $fild .= "h." . $this->fild_doc_type . " as doc_type,";
        $fild .= "DATE_FORMAT(h.".$this->fild_posting_date.",'%d-%m-%Y') as posting_date,";
        $fild .= "h." . $this->fild_doc_date . " as doc_date,";
        $fild .= "d." . $this->fild_posting_key . " as post_key,";
        $fild .= "d." . $this->fild_amount . " as amount,";
        $fild .= "d." . $this->fild_gl_account . " as gl_account,";
        $fild .= "d." . $this->fild_bisnis_area . " as bisnis_area,";
        $fild .= "d." . $this->fild_line_item . " as item,";
        $fild .= "d." . $this->fild_text . " as text";

        $filter['h.' . $this->fild_comp_code] = isset($arrayId[0]) ? $arrayId[0] : '';
        $filter['h.' . $this->fild_acounting_no] = isset($arrayId[1]) ? $arrayId[1] : '';
        $filter['h.' . $this->fild_fiscal_year] = isset($arrayId[2]) ? $arrayId[2] : '';
        //$filter['d.' . $this->fild_posting_key] = NULL; //$this->cfg_arClearing->cf_bank_key;

        $res = $this->mod_mstr->get_join($fild, $filter);

        //$res = array_merge($res, $this->get_titipan());
        if (count($res) > 0)
            $out['data'] = $res;

        echo json_encode($out);
    }

    function get_findItem($jsonOut = TRUE) {
        $this->load->model('master/master_model', 'mod_mstr');
        $doc_type = array('RV', 'LL', 'DN', 'DP');
        $out['data'] = array();
        $bisnis_area = $this->input->post('bisnis_area');
        $text = $this->input->post('text');
        $custon_doc_num = $this->input->post('doc_number');
        $customer = $this->input->post('customer');
        $yer = $this->input->post('yer')+0;
        $param = $this->input->post('param');
        $filter_billing_doc = $this->input->post('billing_doc');
        $filter_cmp_code = $this->input->post('cmp_code');

        $fild = "h." . $this->fild_comp_code . " as company_code,";
        $fild .= "h." . $this->fild_acounting_no . " as doc_number,";
        $fild .= "h." . $this->fild_fiscal_year . " as doc_yer,";
        $fild .= "h." . $this->fild_doc_type . " as doc_type,";
        $fild .= "d." . $this->fild_posting_key . " as post_key,";
        $fild .= "d." . $this->fild_amount . " as amount,";
        $fild .= "d." . $this->fild_line_item . " as item,";
        $fild .= "d." . $this->fild_customer . " as customer,";
        $fild .= "h.".$this->fild_billing_doc." as billing_doc,";
        $fild .= "d." . $this->fild_bisnis_area . " as bisnis_area,";
        $fild .= "d.VCH_NUMBPOT_SSP as numb_bukpot,";
        $fild .= "d.sspt_ppnind,";
        if (!$jsonOut) {
            $fild .= "h." . $this->fild_posting_date . " as posting_date,";
            $fild .= "h." . $this->fild_doc_date . " as doc_date,";
        }

        $fild .= "d." . $this->fild_bisnis_area . " as bisnis_area,";
        $fild .= "h." . $this->fild_doc_hider . " as text";

        $filter['d.' . $this->fild_posting_key] = '01';
        $filter['h.' . $this->fild_doc_type . " IN('" . implode("','", $doc_type) . "')"] = NULL;
        //$filter['d.' . $this->fild_clearing_doc_no . ' IS NULL'] = NULL;


        if (strlen($custon_doc_num) > 0  && $jsonOut) {
            $custon_doc_num_split = explode(',', $custon_doc_num);
            if (count($custon_doc_num_split) >1) {
                $filter['d.'.$this->fild_acounting_no.' IN ("'.implode('","', $custon_doc_num_split).'")'] = null;
            } else {
                $filter['d.'.$this->fild_acounting_no.' LIKE "%'.$custon_doc_num.'%"'] = null;
            }
        }

        if(strlen($yer)>0 && $jsonOut || $yer !=0 && $jsonOut)
            $filter['d.'.$this->fild_fiscal_year]=trim($yer);

        if(strlen($text)>0 && $jsonOut){
            $text_split = explode(',',$text);
            if(count($text_split) >1)
                $filter['h.'.$this->fild_doc_hider.' IN ("'.implode('","',$text_split).'")'] = NULL;
            else
                $filter['h.'.$this->fild_doc_hider.' LIKE "%'.$text.'%"'] = NULL;
        }

        if(strlen($filter_billing_doc) > 0  && $jsonOut){
            $billing_split = explode(',',$filter_billing_doc);
            if(count($billing_split) >1)
                $filter['h.'.$this->fild_billing_doc.' IN ("'.implode('","',$billing_split).'")'] = NULL;
            else
                $filter['h.'.$this->fild_billing_doc.' LIKE "%'.$filter_billing_doc.'%"'] = NULL;
        }

        if($customer !='-1' && $jsonOut && strlen($customer) > 0 ){
                $filter['d.'.$this->fild_customer] = $customer;
        }

        if($jsonOut)
            $filter['d.' . $this->fild_comp_code] = $filter_cmp_code;

        if (!$jsonOut) {
            $filterId = explode(",", $this->input->post('id'));
            foreach ($filterId as $row) {
                $ar_limit_id = explode('-', $row);
                $company_code[] = $ar_limit_id[0];
                $doc_number[] = $ar_limit_id[1];
                $fiscal_yer[] = $ar_limit_id[2];
                $row_index[] = $ar_limit_id[3];
            }
            $filter['d.' . $this->fild_comp_code . " IN('" . implode("','", $company_code) . "')"] = NULL;

            $filter['d.' . $this->fild_acounting_no . " IN('" . implode("','", $doc_number) . "')"] = NULL;
            $filter['d.' . $this->fild_fiscal_year . " IN('" . implode("','", $fiscal_yer) . "')"] = NULL;
            $filter['d.' . $this->fild_line_item . " IN('" . implode("','", $row_index) . "')"] = NULL;
        }
        $filter['d.'.$this->fild_sspt_ppnind] = '2';
        $filter['d.ssp_status'] = '0';

        $res = $this->mod_mstr->get_join($fild, $filter);


        $limitOut = $this->mod_mstr->get_tmp(
                array(
                    'user_create' => $this->session->userdata('user')
                    //'tmp_for' => 'ARINT'
                )
        );
        if (count($limitOut) > 0) {
            foreach ($res as $key => $row) {
                foreach ($limitOut as $keylimit => $ar_limit) {
                    if (($ar_limit->company_code == $row->company_code) && ($ar_limit->doc_number == $row->doc_number) && ($ar_limit->doc_yer == $row->doc_yer) && ($ar_limit->item == $row->item))
                        unset($res[$key]);
                }
            }

            $res_out = array();
            foreach ($res as $key => $value) {
                $res_outx['company_code'] = $value->company_code;
                $res_outx['doc_number'] = $value->doc_number;
                $res_outx['bisnis_area'] = $value->bisnis_area;
                $res_outx['doc_yer'] = $value->doc_yer;
                $res_outx['doc_type'] = $value->doc_type;
                $res_outx['post_key'] = $value->post_key;
                $res_outx['amount'] = $value->amount;
                $res_outx['item'] = $value->item;
                $res_outx['text'] = $value->text;
                $res_outx['sspt_ppnind']    = $value->sspt_ppnind;
                $res_outx['customer'] = $value->customer;
                 $res_outx['numb_bukpot']    = $value->numb_bukpot;
                if (!$jsonOut) {
                    $res_outx['posting_date'] = $value->posting_date;
                    $res_outx['bisnis_area'] = $value->bisnis_area;
                    $res_outx['doc_date'] = $value->doc_date;
                }
                array_push($res_out, $res_outx);
            }
            $res = json_decode(json_encode($res_out));
        }


        $out['data'] = $res;
        if ($jsonOut)
            if($param =='')
                echo json_encode(array('data' => array()));
            else
                echo json_encode($out);
        else
            return $out;
    }

    function get_posted() {
        $out['data'] = array();
        $post_id = $this->input->post('id');
        $row_item = array();
        $row = array();
        $this->load->model('master/master_model', 'mod_mstr');

        if ($post_id) {
            $x_row = current($this->get_findItem(FALSE));
            foreach ($x_row as $key => $value) {
                $posted_value = $this->get_posted_value($value->company_code, $value->doc_number, $value->doc_yer);
                $tax = $this->get_tax($value->company_code, $value->doc_number, $value->doc_yer);
                $biaya_materai      = $this->get_meterai_fee($value->company_code, $value->doc_number, $value->doc_yer);
                $balance = ($value->amount - $tax-$biaya_materai) * (2 / 100) + 0 + $posted_value;

                $dsx['tmp_for'] = 'SPI';
                $dsx['user_create'] = $this->session->userdata('user');
                $dsx['company_code'] = $value->company_code;
                $dsx['doc_number'] = $value->doc_number;
                $dsx['doc_yer'] = $value->doc_yer;
                $dsx['doc_type'] = $value->doc_type;
                $dsx['doc_date'] = $value->doc_date;
                $dsx['posting_date'] = $value->posting_date;
                $dsx['bisnis_area'] = $value->bisnis_area;
                $dsx['posted_value'] = $posted_value;
                $dsx['item'] = $value->item;
                $dsx['ar_total'] = $value->amount;
                $dsx['customer'] = $value->customer;
                $dsx['sspt_ppnind'] = $value->sspt_ppnind;
                $dsx['numb_bukpot'] = $value->numb_bukpot;
                $dsx['ar_tax'] = $tax;
                $dsx['pph_indikator'] = $this->cfg_arClearing->df_pph;
                $dsx['clear_amount'] = '0';
                $dsx['status'] = ($value->amount == $balance) ? 'Clear' : 'Un Clear';
                $dsx['materai_fee']     = $biaya_materai;

                $this->mod_mstr->add_tmp($dsx);
            }
        }


        $row = $this->mod_mstr->get_tmp(
                array(
                    'user_create' => $this->session->userdata('user'),
                    'tmp_for' => 'SPI'
                )
        );
        if (count($row) > 0 && $row != FALSE)
            $out['data'] = $row;

        echo json_encode($out);
    }

    function get_tax($comp_code, $aconting_number, $yer) {
        $this->load->model('master/master_model', 'mod_mstr');

        $gl = $this->config->item('gl_tax');
        $fild = 'SUM(' . $this->fild_amount . ') as tax';

        $filter[$this->fild_gl_account . ' IN("' . implode('","', $gl) . '")'] = NULL;
        $filter[$this->fild_comp_code] = $comp_code;
        $filter[$this->fild_acounting_no] = $aconting_number;
        $filter[$this->fild_fiscal_year] = $yer;

        $res = current($this->mod_mstr->get_detail($fild, $filter));
        if ($res->tax != '')
            return $res->tax;
        else
            return 0;
    }

    public function get_meterai_fee($comp_code, $aconting_number, $yer)
    {
        $this->load->model('master/master_model', 'mod_mstr');

        $gl = $this->config->item('gl_materai');
        $fild = 'SUM('.$this->fild_amount.') as tax';

        $filter[$this->fild_gl_account.' IN("'.implode('","', $gl).'")']= null;
        $filter[$this->fild_comp_code] = $comp_code;
        $filter[$this->fild_acounting_no] = $aconting_number;
        $filter[$this->fild_fiscal_year] = $yer;

        $res = current($this->mod_mstr->get_detail($fild, $filter));
        if ($res->tax !='') {
            return $res->tax;
        } else {
            return 0;
        }
    }

    function get_posted_value($comp_code, $aconting_number, $yer) {
        $this->load->model('master/master_model', 'mod_mstr');
        $fild = 'SUM(' . $this->fild_amount . ') as posted_value';

        $filter[$this->fild_comp_code] = $comp_code;
        $filter[$this->fild_doc_number_alias] = $aconting_number;
        $filter[$this->fild_doc_yer_alias] = $yer;
        $filter['LEFT('.$this->fild_acounting_no.',3) IN("SPI","SPL")'] = NULL;

        $res = current($this->mod_mstr->get_detail($fild, $filter));

        if ($res->posted_value != '')
            return $res->posted_value;
        else
            return 0;
    }

    function posteditem_deltete() {
        $delId = $this->input->post('id');
        $row = $this->session->userdata('arClearing');
        $this->load->model('master/master_model', 'mod_mstr');
        $this->mod_mstr->tmp_delete(array('no' => $delId));
        $out['status'] = TRUE;
        echo json_encode($out);
    }

    function ar_clearing_submit() {

        $this->load->model('master/master_model', 'mod_mstr');
        $parent_comp_code = $this->input->post('company_code');
        $parent_bisnis_area = $this->input->post('bisnis_area');
        $parent_doc_number = $this->input->post('doc_number');
        $parent_doc_yer = $this->input->post('doc_yer');
        $parent_curency = $this->input->post('curency');
        $parent_amount = $this->input->post('amount');
        $parent_posting_date = $this->input->post('posting_date');
        $parent_posting_date_new= $this->input->post('posting_date_new');
        $parent_doc_date = $this->input->post('doc_date');
        $parent_doc_type = $this->input->post('doc_type');
        $parent_text = $this->input->post('text');
        $parent_gl_account      = $this->input->post('gl_account');
        $parent_item      = $this->input->post('item');
        $customer_balance       = $this->input->post('customer_balance');
        $doc_number_new = $this->app->getAutoId($this->fild_acounting_no, $this->tbl_hider, $this->cfg_arClearing->initial_ar_clearing);
        $item_post = $this->input->post('models');
        $curdate = date('Y-m-d');
        $out['status'] = FALSE;
        $out['messages'] = '';
        $calculate = $this->input->post('pcalculate');
        $no = 1;

        $balance_ind            = $this->input->post('balance_indicator');
        $balance_amount         = (is_numeric($this->input->post('balance_amount')))?$this->input->post('balance_amount'):0;
        $balance_type           = $this->input->post('balance_type');
        $cfg_titipan            = json_decode(json_encode($this->config->item('titipan')));

        if ($calculate == 0) {
            foreach ($item_post as $value) {
                $clear_amount = ($value['clear_amount'] == 'null' || $value['clear_amount'] == '') ? 0 : $value['clear_amount'];
                //$ppn_ind = $value['sspt_ppnind'];
                /*$balance =0; 
                $balance = (ceil(($value['ar_total'] - $value['ar_tax']) * ($value['pph_indikator'] / 100)) + $clear_amount + $value['posted_value']);
                if($ppn_ind =='2'){
                    $balance = $balance + $value['ar_tax'];
                }
                $status = ($value['ar_total'] == $balance) ? 'Clear' : 'Un Clear';*/

                $balance            = $value['ar_tax']-($clear_amount+$value['posted_value']);
                $status             = ($balance==0)?'Clear':'Un Clear';

                $data = array(
                    'user_create' => $this->session->userdata('user'),
                    'tmp_for' => 'SPI',
                    'company_code' => $value['company_code'],
                    'doc_number' => $value['doc_number'],
                    'doc_yer' => $value['doc_yer'],
                    'doc_type' => $value['doc_type'],
                    'posting_date' => $value['posting_date'],
                    'bisnis_area' => $value['bisnis_area'],
                    'posted_value' => $value['posted_value'],
                    'item' => $value['item'],
                    'ar_total' => $value['ar_total'],
                    'ar_tax' => $value['ar_tax'],
                    'sspt_ppnind'   => $value['sspt_ppnind'],
                    'pph_indikator' => $value['pph_indikator'],
                    'customer'      => $value['customer'],
                    'ar_status'         => $value['ar_status'],
                    'bp_status'         => $value['bp_status'],
                    'numb_bukpot'         => $value['numb_bukpot'],
                    'clear_amount' => $clear_amount,
                    'status' => $status
                );
                $delId = $value['no'];
                $this->mod_mstr->tmp_delete(array('no' => $delId));
                $this->mod_mstr->add_tmp($data);
            }
            $out['messages'] = lang('msg_calculated');
        }

        $res = FALSE;
        $t_amount = 0;
        $stCek = FALSE;
        $clearCek = FALSE;
        $cekDivBisnis_area = TRUE;
        $t_sub_amount = 0;
        if ($calculate > 0) {
            do {

                $ck_used = $this->mod_mstr->clearing_bank_is_used($parent_doc_number,$parent_doc_yer,$parent_comp_code);
                if(count($ck_used) > 0){
                    $out['messages'] = 'Doc Number '.$ck_used[0]->VCH_BELNR.' has been processed by '.$ck_used[0]->VCH_AUGBL;
                    break;
                }
                
                foreach ($item_post as $key => $value) {
                    $sub_amount = (isset($value['clear_amount'])) ? $value['clear_amount'] : 0;
                    $pph_pph_ind = (isset($value['pph_indikator'])) ? $value['pph_indikator'] : 0;
                    $sub_posted = (isset($value['posted_value'])) ? $value['posted_value'] : 0;
                    $ar_total = (isset($value['ar_total'])) ? $value['ar_total'] : 0;
                    $tax        = (isset($value['ar_tax'])) ? $value['ar_tax'] : 0;
                    $sub_bis_area   = (isset($value['bisnis_area'])) ? $value['bisnis_area'] : 0;
                    $ar_amout = ($ar_total-$tax)- ($ar_total* ($pph_pph_ind / 100));
                    $meterai_fee    = (isset($value['materai_fee']))?$value['materai_fee']:0;

                    $t_amount += $sub_amount;
                    $balace_cek = ceil(($ar_total -$meterai_fee) - ($sub_posted + $sub_amount + ($ar_amout* ($pph_pph_ind / 100))));


                    if ($balace_cek < 0)
                        $stCek = TRUE;

                    if ($sub_amount <= 0)
                        $clearCek = TRUE;

                    if($sub_bis_area !=$parent_bisnis_area && $sub_bis_area !=0)
                        $cekDivBisnis_area = FALSE;

                    $t_sub_amount += $sub_amount;
                }

                if($t_sub_amount <=0 ){
                    $out['messages'] = 'The sum of CLEAR must be greater than zero';
                    break;
                }

                $Mdate=date_create($parent_posting_date_new);
                $date_new = date_format($Mdate,"m-d-Y");
                list($m, $d, $y) = explode('-', $date_new);
                if(!checkdate($m, $d, $y) || $parent_posting_date_new ==''){
                    $out['messages'] = 'Posting date must be filled, format (dd-mm-yyyy)';
                    break;
                }

                $this->load->model('app_model','mod_app');
                $res_cek_postDate= $this->mod_app->get_date(
                    array('CHR_BUKRS'=>$parent_comp_code)
                );
                if(count($res_cek_postDate)> 0){
                    $cek_postDate_min = date_create($res_cek_postDate[0]->DYR_FROM_THN_TWO.'-'.$res_cek_postDate[0]->VCH_FROM_BLN_TWO.'-01');
                    $cek_postDate_max = strtotime('+1 month', strtotime(date_format(date_create($res_cek_postDate[0]->DYR_TO_THN_TWO.'-'.($res_cek_postDate[0]->VCH_TO_BLN_TWO).'-01'),'Y-m-d')));
                    $cek_postDate_last= strtotime('-1 days', $cek_postDate_max);

                    $c_use = date_format($Mdate,'Ymd')+0;
                    $c_max = date('Ymd',$cek_postDate_last)+0;
                    $c_min = date_format($cek_postDate_min,'Ymd')+0;

                    if($c_use > $c_max || $c_use < $c_min ){
                        $out['messages'] = 'Out Of rang <b>Posting Date</b>, min:'.date_format($cek_postDate_min,"d-m-Y").', max:'.date('d-m-Y',$cek_postDate_last);
                        break;
                    }
                }
                
                if($cekDivBisnis_area){
                    $out['messages'] = lang('msg_difrn_bisnis_area');
                    break;
                }

                if($balance_ind =='1'){
                    $cfg_titipan = json_decode(json_encode($this->config->item('titipan')));
                    $ar_cfg_type_titipan = explode(",", $cfg_titipan->type_for_ar);
                    if($balance_amount== 0){
                        $out['messages'] = lang('msg_empty_balance_amount');
                        break;
                    }

                    if($balance_type==''){
                        $out['messages'] = lang('mgs_balance_null');
                        break;
                    }

                    if($customer_balance=='' && $balance_type=='Titipan'|| $customer_balance =='-1' && $balance_type=='Titipan'){
                        $out['messages'] = "Please Select customer";
                        break;
                    }

                    $balance_amount = intval($balance_amount);
                    if($balance_amount <= 0 && strtoupper($balance_type) == strtoupper($ar_cfg_type_titipan[0])){
                        $out['messages'] = lang('msg_amount_zero_titipan');
                        break;
                    }

                    if($balance_amount > intval($cfg_titipan->max_value) && trim(strtoupper($balance_type)) == trim(strtoupper($ar_cfg_type_titipan[2]))){
                        $out['messages'] = lang('mgs_balance_amount_limit')." (<= ".$cfg_titipan->max_value.")";
                        break;
                    }

                    if($balance_amount < intval($cfg_titipan->min_value) && trim(strtoupper($balance_type)) == trim(strtoupper($ar_cfg_type_titipan[2]))){
                        $out['messages'] = lang('mgs_balance_amount_limit')." (>= ".$cfg_titipan->min_value.")";
                        break;
                    }

                    if($balance_amount > intval($cfg_titipan->max_value) && trim(strtoupper($balance_type)) == trim(strtoupper($ar_cfg_type_titipan[1]))){
                        $out['messages'] = lang('mgs_balance_amount_limit')." (<= ".$cfg_titipan->max_value.")";
                        break;
                    }

                    if($balance_amount <= 0 && trim(strtoupper($balance_type)) == trim(strtoupper($ar_cfg_type_titipan[1]))){
                        $out['messages'] = lang('mgs_balance_amount_limit')." (> 0)";
                        break;
                    }

                    if($t_amount+$balance_amount != $parent_amount){
                        $out['messages'] = lang('msg_amount_invalid');
                        break;
                    }
                }

                if ($stCek) {
                    $out['messages'] = lang('msg_balance_minus');
                    break;
                }


                if ($clearCek) {
                    $out['messages'] = lang('msg_clear_minus');
                    break;
                }

                if ($t_amount != $parent_amount && $balance_ind =='-1') {
                    $out['messages'] = lang('msg_amount_invalid');
                    break;
                }

                $hiderAr = array(
                    $this->fild_comp_code => $parent_comp_code,
                    $this->fild_reverence => $doc_number_new,
                    $this->fild_acounting_no => $doc_number_new,
                    $this->fild_fiscal_year => $parent_doc_yer,
                    $this->fild_doc_date        => $curdate,
                    $this->fild_posting_date    => date('Y-m-d', strtotime($parent_posting_date_new)),
                    $this->fild_doc_hider => $parent_text,
                    $this->fild_curency => $this->config->item('df_curency'),
                    $this->fild_flag => 0,
                    $this->fild_doc_type => $this->cfg_arClearing->df_doc_type_ar,
                    $this->fild_petugas         => $this->session->userdata('user')
                );

                $detailAr = array(
                    $this->fild_comp_code           => $parent_comp_code,
                    $this->fild_acounting_no        => $doc_number_new,
                    $this->fild_fiscal_year         => $parent_doc_yer,
                    $this->fild_line_item           => $no,
                    $this->fild_clearing_date       => $curdate,
                    $this->fild_clearing_doc_no     => $doc_number_new,
                    $this->fild_posting_key         => $this->cfg_arClearing->set_postkey_in,
                    $this->fild_indicator_saldo     => 'S',
                    $this->fild_amount              => $parent_amount,
                    $this->fild_bisnis_area         => $parent_bisnis_area,
                    $this->fild_gl_account          => $parent_gl_account,
                    $this->fild_text                => $parent_text,
                    $this->fild_flag                => 0,
                    $this->fild_doc_number_alias    => $parent_doc_number,
                    $this->fild_doc_yer_alias       => $parent_doc_yer,
                    $this->fild_doc_item_alias      => $parent_item,
                    'bp_status'                     => 1,
                    'bp_status'                     => 1
                );
                if($parent_doc_type=="DZ"){
                    $res_dsx_yz = current($this->mod_mstr->get_detail(
                                    $this->fild_gl_account." as gl_account, ".$this->fild_customer." as customer, bp_status,ar_status",
                                array(
                                    $this->fild_comp_code       => $parent_comp_code,
                                    $this->fild_acounting_no    => $parent_doc_number,
                                    $this->fild_fiscal_year     => $parent_doc_yer
                                )
                            )
                        );
                    $detailAr[$this->fild_special] = "T";
                    $detailAr[$this->fild_customer] = $res_dsx_yz->customer;
                    $detailAr[$this->fild_posting_key] = '19';
                }
                $up_data = array(
                    $this->fild_clearing_date => $curdate,
                    $this->fild_clearing_doc_no => $doc_number_new
                );
                $filter_up_first[$this->fild_comp_code] = $parent_comp_code;
                $filter_up_first[$this->fild_acounting_no] = $parent_doc_number;
                $filter_up_first[$this->fild_fiscal_year] = $parent_doc_yer;
               $this->mod_mstr->update_detail($up_data, $filter_up_first);

                $item_real = array();
                $no_ar = array();
                foreach ($item_post as $row_item) {
                    $no_ar[] = $row_item['no'];
                    $clear_amount = (isset($row_item['clear_amount'])) ? $row_item['clear_amount'] : 0;
                    $bisnis_area = (isset($row_item['bisnis_area'])) ? $row_item['bisnis_area'] : '';
                    $status = (isset($row_item['status'])) ? $row_item['status'] : 'Un Clear';
                    $item_doc_number = (isset($row_item['doc_number'])) ? $row_item['doc_number'] : '';
                    $item_doc_yer = (isset($row_item['doc_yer'])) ? $row_item['doc_yer'] : '';
                    $line_item          = (isset($row_item['item']))?$row_item['item']:'';
                    $comp_codeitem      = (isset($row_item['company_code']))?$row_item['company_code']:'';

                    $res_dsx = current($this->mod_mstr->get_detail(
                                                    $this->fild_gl_account." as gl_account, ".$this->fild_customer." as customer, bp_status,ar_status",
                                                array(
                                                    $this->fild_comp_code       => $comp_codeitem,
                                                    $this->fild_acounting_no    => $item_doc_number,
                                                    $this->fild_fiscal_year     => $item_doc_yer,
                                                    $this->fild_line_item       => $line_item
                                                )
                                            )
                                        );

                    if ($status == 'Clear'){

                        $this->mod_mstr->update_hider_no_prefix(
                                array(
                                    $this->fild_reverence       => 'CONCAT('.$this->fild_reverence.'," - SPREF: '.$doc_number_new.'")' //$doc_number_new,
                                ),
                                array(
                                    $this->fild_comp_code   => $parent_comp_code,
                                    $this->fild_acounting_no => $item_doc_number,
                                    $this->fild_fiscal_year     => $item_doc_yer,
                                )
                            );

                            $this->mod_mstr->update_detail(
                                array(
                                    'ssp_status'                         => 1,
                                    'pph_indicator'                     => $row_item['pph_indikator'],
                                ),
                                array(
                                    $this->fild_comp_code   => $parent_comp_code,
                                    $this->fild_acounting_no => $item_doc_number,
                                    $this->fild_fiscal_year     => $item_doc_yer,
                                    $this->fild_line_item       => $row_item['item']
                                )
                            );

                            $detailAr['ssp_status']  = 1;

                    }

                    $this->mod_mstr->update_detail(
                        array(
                            $this->fild_sspt_ppnind     => $row_item['sspt_ppnind']
                        ),
                        array(
                            $this->fild_comp_code   => $parent_comp_code,
                            $this->fild_acounting_no => $item_doc_number,
                            $this->fild_fiscal_year     => $item_doc_yer,
                            $this->fild_line_item       => $row_item['item']
                        )
                    );

                    $no++;
                    $item[$this->fild_comp_code]        = $parent_comp_code;
                    $item[$this->fild_acounting_no]     = $doc_number_new;
                    $item[$this->fild_fiscal_year]      = $parent_doc_yer;
                    $item[$this->fild_line_item]        = $no;
                    $item[$this->fild_clearing_date]    = $curdate;
                    $item[$this->fild_indicator_saldo]  = 'H';
                    $item[$this->fild_amount]           = $clear_amount;
                    $item[$this->fild_bisnis_area]      = $bisnis_area;
                    $item[$this->fild_doc_number_alias] = $row_item['doc_number'];
                    $item[$this->fild_doc_yer_alias]    = $row_item['doc_yer'];
                    $item[$this->fild_doc_item_alias]   = $row_item['item'];
                    $item[$this->fild_flag]             = 0;
                    $item['ar_status']                  = ($status=='Clear')?1:0;
                    $item[$this->fild_posting_key]      = $this->cfg_arClearing->set_postkey_tag;
                    $item['bp_status']                  = $res_dsx->bp_status;
                    $item[$this->fild_customer]         = $res_dsx->customer;
                    $item[$this->fild_gl_account]       = $res_dsx->gl_account;
                    $item['VCH_NUMBPOT_SSP']                = $row_item['numb_bukpot'];

                    array_push($item_real, $item);
                }

                $item_real2 = array();
                foreach ($item_post as $k => $row_item) {

                    $sub_amount2 = (isset($row_item['clear_amount'])) ? $row_item['clear_amount'] : 0;
                    $bisnis_area = (isset($row_item['bisnis_area'])) ? $row_item['bisnis_area'] : '';
                    if($bisnis_area != $parent_bisnis_area){
                        $dc = array('H', 'S');
                        foreach ($dc as $rdc){
                            $no++;
                            $key = ($rdc == 'H') ? "31" : "01";

                            if($parent_bisnis_area =="1100"){
                                $filter_vecus['CHR_COMPANY_CODE'] = $this->input->post('company_code');
                                if ($rdc == 'H') {
                                    $filds_vecus = 'CHR_VCODE AS account';
                                    $filter_vecus['CHR_BA'] = $this->input->post('bisnis_area');
                                } else {
                                    $filds_vecus = 'CHR_CUST_CODE AS account';
                                    $filter_vecus['CHR_BA'] = $bisnis_area;
                                }

                                $arAccount = $this->mod_mstr->get_vecus($filds_vecus, $filter_vecus);
                                unset($filter_vecus['CHR_BA']);
                                unset($filter_vecus['CHR_COMPANY_CODE']);

                                $customer= $arAccount[0]->account;
                            }else{
                                $customer= ($rdc == 'H') ? "300000" : "1000000";
                            }

                            $no++;
                            $item2[$this->fild_comp_code]            = $parent_comp_code;
                            $item2[$this->fild_acounting_no]         = $doc_number_new;
                            $item2[$this->fild_fiscal_year]          = $parent_doc_yer;
                            $item2[$this->fild_line_item]            = $no;
                            $item2[$this->fild_clearing_date]        = $curdate;
                            $item2[$this->fild_clearing_doc_no]      = $doc_number_new;
                            $item2[$this->fild_posting_key]          = $key;
                            $item2[$this->fild_gl_account]           = $parent_gl_account;
                            $item2[$this->fild_customer]             = $customer;
                            $item2[$this->fild_indicator_saldo]      = $rdc;
                            $item2[$this->fild_amount]               = $sub_amount2;
                            $item2[$this->fild_bisnis_area]          = ($rdc == 'H') ? $this->input->post('bisnis_area'):$bisnis_area;
                            $item2[$this->fild_text]                 = $parent_text;
                            $item2[$this->fild_flag]                 = 0;

                            array_push($item_real2, $item2);
                            $this->customer_tag = $res_dsx->customer;
                        }
                    }
                }

                //---------ADD JURNAL TAMBAHAN--------//
                $no++;
                $cfg_titipan = json_decode(json_encode($this->config->item('titipan')));
                $ar_cfg_type_titipan = explode(",", $cfg_titipan->type_for_ar);
                if($balance_ind == '1'){
                    $res = $this->jurnal_tambahan($no,$this->customer_tag,$doc_number_new);
                    $this->mod_mstr->add_detail(array($res));
                }

                $this->mod_mstr->add_hider($hiderAr);
                $this->mod_mstr->add_detail(array($detailAr));
                $this->mod_mstr->add_detail($item_real2);
                $res = $this->mod_mstr->add_detail($item_real);
            } while (FALSE);

            if ($res) {
                $this->mod_mstr->tmp_delete(
                        array('no IN("' . implode('","', $no_ar) . '")' => NULL)
                );
                $out['messages'] = lang('msg_success_save_record') . " " . $doc_number_new;
            }

            $out['status'] = $res;
        }
        echo json_encode($out);
    }


    function jurnal_tambahan($no,$customer,$doc_number_new){
        $cfg_titipan            = json_decode(json_encode($this->config->item('titipan')));
        $balance_ind            = $this->input->post('balance_indicator');
        $balance_amount         = $this->input->post('balance_amount')+0;
        $balance_type           = $this->input->post('balance_type');
        $parent_bisnis_area     = $this->input->post('bisnis_area');
        $curdate                = date('Y-m-d');
        $parent_comp_code       = $this->input->post('company_code');
        $parent_doc_yer         = $this->input->post('doc_yer');
        $ar_cfg_titipan         = explode(',', $cfg_titipan->type_for_ar);
        $customer_balance       = $this->input->post('customer_balance');

        $item[$this->fild_comp_code]            = $parent_comp_code;
        $item[$this->fild_acounting_no]         = $doc_number_new;
        $item[$this->fild_fiscal_year]          = $parent_doc_yer;
        $item[$this->fild_line_item]            = $no;
        $item[$this->fild_clearing_date]        = $curdate;
        $item[$this->fild_indicator_saldo]      = ($balance_amount > 0 )?'H':'S';
        $item[$this->fild_amount]               = str_replace('-', '', $balance_amount);
        $item[$this->fild_bisnis_area]          = $parent_bisnis_area;
        $item[$this->fild_flag]                 = 0;


        if(strtoupper($ar_cfg_titipan[0]) == strtoupper($balance_type)){
            $item[$this->fild_posting_key]  = $cfg_titipan->posting_key_titipan;
            $item[$this->fild_gl_account]   = $cfg_titipan->gl_titipan;
        }

        if(strtoupper($ar_cfg_titipan[1]) == strtoupper($balance_type)){
            $item[$this->fild_posting_key] = $cfg_titipan->posting_key_bank_chrg;
            $item[$this->fild_gl_account] = $cfg_titipan->gl_charge;
            $item[$this->fild_customer]             = $customer;
        }

        if(strtoupper($ar_cfg_titipan[2]) == strtoupper($balance_type)){
            $item[$this->fild_gl_account] = $cfg_titipan->gl_selisih;
            $item[$this->fild_posting_key] = ($balance_amount >0)? $cfg_titipan->posting_key_slisih_pls : $cfg_titipan->posting_key_slisih_mns;
        }

        if($balance_type ==$ar_cfg_titipan[0]){
            $item[$this->fild_special]  = 'T';
            $item[$this->fild_customer]     = $customer_balance;
        }

        unset($ar_cfg_titipan[0]);
        if(in_array($balance_type,$ar_cfg_titipan)){
            $item[$this->fild_cost_center]= $parent_bisnis_area."00";
        }

        $item['ar_status']                      = 0;


        return $item;
    }



}
