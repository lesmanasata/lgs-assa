<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Petugas extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
        $this->cek_build_access(explode(',', ALLOWED_USER));
        $userId 	= $this->session->userdata('limit_id');
        $customer	= array();
        $admin		= array();
        $arCmb = array(
            array('name' => 'jenis', 'url' => 'user/petugas/level','flow' => ''),
            array('name' => 'salutatsion', 'url' => 'user/petugas/salutatsion','flow' => '')
        );
        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);

        $this->template->build('user/petugas', $data);
    }

    public function get()
    {
        $res 	= array();
        $filter = array();
        $this->load->model('user/user_model', 'mod_user');

        if ($this->input->post('id') !='') {
            $filter['p.id_petugas IN("'.implode('","', explode(',', $this->input->post('id'))).'")']=null;
        }

        $res = $this->mod_user->get_join($filter);

        echo json_encode(array('data'=>$res));
    }

    public function level()
    {
        $data=array(
            array('id'=>USER_ADMIN, 'text'=>"Administrator"),
            array('id'=>USER_AREA, 'text'=>"Admin Area")
        );
        echo json_encode(array('data'=>$data));
    }
    public function user_module()
    {
        $data = array(
            array('id' =>'billing/index','text'=>'Billing Module'),
            array('id'=>'clearing/ar','text'=>'Clearing Module'),
            array('id'=>'jv/index','text'=>'Jurnal Voucher Module'),
            array('id'=>'order/index/sr','text'=>'Order Module'),
            array('id'=>'distribute/index','text'=>'Cost Distribution Module'),
            array('id'=>'notif/disposal','text'=>'Notification Module'),
            array('id'=>'rpt/detail','text'=>'Report Module')
        );
        echo json_encode(array('data'=>$data));
    }
    public function salutatsion()
    {
        $data=array(
            array('id'=>'Mr.', 'text'=>'Mr.'),
            array('id'=>'Ms.', 'text'=>'Mrs.')
        );
        echo json_encode(array('data'=>$data));
    }

    public function statusNon()
    {
        $super = $this->input->post('id');
        if ($super<>'admin') {
            $this->load->model('user/user_model', 'mod_user');
            $id = explode(',', $this->input->post('id'));

            $filter['id_petugas IN("'.implode('","', $id).'")']=null;
            $this->mod_user->nonaktif($filter);
        }
    }
    public function status()
    {
        $this->load->model('user/user_model', 'mod_user');
        $id = explode(',', $this->input->post('id'));

        $filter['id_petugas IN("'.implode('","', $id).'")']=null;
        $this->mod_user->aktif($filter);
    }
    public function proses()
    {
        $this->load->model('user/user_model', 'mod_user');
        $fx_alias   = '';
        $fx_name   	= '';
        $ptg_id 	= $this->input->post('uname');
        $jenis_ptg	= $this->input->post('jenis');
        $mode 		= $this->input->post('mode');
        $email 		= $this->input->post('email');
        $pswd		= md5($this->input->post('password'));
        $joinDate	= date('Y-m-d H:i:s');
        $timeDate 	= strtotime($joinDate);
        $expdDate	= strtotime("+7 day", $timeDate);
        $expDate 	= date('Y-m-d', $expdDate);
        $out['status'] = false;
        $salutasi 	= $this->input->post('salutatsion');
        $first_name	= $this->input->post('first_name');
        $last_name 	= $this->input->post('last_name');
        if ($salutasi=='-1') {
            $ptg_name	= "|".$first_name."|".$last_name;
        }else {
            $ptg_name	= $salutasi."|".$first_name."|".$last_name;
        }

        $area = '';
        $fixmodules ='';
        $alias = $this->input->post('alias');
        $moduls = $this->input->post('module');
        if (count($moduls)>0 && $moduls !=FALSE) {
            $impModules = implode("','",$moduls);
            $fixmodules = str_replace("'","",$impModules);
        }
        do {
            # code...
        } while (FALSE);
        if(count($alias)>0 && $alias !=FALSE){
            $fx_alias = implode("','",$alias);
            $area = str_replace("'","",$fx_alias);
        }

        $data=array(
            'id_petugas'=>$ptg_id,
            'id_alias'=>$area,
            'module' => $fixmodules,
            'password_petugas'=>$pswd,
            'nama_petugas'=> $ptg_name,
            'email'=> $email,
            'jenis_petugas'=> $jenis_ptg,
            'expired_password'=> $expDate,
            'created_date'=> $joinDate,
            'status_petugas'=> 1
        );

        do {
            $cekp = $this->mod_user->get_id($ptg_id);
            if ($cekp !=false && $mode=='add') {
                $out['messages'] =lang('msg_uname_is_taken');
                break;
            }

             if ($pswd =="" && $mode=='add') {
                $out['messages'] ="Please Insert Password";
                break;
            }
            if ($fixmodules=="" && $mode=='add') {
                $out['messages'] ="Please Insert Module";
                break;
            }
            if ($area=="" && $mode=='add') {
                $out['messages'] ="Please Insert Area";
                break;
            }
            if ($mode=='add') {

                $res = $this->mod_user->add($data);
            } else {
                unset($data['id_petugas']);
                unset($data['status_petugas']);
                if ($this->input->post('password') =='') {
                    unset($data['password_petugas']);
                }
                $filter['id_petugas']=$ptg_id;
                $res = $this->mod_user->edit($data, $filter);
            }

            $out['status'] = $res;
            if ($res) {
                $out['messages'] = lang('msg_success_save_record').' '.$ptg_id;
            } else {
                $out['messages']= lang('msg_failed_save_record');
            }
        } while (false);
        echo json_encode($out);
    }

    function get_user_ldap(){
        $this->load->model('user/user_model', 'mod_user');

        $res = json_decode($this->ldap->browse());
        $out['status'] = "false";
        $joinDate   = date('Y-m-d H:i:s');
        $timeDate   = strtotime($joinDate);
        $expdDate   = strtotime("+7 day", $timeDate);
        $expDate    = date('Y-m-d', $expdDate);
        $out['messages'] = "Gagal menemukan data LDAP";

        $x=0;
        foreach ($res->user_data as $key => $value) {
            $x++;
            $out['status'] = "true";
            $out['messages'] = $x.' Users found from LDAP';
            $data=array(
                'id_petugas'=>$value->userid,
                'nama_petugas'=> "|".str_replace(" ", "|", $value->fullnm),
                'password_petugas'=>$this->config->item('psc'),
                'expired_password'=> $expDate,
                'created_date'=> $joinDate,
                'jenis_petugas'=> USER_UNDEFINE,
                'status_petugas'=> 1
            );
            $this->mod_user->add_ignore($data);
        }

        echo json_encode($out);

    }
}
