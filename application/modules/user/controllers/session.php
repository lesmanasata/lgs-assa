<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session extends MY_Controller {


	public function __construct(){
        parent::__construct();
		$this->load->module('app');
		$this->app->cek_permit();
    }

	public function index()
	{

		$this->cek_build_access(explode(',',ALLOWED_USER));
		$userId 	= $this->session->userdata('limit_id');
		$customer	= array();
		$admin		= array();

		$this->template->build('user/log_session');
	}

	function get(){
		$res 	= array();
		$filter = array();
		$this->load->model('user/user_model','mod_user');
		
		if($this->input->post('id') !='')
			$filter['p.id_petugas IN("'.implode('","',explode(',',$this->input->post('id'))).'")']=NULL;
		
		$res = $this->mod_user->get_all($filter);
		
		echo json_encode(array('data'=>$res));
	}

	function hapus(){
		$this->load->model('user/user_model','mod_user');
		$id 	= explode(',',$this->input->post('id'));
		$app 	= $this->config->item('app');
		foreach($id as $k=>$cId)
			$id[$k] = $cId.$app;
		
		$filter['id_petugas IN("'.implode('","',$id).'")']=NULL;
		$this->mod_user->clear($filter);
	}
}
