<div class="grid-form" style="min-height:600px">
 	<div class="grid-form1" >
 		<h3 id="forms-example" class=""><?php  echo lang('titile_pwd'); ?></h3>
 		<form class="form-horizontal" id="f_change_password" method="post" action="<?php echo site_url("user/password"); ?>">
			<div class="form-group">
				<label  class="col-sm-3 control-label hor-form"><?php echo lang('pwd_new'); ?></label>
				<div class="col-sm-9">
					<input type="password" class="form-control"  name="t_password_new" id="t_password_new" placeholder="<?php echo lang('pwd_new'); ?>">
				</div>
			</div>
			
			<div class="form-group">
				<label  class="col-sm-3 control-label hor-form"><?php echo lang('pwd_retype'); ?></label>
				<div class="col-sm-9">
					<input type="password" class="form-control"  name="t_password_confirm" id="t_password_confirm" placeholder="<?php echo lang('pwd_retype'); ?>">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label hor-form"><?php echo lang('pwd_old'); ?></label>
				<div class="col-sm-9">
					<input type="password" class="form-control"  name="t_password_old" id="t_password_old" placeholder="<?php echo lang('pwd_old'); ?>">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<input type="submit" name="t_submit" id="t_submit" value="<?php echo lang('pwd_btn_submit'); ?>" class="btn btn-primary">
					<input type="reset" name="t_reset" id="t_reset" value="<?php echo lang('pwd_btn_cancel'); ?>" class="btn btn-primary">
				</div>
			</div>
		</form>
	</div>
</div>

