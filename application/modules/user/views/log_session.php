<?php $timestamp = time();?>
<div class="banner">
	<div class="btn-group">
			<?php
				echo '<button class="btn btn-primary navbar-btn " id="clear-'.$timestamp.'"><i class="glyphicon glyphicon-trash"></i> '.lang('btn_ptg_clear').' </button> ';
				echo '<button class="btn btn-warning navbar-btn " id="reload-'.$timestamp.'"><i class="fa fa-refresh"></i> '.lang('btn_reload').' </button> ';
			?>
			
	</div>
</div>

<div class="blank">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link"><?php echo lang('titile_log_session'); ?></a></li>
	</ul>

	<div class="tab-content grid-form1" style="border-top:0px;">
		  <div id="home" class="tab-pane fade in active ">
		  	<div class="padding_tab_body">
				<div class="" id="info-petugas-<?php echo $timestamp; ?>"></div>
			</div>
		  </div>
	</div>
</div>


<script>
$(document).ready(function(){
	

	var ds_petugas = new kendo.data.DataSource({
		transport: {
			read: {
				type:"POST",
				dataType: "json",
				url: '<?php echo site_url('user/session/get'); ?>',
			}
		},
		schema: {
			parse: function(response){
				return response.data;
			},
		},
		pageSize: 50,
	});
	
	$('#info-petugas-<?php echo $timestamp; ?>').kendoGrid({
		dataSource: ds_petugas,
		filterable: true,
		sortable: true,
		pageable: true,
		scrollable: true,
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Like",
					eq: "=",
					neq: "!="
				},
			}
		},
		dataBound: function(e) {
			this.collapseRow(this.tbody.find("tr.k-master-row").first());
			var grid = e.sender;
			if (grid.dataSource.total() == 0) {
				var colCount = grid.columns.length;
				$(e.sender.wrapper)
					.find('tbody')
					.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
			}
		},
		columns: [
			{
				title:"<input id='myCheckbox' type='checkbox' onClick='toggle(this)' /> All<br/>",
				width:50,
				template: "<input type='checkbox' class='petugasid' name='petugasid[]' value='#: id #'>"
			},
			{field:"id",width:150,title:"<?php echo lang('tbl_ptg_id'); ?>"},
			{field:"nama_petugas",width:150,title:"<?php echo lang('tbl_ptg_name'); ?>",template:"#nama = nama_petugas.replace('|',' ');# #: nama.replace('|',' ') #"},
			{field:"jenis_petugas",width:150,title:"<?php echo lang('tbl_ptg_type'); ?>", },
			{field:"dtimelogin",width:200,title:"<?php echo lang('tbl_ptg_logtime'); ?>"},
			{field:"user_agent",width:600,title:"<?php echo lang('tbl_ptg_uagent'); ?>"},
		]
	});
	
	$("#reload-<?php echo $timestamp; ?>").click(function(){
		ds_petugas.read();
	});
	
	$('#clear-<?php echo $timestamp; ?>').click(function(){
		var txt;
		var checkedVals = $('.petugasid:checkbox:checked').map(function() {
			return this.value;
		}).get();
		if(checkedVals !=''){
			msg_box("Hapus data sessi petugas Id ("+checkedVals+")...?",[{'btnYES':function(){
				$(this).trigger('closeWindow');
				var petugasId= checkedVals.join(",");
				$.post('<?php echo site_url('user/session/hapus'); ?>',{id:petugasId},function(){
					ds_petugas.read();

				});
			}},'btnNO'],'<?php echo lang('nfc_confirm'); ?>');
		}else
			msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
	});

});

function toggle(source) {
  checkboxes = document.getElementsByClassName('petugasid');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div id="loading-mask"></div>

<script id="grid-window-templates" type="text/x-kendo-template">
	<div class="grid-form-area"></div>
</script>

