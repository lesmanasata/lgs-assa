<?php $timestamp = time();?>
<div class="banner">
	<div class="btn-group">
			<?php
				echo'<button class="btn btn-success navbar-btn " id="add-petugas-'.$timestamp.'">
					<i class="glyphicon glyphicon-plus"></i> '.lang('btn_add_item').'
					</button> ';
				echo '<button class="btn btn-info navbar-btn " id="edit-'.$timestamp.'" >
						<i class="glyphicon glyphicon-pencil"></i> '.lang('btn_edit_record').'
					  </button> ';
				echo '<button class="btn btn-danger navbar-btn " id="nonaktif-'.$timestamp.'">
						<i class="glyphicon glyphicon-remove"></i> '.lang('btn_nonaktif').'
					</button> ';
				?>
				<button class="btn btn-warning navbar-btn" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order" id="reload-<?php echo $timestamp; ?>" ><i class="fa fa-refresh"></i> <?php echo lang('btn_reload'); ?></button>
				<?php
				echo '<button class="btn btn-danger navbar-btn " id="aktif-'.$timestamp.'"><i class="glyphicon glyphicon-check"></i> '.lang('btn_aktif').'</button> ';
			?>

	</div>
</div>

<div class="blank">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link"><?php echo lang('titile_operators'); ?></a></li>
	  <li id="form_link"><a data-toggle="tab" href="#form" id="padding_tab_link">Operator</a></li>
	</ul>

	<div class="tab-content grid-form1" style="border-top:0px;">
		  <div id="home" class="tab-pane fade in active ">
		  		<div class="padding_tab_body">
					<div id="info-petugas-<?php echo $timestamp; ?>"></div>
		  		</div>
		  </div>
		  <div id="form" class="tab-pane fade in ">
			  <div class="padding_tab_body">
				  <form class="form-horizontal" id='form-petugas-<?php echo $timestamp; ?>' action='<?php echo site_url('user/petugas/proses'); ?>' method='POST'>
					<input type='hidden' value='add' name='mode'/>
					<div class="modal-contents">
							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_type'); ?> :</label>
								<div class="col-sm-6">
									<input type='text' name='jenis' class='kendodropdown form-control' style='width:100%' placeholder="&mdash;<?php echo lang('form_ptg_type'); ?> &mdash;" />
								</div>
							</div>

							<div class="form-group alias_form">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_limit'); ?> :</label>
								<div class="col-sm-6">
									<div class="input-group" style='width:100%;'>
										<select data-bind="commaseparatedvalue:" id="alias<?php echo $timestamp; ?>" multiple="multiple" name="alias[]" class="form-control"></select>
									</div>
								</div>
							</div>
							<div class="form-group alias_form" >
								<label class="col-sm-3 control-label" for="">Module Limitation :</label>
								<div class="col-sm-6">
									<div class="input-group" style='width:100%;'>
										<select data-bind="commaseparatedvalue:" id="module<?php echo $timestamp; ?>" multiple="multiple" name="module[]" class="form-control"></select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_sal'); ?> :</label>
								<div class="col-sm-6">
									<input type='text' name='salutatsion' class='kendodropdown form-control' placeholder="&mdash; <?php echo lang('form_ptg_sal'); ?> &mdash;" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_first'); ?>  :</label>
								<div class="col-sm-6">
									<input required type='text' name='first_name' class='form-control'/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_last'); ?> :</label>
								<div class="col-sm-6">
									<input required type='text' name='last_name' class='form-control'/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_email'); ?> :</label>
								<div class="col-sm-6">
									<input required type='email' name='email' class='form-control'/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_uname'); ?>  :</label>
								<div class="col-sm-6">
									<input required type='text' name='uname' class='form-control' />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for=""><?php echo lang('form_ptg_pass'); ?>  :</label>
								<div class="col-sm-6">
									<input  type="password" id="password" name="password" class="form-control" data-toggle="password">
									<small id='smallText'><?php echo lang('msg_info_blank_pass'); ?></small>
								</div>
							</div>
					<div>
					<div class="modal-footer">
						<div style='float:left' id='pesan'></div>
						<button type="button" class="btn btn-default" id="cancel" data-dismiss="modal"><?php echo lang('btn_cancel'); ?></button>
						<button type="submit" class="btn btn-primary" ><?php echo lang('btn_proses'); ?></button>
					</div>
				  </form>
			  </div>
		  </div>
	</div>
</div>
<script>
$(document).ready(function(){

	<?php echo $js_cmb; ?>
	cmb['jenis'].value('-1');
	cmb['salutatsion'].value('-1');
	$("#form_link").hide();
	var ds_petugas = new kendo.data.DataSource({
		transport: {
			read: {
				type:"POST",
				dataType: "json",
				url: '<?php echo site_url('user/petugas/get'); ?>',
			}
		},
		schema: {
			parse: function(response){
				return response.data;
			},
		},
		pageSize: 50,
	});

	$('#info-petugas-<?php echo $timestamp; ?>').kendoGrid({
		dataSource: ds_petugas,
		filterable: true,
		sortable: true,
		pageable: true,
		scrollable: false,
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Like",
					eq: "=",
					neq: "!="
				},
			}
		},
		dataBound: function(e) {
			this.collapseRow(this.tbody.find("tr.k-master-row").first());
			var grid = e.sender;
			if (grid.dataSource.total() == 0) {
				var colCount = grid.columns.length;
				$(e.sender.wrapper)
					.find('tbody')
					.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
			}
		},
		columns: [
			{
				title:"",
				width:20,
				template: "<input type='checkbox' class='petugasid' name='petugasid[]' value='#: id #'>"
			},
			{field:"id",title:"<?php echo lang('tbl_ptg_id'); ?>"},
			{field:"alias",title:"<?php echo lang('tbl_ptg_limit'); ?>", template:"#if(alias==''){# - #}else{# #:alias# #}#"},
			{field:"module",title:"Module Limitation"},
			{field:"nama_petugas",title:"<?php echo lang('tbl_ptg_name'); ?>",template:"#nama = nama_petugas.replace('|',' ');# #: nama.replace('|',' ') #"},
			{field:"email",title:"<?php echo lang('tbl_ptg_email'); ?>"},
			{field:"jenis_petugas",title:"<?php echo lang('tbl_ptg_type'); ?>", },
			{field:"status_petugas",title:"<?php echo lang('tbl_ptg_sts'); ?>", }
		]
	});

	$("#reload-<?php echo $timestamp; ?>").click(function(){
		var $this = $(this);
  		$this.button('loading');

  		$.post('<?php echo site_url('user/petugas/get_user_ldap'); ?>',{},function(res){
  			var out = jQuery.parseJSON(res);

       		$this.button('reset');
       		msg_box(out.messages+", Click YES for update list operators...?",[{'btnYES':function(){
				$(this).trigger('closeWindow');
				ds_petugas.read();
			}},'btnNO'],'<?php echo lang('nfc_confirm'); ?>');
       		
  		});
	});

	$("#cancel").click(function(){
		$('.nav-tabs a[href="#home"]').tab('show');
		$("#form-petugas-<?php echo $timestamp; ?> input[name=uname]").prop("readonly", false);
		//$('#Modal-petugas-<?php //echo $timestamp; ?>').modal('show');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=mode]').val('add');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=alias]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=password]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=uname]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=email]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=alias]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=first_name]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=last_name]').val('');
		$("#form_link").hide();
	});
	$("#add-petugas-<?php echo $timestamp; ?>").click(function(){
		$('#smallText').hide();
		$('#pesan').html('');
		$("#form_link").show();
		$('.nav-tabs a[href="#form"]').tab('show');
		$("#form-petugas-<?php echo $timestamp; ?> input[name=uname]").prop("readonly", false);
		$('#form-petugas-<?php echo $timestamp; ?> input[name=mode]').val('add');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=alias]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=password]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=uname]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=email]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=alias]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=first_name]').val('');
		$('#form-petugas-<?php echo $timestamp; ?> input[name=last_name]').val('');
		var area = $("#alias<?php echo $timestamp; ?>").data("kendoMultiSelect");
		var modules = $("#module<?php echo $timestamp; ?>").data("kendoMultiSelect");
		area.value([]);
		modules.value([]);
	});

	$('.alias_form').hide();
	cmb['jenis'].bind('change',function(){
		var cur_val = cmb['jenis'].value();

		if(cur_val=="<?php echo USER_AREA ; ?>")
			$('.alias_form').show();
		else
			$('.alias_form').hide();

	});
	var tes_dataSource = new kendo.data.DataSource({
  			transport: {
				read: 	{
							type:"POST",
      						url: "<?php echo site_url('area/get'); ?>",
      						dataType: "json"
    					}
  					},
				schema: {
							parse: function(response){
								return response.data;
							},
						}
			});
var module_dataSource = new kendo.data.DataSource({
	transport: {
		read: 	{
					type:"POST",
					url: "<?php echo site_url('user/petugas/user_module'); ?>",
					dataType: "json"
				}
			},
	schema: {
				parse: function(response){
					return response.data;
				},
			}
});
	var module_multi = $("#module<?php echo $timestamp; ?>").kendoMultiSelect({
		  filter: "contains",
		  dataSource: module_dataSource,
		  dataTextField: "text",
		  dataValueField: "id"
	});
	var alisa_multi = $("#alias<?php echo $timestamp; ?>").kendoMultiSelect({
		  filter: "contains",
		  dataSource: tes_dataSource,
		  dataTextField: "text",
		  dataValueField: "id"
	});

	$("#edit-<?php echo $timestamp; ?>").click(function(){

		var checkedVals = $('.petugasid:checkbox:checked').map(function() {
			return this.value;
		}).get();
		if(checkedVals.length ==1){
			$.ajax({
				url:'<?php echo site_url('user/petugas/get'); ?>',
				data:'id='+checkedVals,
				type:'POST',
				dataType:'json',tail:1,
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},
				success:function(res){

					var name = res.data[0].nama_petugas;
					var rname = name.split('|');
					var t_alias = res.data[0].alias;
					var t_mod = (res.data[0].module != null)?res.data[0].module:'';
					var mod	= t_mod.split(',');
					var d_alias = t_alias.split(',');
					var myVal = res.data[0].jenis_id;
					$('#smallText').show();
					$('#pesan').html('');
					$("#form-petugas-<?php echo $timestamp; ?> input[name=uname]").prop("readonly", true);
					$('#form-petugas-<?php echo $timestamp; ?> input[name=mode]').val('edit');
					$('#form-petugas-<?php echo $timestamp; ?> select[name=alias').val(t_alias);
					$('#form-petugas-<?php echo $timestamp; ?> select[name=module').val(mod);
					$('#form-petugas-<?php echo $timestamp; ?> input[name=uname]').val(res.data[0].id);
					$('#form-petugas-<?php echo $timestamp; ?> input[name=email]').val(res.data[0].email);
					$('#form-petugas-<?php echo $timestamp; ?> input[name=first_name]').val(rname[1]);
					$('#form-petugas-<?php echo $timestamp; ?> input[name=last_name]').val(rname[2]);
					var dropdownlist = $("#alias<?php echo $timestamp; ?>").data("kendoMultiSelect");
					var modules = $("#module<?php echo $timestamp; ?>").data("kendoMultiSelect");
					dropdownlist.value(d_alias);
					modules.value(mod);
					dropdownlist.trigger("change");
					modules.trigger("change");

					if(myVal=='<?php echo USER_AREA; ?>' )
						$('.alias_form').show();
					else
						$('.alias_form').hide();

					cmb['salutatsion'].value(rname[0]);
					cmb['jenis'].value(res.data[0].jenis_id);
					$("#form_link").show();
					$('.nav-tabs a[href="#form"]').tab('show');
				}
			}).done(function(){
				$('#loading-mask').removeClass('loader-mask');
			});
		}else if(checkedVals.length >1){
			msg_box('<?php echo lang('msg_one_select_only'); ?>',['btnOK'],'Info!');
		}else
			msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
	});

	$('#nonaktif-<?php echo $timestamp; ?>').click(function(){
		var txt;
		var checkedVals = $('.petugasid:checkbox:checked').map(function() {
			return this.value;
		}).get();
		if(checkedVals !=''){
			msg_box("<?php echo lang('nfc_nonaktif_confirm'); ?> ("+checkedVals+")...?",[{'btnYES':function(){
				$(this).trigger('closeWindow');
				checkedVals.forEach(function(element){
					$("#"+element).hide('slow');
				});
				var petugasId= checkedVals.join(",");
				$.post('<?php echo site_url('user/petugas/statusNon'); ?>',{id:petugasId},function(){
					ds_petugas.read();
				});
			}},'btnNO'],'<?php echo lang('nfc_confirm'); ?>');
		}else
			msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
	});

	$('#aktif-<?php echo $timestamp; ?>').click(function(){
		var txt;
		var checkedVals = $('.petugasid:checkbox:checked').map(function() {
			return this.value;
		}).get();
		if(checkedVals !=''){
			msg_box("<?php echo lang('nfc_aktif_confirm'); ?> ("+checkedVals+")...?",[{'btnYES':function(){
				$(this).trigger('closeWindow');
				checkedVals.forEach(function(element){
					$("#"+element).hide('slow');
				});
				var petugasId= checkedVals.join(",");
				$.post('<?php echo site_url('user/petugas/status'); ?>',{id:petugasId},function(){
					ds_petugas.read();
				});
			}},'btnNO'],'<?php echo lang('nfc_confirm'); ?>');
		}else
			msg_box('<?php echo lang('msg_no_selected'); ?>',['btnOK'],'Info!');
	});

	$("#form-petugas-<?php echo $timestamp; ?>").submit(function(e){
		e.preventDefault();
		$.ajax({
			url:$(this).attr('action'),
			data:$(this).serialize(),
			type:'POST',
			dataType:'json',tail:1,
			beforeSend:function(){
				$('#loading-mask').addClass('loader-mask');
			},
			success:function(res){
				if(res.status){

					$('#smallText').hide();
					$('.alias_form').hide();
					$('#form-petugas-<?php echo $timestamp; ?> input[name=mode]').val('add');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=alias]').val('');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=first_name]').val('');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=last_name]').val('');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=password]').val('');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=uname]').val('');
					$('#form-petugas-<?php echo $timestamp; ?> input[name=email]').val('');
					ds_petugas.read();
					cmb['jenis'].select(0);
					cmb['salutatsion'].select(0);
					var area = $("#alias<?php echo $timestamp; ?>").data("kendoMultiSelect");
					var modules = $("#module<?php echo $timestamp; ?>").data("kendoMultiSelect");
					area.value([]);
					modules.value([]);
					$('#pesan').html('');
					$("#form_link").hide();
					$('.nav-tabs a[href="#home"]').tab('show');
					msg_box(res.messages,['btnOK'],'Info!');
				}else
					$('#pesan').html(res.messages);
			}
		}).done(function(){
			 $('#loading-mask').removeClass('loader-mask');

		});
	});
});

function toggle(source) {
  checkboxes = document.getElementsByClassName('petugasid');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<div id="loading-mask"></div>

<script id="grid-window-templates" type="text/x-kendo-template">
	<div class="grid-form-area"></div>
</script>
