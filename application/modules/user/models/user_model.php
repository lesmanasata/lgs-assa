<?php

class User_model extends CI_Model {

	var $table = 'petugas';
	var $area = 'MF_AREA';
	var $jenis = '';

	function __construct(){
		parent::__construct();
		$this->jenis = explode(',',ALLOWED_USER);
		$this->app = $this->config->item('app');
		$this->db = $this->load->database('default',TRUE);
	}

	function get($username,$password=FALSE){
		$this->db->where('id_petugas',$username);
		if($password !=FALSE)
			$this->db->where('password_petugas',$password);
		$this->db->where_in('jenis_petugas',$this->jenis);
		$stmt = $this->db->get($this->table);

		if ($stmt->num_rows() > 0){
			return $stmt;
		}else{
			return FALSE;
		}
	}

	function add($data){
		$this->db->insert(
			$this->table,$data
		);

		if($this->db->affected_rows()> 0)
			return TRUE;
		else
			return FALSE;
	}

	function add_ignore($data){
		$insert_query = $this->db->insert_string($this->table,$data);
		$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
		$this->db->query($insert_query);
		if($this->db->affected_rows()> 0)
			return TRUE;
		else
			return FALSE;
	}

	function get_join($filter){
		$this->db->select('p.id_petugas as id, p.module, p.email, IF(p.id_alias IS NULL,"-",p.id_alias) as alias, p.nama_petugas,
			CASE
				WHEN p.jenis_petugas = "'.USER_ADMIN.'" THEN "Administrator"
				WHEN p.jenis_petugas = "'.USER_AREA.'" THEN "Admin Area"
				WHEN p.jenis_petugas = "'.USER_UNDEFINE.'" THEN "LDAP"
				ELSE p.jenis_petugas END
			AS jenis_petugas, p.jenis_petugas as jenis_id,
			IF(p.status_petugas=1,"Aktif","Non Aktif") as status_petugas, a.VCH_AREANAME  as alias_nama',FALSE)
			->from($this->table.' p')
			->join($this->area.' a','p.id_alias=a.CHR_AREAID','LEFT');
		if($filter !=FALSE && count($filter)>0)
			$this->db->where($filter);
		$res =$this->db->get();
		$arr = $res->result();
		if($res->num_rows() > 0)
			return $arr;
		else
			return array();
	}

	function get_all($filter){
		$this->db->select('p.id_petugas as id,  p.nama_petugas,
			CASE
				WHEN p.jenis_petugas = "'.USER_ADMIN.'" THEN "Administrator"
				WHEN p.jenis_petugas = "'.USER_AREA.'" THEN "Admin Area"
				ELSE p.jenis_petugas END
			AS jenis_petugas, p.jenis_petugas as jenis_id,
			s.user_agent, s.dtimelogin,
			a.VCH_AREANAME as alias_nama',FALSE)
			->from($this->table.' p')
			->join($this->area.' a','p.id_alias=a.CHR_AREAID','LEFT')
			->join('session s','s.id_petugas=CONCAT(p.id_petugas,"'.$this->app.'")');
		if($filter !=FALSE && count($filter)>0)
			$this->db->where($filter);
		$res =$this->db->get();

		$arr = $res->result();

		if($res->num_rows() > 0){
			return $arr;
		}else
			return array();
	}

	function edit($data,$filter){
		$this->db->where($filter);
		$this->db->update(
			$this->table,$data
		);

		if($this->db->affected_rows()> 0)
			return TRUE;
		else
			return FALSE;
	}

	function get_id($id){
		$this->db->where('id_petugas',$id);
		$stmt = $this->db->get($this->table);
		if ($stmt->num_rows() > 0){
			return $stmt;
		}else{
			return FALSE;
		}
	}

	function change_password($id, $password){
		$data = array(
			'password_petugas'=>md5($password),
		);

		return $this->db->update($this->table,$data,array('id_petugas'=>$id));
	}

	function update_last_login($username){
		$data = array(
			'password_transaksi'=>$this->session->userdata('session_id'),
			'lastlogin'=>date('Y-m-d H:i:s'),
			'ip_address'=>$this->input->ip_address(),
			'user_agent'=>$this->session->userdata('user_agent')
		);
		$this->db->where_in('jenis_petugas',$this->jenis);
		$this->db->where(array('id_petugas' => $username.$this->app, 'status_petugas'=>1));
		return $this->db->update($this->table,$data);
	}

	function session_log($username){
		$stmt = $this->get_session_log($username);
		$session = $stmt!==FALSE?$stmt->row()->sessionid:$this->session->userdata('session_id');

		return $this->db->query('INSERT INTO '.$this->db->dbprefix('session').'(id_petugas,sessionid,ip_address,user_agent,dtimelogin,dtimeexpired)
		VALUES("'.$username.$this->app.'","'.$session.'","'.$this->get_client_ip().'","'.$this->session->userdata('user_agent').'",NOW(),DATE_ADD(NOW(),INTERVAL '.$this->config->item('sess_expiration').' SECOND))
		ON DUPLICATE KEY UPDATE id_petugas="'.$username.$this->app.'",sessionid="'.$this->session->userdata('session_id').'",user_agent="'.$this->session->userdata('user_agent').'",dtimeexpired=DATE_ADD(NOW(),INTERVAL '.$this->config->item('sess_expiration').' SECOND)');
	}

	function session_destroy($username){
		$this->db->where('id_petugas',$username.$this->app);
		return $this->db->delete('session');
	}

	function get_session_log($username){
		$this->db->select('id_petugas,sessionid,UNIX_TIMESTAMP(NOW()) dtimenow,UNIX_TIMESTAMP(dtimeexpired) dtimeexpired');
		$this->db->where('id_petugas',$username.$this->app);
		$stmt = $this->db->get('session');

		if ($stmt->num_rows() > 0){
			return $stmt;
		}else{
			return FALSE;
		}
	}

	function nonaktif($filter){
		$data = array('status_petugas' => '0' );
		$this->db->where($filter);
		$this->db->update('petugas', $data);
	}
	function aktif($filter){
		$data = array('status_petugas' => '1' );
		$this->db->where($filter);
		$this->db->update('petugas', $data);
	}

	function clear($filter){
		$this->db->where($filter);
		$this->db->delete('session');
	}

	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}
}
