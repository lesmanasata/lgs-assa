<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Plan_model extends CI_Model
{
    public $tbl_plan_header         = 'MF_MAINTENANCE_PLAN';
    public $tbl_plan_header_item    = 'MF_MAINTENANCE_PLAN_ITEM';
    public $tbl_plan                = 'TF_MAINTENANCE_PLAN';
	public $tbl_equipment           = 'MF_EQUIPMENT';
    public $fild_plan               = 'mp.VCH_Plan_Number,mp.VCH_Plan_Description,mp.DAT_Created,mp.VCH_Created_By, mp.INT_Total_Counter, mp.VCH_Uom, mp.VCH_Func_Loc, mp.VCH_Equipment, mp.VCH_Equipment_Desc, mp.VCH_Is_Used, mp.INT_indicator, mp.VCH_sortfild, mp.last_insert';
	public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

	function get_plan_data($filter=array(),$fild='*',$group=false){
        
        $this->db_main->select($fild, false)
            ->from($this->tbl_plan_header);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();

        return $res->result();
	}

    function get_plan_data_join($filter=array(),$fild='*',$group=false){
        
        $this->db_main->select($this->fild_plan.', e.VCH_ChasisNo, e.VCH_LicensePlatNumber as VCH_Plat_Number', false)
            ->from($this->tbl_plan_header.' mp')
            ->join($this->tbl_equipment.' e', 'e.VCH_EQUIPMENT=mp.VCH_Equipment');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        
        return $res->result();
    }


    function update_plan_data($data,$filter){
        $this->db_main->where($filter);
        $this->db_main->update(
            $this->tbl_plan_header,$data
        );

        if($this->db_main->affected_rows()> 0)
            return TRUE;
        else
          return FALSE;
    }

    function get_detail($filter=array(),$fild='*',$group=false){ //date_format(DAT_Plane_Date,'%d-%m-%Y')
        $c_fild = 'VCH_Plan_Number, VCH_Call_No,date_format(DAT_Plane_Date,"%d-%m-%Y") as DAT_Plane_Date,DAT_Call_Date,VCH_Due_Package,VCH_Scaduling,VCH_Counter,VCH_Next_Planed,VCH_Order';
        $this->db_main->select($c_fild, false)
            ->from($this->tbl_plan_header_item);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $this->db_main->order_by('VCH_Call_No','ASC');
        $res=$this->db_main->get();

        //echo $this->db_main->last_query();
        return $res->result();

    }

    function add($data){
        $this->db_main->insert(
            $this->tbl_plan,
            $data
        );

        //echo $this->db_main->last_query();
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }


    function cek_last_record($mxid){
        $qfilter  = '(select max(concat(VCH_Plan_Number,VCH_Call_No)) ';
        $qfilter .= 'from MF_MAINTENANCE_PLAN_ITEM ';
        $qfilter .= 'where concat(VCH_Plan_Number,VCH_Call_No) < "'.$mxid.'")';

        $this->db_main->select('VCH_Plan_Number,VCH_Call_No,VCH_Scaduling')
            ->from('MF_MAINTENANCE_PLAN_ITEM')
            ->where('concat(VCH_Plan_Number,VCH_Call_No)',$qfilter,FALSE);

        $res=$this->db_main->get();
        return $res->result();
    }


}