<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Order_model extends CI_Model
{

    public $tbl_master_order    = 'MF_ORDER';
    public $tbl_master_order_zbak = 'MF_ORDER_ZBAK';
    public $tbl_plant_dest      = 'MF_PLANT_DESTINATION';
    public $tbl_work_center     = 'MF_WORKCENTER';
    public $tbl_bis_area        = 'MF_AREA';
    public $tbl_material        = 'MF_MATERIAL';
    public $tbl_equipment       = 'MF_EQUIPMENT';
    public $tbl_purc_group      = 'MF_PURCHASING_GROUP';
    public $tbl_order_header    = 'TF_ORDER_HEADER';
    public $tbl_order_componets = 'TF_ORDER_COMPONENTS';
    public $tbl_order_operation = 'TF_ORDER_OPERATION';
    public $tbl_order_operation_item = 'TF_ORDER_OPERATION_ITEM';
    public $tbl_log_order_header = 'log_tf_order_header';
    public $tbl_customer        = 'MF_CUSTOMER';
    public $tbl_cost_element    = 'MF_COST_ELEMENTGL';
    
   

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
        $this->db_server = $this->load->database('server', true);
    }

    
    public function add_order_hider($data)
    {
        $this->db_main->insert(
            $this->tbl_order_header,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_log($fild='*',$filter=array(),$group=false){
        
        $this->db_main->select($fild, false)
            ->from($this->tbl_log_order_header);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();

        return $res->result();
    }
    function get_order_spk_data($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_header);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
       //echo $this->db_main->last_query();
        return $res->result();
    }

    function spk_data_delete($filter){
        $this->db_main->where($filter);
        $this->db_main->delete($this->tbl_order_header);

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function spk_data_update($data,$filter){
        unset($data['VCH_order_id']);
        $this->db_main->where($filter);
        $this->db_main->update(
            $this->tbl_order_header,$data
        );
        //echo $this->db_main->last_query();
        if($this->db_main->affected_rows()> 0)
            return TRUE;
        else
          return FALSE;
    }

    public function add_order_operation($data)
    {
        $this->db_main->insert(
            $this->tbl_order_operation,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }
    

    function get_grid_operation_join($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_operation.' h')
            ->join($this->tbl_order_operation_item.' d', 'h.VCH_order_id = d.VCH_order_id AND h.CHR_operation_id=d.VCH_operation_id','LEFT');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $this->db_main->order_by('CHR_operation_id','ASC');
        $res=$this->db_main->get();

        return $res->result();
    }


    function get_grid_operation($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_operation);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $this->db_main->order_by('CHR_operation_id','ASC');
        $res=$this->db_main->get();

        return $res->result();
    }

    public function add_order_operation_item($data)
    {
        $this->db_main->insert(
            $this->tbl_order_operation_item,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_grid_operation_item($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_operation_item);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);
        
        $this->db_main->order_by('VCH_operation_item_no','ASC');
        $res=$this->db_main->get();
        return $res->result();
    }

    public function add_order_componets($data)
    {
        $this->db_main->insert(
            $this->tbl_order_componets,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_grid_componet($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_componets);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $this->db_main->order_by('VHC_component_id','ASC');
        $res=$this->db_main->get();

        return $res->result();
    }

    function get_order_selection($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_master_order);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();

        return $res->result();
    }

    function get_order_selection_join($fild='*',$filter=array(),$group=false,$mode=array())
    {
        $use = json_decode(json_encode($mode));
        $this->db_server->select($fild, false)
            ->from($this->tbl_master_order.' m')
            ->join($this->tbl_equipment.' e', 'm.VCH_equipment + 0 = e.VCH_EQUIPMENT')
            ->join($this->tbl_order_header.' t','m.VCH_notif_id = t.VCH_notif_id','LEFT');
        if ($filter !=false && count($filter)>0) {
            $this->db_server->where($filter);
        }

        if($group != false)
            $this->db_server->group_by($group);

        if(!$use->total)
            $this->db_server->limit($use->take,$use->skip);

        $res=$this->db_server->get();
    
        //echo $this->db_server->last_query();

        if($use->total)
            return $res->num_rows();
        else
            return $res->result();

    }

    function get_order_zbak_selection_join($fild='*',$filter=array(),$group=false,$mode=array())
    {
        $use = json_decode(json_encode($mode));
        $this->db_server->select($fild, false)
            ->from($this->tbl_master_order_zbak.' m')
            ->join($this->tbl_equipment.' e', 'm.VCH_equipment + 0 = e.VCH_EQUIPMENT')
            ->join($this->tbl_order_header.' t','m.VCH_order_no = t.VCH_notif_id','LEFT');
        if ($filter !=false && count($filter)>0) {
            $this->db_server->where($filter);
        }

        if($group != false)
            $this->db_server->group_by($group);

        if(!$use->total)
            $this->db_server->limit($use->take,$use->skip);

        $res=$this->db_server->get();

        if($use->total)
            return $res->num_rows();
        else
            return $res->result();
    }

    function get_order_zbak_selection($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_master_order_zbak);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        return $res->result();
    }
    
    function get_order_header($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_order_header);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();

        return $res->result();
    }

    function get_plant($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_bis_area);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        return $res->result();
    }

     function get_equpment($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_equipment);
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }

    function get_work_center($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_work_center);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        return $res->result();
    }

    function get_part_number($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_material);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        return $res->result();
    }

    function get_purc_group($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_purc_group);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        return $res->result();
    }

    function get_cost_element($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_cost_element);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();
        
        return $res->result();
    }

    public function update_order($data, $filter=array())
    {
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
            $res = $this->db_main->update($this->tbl_master_order, $data);
            return $res;
        } else {
            return false;
        }
    }

    function order_item_delete($tbl,$filter){
        $this->db_main->where($filter);
        $this->db_main->delete($tbl);

        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_data_customer($fild,$filter,$cur_page,$take){
        $this->db_main->select($fild, false)
            ->from($this->tbl_customer);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        $this->db_main->limit($take,$cur_page);

        $res=$this->db_main->get();


       //echo $this->db_main->last_query();
        return $res->result();
    }

    function get_data_equipment($fild,$filter,$cur_page,$take){
        $this->db_main->select($fild, false)
            ->from($this->tbl_equipment);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        $this->db_main->limit($take,$cur_page);

        $res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }

    function get_total($tbl,$filter){
         $this->db_main->select('*', false)
            ->from($tbl);

         if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        $res=$this->db_main->get();

        return $res->num_rows();

    }

    function log_act($log_act_type='',$post_data=array()){
        //cfg_log = VCH_log_id|VCH_log_mth|ENM_loc_act
        $tbl_log_order_header        = 'log_tf_order_header';
        $tbl_log_order_componets     = 'log_tf_order_components';
        $tbl_log_order_operation     = 'log_tf_order_operation';
        $tbl_log_order_operation_item= 'log_tf_order_operation_item';
        $cfg_log_id                  = $this->getAutoId('VCH_log_id', 'log_tf_order_header','LOG');
        $cfg_log_mth                 = date('Ym');
        $cfg_log_act                 = strtoupper($log_act_type);

        if($log_act_type !=''){
            $post_data['header']['VCH_log_id'] = $cfg_log_id;
            $post_data['header']['VCH_log_mth'] = $cfg_log_mth;
            $post_data['header']['VCH_user_process'] = $this->session->userdata('user');
            $post_data['header']['ENM_log_act'] = $cfg_log_act;
            $post_data['header']['DTM_log_date'] = date('Y-m-d H:i:s');

            if(isset($post_data['header']['last_insert']))
                unset($post_data['header']['last_insert']);

            if(isset($post_data['header']['last_update']))
                unset($post_data['header']['last_update']);

            if(isset($post_data['header']['flag']))
                unset($post_data['header']['flag']);

            if(isset($post_data['header']['VCH_user_create']))
                unset($post_data['header']['VCH_user_create']);
            
            $this->db_main->insert($tbl_log_order_header,$post_data['header']);
            if($cfg_log_act=='ADD' || $cfg_log_act=='EDIT'){
                if(isset($post_data['componets']) && count($post_data['componets']) > 0){
                    $cmp_data = $post_data['componets'];
                    $item = 1;
                    foreach ($cmp_data as $key => $value) {
                        $cmp_data_add = $cmp_data[$key];
                        $cmp_data_add['VCH_log_id'] = $cfg_log_id;
                        $cmp_data_add['VCH_log_mth'] = $cfg_log_mth;
                        $cmp_data_add['VCH_log_item'] = $item;
                        $this->db_main->insert($tbl_log_order_componets, $cmp_data_add);
                        $item++;
                    }
                }

                if(isset($post_data['operation_item']) && count($post_data['operation_item']) > 0){
                    $cmp_data_opi = $post_data['operation_item'];
                    $item = 1;
                    foreach ($cmp_data_opi as $key => $value) {
                        $cmp_data_opi_add = $cmp_data_opi[$key];
                        $cmp_data_opi_add['VCH_log_id'] = $cfg_log_id;
                        $cmp_data_opi_add['VCH_log_mth'] = $cfg_log_mth;
                        $cmp_data_opi_add['VCH_log_item'] = $item;
                        $this->db_main->insert($tbl_log_order_operation_item, $cmp_data_opi_add);
                        $item++;
                    }
                }

                if(isset($post_data['operation']) && count($post_data['operation']) > 0){
                    $cmp_data_op = $post_data['operation'];
                    $item = 1;
                    foreach ($cmp_data_op as $key => $value) {
                        $cmp_data_op_add = $cmp_data_op[$key];
                        $cmp_data_op_add['VCH_log_id'] = $cfg_log_id;
                        $cmp_data_op_add['VCH_log_mth'] = $cfg_log_mth;
                        $cmp_data_op_add['VCH_log_item'] = $item;
                        $this->db_main->insert($tbl_log_order_operation, $cmp_data_op_add);
                        $item++;
                    }
                }
            }
        }
    }

    public function getAutoId($fields, $table, $inisial)
    {
        $leng_inisial = strlen($inisial);
        $query      = $this->db_main->query('SELECT MAX('.$fields.') as max from '.$table.' WHERE left('.$fields.','.$leng_inisial.')="'.$inisial.'"');
        $result     = current($query->result());
        $number     = 0;
        $imax           = 10;
        $tmp        = "";

        if ($result->max !='') {
            $number = substr($result->max, strlen($inisial));
        }

        $number ++;
        $number = strval($number);
        for ($i=1; $i<=($imax-strlen($inisial)-strlen($number)); $i++) {
            $tmp=$tmp."0";
        }

        return $inisial.$tmp.$number;
    }

}