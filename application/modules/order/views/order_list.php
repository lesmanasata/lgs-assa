<?php $token = time('his'); ?>
<h1 class="page-title">List Order/SPK</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Order-SPK </a></li>
  <li class="active"><strong>list</strong></li>
</ol>

<div class="banner">
	<div class="btn-group">
		<?php
			echo '<button class="btn btn-warning navbar-btn " id="edit-'.$token.'"><i class="fa fa-pencil"></i> Change </button> ';
			echo '<button class="btn btn-danger navbar-btn " id="delete-'.$token.'"><i class="glyphicon glyphicon-trash"></i> Delete </button> ';
			echo '<button class="btn btn-info navbar-btn " id="display-'.$token.'"><i class="fa fa-desktop"></i> Print SPK </button> ';
			echo '<button class="btn btn-success navbar-btn " id="approve-'.$token.'"><i class="fa fa-location-arrow"></i> Print PKB </button> ';
		?>
			
	</div>
</div>

<div class="blank">
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">List Of Order / SPK</a></li>
	</ul>

	<div class="tab-content grid-form1" style="border-top:0px;">
		  <div id="home" class="tab-pane fade in active ">
		  	<div class="padding_tab_body">
				<div class="" id="spkgrid-<?php echo $token; ?>"></div>
			</div>
		  </div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	

	var ds_order_spk = new kendo.data.DataSource({
		transport: {
			read: {
				type:"POST",
				dataType: "json",
				url: '<?php echo site_url('order/get_order_data'); ?>',
			}
		},
		schema: {
			parse: function(response){
				return response.data;
			},
			model: {
					fields: {
						create_date: { type: "string"},
					}
			}
		},
		pageSize: 10,
		serverFiltering: true,
	});

	$('#spkgrid-<?php echo $token; ?>').kendoGrid({
		dataSource: ds_order_spk,
		filterable: true,
		sortable: true,
		resizable: true,
		pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
		scrollable: true,
		filterable: {
			extra: false,
			operators: {
				string: {
					contains: "Like",
					eq: "=",
					neq: "!="
				},
			}
		},
		dataBound: function(e) {
			this.collapseRow(this.tbody.find("tr.k-master-row").first());
			var grid = e.sender;
			if (grid.dataSource.total() == 0) {
				var colCount = grid.columns.length;
				$(e.sender.wrapper)
					.find('tbody')
					.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; Data Kosong &mdash;&mdash;</td></tr>');
			}
		},
		columns: [
			{
				title:" ",
				width:35,
				template: "<input type='checkbox' class='order_spk' name='order_spk_id[]' value='#: VCH_order_id #' id='#: enc_order_spk_id #'>"
			},{
				field:"staus",
				width:100,
				title:"Status",
				template: "#if(staus ==0){# <span class='label label-success'>Open</span>#}else{#<span class='label label-info'>Approved</span>#}#",
				filterable: false,
			},
			{field:"pkb_number",width:150,title:"PKB No"},
			{field:"VCH_order_id",width:150,title:"Order/SPK No"},
			{
				field:"last_insert",
				//template: "#= kendo.toString(kendo.parseDate(create_date, 'dd-MM-yyyy'), 'dd-MM-yyyy') #",
				width: 130, 
				title:"Order Date",
				filterable : {
				  ui: function (element) {
				    element.kendoDatePicker({
				      format: "dd-MM-yyyy"
				    });
				  }
				},
				format: "{0:dd-MM-yyyy}",
				parseFormats: ["0:dd-MM-yyyy"],
			},
			{field:"VCH_order_create_type",width:90,title:"TYPE", attributes:{"style":"text-align:center;"}},
			{field:"VCH_functional_loc",width:150,title:"Func Location"},
			{field:"VCH_equipment_id",width:200,title:"Equipment"},
			{field:"TXT_description",width:300,title:"Order Description"},
		]
	});

	$('#approve-<?php echo $token; ?>').click(function(){
		var checkedVals = $('.order_spk:checkbox:checked').map(function() {
			return this.value;
		}).get();

		if(checkedVals.length ==1){
			$.ajax({
				url:'<?php echo site_url('order/cek_status'); ?>',
				data:'id='+checkedVals,
				type:'POST',
				dataType:'json',tail:1,
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},success:function(res){
					$('#loading-mask').removeClass('loader-mask');
					var msg = '';
					if(res.data[0].staus=='0'){
						msg = "Print PKB akan merubah status Order ("+checkedVals+") menjadi Approved ?";
					}else{
						msg = "Cetak Ulang PKB "+res.data[0].VCH_pkb_number+" ?";
					}

					msg_box(msg,[{'btnYES':function(){
						$(this).trigger('closeWindow');
						$.ajax({
							url:'<?php echo site_url('order/approve_spk'); ?>',
							data:'id='+checkedVals+"&ord_status="+res.data[0].staus,
							type:'POST',
							dataType:'json',tail:1,
							success:function(resx){
								if(res){
									ds_order_spk.read();
									var checkedVals_print = $('.order_spk:checkbox:checked').map(function() {
										return this.id;
									}).get();
									window.open('<?php echo site_url('order/print_order_pkb/'); ?>/'+checkedVals_print, '_blank');
								}
							}
						});
					}},'btnNO'],'Konfirmasi');

				}
			});
			
			
		}else if(checkedVals.length >1){
			msg_box('Select Only One data',['btnOK'],'Info!');
		}else
			msg_box('No data Selected',['btnOK'],'Info!');
	});

	$('#edit-<?php echo $token; ?>').click(function(){
		var checkedVals = $('.order_spk:checkbox:checked').map(function() {
			return this.id;
		}).get();

		if(checkedVals.length ==1){

			$.ajax({
				url:'<?php echo site_url('order/cek_stat_before_edit'); ?>',
				data:'id='+checkedVals,
				type:'POST',
				dataType:'json',tail:1,
				success:function(res){
					if(res.status)
						window.location.href='<?php echo site_url('order/index/'); ?>/'+checkedVals;
					else
						msg_box(res.messages,['btnOK'],'Info!');	
				}
			});
			
		}else if(checkedVals.length >1){
			msg_box('Select Only One data',['btnOK'],'Info!');
		}else
			msg_box('No data Selected',['btnOK'],'Info!');
	});

	$('#display-<?php echo $token; ?>').click(function(){
		var checkedVals = $('.order_spk:checkbox:checked').map(function() {
			return this.id;
		}).get();

		if(checkedVals.length ==1){
			window.open('<?php echo site_url('order/print_order_spk/'); ?>/'+checkedVals, '_blank');
		}else if(checkedVals.length >1){
			msg_box('Select Only One data',['btnOK'],'Info!');
		}else
			msg_box('No data Selected',['btnOK'],'Info!');
	});

	$('#delete-<?php echo $token; ?>').click(function(){
		var checkedVals = $('.order_spk:checkbox:checked').map(function() {
			return this.value;
		}).get();

		if(checkedVals.length ==1){
			msg_box("Hapus data Forwarder Id ("+checkedVals+")...?",[{'btnYES':function(){
				$(this).trigger('closeWindow');
				$.ajax({
					url:'<?php echo site_url('order/delete_spk'); ?>',
					data:'id='+checkedVals,
					type:'POST',
					dataType:'json',tail:1,
					success:function(res){
						ds_order_spk.read();
						if(res.status){
							msg_box('Order/SPK No '+checkedVals+' Berhasil Dihapus',['btnOK'],'Info!');
						}else{
							msg_box('Gagal Menghapus Order/SPK No '+checkedVals,['btnOK'],'Info!');
						}
					}
				});
			}},'btnNO'],'Konfirmasi');

		}else if(checkedVals.length >1){
			msg_box('Select Only One data',['btnOK'],'Info!');
		}else
			msg_box('No data Selected',['btnOK'],'Info!');
	});
});
</script>