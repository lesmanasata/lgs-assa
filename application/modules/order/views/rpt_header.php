<?php $timestamp = time();?>

<div class="banner">
	<div class="btn-group">
		<?php
			echo '<button class="btn btn-info navbar-btn " id="display-'.$timestamp.'"><i class="fa fa-desktop"></i> Print SPK </button> ';
			echo '<button class="btn btn-success navbar-btn " id="approve-'.$timestamp.'"><i class="fa fa-location-arrow"></i> Print PKB </button> ';
		?>
			
	</div>
</div>

<div class="padding_tab_body">
	<div id="order_sub<?php echo $timestamp; ?>"></div>
</div>


<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function(){
		$.fn.show_detail=function(){
			var url = $(this).data("url");
	        var title = $(this).data("id");
	        var id = $(".nav-tabs_x").children().length;
	        $('.nav-tabs_x').append('<li><a href="#contact_'+id+'">'+title+'  &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;</a> <span class="close">x</span> </li>');
	        $('.tab-content_x').append('<div class="tab-pane" id="contact_'+id+'"></div>');

	        $.ajax({
				url:url,
				type:'POST',
				dataType: 'html',
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},success:function(res){
					$("#contact_"+id).html(res);
				}
			}).done(function(){
				$('#loading-mask').removeClass('loader-mask');
			});

	        $('.nav-tabs_x a[href="#contact_' + id + '"]').trigger('click');
		}


		var ds_order_detail = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('order/rpt/getHeader'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 DAT_basic_date_start: {type: "date"},
						 DAT_basic_date_end: {type: "date"}
					 }
				}
			},
			pageSize: 50,
		});


		$("#order_sub<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_order_detail,
			pageable: true,
			resizable: true,
			height: 550,
			scrollable: true,
			filterable: {
				extra: false,
				operators: {
					string: {
						startswith: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					title:"SELECT",
					width:80,
					template: "<input type='checkbox' class='order_spk' name='order_spk_id[]' value='#: id #' id='#: enc_order_spk_id #'>"
				},{
					title:"ACTION",
					filterable: false,
					template: "<a href='javascript:void(0)'  onclick='$(this).show_detail();' data-url='<?php echo site_url('order/rpt/itemdetail'); ?>/#: id #' data-id='#: id #'><button class='btn btn-xs btn-info'> <i class='glyphicon glyphicon-pencil'></i> Detail</button></a>",
					width: 100,
				},{
					field:"staus",
					width: 100, 
					title:"<?php echo lang('ar_status'); ?>",
					template: "#if(staus ==0){# <span class='label label-success'>Open</span>#}else{#<span class='label label-info'>Approved</span>#}#",
					filterable: false
				},{
					field:"id",
					width: 120,
					title:"<?php echo lang('rpt_odr_id'); ?>",
					filterable: true
				},{
					field:"pkb_number",
					width:150,
					title:"PKB No"
				},{
					field:"notif_id",
					width: 130, 
					title:"<?php echo lang('rpt_odr_notif'); ?>",
					filterable: true
				},{
					field:"create_type",
					width: 150, 
					title:"<?php echo lang('rpt_odr_type'); ?>",
					filterable: true
				},{
					field:"VCH_functional_loc	",
					width: 150, 
					title:"<?php echo lang('rpt_odr_funclock'); ?>",
					filterable: true
				},{
					field:"VCH_equipment_id	",
					width: 200, 
					title:"<?php echo lang('rpt_odr_equipment'); ?>",
					filterable: true
				},{
					field:"VCH_equpment_description",
					width: 250, 
					title:"<?php echo lang('rpt_odr_equipment_desc'); ?>",
					filterable: true
				},{
					field:"VCH_sold_toparty",
					width: 130, 
					title:"<?php echo lang('rpt_odr_soldtoprty'); ?>",
					filterable: true
				},{
					field:"DAT_basic_date_start",
					width: 130, 
					title:"<?php echo lang('rpt_odr_date_start'); ?>",
					filterable: true,
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["0:dd-MM-yyyy"],
					filterable : {
					  ui: function (element) {
					    element.kendoDatePicker({
					      format: "dd-MM-yyyy"
					    });
					  }
					}
				},{
					field:"DAT_basic_date_end",
					width: 130, 
					title:"<?php echo lang('rpt_odr_date_end'); ?>",
					filterable : {
					  ui: function (element) {
					    element.kendoDatePicker({
					      format: "dd-MM-yyyy"
					    });
					  }
					},
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["0:dd-MM-yyyy"],
				}

			]
		});

		$('#display-<?php echo $timestamp; ?>').click(function(){
			var checkedVals = $('.order_spk:checkbox:checked').map(function() {
				return this.id;
			}).get();

			if(checkedVals.length ==1){
				window.open('<?php echo site_url('order/print_order_spk/'); ?>/'+checkedVals, '_blank');
			}else if(checkedVals.length >1){
				msg_box('Select Only One data',['btnOK'],'Info!');
			}else
				msg_box('No data Selected',['btnOK'],'Info!');
		});

		$('#approve-<?php echo $timestamp; ?>').click(function(){
			var checkedVals = $('.order_spk:checkbox:checked').map(function() {
				return this.value;
			}).get();

			if(checkedVals.length ==1){
				$.ajax({
					url:'<?php echo site_url('order/cek_status'); ?>',
					data:'id='+checkedVals,
					type:'POST',
					dataType:'json',tail:1,
					beforeSend:function(){
						$('#loading-mask').addClass('loader-mask');
					},success:function(res){
						$('#loading-mask').removeClass('loader-mask');
						var msg = '';
						if(res.data[0].staus=='0'){
							msg = "Print PKB akan merubah status Order ("+checkedVals+") menjadi Approved ?";
						}else{
							msg = "Cetak Ulang PKB "+res.data[0].VCH_pkb_number+" ?";
						}

						msg_box(msg,[{'btnYES':function(){
							$(this).trigger('closeWindow');
							$.ajax({
								url:'<?php echo site_url('order/approve_spk'); ?>',
								data:'id='+checkedVals+"&ord_status="+res.data[0].staus,
								type:'POST',
								dataType:'json',tail:1,
								success:function(resx){
									if(res){
										ds_order_detail.read();
										var checkedVals_print = $('.order_spk:checkbox:checked').map(function() {
											return this.id;
										}).get();
										window.open('<?php echo site_url('order/print_order_pkb/'); ?>/'+checkedVals_print, '_blank');
									}
								}
							});
						}},'btnNO'],'Konfirmasi');

					}
				});
				
				
			}else if(checkedVals.length >1){
				msg_box('Select Only One data',['btnOK'],'Info!');
			}else
				msg_box('No data Selected',['btnOK'],'Info!');
		});

	});
</script>