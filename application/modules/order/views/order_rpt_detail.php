<?php $token = time('his'); $modid = $order_header->VCH_order_id; ?>
<div class="padding_tab_body">

	<form class="form-horizontal" id="<?php echo $timestamp; ?>" method='POST'>
		<div class="col-md-6">
			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Order Type</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_order_create_type; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Func Location</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_functional_loc; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Equipment Id</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_equipment_id; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Equipment Description</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_equpment_description; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Work Center</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_workcenter_plan.'-'.$order_header->VCH_workcenter_main; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Basic Start Date</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->DAT_basic_date_start ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Basic End Date</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->DAT_basic_date_end ?>"  readonly>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Customer Id</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_sold_toparty; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Vendor Id</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_vendor_id; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Vendor name</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_vendor_name; ?>"  readonly>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">Vendor Addres</label>
				<div class="col-sm-8">
					<textarea readonly="readonly" class="form-control"><?php echo $order_header->VCH_vendor_addres.','.$order_header->VCH_vendor_kota.' Tlp:'.$order_header->VCH_vendor_telpon; ?></textarea>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label hor-form">User Create</label>
				<div class="col-sm-8">
					<input type='text' class="form-control" value="<?php echo $order_header->VCH_user_create; ?>"  readonly>
				</div>
			</div>
		</div>
	</form>
	<div style="clear: both;">&nbsp;</div>
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home<?php echo $token; ?>">Operation</a></li>
	  <li><a data-toggle="tab" href="#menu1<?php echo $token; ?>">Item Operation</a></li>
	  <li><a data-toggle="tab" href="#menu2<?php echo $token; ?>">Component</a></li>
	</ul>

	<div class="tab-content">
	  <div id="home<?php echo $token; ?>" class="tab-pane fade in active">
	  	<div class="padding_tab_body">
	  		<div id="order_grid_operation<?php echo $token; ?>"></div>
	  	</div>
	  </div>
	  <div id="menu1<?php echo $token; ?>" class="tab-pane fade">
	  	<div class="padding_tab_body">
	    	<div id="order_grid_componet_item<?php echo $token; ?>"></div>
		</div>
	  </div>
	  <div id="menu2<?php echo $token; ?>" class="tab-pane fade">
	    <div class="padding_tab_body">
	    	<div id="order_grid_componet<?php echo $token; ?>"></div>
	  	</div>
	  </div>
	</div>
	
</div>

	

<script type="text/javascript">
	$(document).ready(function(){

/////////////////////////////////////////COMPONENTS///////////////////////////////////////////////////////////////////////////
			var ds_component = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_component'); ?>',
					},
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							comp_number: { editable: false },
							opration_number: { editable: true },
							componet_type: { editable: true },
							purch_group: { editable: true },
							part_description: { editable: true },
							part_number: { editable: true },
							uom: { editable: true },
							qty: { type: "number", editable: true },
							order_prce: { type: "number", editable: true },
						}
					}
				},  
				batch: true,
				pageSize: 100,
			});

			$("#order_grid_componet<?php echo $token; ?>").kendoGrid({
				dataSource: ds_component,
				pageable: false,
				scrollable: true,
				editable: false,
				resizable: true,
				height:300,
				selectable: "multiple",
				/*toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},*/
				dataBinding: function() {
				  row_number_component = (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					/*{ 
						command: {
							text: "", 
							template: "<a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a>", 
							click: function(){
								var grid = $(this).data("kendoGrid");
	         					grid.removeRow($(this).closest('tr'));
							} 
						}, 
						title: "<?php echo lang('label_action'); ?>", 
						width: 65 
					},*/{
						field:"comp_number",
						width: 150, 
						title:"Comp. Number",
						filterable: false,
						template: "#= ++row_number_component #0",
					},{
						field:"opration_number",
						width: 150, 
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						editor: function(container, options){
							var grid = $("#order_grid_operation<?php echo $token; ?>").data("kendoGrid");
							var row_data = grid.dataSource._data;
							var curent_ds = [];
							$.each(row_data, function(i, item) {
								 curent_ds.push({'id': (i+1)+'0', 'text': (i+1)+'0'});
							});
							   
				            $('<input required id="' + options.field + '" name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: curent_ds
				            });
				        },
					},{
						field:"part_number", 
						width: 150, 
						title:"<?php echo lang('ord_part_number'); ?>",
						filterable: false,
						editor: function(container, options){
				            var localDropDown = $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                //autoBind: false,
				                filter: "contains",
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: true,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_part_number'); ?>",
				                    }
				                  }
				                },
				                dataBound: adjustDropDownWidth,
				                change:function(e){
									var grid = $("#order_grid_componet<?php echo $token; ?>").data("kendoGrid");
									var selectedItem = grid.dataItem(grid.select());
									var cur_val = selectedItem.part_number;
									$.ajax({
										url:'<?php echo site_url('order/get_part_number_by_id'); ?>',
										data:'material_no='+cur_val,
										type:'POST',
										dataType:'json',
										tail:1,
										success:function(res){
											if(res.status){
												selectedItem.set('part_description',res.data[0].text);
												selectedItem.set('uom',res.data[0].uom);
											}
										}
									});
								}
				            });
				        },
					},
					{field:"part_description",width: 200, title:"<?php echo lang('ord_part_description'); ?>",filterable: false},
					{field:"uom",width: 100, title:"<?php echo lang('ord_uom'); ?>",filterable: false},
					{field:"qty",width: 200, title:"<?php echo lang('ord_qty'); ?>",filterable: false},
					{
						field:"componet_type", 
						width: 100, 
						title:"<?php echo lang('ord_type'); ?>",
						filterable: false,
						editor: function(container, options){
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_type_component'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},
					{field:"order_prce",width: 200, title:"<?php echo lang('ord_price'); ?>",filterable: false},
					
				]
			});


/////////////////////////////////////////OPERATION/////////////////////////////////////////////////////////////////////
			var ds_operation = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_operation'); ?>',
					},
					create: {
						url: '<?php echo site_url('order/order_submit'); ?>',
						type:"POST",
						dataType: "json",
						data: function(){
							var componet_data_grid  = $('#order_grid_componet<?php echo $token; ?>').data("kendoGrid");
							var componet_data 		= {'components':componet_data_grid.dataSource._data};

							var item_operation_grid = $('#order_grid_componet_item<?php echo $token; ?>').data("kendoGrid");
							var item_operation_data	= {'itemoperation':item_operation_grid.dataSource._data};

							$.extend( componet_data, item_operation_data );

							var hider_data 			= $(this).hider_data();
							$.extend( componet_data, hider_data );

							return JSON.parse(JSON.stringify(componet_data));
						},complete: function(res){
							$('#loading-mask').removeClass('loader-mask');
							var res_msg = JSON.parse(res.responseText);
							msg_box(res_msg.messages,['btnOK'],'Info!');
							if(res_msg.status){
								$('#form_link').hide();
								$('.nav-tabs a[href="#home"]').tab('show');

								$('#order_selection_table<?php echo $token; ?>').html('');

								ds_operation.read();
								ds_component.read(); 
								ds_operation_item.read();
							}

						}
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							operation_number: { editable: false },
							purch_group_comp: { editable: true },
							order_type: { editable: true },
							ord_description: { editable: true },
						}
					}
				},
				batch: true,
				pageSize: 100,
			});

			$("#order_grid_operation<?php echo $token; ?>").kendoGrid({
				dataSource: ds_operation,
				pageable: false,
				scrollable: true,
				resizable: true,
				editable: false,
				height:300,
				selectable: "multiple",
				/*toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},*/
				dataBinding: function(){
				  row_number_operation =  (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					/*{ 
						command: {
							text: "", 
							template: "<a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a>", 
							click: function(){
								var grid = $(this).data("kendoGrid");
	         					grid.removeRow($(this).closest('tr'));
							} 
						}, 
						title: "<?php echo lang('label_action'); ?>", 
						width: 65 
					},*/{
						field:"operation_number",
						width: 130,
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						template: "#= ++row_number_operation #0",
					},{
						field:"order_type",
						width: 150, 
						title:"<?php echo lang('ord_type'); ?>",
						filterable: false,
						editor: function(container, options){
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_type'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},/*{
						field:"purch_group_comp",
						width: 200, 
						title:"<?php //echo lang('ord_purch_group'); ?>",
						filterable: false,
						editor: function(container, options){
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                filter: "contains",
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php //echo site_url('order/get_cmb_purc_group'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},*/
					{field:"ord_description",width: 200, title:"<?php echo lang('ord_description'); ?>",filterable: false},
				]
			});




////////////////////////////////////////////ITEM OPERATION//////////////////////////////////////////////////////////////////////
			var ds_operation_item = new kendo.data.DataSource({ 
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_operation_item'); ?>',
					},
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							op_no_item: { editable: false },
							opration_number: { editable: true },
							sort_text: { editable: true },
							item_qty: { type:'number', editable: true },
							item_price: { type:'number', editable: true },
						} 
					}
				},  
				batch: true,
				pageSize: 100,
			});

			$("#order_grid_componet_item<?php echo $token; ?>").kendoGrid({
				dataSource: ds_operation_item,
				pageable: false,
				scrollable: true,
				editable: false,
				height:300,
				resizable: true,
				selectable: "multiple",
				/*toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},*/
				dataBinding: function() {
				  row_number_op_item =  (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					/*{ 
						command: {
							text: "", 
							template: "<a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a>", 
							click: function(){
								var grid = $(this).data("kendoGrid");
	         					grid.removeRow($(this).closest('tr'));
							} 
						}, 
						title: "<?php echo lang('label_action'); ?>", 
						width: 65 
					},*/{
						field:"opration_number",
						width: 150, 
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						editor: function(container, options){
							var grid = $("#order_grid_operation<?php echo $token; ?>").data("kendoGrid");
							var row_data = grid.dataSource._data;
							var curent_ds = [];
							$.each(row_data, function(i, item) {
								if(item.order_type=='PM02')
									curent_ds.push({'id': (i+1)+'0', 'text': (i+1)+'0'});
							});
							   
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: curent_ds
				            });
				        },
					},{
						field:"sort_text",
						width: 150, 
						title:"<?php echo lang('ord_sort_text'); ?>",
						filterable: false
					},{
						field:"item_qty", 
						width: 80, 
						title:"<?php echo lang('ord_qty'); ?>",
						filterable: false
					},{
						field:"item_price",
						width: 100, 
						title:"<?php echo lang('ord_price'); ?>",
						filterable: false
					}
				]
			});
	});
</script>
