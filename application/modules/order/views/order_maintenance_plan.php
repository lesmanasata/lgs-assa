<?php $timestamp = time('his'); ?>
<h1 class="page-title">Maintenance Plan</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Order</a></li>
  <li class="active"><strong>Maintenance Plan</strong></li>
</ol>

<div class="row">
	<div class="col-md-12 animatedParent animateOnce z-index-50">
		<div class="tabs-container animated fadeInUp">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Selection Form</a></li>
			  <li id="form_link"><a data-toggle="tab" href="#form" id="padding_tab_link">Maintenace Plan</a></li>
			</ul>

			<div class="tab-content grid-form1" style="border-top:0px;">
			  	<div id="home" class="tab-pane fade in active ">
			  		<div class="padding_tab_body"> 
			  			<form class="form-horizontal" id="maintenance_selection" method='POST'>
			  				
			  				<div class="form-group">
						      	<label class="control-label col-sm-2" >Filter By</label> 
						      	<div class="col-sm-5">
						      		<label class="radio-inline"><input type="radio" value="byid" id="plan_number" name="filter_type" checked="checked">Maintenance Plan Number</label>
									<label class="radio-inline"><input type="radio" value="bydate" id="create_date" name="filter_type">Create Date</label>
									<label class="radio-inline"><input type="radio" value="bynopol" id="fnopol" name="filter_type">Nopol</label>
									<label class="radio-inline"><input type="radio" value="byrangka" id="frangka" name="filter_type">No. Rangka</label>
									<label class="radio-inline"><input type="radio" value="equipment" id="fequipment" name="filter_type">Equipment</label>
						      	</div>
						    </div>
			  				
		        			<div class="form-group" id="byid">
						      <label class="control-label col-sm-2" >Maintence Plan No</label>
						      <div class="col-sm-5">
						        <input type="text" id="find_by_id" name="plan_number" class="form-control"  placeholder="<?php echo lang('rpt_find_id'); ?>" >
						      </div>
						    </div>

						    <div class="form-group" id="bynopol" >
						      <label class="control-label col-sm-2" >Nopol</label> 
						      <div class="col-sm-5">
						        <input type="text" id="find_by_id" name="tnopol" class="form-control"  placeholder="Nopol" >
						      </div>
						    </div>

						    <div class="form-group" id="byrangka" >
						      <label class="control-label col-sm-2" >No. Rangka</label>
						      <div class="col-sm-5">
						        <input type="text" id="find_by_id" name="trangka" class="form-control"  placeholder="No. Rangka" >
						      </div>
						    </div>

						    <div class="form-group" id="equipment" >
						      <label class="control-label col-sm-2" >Equpment</label>
						      <div class="col-sm-5">
						        <input type="text" id="find_by_id" name="tequpment" class="form-control"  placeholder="Equpment">
						      </div>
						    </div>

						    <div class="form-group" id="bydate">
						    	<label class="control-label col-sm-2" >Created date</label>
						    	<div class="col-sm-3">
						    		<div class="input-group input-daterange">
    									<input type="text" name="date_start" id='date_start'>
    									<div class="input-group-addon">to</div>
    									<input type="text" name="date_end" id="date_end">
									</div>
						      	</div>
						    </div>

						    <div class="form-group">
						      <div class="col-sm-offset-2 col-sm-10">
						        <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
						        <button type="button" id="ref<?php echo $timestamp; ?>" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
						      </div>
						    </div>
						</form>

						<div id="result_selection<?php echo $timestamp; ?>"></div>
			  		</div>
				</div>

				<div id="form" class="tab-pane">
			  		<div class="padding_tab_body">
			  			<form class="form-horizontal" id="maintenance_submit" method='POST'>
			  				<input type="hidden" name="indicator_c" id='indicator_c'>
			  				<input type="hidden" name="mode" id="mode">
			  				<div class="col-sm-6">
				  				<div class="form-group">
							      <label class="control-label col-sm-4" >Maintence Plan No</label>
							      <div class="col-sm-8">
							        <input type="text"  name="plan_number" class="form-control" readonly="readonly" >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Description</label>
							      <div class="col-sm-8">
							        <input type="text"  name="description" class="form-control" readonly="readonly"  >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Functional Location</label>
							      <div class="col-sm-8">
							        <input type="text" name="func_loc" class="form-control" readonly="readonly"  >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Equipment</label>
							      <div class="col-sm-8">
							        <input type="text"  name="equipment" class="form-control" readonly="readonly"   >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Equipment Description</label>
							      <div class="col-sm-8">
							        <input type="text" name="equipment_desc" class="form-control" readonly="readonly"  >
							      </div>
							    </div>
							</div>

							<div class="col-sm-6">
				  				<div class="form-group">
							      <label class="control-label col-sm-4" >Created Date</label>
							      <div class="col-sm-8">
							        <input type="text"  name="create_date" class="form-control" readonly="readonly"  >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Created By</label>
							      <div class="col-sm-8">
							        <input type="text"  name="create_by" class="form-control" readonly="readonly" >
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Total Counter Reading</label>
							      <div class="col-sm-8">
						    		<div class="input-group input-daterange">
    									<input type="text" id='t_counter' name="t_counter" class="form-control" readonly="readonly" >
    									<div class="input-group-addon">UOM</div>
    									<input type="text" name="t_uom" class="form-control" readonly="readonly" >
									</div>
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >No Pol</label>
							      <div class="col-sm-8">
							        <input type="text"  name="no_pol" class="form-control" readonly ="readonly">
							      </div>
							    </div>

							    <div class="form-group">
							      <label class="control-label col-sm-4" >Sort Field</label>
							      <div class="col-sm-8">
							        <input type="text"  name="sort_fild" class="form-control" readonly ="readonly">
							      </div>
							    </div>
							</div>

							<div class="form-group">
						      <!-- <label class="control-label col-sm-2" ></label> -->
						      <div class="col-sm-12">
						        <table class="table table-bordered">
								    <thead>
								      <tr>
								        <th></th>
								        <th>Call No</th>
								        <th>Planned Date</th>
								        <th>Due Pakcages</th>
								        <th>Scheduling type/status</th>
								        <th>Complete Counter Reading</th>
								        <th>Next Planned Counter Reading</th>
								        <th>Order</th>
								      </tr>
								    </thead>
								    <tbody id="item_table">
								      
								  </tbody>
								</table>
						      </div>
						    </div>
							
							<input  type="hidden" name="s_counter" readonly="readonly"> 
							<input  type="hidden" name="s_date" readonly="readonly">


			  				<div class="form-group"> 
						      <div class="col-sm-offset-2 col-sm-10">  
						        <button type="button" id="start" class="proses btn btn-info">Start</button>
						        <button type="button" id="release" class="proses btn btn-warning">Release</button>
						        <button type="button" id="restart" class="proses btn btn-danger">Restart</button>
						      </div>
						    </div>
			  			</form>
			  		</div>
				</div>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#form_link").hide();

		$("#bynopol, #byrangka, #equipment, #bydate").hide();
		$("#maintenance_selection input[name='filter_type']").change(function(){
			var value = $("#maintenance_selection input[name='filter_type']:checked").val();
			$("#byid, #bynopol, #byrangka, #equipment, #bydate").hide();
			$("#"+value).show();
		});

		$("#maintenance_selection").submit(function(e){
			e.preventDefault();
			
			$.ajax({
				url: "<?php echo site_url('order/plan/get_selection'); ?>",
				type:"POST",
				data:$(this).serialize(),
				beforeSend:function(){
					$("#result_selection<?php echo $timestamp; ?>").addClass('loader');
				},
				success:function(result){
					$("#result_selection<?php echo $timestamp; ?>").removeClass('loader');
					$("#result_selection<?php echo $timestamp; ?>").html(result);
				}
			});
		});

		

		var start = $("#date_start").kendoDatePicker({
            format: "dd-MM-yyyy",
            change: startChange
        }).data("kendoDatePicker");

        var end = $("#date_end").kendoDatePicker({
            format: "dd-MM-yyyy",
            change: endChange
        }).data("kendoDatePicker");

        start.readonly(true);
        end.readonly(true);
        $("#plan_number, #create_date").click(function(){
         	var curId = $(this).attr('id');
         	$('#find_by_id').val("");
         	start.value("");
         	end.value("");
         	if(curId=="plan_number"){
         		$('#find_by_id').prop("disabled", false);
         		start.readonly(true);
        		end.readonly(true);
         	}else{
         		$('#find_by_id').prop("disabled", true);
         		start.readonly(false);
        		end.readonly(false);
         	}
        });

		function startChange() {
            var startDate = start.value(),
            endDate = end.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                end.min(startDate);
            } else if (endDate) {
                start.max(new Date(endDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        function endChange() {
            var endDate = end.value(),
            startDate = start.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                start.max(endDate);
            } else if (startDate) {
                end.min(new Date(startDate));
            } else {
                endDate = new Date();
                start.max(endDate);
                end.min(endDate);
            }
        }

        start.max(end.value());
        end.min(start.value());
        
        

        $("#start, #release, #restart").click(function(e){
			e.preventDefault();
			var mode 	= $(this).attr('id');
			var counter = $('#t_counter').val();
			var type = $('#indicator_c').val();
			var ex 		= '';
			$('#mode').val(mode);
			$('#p_countr_reading').hide();
			$('#p_countr_date').hide();

			if(mode=='start' || mode=='restart'){
				if(type == '3'){
					$('#modal_title').html('Start Counter Reading');
					$('#p_countr_reading').show();
					$('#t_countr_reading').val(counter);
				}else{
					$('#modal_title').html('Start Date');
					$('#p_countr_date').show();
				}
				$('#extra_fild').html(ex);
				$('#myModal').modal('toggle');
			}else{
				$.ajax({
					url:"<?php echo site_url('order/plan/submit'); ?>",
					type:'POST',
					data:$('#maintenance_submit').serialize(),
					dataType: 'json',tail:1,
					beforeSend:function(){
						$("#loading-mask").addClass('loader-mask');
					},
					success:function(res){
						$("#loading-mask").removeClass('loader-mask');
						if(res.status){
							$("#form_link").hide();
							$('.nav-tabs a[href="#home"]').tab('show');
						}
						msg_box(res.messages,['btnOK'],'Info!');
						
					}
				});
			}
		});

		$('#modal_submit').click(function(e){
			e.preventDefault();
			$('#myModal').modal('toggle');
			$('#maintenance_submit input[name=s_counter]').val($('#t_countr_reading').val());
			$('#maintenance_submit input[name=s_date]').val($('#t_counter_date').val());

			$.ajax({
				url:"<?php echo site_url('order/plan/submit'); ?>",
				type:'POST',
				data:$('#maintenance_submit').serialize(),
				dataType: 'json',tail:1,
				beforeSend:function(){
					$("#loading-mask").addClass('loader-mask');
				},
				success:function(res){
					$("#loading-mask").removeClass('loader-mask');
					if(res.status){
						$("#form_link").hide();
						$('.nav-tabs a[href="#home"]').tab('show');
					}
					msg_box(res.messages,['btnOK'],'Info!');
					
				}
			});
		});

	});

</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_title">Start Date</h4>
      </div>
      <div class="modal-body">
        	<div class="form-group row" id="p_countr_reading">
		    	<label class="col-sm-3 col-form-label">Start Of Cycle</label>
		    	<div class="col-sm-7">
		      		<input type="number" class="form-control" id="t_countr_reading" placeholder="Counter Reading">
		    	</div>
		    	<div class="col-sm-2">
		      		<input type="text" class="form-control" readonly="readonly" value="KM">
		    	</div>
		  	</div>

		  	<div class="form-group row" id="p_countr_date" >
		    	<label for="inputPassword" class="col-sm-3 col-form-label">Start Of Cycle</label>
		    	<div class="col-sm-9">
		      		<input type="text" class="form-control datepicker" id="t_counter_date" value="<?php echo date('d-m-Y'); ?>">
		    	</div>
		  	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="modal_submit" >Proses</button>
      </div>
    </div>

  </div>
</div>