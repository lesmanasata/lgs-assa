<?php $timestamp = time();?>
<input type="hidden" name="counter_reading" id="counter_reading" readonly="readonly" value="0">
<div id="maintenace_plan_selection<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	$(document).ready(function(){
		var no_urut = 0;

		var ds_order_selection = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($filter); ?>,
					url: '<?php echo site_url('order/plan/get_selection_data'); ?>',
				}
			},
			schema: {
				parse: function(response){
					return response.data;
				},model: {
					fields: {
						INT_Total_Counter: { type: "number"},
						DAT_Created: { type: "date"},
					}
				},
			},
			pageSize: 20,
		});

		$("#maintenace_plan_selection<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_order_selection,
			scrollable: true,
			filterable: {
				extra: false,
				operators: {
					string: {
						contains: "Like",
						eq: "=",
						neq: "!="
					},
				}
			},
			pageable: {
	            refresh: true,
	            pageSizes: true,
	        },
			sortable: true,
			resizable: true,
			height: 550,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					title:"Select",
					width:80,
					template: '<button  onClick="$(this).dialog_proses();" data-counter="#:INT_Total_Counter#" data-id="#:VCH_Plan_Number#" class="pilih btn btn-xs btn-primary"><?php echo lang('btn_select'); ?></button>',
				},{
					title:"Plan Number",
					field:'VCH_Plan_Number',
					width:130,
				},{
					title:"Plan Description",
					field:'VCH_Plan_Description',
					width:250,
				},{
					title:"No. Rangka",
					field:'VCH_ChasisNo',
					width:150,
				},{
					field:"DAT_Created",
					width: 150, 
					title:"Created Date",
					filterable : {
					  ui: function (element) {
					    element.kendoDatePicker({
					      format: "dd-MM-yyyy"
					    });
					  }
					},
					format: "{0:dd-MM-yyyy}",
					parseFormats: ["0:dd-MM-yyyy"],
				},{
					title:"Sort Field",
					field:'VCH_sortfild',
					width:150,
				},{
					title:"Total Counter",
					field:'INT_Total_Counter',
					attributes:{style:'text-align:right'},
					template:'#= kendo.toString(INT_Total_Counter, "n0")#',
					width:150,
				},{
					title:"Uom",
					field:'VCH_Uom', 
					width:60,
				},{
					title:"Functional Loc",
					field:'VCH_Func_Loc', 
					width:150,
				},{
					title:"Equipment",
					field:'VCH_Equipment',
					width:150,
				},{
					title:"Equipment Description",
					field:'VCH_Equipment_Desc',
					width:300,
				},{
					title:"Plat Number",
					field:'VCH_Plat_Number',
					width:150,
				},
			]
		});


		$.fn.dialog_proses = function() {
			$('#start').prop( "disabled", true );
			$('#release').prop( "disabled", true );
			$('#restart').prop( "disabled", true ); 
			$('#item_table').html('');
			$('#counter_reading').val($(this).data('counter'));
			var uid = $(this).data('id');
			$.ajax({
				url:'<?php echo site_url('order/plan/get_plan'); ?>',
				type:'POST',
				data:'id='+uid,
				dataType: 'json',tail:1,
				beforeSend:function(){
					$('#loading-mask').addClass('loader-mask');
				},
				success:function(res){
					var tbl='';
					$('#maintenance_submit input[name=plan_number]').val(res.header[0].VCH_Plan_Number);
					$('#maintenance_submit input[name=description]').val(res.header[0].VCH_Plan_Description);
					$('#maintenance_submit input[name=func_loc]').val(res.header[0].VCH_Func_Loc);
					$('#maintenance_submit input[name=equipment]').val(res.header[0].VCH_Equipment);
					$('#maintenance_submit input[name=equipment_desc]').val(res.header[0].VCH_Equipment_Desc);
					$('#maintenance_submit input[name=create_date]').val(res.header[0].DAT_Created);
					$('#maintenance_submit input[name=create_by]').val(res.header[0].VCH_Created_By);
					$('#maintenance_submit input[name=maintenance_plan_no]').val(res.header[0].VCH_Plan_Number);
					$('#maintenance_submit input[name=no_pol]').val(res.header[0].VCH_Plat_Number);
					$('#maintenance_submit input[name=sort_fild]').val(res.header[0].VCH_sortfild);
					$('#maintenance_submit input[name=t_counter]').val(res.header[0].INT_Total_Counter);
					$('#maintenance_submit input[name=t_uom]').val(res.header[0].VCH_Uom);
					$('#maintenance_submit input[name=indicator_c]').val(res.header[0].INT_indicator);

					if(res.act == '0')
						$('#start').prop( "disabled", false );

					if(res.act == '1'){
						$('#release').prop( "disabled", false);
						$('#restart').prop( "disabled", false );
					}

					if(res.act == '2')
						$('#restart').prop( "disabled", false );

					for(var i = 0; i < res.detail.length; i++) {
						tbl +='<tr>';
						var str 		= res.detail[i].VCH_Scaduling;
						var str_low 	= str.toLowerCase();
						var str_cek 	= str_low.includes("hold");
						if(!str_cek)
							tbl +='<td></td>';
						else
							tbl +='<td><input type="radio" name="item_id" value="'+res.detail[i].VCH_Call_No+'"></td>';

						tbl +='<td>'+res.detail[i].VCH_Call_No+'</td>';
						tbl +='<td>'+res.detail[i].DAT_Plane_Date+'</td>';
						tbl +='<td>'+res.detail[i].VCH_Due_Package+'</td>';
						tbl +='<td>'+res.detail[i].VCH_Scaduling+'</td>';
						tbl +='<td>'+res.detail[i].VCH_Counter+'</td>';
						tbl +='<td>'+res.detail[i].VCH_Next_Planed+'</td>';
						tbl +='<td>'+res.detail[i].VCH_Order+'</td>';
						tbl +='</tr>';
					}
					$('#item_table').html(tbl);

					$("#form_link").show();
					$('.nav-tabs a[href="#form"]').tab('show');
				}
			}).done(function(){
				$('#loading-mask').removeClass('loader-mask');

			});
		}

		

	});
</script>