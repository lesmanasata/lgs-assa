<?php $timestamp = time('his'); ?>
<h1 class="page-title">Report Order/SPK</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Oder-SPK</a></li>
  <li class="active"><strong>Report Order-SPK</strong></li>
</ol>

<div class="row">
	<div class="col-md-12 animatedParent animateOnce z-index-50">
		<div class="tabs-container animated fadeInUp">
			<ul class="nav nav-tabs nav-tabs_x">
			  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Report Filter</a></li>
			</ul>
			<div class="tab-content tab-content_x grid-form1" style="border-top:0px;">
			  <div id="home" class="tab-pane fade in active ">
			  	<div class="padding_tab_body">
			  		<div class="col-md-12">
			  			<form class="form-horizontal" id="rpt_order<?php echo $timestamp; ?>" method='POST'>
		        			<div class="form-group">
						      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_id'); ?></label>
						      <div class="col-sm-3">
						        <input type="text" id="find_btn_id" name="find_id" class="form-control"  placeholder="<?php echo lang('rpt_find_id'); ?>" >
						      </div>
						    </div>

						    <div class="form-group">
						      <div class="col-sm-offset-2 col-sm-10">
						        <div class="checkbox">
						          <label><input type="checkbox" id="custome_filter" name="custome_filter"> <?php echo lang('rpt_find_custome'); ?></label>
						        </div>
						      </div>
						    </div>
						    <div class="form-group">
						      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_peride'); ?></label>
						      <div class="col-sm-10">
						        <label><input type="radio" id="rb_date" value="rb_date"checked name="find_periode"> <?php echo lang('rpt_find_peride_one'); ?></label>&nbsp; &nbsp;
						        <label><input type="radio" id="rb_month" value="rb_month" name="find_periode"> <?php echo lang('rpt_find_peride_two'); ?></label>
						      </div>
						    </div>

						    <div class="form-group" id="dv_date">
						      <div class="col-sm-offset-2 col-sm-10">
						        <input type="text" id="find_tanggal" name="find_tanggal" value="<?php echo date('d-m-Y'); ?>" class="form-control"  placeholder="<?php echo lang('rpt_find_date'); ?>" >
						      </div>

						    </div>

						    <div class="form-group" id="dv_month">
						    	<div class="col-sm-offset-2 col-sm-2">
						        	<input type="number" onkeypress='numberOnly(event)' name="find_year" class="form-control" value="<?php echo date('Y'); ?>" placeholder="<?php echo lang('rpt_find_yer'); ?>" >
						      	</div>
						      	<div class=" col-sm-4">
						        	<input type="text" id="find_bulan" name="find_bulan" style="width: 100%" class="form-control"  placeholder="<?php echo lang('rpt_find_month'); ?>" >
						      	</div>
						    </div>

						    <div class="form-group">
						      <label class="control-label col-sm-2" ><?php echo lang('rpt_find_user'); ?></label>
						      <div class="col-sm-4">
						        <input type="text" id="find_user" name="find_user" style="width: 100% " class="form-control"  placeholder="<?php echo lang('rpt_find_user'); ?>" >
						      </div>
						    </div>

						    <div class="form-group">
						      <div class="col-sm-offset-2 col-sm-10">
						        <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
						        <button type="button" id="ref<?php echo $timestamp; ?>" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
						      </div>
						    </div>
						</form>

			  		</div>
			  		<div style="clear: both;">&nbsp;</div>
			  		<div class="padding_tab_body">
						<div id="item_row<?php echo $timestamp; ?>"></div>
					</div>
			  	</div>
			  </div>
			</div>
		</div>
	</div>
</div>


		        	
<script type="text/javascript">
	$(document).ready(function(){
		$("#dv_month").hide();
        $("#dv_year").hide();
        $("#dv_date").show();
        $("#rb_date, #rb_month").click(function(){
         	var curId = $(this).attr('id');
         	if(curId=="rb_date"){
         		$("#dv_month").hide();
         		$("#dv_year").hide();
         		$("#dv_date").show();
         	}else{
         		$("#dv_month").show();
         		$("#dv_year").show();
         		$("#dv_date").hide();
         	}
         });

        $('#ref<?php echo $timestamp; ?>').click(function() {
            location.reload();
            $('#frm_trx<?php echo $timestamp; ?> input[name=find_id]').val('');
            $('#frm_trx<?php echo $timestamp; ?> input[name=custome_filter]')[0].checked = false;
        });

        var cmbBulan = $('#rpt_order<?php echo $timestamp; ?> input[name=find_bulan]')
          .kendoDropDownList({
            autoBind: true,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/getcmbBulan'); ?>",
                }
              }
            }
        });

        var cmbUser = $('#find_user')
          .kendoDropDownList({
            autoBind: true,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_petugas'); ?>",
                }
              }
            }
        });

        $("#find_tanggal").kendoDatePicker({format: "dd-MM-yyyy"});
		var datepicker = $("#find_tanggal").data("kendoDatePicker");

        $('#rpt_order<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', true);
       	$('#rb_date').prop('disabled', true);
        $('#rb_month').prop('disabled', true);
        datepicker.readonly(true);
        $("#find_user").data("kendoDropDownList").enable(false);
        $("#find_bulan").data("kendoDropDownList").enable(false);
        $("#custome_filter").click(function(){
        	var curStat = $(this).is(":checked");
        	if(curStat){
        		$('#rpt_order<?php echo $timestamp; ?> input[name=find_id]').val('');
        		$('#rpt_order<?php echo $timestamp; ?> input[name=find_id').prop('readonly', true);
        		$('#rpt_order<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', false);
        		$('#rb_date').prop('disabled', false);
        		$('#rb_month').prop('disabled', false);
        		datepicker.readonly(false);
        		$("#find_user").data("kendoDropDownList").enable(true);
        		$("#find_bulan").data("kendoDropDownList").enable(true);

        	}else{
        		$('#rpt_order<?php echo $timestamp; ?> input[name=find_id]').prop('readonly', false);
        		$('#rpt_order<?php echo $timestamp; ?> input[name=find_year]').prop('readonly', true);
        		$('#rb_date').prop('disabled', true);
        		$('#rb_month').prop('disabled', true);
        		datepicker.readonly(true);
        		$("#find_user").data("kendoDropDownList").enable(false);
        		$("#find_bulan").data("kendoDropDownList").enable(false);
        	}
        });

        

		$(".nav-tabs_x").on("click", "a", function(e){
		      e.preventDefault();
		      $(this).tab('show');
		}).on("click", "span", function () {
		        var anchor = $(this).siblings('a');
		        $(anchor.attr('href')).remove();
		        $(this).parent().remove();
		        $(".nav-tabs_x li").children('a').first().click();
		});



		$("#rpt_order<?php echo $timestamp; ?>").submit(function(e){
        	e.preventDefault();
        	$.ajax({
				url:'<?php echo site_url('order/rpt/getDetial'); ?>',
				type:'POST',
				data:$(this).serialize(),
				dataType: 'html',
				beforeSend:function(){
					$('#item_row<?php echo $timestamp; ?>').addClass('loader');
				},success:function(res){
					$('#item_row<?php echo $timestamp; ?>').html(res);
				}
			}).done(function(){
				$('#item_row<?php echo $timestamp; ?>').removeClass('loader');
			});
        });
	});
</script>


<style type="text/css">

.container {
    margin-top: 10px;

}

.nav-tabs > li {
    position:relative;
}

.nav-tabs > li > a {
    display:inline-block;
}

.nav-tabs > li > span {
    font-size: 16px;
    box-sizing:border-box;
    border: 1px solid #505050;
   	background: #ddd;
    font-weight: bold;
    cursor:pointer;
    position:absolute;
    right: 6px;
    top: 8px;

    padding-left: 4px;
    padding-right: 4px;
    padding-bottom: 3px;
    border-radius: 4px;
}

.nav-tabs > li:hover > span {
    display: inline-block;
}
</style>