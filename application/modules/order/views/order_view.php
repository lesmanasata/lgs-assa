<?php $token = time('his'); ?>
<h1 class="page-title">Order - Form</h1>
<ol class="breadcrumb breadcrumb-2">
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
  <li><a href="#">Order</a></li>
  <li class="active"><strong>Form</strong></li>
</ol>

<div class="row">
	<div class="col-md-12 animatedParent animateOnce z-index-50">
		<div class="tabs-container animated fadeInUp">
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#home" id="padding_tab_link">Selection Order</a></li>
			  <li id="form_link"><a data-toggle="tab" href="#form" id="padding_tab_link">Create Order</a></li>
			</ul>

			<div class="tab-content grid-form1" style="border-top:0px;">
			  <div id="home" class="tab-pane fade in active ">
			  	<div class="padding_tab_body">
			  		<div class="col-md-7">
				  		<form class="form-horizontal" id="order<?php echo $token; ?>" method="POST">
				  			<input type='hidden' name='mode' value='<?php echo $this->uri->segment(3);?>'>
							<div class="col-md-9">
								<?php if($this->uri->segment(3)=='sr'){ ?>
									<div class="form-group">
										<label  class="col-sm-4 control-label hor-form">Notif No</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="notif_no" placeholder="Notif Number">
										</div>
									</div>
								<?php } ?>

								<?php if($this->uri->segment(3)=='zbak'){ ?>
									<div class="form-group">
										<label  class="col-sm-4 control-label hor-form">Order Number</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="order_no" placeholder="Order Number">
										</div>
									</div>
								<?php } ?>

								<div class="form-group">
										<label  class="col-sm-4 control-label hor-form">License Plate Number</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="nopol" placeholder="License Plate Number">
										</div>
									</div>

								<div class="form-group">
									<label  class="col-sm-4 control-label hor-form">Created Date From</label>
									<div class="col-sm-8">
										<input type="text" class="form-control startDate_selection" name="created_date_from"   placeholder="Created Date From">
									</div>
								</div>

								<div class="form-group">
									<label  class="col-sm-4 control-label hor-form">Created Date To</label>
									<div class="col-sm-8">
										<input type="text" class="form-control endDate_selection" name="created_date_to"   placeholder="Created Date To">
									</div>
								</div>

								<div class="form-group">
									<label  class="col-sm-4 control-label hor-form"></label>
									<div class="col-sm-8">
										<button type="submit" class="btn btn-primary">Proses Data</button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="col-md-12" id='order_selection_table<?php echo $token; ?>'></div>
			  	</div>
			  </div>

			  <div id="form" class="tab-pane fade ">
			  	<div class="padding_tab_body">
			  		<form class="form-horizontal" id="order_proses<?php echo $token; ?>" method="POST">
				  		<div class="tabs-container tabs-vertical">
							<ul class="nav nav-tabs" style="width:120px !important;">
								<li class="active"><a aria-expanded="true" href="#home-1" data-toggle="tab">Header</a></li>
								<li><a aria-expanded="false" href="#profile-1" data-toggle="tab">Operation</a></li>
								<li><a aria-expanded="false" href="#settings" data-toggle="tab">Item Operation</a></li>
								<li><a aria-expanded="false" href="#messages" data-toggle="tab">Componets</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="home-1">
									<div class="panel-body" style="border: none !important;">
										<div id="order_form_header<?php echo $token; ?>"></div>
									</div>
								</div>
								<div class="tab-pane" id="profile-1">
									<div class="panel-body" style="border: none !important;">
										<div id="order_grid_operation<?php echo $token; ?>"></div>
									</div>
								</div>
								<div class="tab-pane" id="messages">
									<div class="panel-body" style="border: none !important;">
										<div id="order_grid_componet<?php echo $token; ?>"></div>
									</div>
								</div>
								<div class="tab-pane" id="settings" >
									<div class="panel-body" style="border: none !important;">
										<!-- <div id="order_grid_componet_item<?php echo $token; ?>"></div> -->
										<div id="order_grid_componet_item"></div>
									</div>
								</div>
							</div>
						</div>
						<div style="clear: both;">&nbsp;</div>
						<div class="panel-footer">
				          <div class="row">
				            <div class="col-sm-10 col-sm-offset-2" align="right">
				            <button type="submit"  class="save<?php echo $token; ?> btn btn-primary"><?php echo lang('btn_save'); ?></button>
				            <?php echo anchor('order/index','<button type="button"  class="cancel<?php echo $token; ?> btn btn-danger">'.lang('btn_cancel').'</button>'); ?> 
				            
				            </div>
				          </div>
				        </div>
					</form>
			  	</div>
			  </div>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var row_number_operation =0;
	var row_number_component =0;
	var row_number_op_item	 =0;
	var mx_op 				 =0
	$(document).ready(function(){
		
			$('#form_link').hide();
		<?php if($this->uri->segment(4) !='') { ?>
			$('#form_link').show();
			$('.nav-tabs a[href="#form"]').tab('show');

			$.ajax({
				url:"<?php echo site_url('order/show_order_form'); ?>",
				type:'POST',
				data:'type=<?php echo $type; ?>&notif_id=<?php echo $notif_id; ?>&order_crete_type=<?php echo $order_crete_type; ?>&modeid=<?php echo $modid; ?>',
				dataType:'html',
				beforeSend:function(){
					$('#order_form_header<?php echo $token; ?>').addClass('loader');
				},success:function(res){
					$('#order_form_header<?php echo $token; ?>').removeClass('loader');
					$('#order_form_header<?php echo $token; ?>').html(res);
				}
			});
		<?php } ?>

		$('#order<?php echo $token; ?>').submit(function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo site_url('order/get_selection'); ?>",
				type:'POST',
				data:$(this).serialize(),
				dataType:'html',
				beforeSend:function(){
					$('#order_selection_table<?php echo $token; ?>').addClass('loader');
				},success:function(res){
					$('#order_selection_table<?php echo $token; ?>').removeClass('loader');
					$('#order_selection_table<?php echo $token; ?>').html(res);
				}
			});
		});

		$('#order_proses<?php echo $token; ?>').submit(function(e){
			e.preventDefault();	

			var operation_data_grid  = $('#order_grid_operation<?php echo $token; ?>').data("kendoGrid");
			var operation_data 		= operation_data_grid.dataSource._data;
			
			if(operation_data.length > 0){
				$('#loading-mask').addClass('loader-mask');
				ds_operation.sync();
			}else
				msg_box('Please add Operation Data',['btnOK'],'Info!');

		});

		$.fn.crete_order=function(){
			var order_type 		= $(this).attr('id');
			var order_layout 	= $(this).data('layout');

			var checkedVals = $('.order_id:checkbox:checked').map(function() {
				return this.value;
			}).get();

			if(checkedVals.length ==1 && order_type !='WO03' || checkedVals.length <= 0 && order_type =='WO03' || order_type =='WO05'){
				$("#form_link").show();
				$('.nav-tabs a[href="#form"]').tab('show');

				$.ajax({
					url:"<?php echo site_url('order/show_order_form'); ?>",
					type:'POST',
					data:'type='+order_layout+'&notif_id='+checkedVals+'&order_crete_type='+order_type,
					dataType:'html',
					beforeSend:function(){
						$('#order_form_header<?php echo $token; ?>').addClass('loader');
					},success:function(res){
						$('#order_form_header<?php echo $token; ?>').removeClass('loader');
						$('#order_form_header<?php echo $token; ?>').html(res);
					}
				});
			}else if(checkedVals.length >1){
				msg_box('Select Only One data',['btnOK'],'Info!');
			}else{
				var ms = (order_type=='ZBAK'|| order_type=='OR01')?'Order':'Notif';
				msg_box('No data Selected or create '+order_type+' without '+ms,['btnOK'],'Info!');
			}
		}

		$.fn.hider_data=function(){
			var out= {'header':{
							'description'			: $('#order_form_header<?php echo $token; ?> input[name=description]').val(),
							'functional_location'	: $('#order_form_header<?php echo $token; ?> input[name=func_loc]').val(),
							'equipment'				: $('#order_form_header<?php echo $token; ?> input[name=equipment]').val(),
							'equipment_desc'		: $('#order_form_header<?php echo $token; ?> input[name=equipment_desc]').val(),
							'pm_act'				: $('#order_form_header<?php echo $token; ?> input[name=act_type]').val(),
							'main_work_center' 		: $('#order_form_header<?php echo $token; ?> input[name=main_work_center]').val(),
							'plan_work_center'		: $('#order_form_header<?php echo $token; ?> input[name=plan_work_center]').val(),
							'system_status'			: $('#order_form_header<?php echo $token; ?> input[name=sys_status]').val(),
							'user_status'			: $('#order_form_header<?php echo $token; ?> input[name=user_status]').val(),
							'basic_start_date'		: $('#order_form_header<?php echo $token; ?> input[name=basic_start]').val(),
							'basic_end_date'		: $('#order_form_header<?php echo $token; ?> input[name=basic_end]').val(),
							'notif'					: $('#order_form_header<?php echo $token; ?> input[name=notif_id]').val(),
							'vendor_id'				: $('#order_form_header<?php echo $token; ?> input[name=vendor]').val(),
							'vendor_name'			: $('#order_form_header<?php echo $token; ?> input[name=nama]').val(),
							'vendor_kota'			: $('#order_form_header<?php echo $token; ?> input[name=kota]').val(),
							'vendor_tlp'			: $('#order_form_header<?php echo $token; ?> input[name=telp]').val(),
							'vendor_npwp'			: $('#order_form_header<?php echo $token; ?> input[name=npwp]').val(),
							'vendor_addres'			: $('#order_form_header<?php echo $token; ?> textarea[name=alamat]').val(),
							'sold_toparty'			: $('#order_form_header<?php echo $token; ?> input[name=soldtoparty]').val(),
							'vendor_pos'			: $('#order_form_header<?php echo $token; ?> input[name=kd_pos]').val(),
							'type_order_create'		: $('#order_form_header<?php echo $token; ?> input[name=type_order_create]').val(),
							'zbak_priority'			: $('#order_form_header<?php echo $token; ?> input[name=zbak_priority]').val(),
							'po_number'				: $('#order_form_header<?php echo $token; ?> input[name=po_number]').val(),
							'po_date'				: $('#order_form_header<?php echo $token; ?> input[name=po_date]').val(),
							'sys_cond'				: $('#order_form_header<?php echo $token; ?> input[name=sys_cond]').val(),
							'keluhan'				: $('#order_form_header<?php echo $token; ?> input[name=keluhan]').val(),
							'km_unit'				: $('#order_form_header<?php echo $token; ?> input[name=km_unit]').val(),
							'order_id'				: '<?php echo $modid; ?>',
							'form_action'			: $('#order_form_header<?php echo $token; ?> input[name=form_action]').val(),
							'cost'					: $('#order_form_header<?php echo $token; ?> input[name=cost]').val(),
							'purc_group'			: $('#order_form_header<?php echo $token; ?> input[name=purc_group]').val(),
							'ppn_type'				: $('#order_form_header<?php echo $token; ?> input[name=ppn_type]').val(),
							'payment_terms'			: $('#order_form_header<?php echo $token; ?> input[name=pay_term]').val()
						}
					}
			return out;
		}

/////////////////////////////////////////OPERATION/////////////////////////////////////////////////////////////////////
			var ds_operation = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_operation'); ?>',
					},
					create: {
						url: '<?php echo site_url('order/order_submit'); ?>',
						type:"POST",
						dataType: "json",
						data: function(){
							var componet_data_grid  = $('#order_grid_componet<?php echo $token; ?>').data("kendoGrid");
							var componet_data 		= {'components':componet_data_grid.dataSource._data};

							//var item_operation_grid = $('#order_grid_componet_item<?php echo $token; ?>').data("kendoGrid");
							var item_operation_grid = $('#order_grid_componet_item').data("kendoGrid");
							var item_operation_data	= {'itemoperation':item_operation_grid.dataSource._data};

							$.extend( componet_data, item_operation_data );

							var hider_data 			= $(this).hider_data();
							$.extend( componet_data, hider_data );

							return JSON.parse(JSON.stringify(componet_data));
						},complete: function(res){
							$('#loading-mask').removeClass('loader-mask');
							var res_msg = JSON.parse(res.responseText);
							msg_box(res_msg.messages,['btnOK'],'Info!');
							if(res_msg.status){
								$('#form_link').hide();
								$('.nav-tabs a[href="#home"]').tab('show');

								$('#order_selection_table<?php echo $token; ?>').html('');

								ds_operation.read();
								ds_component.read(); 
								ds_operation_item.read();
							}

						}
					}
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							operation_number: { editable: false },
							purch_group_comp: { editable: true },
							order_type: { editable: true },
							ord_description: { editable: true, validation: { maxlength:40} },
							VCH_mod: { editable: false },
						}
					}
				},
				batch: true,
				pageSize: 100,
			});

			$("#order_grid_operation<?php echo $token; ?>").kendoGrid({
				dataSource: ds_operation,
				scrollable: true,
				pageable: {
		            refresh: true,
		            pageSizes: true,
		        },
				editable: true,
				height:300,
				selectable: "multiple",
				toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},
				dataBinding: function(){
				  row_number_operation =  (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{ 
						template: "#if(VCH_mod == '2'){#  #}else{# <a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a> #}#", 
						title: "<?php echo lang('label_action'); ?>", 
						width: 30 
					},{
						field:"operation_number",
						width: 130,
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						template: "#if(VCH_mod == '2'){# #= operation_number# # mx_op = parseInt(operation_number); # #}else{# #= mx_op+(++row_number_operation * 10) # #}#",
					},{
						field:"order_type",
						width: 150, 
						title:"<?php echo lang('ord_type'); ?>",
						filterable: false,
						editor: function(container, options){
							var isedit = (options.model.VCH_mod=='2')?'readonly':'';
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" '+isedit+' />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_type'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},{
						field:"ord_description",
						width: 200, 
						title:"<?php echo lang('ord_description'); ?>",
						filterable: false,
					},
				]
			});


/////////////////////////////////////////COMPONENTS///////////////////////////////////////////////////////////////////////////
			var ds_component = new kendo.data.DataSource({
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_component'); ?>',
					},
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							comp_number: { editable: false },
							opration_number: { editable: true },
							componet_type: { editable: true },
							purch_group: { editable: true },
							part_description: { editable: true },
							part_number: { editable: true },
							uom: { editable: true },
							qty: { type: "number", editable: true },
							order_prce: { type: "number", editable: true },
						}
					}
				},  
				batch: true,
				pageSize: 100,
			});

			$("#order_grid_componet<?php echo $token; ?>").kendoGrid({
				dataSource: ds_component,
				pageable: false,
				pageable: {
		            refresh: true,
		            pageSizes: true,
		        },
				editable: true,
				height:300,
				selectable: "multiple",
				toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},
				dataBinding: function() {
				  row_number_component = (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{ 
						command: {
							text: "", 
							template: "<a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a>", 
							click: function(){
								var grid = $(this).data("kendoGrid");
	         					grid.removeRow($(this).closest('tr'));
							} 
						}, 
						title: "<?php echo lang('label_action'); ?>", 
						width: 65 
					},{
						field:"comp_number",
						width: 150, 
						title:"Comp. Number",
						filterable: false,
						template: "#= ++row_number_component #0",
					},{
						field:"opration_number",
						width: 150, 
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						editor: function(container, options){
							var grid = $("#order_grid_operation<?php echo $token; ?>").data("kendoGrid");
							var row_data = grid.dataSource._data;
							//alert(JSON.stringify(grid));
							var curent_ds = [];
							var op_last   = 0;
							var reset_op  = 0;
							$.each(row_data, function(i, item){
								if(row_data[i].operation_number ==''){
									reset_op++;
									var op_use = parseInt(op_last) + (reset_op*10);
									curent_ds.push({'id': op_use, 'text':op_use});
								}else{
									op_last = row_data[i].operation_number;
									curent_ds.push({'id': row_data[i].operation_number, 'text': row_data[i].operation_number});
								}
							});
							   
				            $('<input required id="' + options.field + '" name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: curent_ds
				            });
				        },
					},{
						field:"part_number", 
						width: 150, 
						title:"<?php echo lang('ord_part_number'); ?>",
						filterable: false,
						editor: function(container, options){
				            var localDropDown = $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                //autoBind: false,
				                filter: "contains",
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: true,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_part_number'); ?>",
				                    }
				                  }
				                },
				                dataBound: adjustDropDownWidth,
				                change:function(e){
									var grid = $("#order_grid_componet<?php echo $token; ?>").data("kendoGrid");
									var selectedItem = grid.dataItem(grid.select());
									var cur_val = selectedItem.part_number;
									$.ajax({
										url:'<?php echo site_url('order/get_part_number_by_id'); ?>',
										data:'material_no='+cur_val,
										type:'POST',
										dataType:'json',
										tail:1,
										success:function(res){
											if(res.status){
												selectedItem.set('part_description',res.data[0].text);
												selectedItem.set('uom',res.data[0].uom);
											}
										}
									});
								}
				            });
							//var localDropDown = $("#" + options.field).data("kendoDropDownList");
    						//localDropDown.list.width("auto"); 
				        },
					},
					{field:"part_description",width: 200, title:"<?php echo lang('ord_part_description'); ?>",filterable: false},
					{field:"uom",width: 100, title:"<?php echo lang('ord_uom'); ?>",filterable: false},
					{field:"qty",width: 200, title:"<?php echo lang('ord_qty'); ?>",filterable: false},
					{
						field:"componet_type", 
						width: 100, 
						title:"<?php echo lang('ord_type'); ?>",
						filterable: false,
						editor: function(container, options){
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_type_component'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},
					{field:"order_prce",width: 200, title:"<?php echo lang('ord_price'); ?>",filterable: false},
				]
			});

////////////////////////////////////////////ITEM OPERATION//////////////////////////////////////////////////////////////////////
			var ds_operation_item = new kendo.data.DataSource({ 
				transport: {
					read: {
						type:"POST",
						data:{'id':'<?php echo $modid; ?>'},
						dataType: "json",
						url: '<?php echo site_url('order/grid_operation_item'); ?>',
					},
				},
				schema: {
					parse: function(response){
						return response.data;
					},
					model: {
						fields: {
							op_no_item: { editable: false },
							opration_number: { editable: true },
							costelement: { editable: true },
							sort_text: { editable: true },
							item_qty: { type:'number', editable: true },
							item_price: { type:'number', editable: true },
						} 
					}
				},  
				batch: true,
				pageSize: 100,
			});

			//$("#order_grid_componet_item<?php echo $token; ?>").kendoGrid({
			$("#order_grid_componet_item").kendoGrid({
				dataSource: ds_operation_item,
				pageable: false,
				scrollable: true,
				pageable: {
		            refresh: true,
		            pageSizes: true,
		        },
				editable: true,
				height:300,
				selectable: "multiple",
				toolbar: [
					{ name: "create", text: "Add New Row" },
            	],
            	editable: {
					createAt: 'bottom'
				},
				dataBinding: function() {
				  row_number_op_item =  (this.dataSource.page() -1) * this.dataSource.pageSize();
				},
				dataBound: function(e) {
					this.collapseRow(this.tbody.find("tr.k-master-row").first());
					var grid = e.sender;
					if (grid.dataSource.total() == 0) {
						var colCount = grid.columns.length;
						$(e.sender.wrapper)
							.find('tbody')
							.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
					}
				},
				columns: [
					{ 
						command: {
							text: "", 
							template: "<a class='k-grid-delete' href='javascript:void(0)'><i class='glyphicon glyphicon-trash'></i></a>", 
							click: function(){
								var grid = $(this).data("kendoGrid");
	         					grid.removeRow($(this).closest('tr'));
							} 
						}, 
						title: "<?php echo lang('label_action'); ?>", 
						width: 65 
					},{
						field:"opration_number",
						width: 150, 
						title:"<?php echo lang('ord_opr_number'); ?>",
						filterable: false,
						editor: function(container, options){
							var grid = $("#order_grid_operation<?php echo $token; ?>").data("kendoGrid");
							var row_data = grid.dataSource._data;
							var curent_ds = [];
							var op_last   = 0;
							var reset_op  = 0;
							$.each(row_data, function(i, item){

								if(row_data[i].operation_number ==''){
									reset_op++;
									var op_use = parseInt(op_last) + (reset_op*10);
									if(item.order_type=='PM02')
										curent_ds.push({'id': op_use, 'text':op_use});
								}else{
									op_last = row_data[i].operation_number;
									if(item.order_type=='PM02')
										curent_ds.push({'id': row_data[i].operation_number, 'text': row_data[i].operation_number});
								}
							});
							/*$.each(row_data, function(i, item) {
								if(item.order_type=='PM02')
									curent_ds.push({'id': (i+1)+'0', 'text': (i+1)+'0'});
							});*/
							   
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: curent_ds
				            });
				        },
					},{
						field:"sort_text",
						width: 150, 
						title:"<?php echo lang('ord_sort_text'); ?>",
						filterable: false
					},{
						field:"item_qty", 
						width: 80, 
						title:"<?php echo lang('ord_qty'); ?>",
						filterable: false
					},{
						field:"item_price",
						width: 100, 
						title:"<?php echo lang('ord_price'); ?>",
						filterable: false
					},{
						field:"costelement", 
						width: 200, 
						title:"Cost Element",
						filterable: false,
						editor: function(container, options){
				            $('<input required name="' + options.field + '" style=" font-size: 14px;" />')
				              .appendTo(container)
				              .kendoDropDownList({
				                autoBind: false,
				                dataTextField: "text",
				                dataValueField: "id",
				                dataSource: {
				                  serverFiltering: false,
				                  transport: {
				                    read: {
				                      dataType: "jsonp",
				                      url: "<?php echo site_url('order/get_cmb_cost_element'); ?>",
				                    }
				                  }
				                }
				            });
				        },
					},
				]
			});


	function startChange_selection() {
        var startDate = start.value(),
        endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange_selection() {
        var endDate = end.value(),
        startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var start = $(".startDate_selection").kendoDatePicker({
        change: startChange_selection,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    var end = $(".endDate_selection").kendoDatePicker({
        change: endChange_selection,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    start.max(end.value());
    end.min(start.value());

    function adjustDropDownWidth(e) {
      var listContainer = e.sender.list.closest(".k-list-container");
      //listContainer.width(listContainer.width() + kendo.support.scrollbar());
      listContainer.width('300px');
    }

});

</script>

