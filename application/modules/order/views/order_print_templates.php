<html>
  <head>
    <title>Print SPK</title>
  </head>
  <body>
    <table width="100%" style="border-bottom: 1px solid #000; ">
      <tr>
        <td>
          <div id="logo">
            <img src="<?php echo BASEPATH.'..'.DIRECTORY_SEPARATOR; ?>assets/images/logo.png">
          </div>
        </td>
        <td>
          <div id="company">
            <h2 class="name">PT Adi Sarana Armada, Tbk</h2>
            <div>Jln. Walisongo KM 9 RT 01 RW 02, Kel. Tambak Aji, Kec.Ngaliyan,</div>
            <div>Semarang Barat. Tel: 024 7612333 Fax: 024 7611777</div>
            <div><b><u>SURAT PERINTAH KERJA (SPK)<u></u></div>
          </div>
        </td>
      </tr>
    </table>      
    
    <main>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>NO : <?php echo $order_header[0]->VCH_order_id; ?></h1>
          <b>Rev # <?php echo $order_revision[0]->max_rev; ?> </b>
        </div>
      </div>
      <table  width="100%" border='0px'>
        <tr>
          <td style="width:100px;">Main Work Center</td>
          <td width="220px">: <?php echo $order_header[0]->VCH_workcenter_main; ?></td>
          <td style="text-align:right">Tanggal</td>
          <td>: <?php $dt = explode('-', $order_header[0]->DAT_basic_date_start); echo $dt[2].'-'.$dt[1].'-'.$dt[0]; ?></td>
        </tr>
        <tr>
          <td style="width:100px;">Vendor</td>
          <td>: <?php echo $order_header[0]->VCH_vendor_name; ?></td>
          <td style="text-align:right">Jam</td>
          <td>: <?php $jam= explode(" ", $order_header[0]->last_insert); echo $jam[1]; ?> </td>
        </tr>
        <tr>
          <td style="width:100px;"></td>
          <td> </td>
          <td style="text-align:right">Konsumen</td>
          <td>: <?php echo $order_header[0]->VCH_sold_toparty; ?></td>
        </tr>
        <tr>
          <td style="width:100px;"></td>
          <td> </td>
          <td style="text-align:right">User</td>
          <td>: <?php echo $order_header[0]->VCH_user_create; ?></td>
        </tr>
        <tr>
			<td colspan='2' >
				<u><b>Keluhan Konsumen :</b></u><br>
				<?php echo $order_header[0]->TXT_keluhan; ?><br><br>
			</td>
			<td colspan='2' >
				<u><b>Tindakan :</b></u><br>
				<?php echo $order_header[0]->TXT_description; ?><br><br>
			</td>
        </tr>
        <tr>
          <td colspan='4' >Mohon dilakukan perawatan / perbaikan / pengiriman / penarikan kendaraan :</td>
        </tr>

        <tr>
          <td style="width:100px;">No Polisi</td>
          <td>: <?php echo $order_equpment[0]->VCH_LicensePlatNumber; ?> </td>
          <td style="text-align:right">KM Unit</td>
          <td>: <?php echo $order_header[0]->INT_km; ?> </td>
        </tr>

        <tr>
          <td style="width:100px;">Tipe</td>
          <td>: <?php echo $order_equpment[0]->VCH_Description; ?> </td>
          <td style="text-align:right">Warna</td>
          <td>: <?php echo $order_equpment[0]->VCH_VehicleNo; ?></td>
        </tr>

        <tr>
          <td style="width:100px;">No. Chasis</td>
          <td>: <?php echo $order_equpment[0]->VCH_ChasisNo; ?> </td>
          <td style="text-align:right"></td>
          <td></td>
        </tr>

        <tr>
          <td style="width:100px;">No. mesin</td>
          <td>: <?php echo $order_equpment[0]->VCH_EngineNo; ?> </td>
          <td style="text-align:right"></td>
          <td></td>
        </tr>
        <tr>
          <td colspan='4'>Pekerjaan / Jasa</td>
        </tr>
        <tr>
          <td colspan='4' >
            <table width="100%" style="border-collapse: collapse; ">
              <tr style="text-align: center; border-bottom: 1px solid #000; font-weight: bold; ">
                <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Nomor</td>
                <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;" >Pekerjaan Perawatan / Perbaikan / Pengiriman / Penarikan</td>
                <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;" >Resource</td>
                <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;" >Biaya</td>
              </tr>
              <?php $t_operation=0; foreach($order_operation as $rjasa){ ?>
              <tr >
                <td style="padding:5px;  border-bottom: 1px solid #ddd; ">00<?php echo $rjasa->CHR_operation_id; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; "><?php echo $rjasa->TXT_description; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; "><?php echo $rjasa->VCH_operation_type; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; ">
                  <?php 
                    $biaya = 0;
                    if($rjasa->VCH_operation_type=='Internal')
                      $biaya = 0;
                    else
                      $biaya = $rjasa->BIG_qty * $rjasa->BIG_price;

                    $t_operation =  $t_operation + $biaya;
                    echo number_format($biaya);
                  ?>
                </td>
              </tr>
              <?php } ?>
              <tr >
                <td style="padding:5px;  border-bottom: 1px solid #ddd; " colspan='2'></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold; ">Biaya Jasa</td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold; "><?php echo 'IDR '.number_format($t_operation); ?></td>
              </tr>
            </table>
            <br><br>
          </td>
        </tr>

        <tr>
          <td colspan='4'>Part / Bahan</td>
        </tr>
        <tr>
          <td colspan='4' >
            <table width="100%" style="border-collapse: collapse; ">
              <tr style="text-align: center; border-bottom: 1px solid #000; font-weight: bold; ">
                <td style="background-color: #f2f2f2; padding:5px;  border-bottom: 1px solid #000; font-weight: bold;">Nomor</td>
                <td style="background-color: #f2f2f2; padding:5px; border-bottom: 1px solid #000; font-weight: bold;">Part / Bahan</td>
                <td style="background-color: #f2f2f2; padding:5px; border-bottom: 1px solid #000; font-weight: bold;">Qty</td>
                <td style="background-color: #f2f2f2; padding:5px; border-bottom: 1px solid #000; font-weight: bold;">Harga Satuan</td>
                <td style="background-color: #f2f2f2; padding:5px; border-bottom: 1px solid #000; font-weight: bold;">Biaya</td>
              </tr>
              <?php $t_comp = 0; foreach($order_component as $row){ ?>
              <tr >
                <td style="padding:5px;  border-bottom: 1px solid #ddd;">00<?php echo $row->VHC_component_id; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd;"><?php echo $row->VCH_part_description; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd;"><?php echo $row->BIG_qty.' '.$row->VCH_uom; ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd;"><?php echo number_format($row->BIG_price); ?></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd;"><?php echo number_format($row->BIG_price * $row->BIG_qty); ?></td>
              </tr>
              <?php 
                  $t_comp = $t_comp + ($row->BIG_price * $row->BIG_qty); 
                } 
              ?>

              <tr >
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;" colspan='3'></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;">Biaya Part</td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;">
                  <?php 
                    echo 'IDR '.number_format($t_comp);
                  ?>
                </td>

                <tr >
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;" colspan='3'></td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;">Total Biaya Part & Jasa</td>
                <td style="padding:5px;  border-bottom: 1px solid #ddd; font-weight: bold;">
                  <?php 
                    echo 'IDR '.number_format($t_comp + $t_operation);
                  ?>
                </td>

              </tr>
            </table>
            <br> <br>
          </td>
        </tr>

        <tr>
          <td colspan='3'>Mohon Pekerjaan bisa diselesaikan pada tanggal : <?php $dtl = explode('-', $order_header[0]->DAT_basic_date_end); echo $dtl[2].'-'.$dtl[1].'-'.$dtl[0]; ?></td>
          <td>Jam : <?php echo $jam[1]; ?></td>
        </tr>
        <tr>
          <td colspan='4'>
            <table width="100%">
              <tr>
                <td width="200px">
                  Pembuat Order<br><br><br><br><br> SA / SCO <br> Nama & tanda tangan
                </td>
                <td>
                  Mengetahui dan Menyetujui<br><br><br><br><br> ASH / CSH / Dept. Head / Div. Head <br> Nama & tanda tangan
                </td>
                <td>
                  Yang Mengerjakan <br><br><br><br><br><br> Tanda tangan, Nama Jelas & Stempel
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </main>
  </body>
</html>

<style type="text/css">
  @font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 18cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  /*color: #555555;*/
  color: #000;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: SourceSansPro;
}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
float: left;
 margin-top: 8px;
}

#logo img {
  height: 70px;
}

#company {
  float: right;
  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  border-left: 6px solid #0087C3;
  float: left;
}

#client .to {
  color: #777777;
}

h2.name {
  font-size: 1.4em;
  font-weight: normal;
  margin: 0;
}

#invoice {
  float: right;
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}

#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}  
</style>