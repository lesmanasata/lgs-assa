<?php $timestamp = time();?>
<div align="right">
	<div class="btn-group">
		  <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-upload" aria-hidden="true"></i> Create Order <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">
		  	<?php foreach($order_type as $key=>$object){ ?>
				<li>
					<a href="javascript:void(0);" onclick="$(this).crete_order();" id="<?php echo $object->key; ?>" data-layout="<?php echo $object->layout; ?>" >
						<?php echo $object->desc; ?>
					</a>
				</li>
			<?php } ?>
		  </ul>
	</div>

</div>
<p></p>
<div id="order_selection_table<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {

		var ds_order_selection = new kendo.data.DataSource({
			transport: {
				read: {
					type:"POST",
					dataType: "json",
					data:<?php echo json_encode($filter); ?>,
					url: '<?php echo site_url('order/get_selection_data'); ?>',
				}
			},
			serverPaging: true,
			schema: {
				data: "data",
				total: "total",
			},
			pageSize: 50,
		});

		$("#order_selection_table<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_order_selection,
			scrollable: true,
			pageable: {
                refresh: true,
                pageSizes: true,
            },
			sortable: true,
			resizable: true,
			height: 550,
			dataBinding: function() {
			  no_urut =  (this.dataSource.page() -1) * this.dataSource.pageSize();
			},
			dataBound: function(e) {
				this.collapseRow(this.tbody.find("tr.k-master-row").first());
				var grid = e.sender;
				if (grid.dataSource.total() == 0) {
					var colCount = grid.columns.length;
					$(e.sender.wrapper)
						.find('tbody')
						.append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
				}
			},
			columns: [
				{
					title:"Select", 
					width:60,
					template: '<input type="checkbox" class="order_id" name="order_id[]" value="#:notif_id#" >',

				},
				{field:"notif_id",width: 150, title:"<?php echo ($mode=='sr') ? lang('ord_notif_no'):'Order Number'; ?>",filterable: false},
				{field:"notif_type",width: 100, title:"<?php echo ($mode=='sr') ? 'Type':'Type'; ?>",filterable: false},
				{field:"description",width: 300, title:"<?php echo lang('ord_description'); ?>",filterable: false},
				<?php if($mode=='sr'){ ?>
					{field:"soldtoparty",width: 150, title:"<?php echo lang('ord_sold_toparty'); ?>",filterable: false},
					{field:"soldtoparty_desc",width: 300, title:"<?php echo lang('ord_sold_toparty_desc'); ?>",filterable: false},
				<?php } ?>
				{field:"reported",width: 100, title:"<?php echo lang('ord_reported'); ?>",filterable: false, },
				{field:"equipment",width: 200, title:"<?php echo lang('ord_equipment'); ?>",filterable: false, },
				{field:"functional_loc",width: 150, title:"<?php echo lang('ord_functional_loc'); ?>",filterable: false, },
				{field:"bisnis_area",width: 100, title:"Area",filterable: false, },
				<?php if($mode=='sr'){ ?>
					{field:"voc_number",width: 200, title:"<?php echo lang('ord_voc_number'); ?>",filterable: false, },
					{field:"serca_number",width: 150, title:"<?php echo lang('ord_serca_number'); ?>",filterable: false,},
				<?php } ?>
			]
		});

	});
</script>
