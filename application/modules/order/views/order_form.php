<input type='hidden' name='form_action' value='<?php echo $form_action; ?>' readonly/>
<div class="col-md-6">
  <?php if(isset($res[0]->VCH_order_id) && $res[0]->VCH_order_id !=''){ ?>
    <div class="form-group">
      <label  class="col-sm-4 control-label hor-form">Order Number</label>
      <div class="col-sm-8">
        <input type='text' class="form-control" value="<?php echo $res[0]->VCH_order_id; ?>" readonly>
      </div>
    </div>
  <?php } ?>
	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Order Type</label>
		<div class="col-sm-8">
			<input type='text' class="form-control" value="<?php echo $type_order_cretae; ?>" name='type_order_create' readonly>
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Description</label>
		<div class="col-sm-8">
      <input type="hidden" name="keluhan"  value='<?php $k = ($form_action=="edit")?$header_data->keluhan:$res[0]->VCH_description; echo $k; ?>'>
			<input type="text" class="form-control" name="description"   placeholder="Description" value='<?php echo $res[0]->VCH_description; ?>' maxlength="40">
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Func Location</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" name="func_loc"  id='func_loc_id' placeholder="Functional Location" value='<?php echo $res[0]->VCH_functional_lock; ?>' readonly>
		</div>
	</div>

<?php if($notif_id !='') { ?>
	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Equipment</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" name="equipment" placeholder="Equipment" value='<?php echo $res[0]->VCH_equipment; ?>' readonly>
		</div>
	</div>
<?php }else{ ?>
  <div class="form-group">
    <label  class="col-sm-4 control-label hor-form">Equipment</label>
    <div class="col-sm-8">
      <div class="input-group">
        <input type="text" name="equipment" id="equipment" class="form-control" placeholder="Equipment" value='<?php echo $res[0]->VCH_equipment; ?>' readonly>
        <span class="input-group-btn">
          <button type="button" id="find_equipment" class="find btn btn-primary">Find</button>
        </span>
      </div>
    </div>
  </div>
<?php } ?>


	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Equipment Description</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" name="equipment_desc" placeholder="Equipment Description" value='<?php if(isset($res[1]->VCH_Description)) echo $res[1]->VCH_Description; ?>' readonly>
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">PM Act Type</label>
		<div class="col-sm-8">
			<input type="text" class="form-control act_type" style="width:100%;" id='act_type' name="act_type"  placeholder="&mdash;&mdash;Select Act Type &mdash;&mdash;">
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Main Work Center</label>
		<div class="col-sm-8">
			<input type="text" class="form-control cmb_work_center" id='cmb_work_center' name="main_work_center"  style="width:100%;">
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Plant Work Center</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id='cmb_work_center_plant' name="plan_work_center"  readonly >
		</div>
	</div>

	<div style="display: none;">
		<div class="form-group">
			<label  class="col-sm-4 control-label hor-form">System Status</label>
			<div class="col-sm-8">
				<input type="text" class="kendodropdown form-control" style="width:100%;" name="sys_status"  placeholder="&mdash;&mdash;Select Sys. Status &mdash;&mdash;">
			</div>
		</div>

		<div class="form-group">
			<label  class="col-sm-4 control-label hor-form">User Status</label>
			<div class="col-sm-8">
				<input type="text" class="kendodropdown form-control" style="width:100%;" name="user_status"  placeholder="&mdash;&mdash;Select User Status &mdash;&mdash;">
			</div>
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Basic Start Date</label>
		<div class="col-sm-8">
			<input type="text" class="form-control startDate" id='basic_start' name="basic_start"   value="<?php echo date('d-m-Y'); ?>" placeholder="Basic start date">
		</div>
	</div>

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form">Basic End Date</label> 
		<div class="col-sm-8">
			<input type="text" class="form-control endDate" id ='basic_end' name="basic_end"   placeholder="Basic End Date">
		</div>
	</div>

  <div class="form-group">
    <label  class="col-sm-4 control-label hor-form">KM Unit</label> 
    <div class="col-sm-8">
      <?php 
        $km_unit = 0;
        if($form_action=='edit'){ $km_unit= $header_data->km_unit; }
      ?>
      <input type="number" class="form-control"  min="1" oninput="maxLengthCheck(this)" max="100000" name="km_unit" maxlength = "6" value="<?php echo $km_unit; ?>"   placeholder="KM Unit">
    </div>
  </div>

  <?php 

  if($type_order_cretae =='OR01' || $type_order_cretae =='ZBAK'){ //$type_order_cretae =='ZBAK' ?>
    <div class="form-group">
      <label  class="col-sm-4 control-label hor-form">Cost</label>
      <div class="col-sm-8">
        <input type="number" class="form-control" name="cost" value='<?php echo (isset($res[0]->INT_cost))?$res[0]->INT_cost:""; ?>' placeholder="Cost">
      </div>
    </div>
  <?php } ?>
  <div class="form-group">
    <label  class="col-sm-4 control-label hor-form">Purchase Group</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="purc_group" style="width:100%;" name="purc_group"  >
    </div>
  </div>

  <?php if($type_order_cretae !='ZBAK'){ ?>
    <div class="form-group">
      <label class="control-label col-sm-4">Payment Term:</label>
      <div class="col-sm-8">
          <input type="text" class="form-control kendodropdown"  name="pay_term" style="width:100%;" placeholder="Select...">
      </div>
    </div> 

    <div class="form-group">
      <label  class="col-sm-4 control-label hor-form">Tax Code (PPN)</label> 
      <div class="col-sm-8">
        <input type="text" class="form-control"  id='ppn_type' name="ppn_type" style="width:100%;" placeholder="Select...">
      </div>
    </div>
  <?php } ?>
</div>

<div class="col-md-6">
	<?php 
    $vch_cust = ''; 
    if($type_order_cretae !='OR01'){
      if(isset($res[0]->VCH_order_id) && strpos(strtolower($res[0]->VCH_order_id), 'ord')==false)
        $vch_cust = $res[0]->VCH_sold_toparty;
      else
        $vch_cust = $res[0]->VCH_customer_code;
    }else{
        $vch_cust = $res[0]->VCH_customer;
    }

  ?>

  <?php if($notif_id !='' && $type_order_cretae !='ZBAK'|| isset($res[0]->VCH_order_id) && strpos(strtolower($res[0]->VCH_order_id), 'ord')==false && $type_order_cretae !='ZBAK') { ?>
		<div class="form-group">
			<label  class="col-sm-4 control-label hor-form">Customer Id</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="soldtoparty" value='<?php echo $vch_cust; ?>' placeholder="Customer Id" readonly>
			</div>
		</div>
	<?php }else{ ?> 
    <div class="form-group"> 
      <label  class="col-sm-4 control-label hor-form">Customer Id</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" name="soldtoparty" id="soldtoparty" class="form-control" value='<?php echo $vch_cust; ?>'placeholder="Customer" readonly>
          <span class="input-group-btn">
            <button type="button" id="find_soldtoparty" class="find btn btn-primary">Find</button>
          </span>
        </div>
      </div>
    </div>
  <?php } ?>
    <div class="form-group">
      <label  class="col-sm-4 control-label hor-form">Customer Name</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id='soldtoparty_desc' name="soldtoparty_desc" value='<?php echo $vch_cust; ?>' placeholder="Customer Name" readonly>
      </div>
    </div>
	

	<div class="form-group">
		<label  class="col-sm-4 control-label hor-form"><?php $label_n = ($type_order_cretae=='OR01')?'Order ZBAK Number':'Notif Number'; echo $label_n; ?></label>
		<div class="col-sm-8">
			<input type="text" class="form-control" name="notif_id" placeholder="<?php echo $label_n; ?>" value='<?php echo $res[0]->VCH_notif_id; ?>' readonly>
		</div>
	</div>

  <?php if($type_order_cretae =='OR01' || $type_order_cretae =='ZBAK'){ ?>
    <div class="form-group">
        <label class="control-label col-sm-4">ZBAK Priority:</label>
        <div class="col-sm-8">
            <input type="text" class="kendodropdown form-control" style="width:100%;" name="zbak_priority"  placeholder="&mdash;&mdash;Select Priority &mdash;&mdash;">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">BAK Manual:</label>
        <div class="col-sm-8">
            <input type="text" maxlength="35" class="form-control" id='po_number' name="po_number" value='<?php echo (isset($res[0]->VCH_po))?$res[0]->VCH_po:""; ?>'>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">BAK Date:</label> 
        <div class="col-sm-8">
            <?php 
            $date_po = '';
            if(isset($res[0]->VCH_po_date)){
                $dt_po = explode('-', $res[0]->VCH_po_date); 
                $date_po = $res[0]->VCH_po_date;
                if(count($dt_po)>2){
                  $date_po = $dt_po[2].'-'.$dt_po[1].'-'.$dt_po[0];
                }
              }
            ?>
            <input type="text" class="form-control myDate"  id='po_date' name="po_date" value='<?php echo $date_po; ?>'>
        </div>
    </div>

    <div class="form-group">
      <label  class="col-sm-4 control-label hor-form">System Condition</label>
      <div class="col-sm-8">
        <input type="text" class="kendodropdown form-control" style="width:100%;" name="sys_cond"  placeholder="&mdash;&mdash;Select System Cond &mdash;&mdash;">
      </div>
    </div>

  <?php } ?>

  <?php if($type_order_cretae !='ZBAK'){ ?>
  	<div class="form-group">
  		<label  class="col-sm-4 control-label hor-form">Vendor</label>
  		<div class="col-sm-8">
        <!-- <input type="text" class="kendodropdown form-control" style="width:100%;" name="vendor"  placeholder="&mdash;&mdash;Select Vendor &mdash;&mdash;"> -->
  			<input type="text" class="form-control" style="width:100%;" name="vendor"  id="cmb_vendor" placeholder="&mdash;&mdash;Select Vendor &mdash;&mdash;">
  		</div>
  	</div>

  	<div class="form-group">
        <label class="control-label col-sm-4">Name:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="nama" name="nama" >
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Kota:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="kota" name="kota"  >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Telpon:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="telp" name="telp" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">NPWP:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="npwp" name="npwp" >
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Alamat:</label>
        <div class="col-sm-8">
            <textarea name="alamat" class="form-control" id="alamat" ></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Kode Pos:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="kd_pos" name="kd_pos" >
        </div>
    </div>
  <?php } ?>
</div>

<script>
  function maxLengthCheck(object)
  {
    if (object.value.length > object.maxLength)
      object.value = object.value.slice(0, object.maxLength)
  }
	$(document).ready(function(){
		<?php echo $js_cmb; ?> 

    var cmbVendor = $('#cmb_vendor')
          .kendoDropDownList({
            autoBind: false,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('app/get_cmb_vendor'); ?>",
                }
              }
            }
        });
    


    cmb['user_status'].value('-1');

    <?php if($type_order_cretae !='ZBAK'){ ?>
      cmb['pay_term'].value('-1');

      var dropdownlist_vendor = $("#cmb_vendor").data("kendoDropDownList");
		  dropdownlist_vendor.value('-1');

    <?php } ?>

    <?php if($type_order_cretae =='OR01' || $type_order_cretae =='ZBAK'){ ?>
      cmb['sys_cond'].value('-1');

      //var priority_x = ('<?php //echo ; ?>'=='')?'-1':'<?php //echo $res[0]->VCH_priority; ?>';
      var priority_x = "<?php echo (isset($res[0]->VCH_priority) && $res[0]->VCH_priority !='')?$res[0]->VCH_priority:'-1';  ?>";
      cmb['zbak_priority'].value(priority_x);
    <?php } ?>
		cmb['sys_status'].value('CRTD');
		cmb['sys_status'].readonly(true);

    <?php if($type_order_cretae !='ZBAK'){ ?>
  		cmbVendor.bind('change',function(){
  			var vendorId = dropdownlist_vendor.value();
  			$.ajax({
  				url:"<?php echo site_url('order/get_vendor'); ?>",
  				type:'POST',
  				data:'vendor_id='+vendorId,
  				dataType:'json',
  				tail:1,
  				success:function(res){
  					$('#nama').val(res.data[0].VCH_Name);
  					$('#kota').val(res.data[0].VCH_City);
  					$('#telp').val(res.data[0].VCH_Telephone);
  					$('#npwp').val(res.data[0].VCH_Stceg);
  					$('#alamat').val(res.data[0].VCH_Address);
  					$('#kd_pos').val(res.data[0].CHR_PostCode);
  				}
  			});
  		});
    <?php } ?>

    $.post('<?php echo site_url('order/get_cust'); ?>',{id:'<?php echo $vch_cust; ?>'},function(res){
      $('#soldtoparty_desc').val(res);
    });


    <?php if($type_order_cretae !='ZBAK'){ ?>
      var cmb_ppn_type = $('#ppn_type').kendoDropDownList({
            autoBind: true,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            optionLabel:"Select Tax Code (PPN)",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                    dataType: "jsonp",
                    url: "<?php echo site_url('app/get_cmb_ppntype'); ?>",
                }
              }
            }
        });
    <?php } ?>
    var cmb_purc_group = $('#purc_group').kendoDropDownList({
          autoBind: true,
          filter: "contains",
          dataTextField: "text",
          dataValueField: "id",
          optionLabel:"Purchase Group",
          dataSource: {
            serverFiltering: true,
            transport: {
              read: {
                  dataType: "jsonp",
                  url: "<?php echo site_url('order/get_cmb_purc_group'); ?>",
              }
            }
          }
      });

		var cmb_act_type = $('#act_type').kendoDropDownList({
            autoBind: true,
            //filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            optionLabel:"Select Act Type",
            dataSource: {
              serverFiltering: true,
              transport: {
                read: {
                	   data:{'mod_proses':'<?php echo $type_order_cretae; ?>'},
                  	dataType: "jsonp",
                  	url: "<?php echo site_url('order/get_cmb_act_type'); ?>",
                }
              }
            }
        }).bind('change',function(){
          var cmbAct = $('#act_type').data('kendoDropDownList');
          var orderType = $('input[name=type_order_create]').val();
          var curval = cmbAct.value();
          var lcek   = ['509','510','513'];
          var igrid = $('#order_grid_componet_item').data("kendoGrid");
          igrid.hideColumn("costelement");
          if(lcek.indexOf(curval) >= 0 && orderType=='WO05'){
            igrid.showColumn("costelement");
          }
        });

        var func_loc_d=$('input[name=func_loc]').val();
        var ar_plant_func = func_loc_d.split('-');
        var ds_cmb_work_center = {
              serverFiltering: true,
              transport: {
                read: {
                  data:{'plan_id':ar_plant_func[1]},
                  dataType: "jsonp",
                  url: "<?php echo site_url('order/get_cmb_work_center'); ?>",
                }
              }
            }

        $('#cmb_work_center').kendoDropDownList({
            autoBind: true,
            filter: "contains",
            dataTextField: "text",
            dataValueField: "id",
            optionLabel:"Select Work Center",
            dataSource: ds_cmb_work_center,
            change:function(e){
            	var work_center_selected = this.value();
              var plant_arr = work_center_selected.split('-');

              $('#cmb_work_center_plant').val(plant_arr[1]);
  
            }
        });

        $.fn.selectItem = function (id){
          var record = id.split('-');
          var splid  = record[0].split('_');
          $('input[name='+splid[1]+']').val(record[1]);
          $('input[name='+splid[1]+'_desc]').val(record[2]);

          if(splid[1]=='equipment'){
            $('input[name=func_loc]').prop('readonly', true);
              $.post('<?php echo site_url('order/get_equip'); ?>',{id:record[1]},function(res){
                $('input[name=func_loc]').val(res);
                var ar_func_lo_plan = res.split('-');
                var ds_cmb_work_center2 = {
                    serverFiltering: true,
                    transport: {
                      read: {
                        data:{'plan_id':ar_func_lo_plan[1]},
                        dataType: "jsonp",
                        url: "<?php echo site_url('order/get_cmb_work_center'); ?>",
                      }
                    }
                  }

                $("#cmb_work_center").data("kendoDropDownList").setDataSource(new kendo.data.DataSource(ds_cmb_work_center2));
              });
          }
          kendoWindow.data("kendoWindow").close();
        }

    <?php if($form_action=='edit'){ ?>
      cmb['user_status'].value('-1');
      <?php if($type_order_cretae =='OR01' || $type_order_cretae=='ZBAK'){ ?>
        cmb['sys_cond'].value('<?php echo $header_data->system_cond; ?>');
      <?php } ?>

      <?php //if($type_order_cretae !='ZBAK'){ ?>
      <?php //} ?>
	  var drChangeVendor = $("#cmb_vendor").data("kendoDropDownList");
        drChangeVendor.value('<?php echo ($header_data->vendor_id!='')?$header_data->vendor_id:'-1'; ?>');

      cmb['sys_status'].value('CRTD');
      cmb['sys_status'].readonly(true);

      $('#nama').val('<?php echo $header_data->vendor_name; ?>');
      $('#kota').val('<?php echo $header_data->vendor_kota; ?>');
      $('#telp').val('<?php echo $header_data->vendor_telepon; ?>');
      $('#npwp').val('<?php echo $header_data->vendor_npwp; ?>');
      $('#alamat').val('<?php echo $header_data->vendor_alamat; ?>');
      $('#kd_pos').val('<?php echo $header_data->vendor_kode_pos; ?>');  
      $('#cmb_work_center_plant').val('<?php echo $header_data->plant_work_center; ?>');
      $('#basic_start').val('<?php echo $header_data->basic_start_date; ?>');
      $('#basic_end').val('<?php echo $header_data->basic_end_date; ?>');

      $("#cmb_work_center").data('kendoDropDownList').value("<?php echo $header_data->main_work_center; ?>");
      $("#act_type").data('kendoDropDownList').value("<?php echo $header_data->pm_act_type; ?>");
      
      <?php if($type_order_cretae !='ZBAK'){ ?>
        cmb['pay_term'].value('<?php echo ($res[0]->VCH_payment_terms=='0')?'-1':$res[0]->VCH_payment_terms; ?>');
      <?php } ?>

      <?php if($type_order_cretae !='ZBAK'){ ?>
        <?php if($res[0]->VCH_ppntype !='0' ){ ?>
          $("#ppn_type").data('kendoDropDownList').value("<?php echo $res[0]->VCH_ppntype; ?>");
        <?php } ?>
      <?php } ?>
          $("#purc_group").data('kendoDropDownList').value("<?php echo $res[0]->VCH_purch_group; ?>");
      
    <?php } ?>


	function startChange() {
        var startDate = start.value(),
        endDate = end.value();

        if (startDate) {
            startDate = new Date(startDate);
            startDate.setDate(startDate.getDate());
            end.min(startDate);
        } else if (endDate) {
            start.max(new Date(endDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    function endChange() {
        var endDate = end.value(),
        startDate = start.value();

        if (endDate) {
            endDate = new Date(endDate);
            endDate.setDate(endDate.getDate());
            start.max(endDate);
        } else if (startDate) {
            end.min(new Date(startDate));
        } else {
            endDate = new Date();
            start.max(endDate);
            end.min(endDate);
        }
    }

    var start = $(".startDate").kendoDatePicker({
        change: startChange,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    var end = $(".endDate").kendoDatePicker({
        change: endChange,
        format: "dd-MM-yyyy",
    }).data("kendoDatePicker");

    start.max(end.value());
    end.min(start.value());
		

    $(".myDate").kendoDatePicker({
      format: "dd-MM-yyyy"
    });

    <?php if(strpos(strtolower($this->input->post('modeid')), 'ord') === false && $this->input->post('modeid') != false){ ?> 
      $('input[name=func_loc]').attr('readonly', true);
      $('#find_equipment').prop('disabled', true);
    <?php } ?>

    $('.find').click(function(){
       kendoWindow.data("kendoWindow")
        .content($("#dialog_templates").html())
        .center().open();

      var sendItem = {'id':$(this).attr('id')};
      var curId    = $(this).attr('id');
      var ds_find = new kendo.data.DataSource({
        transport: {
          read: {
            type:"POST",
            dataType: "json",
            data:sendItem,
            url: '<?php echo site_url('order/get_data_brouse'); ?>',
          }
        },
        schema: {
          data: "data",
          total: "total"
        },
        pageSize: 10,
        serverFiltering: true,
        serverSorting: true,
        serverPaging: true
      });

      kendoWindow.find('.myGrid').kendoGrid({
        dataSource: ds_find,
        filterable: true,
        sortable: true,
        pageable: true,
        height: "400px",
        scrollable: false,
        filterable: {
          extra: false,
          operators: {
            string: {
              contains: "Like",
              eq: "=",
              neq: "!="
            },
          }
        },
        dataBound: function(e) {
          this.collapseRow(this.tbody.find("tr.k-master-row").first());
          var grid = e.sender;
          if (grid.dataSource.total() == 0) {
            var colCount = grid.columns.length;
            $(e.sender.wrapper)
              .find('tbody')
              .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align:center">&mdash;&mdash; <?php echo lang('nfc_blank_table_row'); ?> &mdash;&mdash;</td></tr>');
          }
        },
        columns: [
          {
            title:"<?php echo lang('label_action'); ?>",
            width:20,
            template: '<button  onClick="$(this).selectItem(this.id);" id="'+curId+'-#:id#-#:text#" class="pilih btn btn-xs btn-primary"><?php echo lang('btn_select'); ?></button>',
          },
          {field:"id",title:"<?php echo lang('lbl_code'); ?>"},
          {field:'text',title:"<?php echo lang('lbl_text'); ?>"},
        ]
      });
    });

    kendoWindow = $("<div />").kendoWindow({
      title: "<?php echo lang('nfc_dialog'); ?>",
      width: "500px",
      height: "450px",
      resizable: false,
      modal: true,
    });


    


	});

    
</script>

<script id="dialog_templates" type="text/x-kendo-template">
  <div class="myGrid"></div>
</script>