<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Plan extends MY_Controller
{   public $tbl_plan_trx ="TF_MAINTENANCE_PLAN";
    public $plan_initial ="PLN";

	public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    function index(){
        $this->template->build('order/order_maintenance_plan');
    }

    function get_selection(){
    	$data['filter']= array(
    		'mode'=> $this->input->post('filter_type'),
    		'plan_no'=> $this->input->post('plan_number'),
    		'date_start'=> $this->input->post('date_start'),
            'date_end'=> $this->input->post('date_end'),
            'tnopol'=> $this->input->post('tnopol'),
            'trangka'=> $this->input->post('trangka'),
            'tequpment'=> $this->input->post('tequpment')
    	);
    	$this->load->view('order/maintenance_plan_tbl',$data);
    }

    function get_selection_data(){
    	$this->load->model('order/plan_model','mod_plan');

    	$mod = $this->input->post('mode');
    	$plan_no = $this->input->post('plan_no');
        $equipment = $this->input->post('tequpment');
        $tnopol = $this->input->post('tnopol');
        $trangka = $this->input->post('trangka');
    	$date_start = date_create($this->input->post('date_start'));
    	$date_end = date_create($this->input->post('date_end'));
    	$date_fx_start = date_format($date_start,'Y-m-d');
    	$date_fx_end = date_format($date_end,'Y-m-d');
    	$filter = array();

    	if($mod=='bydate')
    		$filter['mp.DAT_Created BETWEEN "'.$date_fx_start.'" AND "'.$date_fx_end.'"']=NULL;
    	elseif($mod=='byid')
    		$filter['mp.VCH_Plan_Number']=$plan_no;
        elseif($mod=='equipment')
            $filter['mp.VCH_Equipment']=$equipment;
        elseif($mod=='bynopol')
            $filter['e.VCH_LicensePlatNumber']=$tnopol;
        elseif($mod=='byrangka')
            $filter['e.VCH_ChasisNo']=$trangka;
    	
        $filter['mp.VCH_Is_Used'] = '0';

    	$res = $this->mod_plan->get_plan_data_join($filter);
        //$res = $this->mod_plan->get_plan_data($filter);
    	echo json_encode( array('data' => $res ));
    }

    function get_plan(){
        $this->load->model('order/plan_model','mod_plan');
        $plan_id = $this->input->post('id');
        $act     = 0;

        $filter['VCH_Plan_Number'] = $plan_id;
        // $res_header = $this->mod_plan->get_plan_data($filter);
        $res_header = $this->mod_plan->get_plan_data_join($filter);
        $res_item   = $this->mod_plan->get_detail($filter);

        if(count($res_item)> 0){
            $act = 1;
            $a   = 0;
            foreach($res_item as $value){
                $x = strpos(strtolower($value->VCH_Scaduling), 'hold');
                if($x == false){
                    $a = $a+1;
                }
            }

            if($a == count($res_item))
                $act= 2;
        }

        echo json_encode(array('header' =>$res_header, 'detail'=>$res_item,'act'=>$act));
    }

    function submit(){
        $this->load->model('order/plan_model','mod_plan');

        $out['status']  = FALSE;
        $out['messages']= '';
        $plan_number    = $this->input->post('plan_number'); 
        $plan_item      = $this->input->post('item_id');
        $plan_id        = $this->app->getAutoId('VCH_Plan_id', $this->tbl_plan_trx,$this->plan_initial);
        $user_create    = $this->session->userdata('user');
        $action         = $this->input->post('mode');
        $counter_read   = $this->input->post('t_counter');
        $s_counter      = $this->input->post('s_counter');
        $s_param        = $this->input->post('indicator_c');
        $s_date         = date_create($this->input->post('s_date'));

        $action_group   = array('start','restart');
        $data = array(
            'VCH_Plan_Number'=>$plan_number,
            'VCH_Plan_Item'=>$plan_item,
            'VCH_Plan_id'=>$plan_id,
            'VCH_User_Create'=>$user_create,
            'VCH_Action'=>$action,
            'flag'=>0,
            'last_insert'=>date('Y-m-d H:i:s')
        );

        if(in_array($action, $action_group)){
            if($s_param == '3')
                $data['INT_count_reading'] = $s_counter;
            else
                $data['DAT_start_date'] = date_format($s_date,'Y-m-d');
        }

        do{


            if(in_array($action, $action_group)){
                if($s_param == '3' && $s_counter > $counter_read){
                    $out['messages']= 'Max Start Of Cycle adalah '.number_format($counter_read);
                    break;
                }
                
            }

            if($plan_item =='' && !in_array($action, $action_group) ){
                $out['messages']= 'Please Select One Item';
                break;
            }

            $filter_plan['VCH_Plan_Number'] = $plan_number;
            $filter_plan['VCH_Is_Used']     = 1;
            $res_filter = $this->mod_plan->get_plan_data($filter_plan);
            if(count($res_filter) > 0){
                $out['messages']= 'Plant '.$plan_number.' has already been processed';
                break;
            }
            
            /*
            $filter['VCH_Plan_Number'] = $plan_number;
            $filter['VCH_Scaduling LIKE "%hold%"'] = NULL;
            $res_item   = $this->mod_plan->get_detail($filter);

            if(count($res_item)>0 && $action =='restart'){
                $out['messages']= 'Make sure all item Scheduled is nothing hold';
                break;
            }*/

            if($action=='release'){
                $whereFilter['VCH_Plan_Number'] = $plan_number;
                $whereFilter['VCH_Call_No']      = $plan_item;

                $res_item2  = $this->mod_plan->get_detail($whereFilter);
                $x = strpos(strtolower($res_item2[0]->VCH_Scaduling), 'hold');
                //if($res_item2[0]->VCH_Scaduling=='Scheduled Complete'){
                if($x == false){
                    $out['messages']= 'Curent Scheduled  is Complete, Called, Skipped';
                    break;
                }

                $res_clast = $this->mod_plan->cek_last_record($plan_number.$plan_item);
                
                if(count($res_clast) > 0){
                    //if($res_clast[0]->VCH_Scaduling !='Scheduled Complete'){
                    if(strpos(strtolower($res_clast[0]->VCH_Scaduling), 'hold') !=false && $plan_number == $res_clast[0]->VCH_Plan_Number){
                        $out['messages']= 'The last Schadule should be Complete';
                        break;
                    }
                }
            }   

           
            $res = $this->mod_plan->add($data);
            if($res){
                $out['status']  = $res;
                $out['messages']= lang('msg_success_save_record').' <b>id :'.$plan_id.'</b>';
                $this->mod_plan->update_plan_data(
                    array('VCH_Is_Used'=>'1'),
                    array('VCH_Plan_Number'=>$plan_number)
                );
            }

        }while(FALSE);
        echo json_encode($out);
    }

}