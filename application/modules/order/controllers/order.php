<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Order extends MY_Controller
{
    var $tbl_activity_type          = 'MF_ACTIVITY_TYPE';
    var $tbl_vendor                 = 'MF_VENDOR';
    var $op_external_type           = "PM02";
    var $type_order_layout          = array('WO02','WO03','WO05','ZBAK');
    var $tbl_order_hider            = 'TF_ORDER_HEADER';
    var $order_initial              = 'ORD';
    var $order_zbak                 = 'OR01';
    var $tbl_order_operation        = 'TF_ORDER_OPERATION';
    var $tbl_order_operation_item   = 'TF_ORDER_OPERATION_ITEM';
    var $tbl_order_componets        = 'TF_ORDER_COMPONENTS'; 
    var $tbl_payment                = "MF_PAYMENT_TERMS";
    var $tbl_equipment              = "MF_EQUIPMENT";
    var $tbl_customer               = "MF_CUSTOMER"; 

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
        $this->cfg_order = json_decode(json_encode($this->config->item('order')));
    }

    public function index()
    {
        $type_order = array('sr','zbak');
        if(!in_array($this->uri->segment(3), $type_order))
            redirect('order/index/sr');

        $data['order_type'] = $this->uri->segment(3);
        $data['modid']   ='';
        if($this->uri->segment(4) !=''){
            $this->load->model('order/order_model','mod_order');
            $this->load->library('encrypt');

            $kategori   = array();
            $cfg_order  = json_decode(json_encode($this->config->item('order')));
            $kategori   = ($this->uri->segment(3)=='sr')? $cfg_order->order_type_sr:$cfg_order->order_type_zbak;
            $order_has  = $this->uri->segment(4);
            $order_surl = trim($this->url_back($order_has));
            $order_id   = $this->encrypt->decrypt($order_surl); 

            $fild='VCH_notif_id, VCH_order_create_type';
            $filter['VCH_order_id'] = $order_id;
            $res = $this->mod_order->get_order_spk_data($fild,$filter);
            
            $data['notif_id']           = $res[0]->VCH_notif_id;
            $data['order_crete_type']   = $res[0]->VCH_order_create_type;
            $data['type']               = '';
            $data['modid']              = $order_id;

            foreach ($kategori as $key => $value) {
               if($value->key ==$res[0]->VCH_order_create_type){
                    $data['type'] = $kategori[$key]->layout;
               }
            }
        }

        $this->template->build('order/order_view', $data);
    }

    function list(){
        $this->template->build('order/order_list');
    }

    public function cmb_payment()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_Type as id, CONCAT(CHR_Type,' - (',INT_Terms,' days)') as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_payment);

        echo json_encode(array('data'=>$res));
    }

    function order_submit(){
        $this->load->model('order/order_model','mod_order');
        $out['status']          = false;
        $out['messages']        = '';
        $header_data            = json_decode(json_encode($this->input->post('header')));
        $operation_data         = json_decode(json_encode($this->input->post('models')));
        $operation_data2        = json_decode(json_encode($this->input->post('models')));
        $operation_item_data    = json_decode(json_encode($this->input->post('itemoperation')));
        $components_data2        = json_decode(json_encode($this->input->post('components')));
        $components_data        = json_decode(json_encode($this->input->post('components')));
        
        $order_id = $this->app->getAutoId('VCH_order_id', $this->tbl_order_hider,$this->order_initial);
        $ar_start_date = explode("-", $header_data->basic_start_date);
        $ar_end_date = explode("-", $header_data->basic_end_date);
        $work_center = explode('-', $header_data->main_work_center);
        $log_data   = array(); 

        $header_add = array(
            'VCH_order_id'              => $order_id,
            'VCH_order_create_type'     => $header_data->type_order_create,
            'TXT_description'           => substr($header_data->description, 0,40),
            'VCH_functional_loc'        => $header_data->functional_location,
            'VCH_equipment_id'          => $header_data->equipment,
            'VCH_equpment_description'  => $header_data->equipment_desc,
            'VCH_pm_act_type'           => $header_data->pm_act,
            'VCH_workcenter_main'       => $work_center[0],
            'VCH_workcenter_plan'       => $header_data->plan_work_center,
            'VCH_system_status'         => $header_data->system_status,
            'VCH_user_status'           => $header_data->user_status,
            'DAT_basic_date_start'      => date('Y-m-d'),
            'DAT_basic_date_end'        => date('Y-m-d'),
            'VCH_notif_id'              => $header_data->notif,
            'VCH_vendor_id'             => (isset($header_data->vendor_id))?$header_data->vendor_id:'',
            'VCH_vendor_name'           => (isset($header_data->vendor_name))?$header_data->vendor_name:'',
            'VCH_vendor_kota'           => (isset($header_data->vendor_kota))?$header_data->vendor_kota:'',
            'VCH_vendor_telpon'         => (isset($header_data->vendor_tlp))?$header_data->vendor_tlp:'',
            'VCH_vendor_npwp'           => (isset($header_data->vendor_npwp))?$header_data->vendor_npwp:'',
            'VCH_vendor_addres'         => (isset($header_data->vendor_addres))?$header_data->vendor_addres:'',
            'VCH_vendor_post_code'      => (isset($header_data->vendor_pos))?$header_data->vendor_pos:'',
            'VCH_sold_toparty'          => $header_data->sold_toparty,
            'TXT_keluhan'               => $header_data->keluhan,
            'INT_cost'                  => (isset($header_data->cost))?$header_data->cost:0,
            'INT_km'                    => $header_data->km_unit+0,
            'VCH_ppntype'               => (isset($header_data->ppn_type))?$header_data->ppn_type:'',
            'VCH_payment_terms'         => (isset($header_data->payment_terms))?$header_data->payment_terms:'',
            'VCH_purch_group'           => $header_data->purc_group,
            'flag'                      => '0',
            'VCH_user_create'           => $this->session->userdata('user'),
            'last_insert'               => date('Y-m-d H:i:s'),
            'last_update'               => date('Y-m-d H:i:s')
        ); 

        if($header_data->type_order_create=='OR01' || $header_data->type_order_create=='ZBAK'){
            $header_add['VCH_zbak_priority']    = $header_data->zbak_priority;
            $header_add['VCH_po_number']        = $header_data->po_number;

            $Mdate=date_create($header_data->po_date);
            $date_new = date_format($Mdate,"Y-m-d");

            $header_add['VCH_po_date']          = $date_new;
            $header_add['VCH_sys_cond']         = $header_data->sys_cond;
        }

        do{

            if($header_data->type_order_create=='OR01' || $header_data->type_order_create=='ZBAK'){
                if($header_data->zbak_priority =='-1'){
                    $out['messages']    = 'Pleaase Select One of ZBAK Priority';
                    break;
                }
            }
			
			if($header_data->type_order_create=='WO05' || $header_data->type_order_create=='WO01' || $header_data->type_order_create=='OR01')
			{
				if($header_data->description==''){
					$out['messages']    = 'Pleaase insert description';
                    break;
				}
				
				if($header_data->purc_group==' ' || $header_data->purc_group=='' || $header_data->purc_group=='-1'){
					$out['messages']    = 'Pleaase select one of Purchase Group';
                    break;
				}
				
				if($header_data->payment_terms=='-1' || $header_data->payment_terms==''){
					$out['messages']    = 'Pleaase select one of Payment Term';
                    break;
				}
				
				if($header_data->ppn_type=='-1' || $header_data->ppn_type==''){
					$out['messages']    = 'Pleaase select one of Tax Code (PPN)';
                    break;
				}
				
				if(strlen($header_data->vendor_pos) >5){
					$out['messages']    = 'invalid Kode Pos, max 5 digit';
                    break;
				}
			}
			
			if($header_data->km_unit + 0 <=0){
                $out['messages']    = 'Pleas Insert KM Unit ( 1 s/d 100000)';
                break;
            }

            if($header_data->km_unit + 0 > 100000){
                $out['messages']    = 'Pleas Insert KM Unit ( 1 s/d 100000)';
                break;
            }
			
			if($header_data->type_order_create=='ZBAK' && $header_data->purc_group==''){
				$out['messages']    = 'Pleas select one of Purchase Group';
                break; 
			}
			
			if($header_data->type_order_create=='ZBAK' && $header_data->po_number==''){
				$out['messages']    = 'Pleas insert BAK Manual';
                break;
			}
			
			if($header_data->type_order_create=='ZBAK' && $header_data->po_date==''){
				$out['messages']    = 'Pleas insert BAK Date';
                break;
			}
			
			if($header_data->type_order_create=='ZBAK' && $header_data->cost + 0 <=0){
				$out['messages']    = 'Pleas Insert Cost (>0)';
                break;
			}

            if($header_data->type_order_create=='OR01' && $header_data->cost + 0 <=0){
                $out['messages']    = 'Pleas Insert Cost (>0)';
                break;
            }
			
			if($header_data->type_order_create=='ZBAK' && $header_data->sys_cond=='-1'){
				$out['messages']    = 'Pleas select one of System Condition';
                break;
			}
			
			if($header_data->type_order_create=='ZBAK' && $header_data->sold_toparty=='-1'){
				$out['messages']    = 'Pleas select one of Customer';
                break;
			}
			
			if($header_data->type_order_create=='ZBAK' && $header_data->description==''){
				$out['messages']    = 'Pleas insert Description';
                break;
			}
			
            if($header_data->pm_act =='' || $header_data->pm_act =='-1'){
                $out['messages']    = 'Pleaase Select One of PM Act Type';
                break;
            }

            if($header_data->main_work_center ==''){
                $out['messages']    = 'Pleaase Select One of Main Work Center';
                break;
            }

            if($header_data->plan_work_center ==''){
                $out['messages']    = 'Please insert plant work center';
                break;
            }

            /*if($header_data->user_status =='-1'){
                $out['messages']    = 'Pleaase Select One of User Status';
                break;
            }*/

            if($header_data->type_order_create =='WO05' && $header_data->equipment==''){
                $out['messages']    = 'Pleaase Select Equipment';
                break;
            }

            if($header_data->basic_start_date =='' || count($ar_start_date) < 2){
                $out['messages']    = 'Pleaase add Basic Start Date';
                break;
            }

            if($header_data->basic_end_date =='' || count($ar_end_date) < 2){
                $out['messages']    = 'Please add Basic End Date';
                break;
            }

            if(isset($header_data->vendor_id) && $header_data->vendor_id =='-1' && $header_data->type_order_create !='ZBAK'){
                $out['messages']    = 'Pleaase Select One of Vendor';
                break;
            }

            $header_add['DAT_basic_date_start'] = ''.$ar_start_date[2].'-'.$ar_start_date[1].'-'.$ar_start_date[0];
            $header_add['DAT_basic_date_end']   = $ar_end_date[2].'-'.$ar_end_date[1].'-'.$ar_end_date[0];

            

            $cek_external_op_numb = array();
            $cek_operation_type = false;
            $op_number = 0;
            $num_cek_componets = 0;

            $cek_cmp_pnm = false;
            $cek_cmp_qty = false;
            $cek_cmp_typ = false;
            $cek_cmp_prc = false;
            foreach($operation_data as $key=>$row_ro){

                if($row_ro->operation_number==''){
                    $op_number++;
                    $operation_data[$key]->operation_number = $oplast + ($op_number*10);
                }
                else
                    $oplast = $row_ro->operation_number;

                if($row_ro->order_type==$this->op_external_type){
                    $cek_operation_type = true;
                    $cek_external_op_numb[] = $operation_data[$key]->operation_number;
                }
                //$op_number = $op_number + 10;

                $cm_plus = false;
                $cmp_number = 10;

                if($components_data !=false && count($components_data) > 0){
                    foreach ($components_data as  $cmp_key => $cmp_val) {
                        
                        if($cmp_val->part_number=='')
                            $cek_cmp_pnm = true;

                        if($cmp_val->qty=='' || $cmp_val->qty==0)
                            $cek_cmp_qty = true;

                        if($cmp_val->componet_type=='')
                            $cek_cmp_typ = true;
                        
                        if($cmp_val->order_prce=='' || $cmp_val->order_prce==0)
                            $cek_cmp_prc = true;

                        $components_data[$cmp_key]->comp_number = $cmp_number;
                        if($operation_data[$key]->operation_number == $cmp_val->opration_number)
                            $cm_plus = true;
                        $cmp_number = $cmp_number +10;
                    }
                }
                if($cm_plus)
                    $num_cek_componets++;
            }

            if($cek_cmp_pnm){
                $out['messages']    = '<b>Part Number</b> on Conponen must be filled';
                break;
            }

            if($cek_cmp_qty){
                $out['messages']    = '<b>Qty</b> on Conponen must be filled (>0)';
                break;
            }

            if($cek_cmp_typ){
                $out['messages']    = '<b>Type</b> on Conponen must be filled';
                break;
            }

            if($cek_cmp_prc){
                $out['messages']    = '<b>Price</b> on Conponen must be filled (>0)';
                break;
            }

            if($cek_operation_type && !$operation_item_data){
                $out['messages']    = 'External Operation should be have Item Operation Data';
                break;
            }

            if($cek_operation_type && isset($header_data->ppn_type) && $header_data->ppn_type=='' && $header_data->type_order_create !='ZBAK'){
                $out['messages']    = 'Please select one of Tax Code (PPN)';
                break;
            }

            if($cek_operation_type && $header_data->purc_group==''){
                $out['messages']    = 'Please select one of Purchase Group';
                break;
            }

            if($cek_operation_type && isset($header_data->payment_terms) && $header_data->payment_terms=='-1' && $header_data->type_order_create !='ZBAK'){
                $out['messages']    = 'Please select one of Payment Term';
                break;
            }

            $num_cek_item_op = 0;
            $cek_itemop_act = false;
            $cek_itemop_qty = false;
            $cek_itemop_prc = false;
            if(count($cek_external_op_numb)>0){
                foreach ($cek_external_op_numb as  $value_ex_op) {
                    $c_plus = false;
                    foreach ($operation_item_data as  $value_item) {
                        if($value_ex_op == $value_item->opration_number)
                             $c_plus = true;

                        if($value_item->sort_text=='')
                            $cek_itemop_act = true;

                        if($value_item->item_qty==0 || $value_item->item_qty=='')
                            $cek_itemop_qty = true;

                        if($value_item->item_price==0 || $value_item->item_price=='')
                            $cek_itemop_prc = true;
                    }
                    if($c_plus)
                        $num_cek_item_op++;
                }

                /*if( $num_cek_componets < count($operation_data) ){
                    $out['messages']    = 'Operation should be have a item Components';
                    break;
                }*/
            }

            if($cek_itemop_act){
                $out['messages']    = '<b>Activity</b> on Item Operation must be filled';
                break;
            }

            if($cek_itemop_qty){
                $out['messages']    = '<b>Qty</b> on Item Operation must be filled (>0)';
                break;
            }

            if($cek_itemop_prc){
                $out['messages']    = '<b>Price</b> on Item Operation must be filled (>0)';
                break;
            }


            if( $num_cek_item_op < count($cek_external_op_numb) ){
                $out['messages']    = $this->op_external_type.'-External Operation, should be have a item Operation';
                break;
            }

            if($header_data->form_action=='add' && $header_data->type_order_create !='WO03'){
                $cek_data = $this->mod_order->get_order_header(
                                'VCH_notif_id',
                                array('VCH_notif_id'=> $header_data->notif )
                            );
                if(count($cek_data)>0 && $cek_data[0]->VCH_notif_id !=''){
                    $out['messages']    = 'Notif id '.$header_data->notif.' is already used';
                    break; 
                }
            }

            if($header_data->form_action=='edit'){
                //unset($header_add['TXT_keluhan']);
                $cek_data = $this->mod_order->get_order_header(
                                'VCH_order_id',
                                array(
                                    'VCH_order_id'=> $header_data->order_id,
                                    'staus'=> '1'
                                )
                            );
                if(count($cek_data)>0 ){
                    $out['messages']    = 'Order id '.$header_data->order_id.' is already Approved';
                    break; 
                }
            }

           /* if(in_array($header_data->type_order_create, $this->type_order_layout)){
                $header_add['VCH_sold_toparty']     = $header_data->sold_toparty;
            }*/

            $res_hider = FALSE;
            if($header_data->form_action=='edit'){  
                //unset($header_add['VCH_order_id']); 
                unset($header_add['last_insert']); 
                unset($header_add['flag']);
                $header_add['VCH_order_id'] = $header_data->order_id;
                $where['VCH_order_id'] = $header_data->order_id;
                $res_hider = $this->mod_order->spk_data_update($header_add,$where);
            }else{
                $res_hider = $this->mod_order->add_order_hider($header_add);
            }

            $log_data['header'] = $header_add;
            if($res_hider){
               /* =========================ADD TO ORDER OPERATION=======================*/
                if($header_data->form_action=='edit'){
                    $this->mod_order->order_item_delete(
                        $this->tbl_order_operation,
                        array('VCH_order_id'=>$header_data->order_id)
                    );

                    $order_id = $header_data->order_id;
                }
                foreach($operation_data as $row){
                    $data_operation = array(
                            'VCH_order_id'      => $order_id,
                            'CHR_operation_id'  => $row->operation_number,
                            'VCH_operation_type'=> $row->order_type,
                            'VCH_purc_group'    => $header_data->purc_group,//$row->purch_group_comp,
                            'TXT_description'   => $row->ord_description,
                            'VCH_mod'           => ($row->VCH_mod=='')?'1':$row->VCH_mod
                        );

                    $this->mod_order->add_order_operation($data_operation );
                    $log_data['operation'][] = $data_operation;
                }

                /* =========================ADD TO ORDER OPERATION ITEM=======================*/
                if($header_data->form_action=='edit'){
                    $this->mod_order->order_item_delete(
                        $this->tbl_order_operation_item,
                        array('VCH_order_id'=>$header_data->order_id)
                    );
                }
                $op_item_no = 10;
                if($operation_item_data != FALSE){
                    foreach($operation_item_data as $row_item_op){
                        $data_operation_item = array(
                                'VCH_operation_item_no' => $op_item_no,
                                'VCH_order_id'          => $order_id,
                                'VCH_operation_id'      => $row_item_op->opration_number,
                                'VCH_purch'             => $row_item_op->sort_text,
                                'BIG_qty'               => $row_item_op->item_qty,
                                'BIG_price'             => $row_item_op->item_price,
                                'VCH_cost_element'      => (isset($row_item_op->costelement))?$row_item_op->costelement:''
                            );
                        $this->mod_order->add_order_operation_item($data_operation_item);
                        $op_item_no = $op_item_no+10;

                        $log_data['operation_item'][] = $data_operation_item;
                    }
                }
                /* =========================ADD TO COMPONENTS ================================*/
                if($header_data->form_action=='edit'){
                    $this->mod_order->order_item_delete(
                        $this->tbl_order_componets,
                        array('VCH_order_id'=>$header_data->order_id)
                    );
                }
                if($components_data !=false && count($components_data) > 0){
                    foreach($components_data as $row_cmp){
                        $data_component=array(
                                'VHC_component_id'      => $row_cmp->comp_number,
                                'VCH_order_id'          => $order_id,
                                'VCH_operation_number'  => $row_cmp->opration_number,
                                'VCH_part_number'       => $row_cmp->part_number,
                                'VCH_part_description'  => $row_cmp->part_description,
                                'VCH_uom'               => $row_cmp->uom,
                                'BIG_qty'               => $row_cmp->qty,
                                'CHR_component_type'    => $row_cmp->componet_type,
                                'BIG_price'             => $row_cmp->order_prce,
                                'CHR_purch_group'       => $header_data->purc_group //$row_cmp->purch_group
                            );

                        $this->mod_order->add_order_componets($data_component);
                        $log_data['componets'][] = $data_component;
                    }
                }

            }

            $this->mod_order->log_act($header_data->form_action,$log_data);
            
            $out['status']      = true;
            $out['messages']    = lang('msg_success_save_record').'<b> ID : '.$order_id.'</b>';
        }while (false);
        echo json_encode($out);
    }

    function show_order_form(){
        $this->load->model('order/order_model','mod_order');
        $mode       = ($this->input->post('order_crete_type') == $this->order_zbak)?'zbak':'sr';
        $res_data   = array();
        $order_id   = $this->input->post('modeid');

        $arCmb = array(
            array('name' => 'user_status', 'url' => 'order/get_cmb_user_stat', 'flow' => ''),
            array('name' => 'sys_status', 'url' => 'order/get_cmb_sys_stat', 'flow' => ''),
            array('name' => 'pay_term', 'url' => 'order/cmb_payment', 'flow' => '')
        );

        if($mode=='zbak'|| $mode=='sr'){
            $arCmb[] = array('name' => 'sys_cond', 'url' => 'order/get_sys_cond', 'flow' => '');
            $arCmb[] = array('name' => 'zbak_priority', 'url' => 'order/get_cmb_zbak_priority', 'flow' => '');
        }

        $fild ='*';
        $filter_fild_id = ($mode=='sr')?'VCH_notif_id':'VCH_order_no';

        if($mode=='sr'){
            if(strpos(strtolower($this->input->post('modeid')), 'ord') == false && $this->input->post('modeid') != false){ 
                $filter['VCH_order_id'] = $order_id;
                $res_data   = $this->mod_order->get_order_spk_data($fild,$filter);
                $filter_eq['VCH_EQUIPMENT'] = trim((int)$res_data[0]->VCH_equipment_id);
            }
            else{
               $filter[$filter_fild_id] = $this->input->post('notif_id');
               $res_data   = $this->mod_order->get_order_selection($fild,$filter);
               $filter_eq['VCH_EQUIPMENT'] = trim((int)$res_data[0]->VCH_equipment);
            }
        }

        if($mode=='zbak'){
            $fild .=", TXT_desc as VCH_description,";
            $fild .="'' as VCH_soldtoparty,";
            $fild .="VCH_order_no as VCH_notif_id";

           $filter[$filter_fild_id] = $this->input->post('notif_id');
            $res_data   = $this->mod_order->get_order_zbak_selection($fild,$filter);

            $filter_eq['VCH_EQUIPMENT'] = trim($res_data[0]->VCH_equipment+0);
        }

        $filds_eq       = 'VCH_EQUIPMENT,VCH_Description';
      
        $res_equpment   = $this->mod_order->get_equpment($filds_eq,$filter_eq);
        $res_out        = array_merge($res_data,$res_equpment);
        //echo "<pre>",print_r($res_equpment),"</pre>";
        $header_data=json_decode(
            json_encode(
                array(
                    'pm_act_type' =>'',
                    'main_work_center'=>'',
                    'plant_work_center'=>'',
                    'basic_start_date'=>'',
                    'basic_end_date'=>'',
                    'sold_toparty'=>'',
                    'vendor_id'=>'',
                    'vendor_name'=>'',
                    'vendor_kota'=>'',
                    'vendor_telepon'=>'',
                    'vendor_npwp'=>'',
                    'vendor_alamat'=>'',
                    'vendor_kode_pos'=>'',
                    'km_unit'=>'',
                    'system_cond'=>''
                )
            )
        );

        $form_action = 'add';
        if($order_id !=FALSE && $order_id !=''){
            $form_action = 'edit';
            $res_order = $this->mod_order->get_order_spk_data('*',
                            array('VCH_order_id'=>$order_id)
                            );
            foreach ($res_out as $key => $value) {
                $res_out[$key]->VCH_description     = $res_order[0]->TXT_description;
                $res_out[$key]->VCH_functional_lock = $res_order[0]->VCH_functional_loc;
                $res_out[$key]->VCH_equipment       = $res_order[0]->VCH_equipment_id;
                $res_out[$key]->VCH_soldtoparty     = $res_order[0]->VCH_sold_toparty;
                $res_out[$key]->VCH_notif_id        = $res_order[0]->VCH_notif_id;
                $res_out[$key]->INT_cost            = $res_order[0]->INT_cost;

               // if($this->input->post('order_crete_type') == $this->order_zbak){
                    $res_out[$key]->VCH_priority    = $res_order[0]->VCH_zbak_priority;
                    $res_out[$key]->VCH_po          = $res_order[0]->VCH_po_number;
                    $res_out[$key]->VCH_po_date     = $res_order[0]->VCH_po_date;
                    $res_out[$key]->VCH_ppntype     = $res_order[0]->VCH_ppntype;
                    $res_out[$key]->VCH_payment_terms     = $res_order[0]->VCH_payment_terms;
               // }

                $header_data->pm_act_type          = $res_order[0]->VCH_pm_act_type;
                $header_data->main_work_center     = $res_order[0]->VCH_workcenter_main.'-'.$res_order[0]->VCH_workcenter_plan;
                $header_data->plant_work_center    = $res_order[0]->VCH_workcenter_plan;
                $header_data->basic_start_date     = $res_order[0]->DAT_basic_date_start;
                $header_data->basic_end_date       = $res_order[0]->DAT_basic_date_end;
                $header_data->sold_toparty         = $res_order[0]->VCH_sold_toparty;
                $header_data->vendor_id            = $res_order[0]->VCH_vendor_id;
                $header_data->vendor_name          = $res_order[0]->VCH_vendor_name;
                $header_data->vendor_kota          = $res_order[0]->VCH_vendor_kota;
                $header_data->vendor_telepon       = $res_order[0]->VCH_vendor_telpon;
                $header_data->vendor_npwp          = $res_order[0]->VCH_vendor_npwp;
                $header_data->vendor_alamat        = $res_order[0]->VCH_vendor_addres;
                $header_data->vendor_kode_pos      = $res_order[0]->VCH_vendor_post_code;
                $header_data->system_cond          = $res_order[0]->VCH_sys_cond;
                $header_data->km_unit              = $res_order[0]->INT_km;
                $header_data->keluhan              = $res_order[0]->TXT_keluhan;

            }
        }

        $data['js_cmb']             = $this->app->dropdown_kendo($arCmb);
        $data['mode']               = $this->input->post('type');
        $data['res']                = $res_out;
        $data['type_order_cretae']  = $this->input->post('order_crete_type');
        $data['mode_request']       = $mode;
        $data['header_data']        = $header_data;
        $data['form_action']        = $form_action;
        $data['notif_id']            = $this->input->post('notif_id');

        $this->load->view('order/order_form',$data);
    }
    
    function get_order_data(){
        $this->load->model('order/order_model','mod_order');
        $this->load->library('encrypt');

        $fild        = 'VCH_order_id,';
        $fild       .= '"" as enc_order_spk_id,';
        //$fild       .= 'DAT_basic_date_start as create_date,';
        $fild       .= 'DATE_FORMAT(last_insert, "%d-%m-%Y") as last_insert,';
        //$fild       .= 'last_insert as create_date,';
        $fild       .= 'VCH_order_create_type ,'; //as order_mode,';
        $fild       .= 'TXT_description ,'; //as order_description,';
        $fild       .= 'VCH_functional_loc ,'; //as funct_loc,';
        $fild       .= 'staus,'; // as status_data,';
        $fild       .= 'VCH_equipment_id,'; // as equipment,';
        $fild       .= 'IFNULL(VCH_pkb_number,"-") as VCH_pkb_number';

        $filter['staus']='0'; 
        $filter['VCH_system_status LIKE "%CRTD%"']=NULL; 

        if (isset($_POST['filter']['filters'])) {
            $setFilter = $_POST['filter']['filters'];
            foreach ($setFilter as $key => $value) {
                $fild_filter       = $setFilter[$key]['field'];
                $keyword    = $setFilter[$key]['value'];
                $operator   = $setFilter[$key]['operator'];

                if($fild_filter=='last_insert'){
                    $c_date     = explode(" ",$keyword);
                    $ar_bln     = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06','Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
                    
                    if(count($c_date)>2)
                        $keyword    = $c_date[3].'-'.$ar_bln[$c_date[1]].'-'.$c_date[2];
                
                }

                if($fild_filter=='last_insert'){
                    if($operator=='contains')
                        $filter["DATE_FORMAT(`last_insert`, '%Y-%m-%d') LIKE '%".$keyword."%'"]=NULL;

                    if($operator=='eq')
                        $filter["DATE_FORMAT(`last_insert`, '%Y-%m-%d') = '".$keyword."'"]=NULL;

                    if($operator=='neq')
                        $filter["DATE_FORMAT(`last_insert`, '%Y-%m-%d') != '".$keyword."'"]=NULL;
                }else{
                    if($operator=='contains')
                    $filter["`".$fild_filter."` LIKE '%".$keyword."%'"]=NULL;

                    if($operator=='eq')
                        $filter["`".$fild_filter."` = '".$keyword."'"]=NULL;

                    if($operator=='neq')
                        $filter["`".$fild_filter."` != '".$keyword."'"]=NULL;
                }
                
            }
        }

        $res = $this->mod_order->get_order_spk_data($fild,$filter);
        //echo "<pre>",print_r($res),"</pre>";
        if(count($res) > 0){
            foreach($res as $key=>$row){
                $enc_id = $this->encrypt->encrypt($row->VCH_order_id); 
                
                $mod = 'zbak';
                if(isset($row->order_mode) && in_array($row->order_mode, $this->type_order_layout))
                     $mod = 'sr';

                $res[$key]->enc_order_spk_id = $mod.'/'.$this->url_save($enc_id);
            }
        }


        echo json_encode(array('data'=>$res));
        
    }

    function print_order_spk(){
        $this->load->library('encrypt');
        $this->load->model('order/order_model','mod_order');
        $id_url = $this->uri->segment(4);
        $id_enc = $this->url_back($id_url);
        $id     = $this->encrypt->decrypt($id_enc);
        

        $data['id']=$id;

        $fild_heder = '*';
        $filter_header['VCH_order_id'] = $id;
        $res_hider = $this->mod_order->get_order_spk_data($fild_heder,$filter_header);

        $filter_equpment['VCH_EQUIPMENT'] = $res_hider[0]->VCH_equipment_id+0;
        $res_equpment = $this->mod_order->get_equpment('*',$filter_equpment);

        $res_component = $this->mod_order->get_grid_componet('*',
                        array('VCH_order_id'=>$id)
                    );

        $res_operation = $this->mod_order->get_grid_operation_join(
                        'h.CHR_operation_id, h.TXT_description, if(h.VCH_operation_type="PM02", "External","Internal") as VCH_operation_type,d.BIG_qty, BIG_price ',
                        array('h.VCH_order_id'=>$id)
                    );

        $data['order_header']    = $res_hider;
        $data['order_equpment']  = $res_equpment;
        $data['order_component'] = $res_component;
        $data['order_operation'] = $res_operation;
        $data['order_revision']  = $this->mod_order->get_log(
                                        'COUNT(VCH_log_id) as max_rev',
                                        array('VCH_order_id'=>$id)
                                    );

        $templates = $this->load->view('order/order_print_templates.php',$data,true);
        $this->dompdf->load_html($templates);
        $this->dompdf->render();
        $this->dompdf->stream(
              'order_display.pdf',
              array(
                'Attachment' => 0
              )
            );
    }

    function print_order_pkb(){
        $this->load->library('encrypt');
        $this->load->model('order/order_model','mod_order');
        $id_url = $this->uri->segment(4);
        $id_enc = $this->url_back($id_url);
        $id     = $this->encrypt->decrypt($id_enc);
        

        $data['id']=$id;

        $fild_heder = '*';
        $filter_header['VCH_order_id'] = $id;
        $res_hider = $this->mod_order->get_order_spk_data($fild_heder,$filter_header);

        $filter_equpment['VCH_EQUIPMENT'] = $res_hider[0]->VCH_equipment_id+0;
        $res_equpment = $this->mod_order->get_equpment('*',$filter_equpment);

        $res_component = $this->mod_order->get_grid_componet('*',
                        array('VCH_order_id'=>$id)
                    );

        $res_operation = $this->mod_order->get_grid_operation_join(
                        'h.CHR_operation_id, h.TXT_description, if(h.VCH_operation_type="PM02", "External","Internal") as VCH_operation_type,d.BIG_qty, BIG_price ',
                        array('h.VCH_order_id'=>$id)
                    );

        $data['order_header']    = $res_hider;
        $data['order_equpment']  = $res_equpment;
        $data['order_component'] = $res_component;
        $data['order_operation'] = $res_operation;
        $data['order_revision']  = $this->mod_order->get_log(
                                        'COUNT(VCH_log_id) as max_rev',
                                        array('VCH_order_id'=>$id)
                                    );

        $templates = $this->load->view('order/pkb_print_templates.php',$data,true);
        $this->dompdf->load_html($templates);
        $this->dompdf->render();
        $this->dompdf->stream(
              'order_display.pdf',
              array(
                'Attachment' => 0
              )
            );
    }


    function delete_spk(){
        $this->load->model('order/order_model','mod_order');
        $order_spk_id = $this->input->post('id');

        $where['VCH_order_id']=$order_spk_id;
        $where['staus']='0';
        $res = $this->mod_order->spk_data_delete($where);

        if($res){ 
            $this->mod_order->order_item_delete(
                $this->tbl_order_operation_item,
                array('VCH_order_id'=>$order_spk_id)
            );

            $this->mod_order->order_item_delete(
                $this->tbl_order_componets,
                array('VCH_order_id'=>$order_spk_id)
            );

            $this->mod_order->order_item_delete(
                $this->tbl_order_operation,
                array('VCH_order_id'=>$order_spk_id)
            );

            $this->mod_order->log_act("DELETE",array(
                    'header'=>array(
                        'VCH_order_id' =>$order_spk_id,
                    )
                )
            );
        }

        echo json_encode(array('status'=>$res));
    }

    function approve_spk(){
        $this->load->model('order/order_model','mod_order');
        $order_spk_id = $this->input->post('id');
        $order_stat   = $this->input->post('ord_status')+0;
        $res          = array();

        if($order_stat <= 0){
            $pkb_no = $this->app->getAutoId('VCH_pkb_number', $this->tbl_order_hider,'PKB');
            $data = array(
                'staus'=>'1',
                'VCH_pkb_number'=>$pkb_no
            );
            $where['VCH_order_id']=$order_spk_id;
            $res = $this->mod_order->spk_data_update($data,$where);
            $this->mod_order->log_act('APPROVE',array(
                    'header'=>array(
                        'staus' =>'1',
                        'VCH_order_id' =>$order_spk_id,
                        'VCH_pkb_number'=>$pkb_no
                    )
                )
            );
        }

        echo json_encode(array('status'=>$res)); 
    }

    function cek_status(){
        $this->load->model('order/order_model','mod_order');
        $order_spk_id = $this->input->post('id');

        $res = $this->mod_order->get_order_spk_data(
            'staus,IFNULL(VCH_pkb_number,"") as VCH_pkb_number',
            array('VCH_order_id'=> $order_spk_id)
        );
        echo json_encode(array('data'=>$res)); 
    }

    function get_cmb_act_type(){
        $this->load->model("app/app_model", "mod_app");
        $mod        = $_GET['mod_proses']; 
        $calback    = $_GET['callback'];
        $filter     = array();
        $fild       = "VCH_Type as id, CONCAT(VCH_Type,' - ',VCH_Description) as text";

        $filter['VCH_mode']= $mod;
        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['VCH_Type LIKE  "%'.$keyword.'%" OR VCH_Description LIKE "%'.$keyword.'%"']=NULL;
        }

        $res = $this->mod_app->get_cmb($fild, $this->tbl_activity_type,$filter);
        echo $calback."(".json_encode($res).")";
    }

    /*function get_cmb_vendor(){
        $this->load->model("app/app_model", "mod_app");
        $fild = "VCH_VendordID as id, CONCAT(VCH_VendordID,' - ',VCH_Name) as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_vendor);
        echo json_encode(array('data'=>$res));
    }*/

    function get_vendor(){
       $this->load->model("app/app_model", "mod_app");
        $fild = "*";
        $filter['VCH_VendordID']= $this->input->post('vendor_id');
        $res = $this->mod_app->get_cmb($fild, $this->tbl_vendor,$filter);
        echo json_encode(array('data'=>$res)); 
    }

    function get_sys_cond(){
        $res[] = array('id'=>'0','text'=>'Not Available');
        for($i=1; $i < 10; $i++){
            $res[] = array('id'=>$i,'text'=>$i.' Kejadian');
        }
      echo json_encode(array('data'=>$res));   
    }

    function get_selection(){
        $mode = $this->input->post('mode');

        $data['order_type']= array();
        $data['mode']= $mode;

        if($mode=='sr')
            $data['order_type']= $this->cfg_order->order_type_sr;

        if($mode=='zbak')
            $data['order_type']= $this->cfg_order->order_type_zbak;

        $data['filter']=array(
            'mode'  => $mode,
            'notif_number' => $this->input->post('notif_no'),
            'order_no' => $this->input->post('order_no'),
            'crete_date_from' => $this->input->post('created_date_from'),
            'crete_date_to' => $this->input->post('created_date_to'),
            'nopol' => $this->input->post('nopol')
        );
        $this->load->view('order/order_selection_table',$data);
    }

    function get_selection_data(){
        $this->load->model('order/order_model','mod_order');
        $notif_no           = $this->input->post('notif_number');
        $crete_date         = $this->input->post('crete_date_from');
        $ex_create_date     = explode('-', $crete_date);
        $crete_date_to      = $this->input->post('crete_date_to');
        $ex_create_date_to  = explode('-', $crete_date_to);
        $filter             = array();
        $res                = array(); 
        $out                = array(); 
        $mode               = $this->input->post('mode');
        $order_no           = $this->input->post('order_no');
        $date_from          ='';
        $date_to            ='';
        $take               = $this->input->post('take');
        $skip               = $this->input->post('skip');
        $nopol              = $this->input->post('nopol');

        if($crete_date !='')
            $date_from          = $ex_create_date[2].'-'.$ex_create_date[1].'-'.$ex_create_date[0];
        
        if($crete_date_to !='')
            $date_to            = $ex_create_date_to[2].'-'.$ex_create_date_to[1].'-'.$ex_create_date_to[0];
        

        $filds       = ($mode=='sr')?'m.VCH_notif_id as notif_id,' : 'm.VCH_order_no as notif_id,';
        $filds      .= ($mode=='sr')?'m.VCH_notif_type as notif_type,':'m.VCH_pm_act_type as notif_type,';
        $filds      .= ($mode=='sr')?'m.VCH_description as description,':'m.TXT_desc as description,';
        $filds      .= ($mode=='sr')?'m.VCH_soldtoparty as soldtoparty,':'"" as soldtoparty,';
        $filds      .= ($mode=='sr')?'m.VCH_soldtoparty_desc as soldtoparty_desc,':'"" as soldtoparty_desc,';
        $filds      .= ($mode=='sr')?'DATE_FORMAT(m.DAT_reported_date,"%d-%m-%Y") as reported,':'DATE_FORMAT(m.VCH_create_date,"%d-%m-%Y") as reported,';
        $filds      .= 'm.VCH_equipment as equipment,';
        $filds      .= 'm.VCH_functional_lock as functional_loc,';
        $filds      .= 'm.CHR_bisnis_area as bisnis_area,';

        if($mode=='sr') 
            $filds      .= 'm.VCH_customer_code, ';
        
        $filds      .= ($mode=='sr')?'m.VCH_voc_number as voc_number,':'"" as voc_number,';
        $filds      .= ($mode=='sr')?'m.VCH_serca_number as serca_number':'"" as serca_number';

        $filter['t.VCH_order_id IS NULL']= NULL;

        if($notif_no !='' && $mode=='sr')
            $filter["m.VCH_notif_id LIKE '%".$notif_no."%'"]= NULL;
            //$filter['m.VCH_notif_id']= $notif_no;

        if($nopol !='')
            $filter['e.VCH_LicensePlatNumber']= $nopol;

        if($order_no !='' && $mode=='zbak')
            $filter["m.VCH_order_no LIKE '%".$order_no."%'"]=NULL;
            //$filter['m.VCH_order_no']= $order_no;
        
        if($crete_date !='' && $mode=='sr'){
            $filter['m.DAT_reported_date >= "'.$date_from.'"']= NULL;
            $filter['m.DAT_reported_date <= "'.$date_to.'"']= NULL;
        }
        
        if($crete_date !='' && $mode=='zbak'){
            $filter['m.VCH_create_date >= "'.$date_from.'"']= NULL;
            $filter['m.VCH_create_date <= "'.$date_to.'"']= NULL;
        }


        if($mode=='sr'){
            $filter['m.VCH_system_status NOT LIKE "%NOCO%"']=NULL;
            $res = $this->mod_order->get_order_selection_join($filds,$filter,FALSE,
                array('take'=>$take, 'skip'=>$skip,'total'=>false )
            );

            $out['total'] = $this->mod_order->get_order_selection_join($filds,$filter,FALSE,
                array('take'=>$take, 'skip'=>$skip,'total'=>true )
            );

           $this->load->model('master/master_model','mod_mstr');
            
            foreach ($res as $key => $value) {
                $res_mark = $this->mod_mstr->getCustomer(
                    array('VCH_CustomerId'=>$value->VCH_customer_code)
                );
               
                $res[$key]->soldtoparty      = (isset($res_mark[0]->VCH_CustomerId))?$res_mark[0]->VCH_CustomerId : '';
                $res[$key]->soldtoparty_desc = (isset($res_mark[0]->VCH_Title))?$res_mark[0]->VCH_Title.' '.$res_mark[0]->VCH_Name1:'';
            }
           $out['data'] = $res;
        }
        
        if($mode=='zbak'){
            $out['data'] = $this->mod_order->get_order_zbak_selection_join($filds,$filter,FALSE,
                array('take'=>$take, 'skip'=>$skip,'total'=>false )
            );
            
            $out['total'] = $this->mod_order->get_order_zbak_selection_join($filds,$filter,FALSE,
                array('take'=>$take, 'skip'=>$skip,'total'=>true )
            );
        }
       
        echo json_encode($out);


    }

    function get_cmb_orderplan(){
        $calback=$_GET['callback'];
        $this->load->model('order/order_model','mod_order');

        $filds       = 'CONCAT(CHR_AREAID,"-",VCH_AREANAME) as id,';
        $filds      .= 'CONCAT(CHR_AREAID,"-",VCH_AREANAME) as text';

        $filter      = array();

        if ($this->session->userdata('level') ==USER_AREA && $this->session->userdata('limit_id') !="") {
            $filter['CHR_AREAID IN("'.str_replace(',', '","', $this->session->userdata('limit_id')).'")'] = NULL;
        }
        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CHR_AREAID LIKE  "%'.$keyword.'%" OR VCH_AREANAME LIKE "%'.$keyword.'%"']=null;
        }

        $res = $this->mod_order->get_plant($filds,$filter);
        echo $calback."(".json_encode($res).")";
    }

    function get_cmb_work_center(){
        $calback=$_GET['callback'];
        $this->load->model('order/order_model','mod_order');
        $get_plant_id = (isset($_GET['plan_id']))?$_GET['plan_id']:'';
        $plant_id       = explode('-',$get_plant_id);
        $filter         = array();
        $filds          = 'CONCAT(VCH_Workcenter,"-",VCH_Plant) as id,';
        $filds         .= 'CONCAT(VCH_Workcenter,"-",VCH_Description) as text';

        if(isset($plant_id[0]) && $plant_id[0] ==''){
            if ($this->session->userdata('level') ==USER_AREA && $this->session->userdata('limit_id') !="") {
                $filter['VCH_Plant IN("'.str_replace(',', '","', $this->session->userdata('limit_id')).'")'] = NULL;
            }
        }

        if(isset($plant_id[0]) && $plant_id[0] !='')
            $filter['VCH_Plant'] = trim($plant_id[0]);

        if(isset($_GET['filter']['filters'])){
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CONCAT(`VCH_Workcenter`,VCH_Description) LIKE  "%'.$keyword.'%"']=NULL;
        }

        $res = $this->mod_order->get_work_center($filds,$filter);
        echo $calback."(".json_encode($res).")";
    }

    function get_cmb_part_number(){
        $calback=$_GET['callback'];
        $this->load->model('order/order_model','mod_order');
        $filter = array();
        $filds          = 'CHR_MaterialNo as id,';
        $filds         .= 'CONCAT(CHR_MaterialNo,"-",CHR_Description) as text';

       if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CONCAT(CHR_MaterialNo,"-",CHR_Description) LIKE  "%'.$keyword.'%"']=NULL;
        }

        $res = $this->mod_order->get_part_number($filds,$filter);
        echo $calback."(".json_encode($res).")";
    }

    function get_cmb_type(){
        $calback=$_GET['callback'];
        $data = array(
            array('id'=>'PM01','text'=>'PM01-Internal'),
            array('id'=>'PM02','text'=>'PM02-External')
        );
        echo $calback."(".json_encode($data).")";
    }

    function get_cmb_type_component(){
        $calback=$_GET['callback'];
        $data = array(
            array('id'=>'N','text'=>'N-External'),
            array('id'=>'L','text'=>'L-Internal')
        );
        echo $calback."(".json_encode($data).")";
    }

    function get_cmb_cost_element(){
        $this->load->model('order/order_model','mod_order');
        $calback    =$_GET['callback'];
        $filter     = array();
        $filds      = 'id,';
        $filds     .= 'CONCAT(id," - ",text) as text';

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CONCAT(id," ",text) LIKE  "%'.$keyword.'%"']=NULL;
        }

        $res = $this->mod_order->get_cost_element($filds,$filter);

        echo $calback."(".json_encode($res).")";
    }

    function get_cmb_purc_group(){
        $this->load->model('order/order_model','mod_order');
        $calback    =$_GET['callback'];
        $filter     = array();
        $filds      = 'VCH_Type as id,';
        $filds     .= 'CONCAT(VCH_Type," - ",VCH_Description) as text';

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CONCAT(VCH_Type," ",VCH_Description) LIKE  "%'.$keyword.'%"']=NULL;
        }

        $res = $this->mod_order->get_purc_group($filds,$filter);

        echo $calback."(".json_encode($res).")";
    }

    function get_part_number_by_id(){
        $this->load->model('order/order_model','mod_order');
        $out['status']  = false;

        $filds          = 'CHR_Description as text,';
        $filds         .= 'CHR_UnitMeasure as uom';

        $filter['CHR_MaterialNo']= $this->input->post('material_no');

        $res = $this->mod_order->get_part_number($filds,$filter);
        if(count($res)>0){
            $out['status']  = true;
            $out['data']    = $res;
        }
        echo json_encode($out);
    }

    function get_cmb_user_stat(){ 
        $res = array(
            array('id'=>'CRTD','text'=>'CRTD-Created'),
            array('id'=>'APPR','text'=>'APPR-Approved'),
            array('id'=>'TGU','text'=>'TGU-Tunggu Sparepart'),
            array('id'=>'COM','text'=>'COM-Complete'),
            array('id'=>'CNCL','text'=>'CNCL-Cancel')
        );
        echo json_encode(array('data'=>$res));
    }

    function get_cmb_sys_stat(){
        $res = array(
            array('id'=>'CRTD','text'=>'CRTD-Created'),
            array('id'=>'MANC','text'=>'MANC-Mat. availability not cheked'),
            array('id'=>'NTUP','text'=>'NTUP-Dates are not updated')
        );
        echo json_encode(array('data'=>$res));
    }

    function grid_operation(){
        $this->load->model('order/order_model','mod_order');
        $res    = array();
        $filter = array();
        $order_id = $this->input->post('id');

        $filds  = 'CHR_operation_id as operation_number,';
        $filds .= 'VCH_operation_type as order_type,';
        $filds .= 'TXT_description as ord_description,';
        $filds .= 'VCH_purc_group as purch_group_comp,';
        $filds .= 'VCH_mod';

        if($order_id != false && $order_id !=''){
            $filter['VCH_order_id'] = $order_id;
            $res = $this->mod_order->get_grid_operation($filds,$filter);
        }

        echo json_encode(array('data'=>$res));
    }

    function grid_operation_item(){
        $this->load->model('order/order_model','mod_order');
        $res    = array();
        $filter = array();
        $order_id = $this->input->post('id');

        $filds  = 'VCH_order_id as order_type,';
        $filds .= 'VCH_purch as sort_text,';
        $filds .= 'VCH_operation_id as opration_number,';
        $filds .= 'BIG_qty as item_qty,';
        $filds .= 'BIG_price as item_price,';
        $filds .= 'VCH_cost_element as costelement';

        if($order_id != false && $order_id !=''){
            $filter['VCH_order_id'] = $order_id;
            $res = $this->mod_order->get_grid_operation_item($filds,$filter);
        }

        echo json_encode(array('data'=>$res));
    }

    function grid_component(){
        $this->load->model('order/order_model','mod_order');
        $res    = array();
        $filter = array();
        $order_id = $this->input->post('id');

        $filds  = 'VHC_component_id as comp_number,';
        $filds .= 'VCH_operation_number as opration_number,';
        $filds .= 'VCH_part_number as part_number,';
        $filds .= 'VCH_part_description as part_description,';
        $filds .= 'VCH_uom as uom,';
        $filds .= 'BIG_qty as qty,';
        $filds .= 'CHR_component_type as componet_type,';
        $filds .= 'BIG_price as order_prce,';
        $filds .= 'CHR_purch_group as purch_group';

        if($order_id != false && $order_id !=''){
            $filter['VCH_order_id'] = $order_id;
            $res = $this->mod_order->get_grid_componet($filds,$filter);
        }

        echo json_encode(array('data'=>$res));
    }
   

    function url_save($value){
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        return str_replace($dirty, $clean, $value);
    }

    function url_back($value){
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        return str_replace($clean, $dirty, $value);
    }

    function cek_stat_before_edit()
    {
        $this->load->library('encrypt');

        $enc_id = $this->input->post('id');
        $rid    = explode("/", $enc_id);
        $c_enc_id  = $this->url_back($rid[1]);
        $fx_id  = $this->encrypt->decrypt($c_enc_id);

        $this->load->model('order/order_model','mod_order');
        $res = $this->mod_order->get_order_spk_data(
            'VCH_order_id,staus',
            array('VCH_order_id'=>$fx_id)
        );

        $out['status'] =  true;
        do{

            if($res[0]->staus == '1'){
                $out['status'] =  false;
                $out['messages'] = 'Order SPK ID '.$res[0]->VCH_order_id.' Was Approved';
                break;
            }

        }while(FALSE);
        echo json_encode($out);
    }

    function get_cust(){
        $this->load->model('order/order_model','mod_order');
        $cus_id = $this->input->post('id');
        $filter['VCH_CustomerId']= $cus_id;
        $res_mark = $this->mod_order->get_data_customer('VCH_Title,VCH_Name1',$filter,0,1);

        if(count($res_mark)>0)
            echo $res_mark[0]->VCH_Title.' '.$res_mark[0]->VCH_Name1;
    }

    function get_equip(){
        $this->load->model('order/order_model','mod_order');
        $eqid = $this->input->post('id');
        $filter['VCH_EQUIPMENT']= $eqid;

        $res_mark = $this->mod_order->get_data_equipment('VCH_FuncLoc',$filter,0,1);

        echo $res_mark[0]->VCH_FuncLoc;
    }

    public function get_cmb_zbak_priority()
    {
        $data = array(
            array('id' =>'1','text'=>'1 Low'),
            array('id'=>'2','text'=>'2 Medium'),
            array('id'=>'3','text'=>'3 High')
        );
         echo json_encode(array('data'=>$data));
    }

    public function get_data_brouse()
    {
        $this->load->model('order/order_model','mod_order');
        $where      = array();
        $take       = $this->input->post('take');
        $cur_page   = $this->input->post('page')-1;
        $mode       = $this->input->post('id');
        $filter     = json_decode(json_encode($this->input->post('filter')));
        $skip       = $this->input->post('skip');
        $table      = ($mode=='find_equipment')?$this->tbl_equipment:$this->tbl_customer;

        $fild_id    = 'VCH_CustomerId';
        $fild_text  = 'CONCAT(VCH_Title, VCH_Name1)';

        if($mode=='find_equipment'){
            $fild_id    = 'VCH_EQUIPMENT';
            $fild_text  = 'VCH_Description';

            $where['VCH_SystemStatus NOT LIKE "%INAC%"'] = NULL;
            $where['VCH_UserStatus NOT IN ("DISP","LOSS")'] = NULL;
            $where['VCH_FuncLoc NOT LIKE "%DP%"'] = NULL;
        }
         
        if(isset($filter->filters)){
           foreach ($filter->filters as $key => $value) {
                $use_fild = ($value->field=='id') ? $fild_id : $fild_text;
          
                if($value->operator=='contains')
                    $where[$use_fild.' LIKE "%'.$value->value.'%"'] = NULL;

                if($value->operator=='eq')
                    $where[$use_fild]=$value->value;

                if($value->operator=='neq')
                    $where["'".$use_fild.' !="'.$value->value.'"']= NULL; 
           }
        }

        $fild  = $fild_id.' as id,';
        $fild .= $fild_text.' as text';

        if($mode=='find_equipment')
            $res    = $this->mod_order->get_data_equipment($fild,$where,$skip,$take);
        else
            $res    = $this->mod_order->get_data_customer($fild,$where,$skip,$take);

        $total  = $this->mod_order->get_total($table,$where);

        echo json_encode(array('total'=>$total,'data'=>$res));
    }

}