<?php if (! defined('BASEPATH')) { exit('No direct script access allowed'); }

class Rpt extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function index()
    {
		$data['sample'] = '';
		$this->template->build('order/order_rpt', $data);
	}

    public function getDetial(){
        $custom_filter  = $this->input->post("custome_filter");
        $filter_id      = $this->input->post("find_id");
        $filter_year    = $this->input->post("find_year");
        $filter_tgl     = $this->input->post("find_tanggal");
        $custom_periode = $this->input->post("find_periode");
        $filter_moth    = $this->input->post("find_bulan");
        $filter_user    = $this->input->post("find_user");
        $initial        = substr($filter_id, 0, 3);

        $postData['res'] = array(
            'custom_filter'=>$custom_filter,
            'filter_id'=>$filter_id,
            'filter_year'=>$filter_year,
            'filter_tgl'=>$filter_tgl,
            'custom_periode'=>$custom_periode,
            'filter_moth'=>$filter_moth,
            'filter_user'=>$filter_user
        );

        $this->load->view('order/rpt_header', $postData);
    }

    public function getHeader(){
        $this->load->model('order/order_model','mod_order');
        $this->load->library('encrypt');

        $custom_filter = $this->input->post('custom_filter');
        $filter_id = $this->input->post('filter_id');
        $filter_year = $this->input->post('filter_year');
        $filter_tgl = $this->input->post('filter_tgl');
        $custom_periode = $this->input->post('custom_periode');
        $filter_moth = $this->input->post('filter_moth');
        $filter_user = $this->input->post('filter_user');
        $res    = array();
        $filter = array();

        $filds  = 'VCH_order_id as id,';
        $filds .= 'VCH_notif_id as notif_id,';
        $filds .= 'CASE WHEN VCH_order_create_type = "WO01" THEN "Breakdown Order" ';
        $filds .= 'WHEN VCH_order_create_type = "WO02" THEN "Corrective Order" ';
        $filds .= 'WHEN VCH_order_create_type = "WO03" THEN "Ekspedisi Order" ';
        $filds .= 'WHEN VCH_order_create_type = "OR01" THEN "Own Risk" ';
        $filds .= 'WHEN VCH_order_create_type = "WO05" THEN "Preventive Order" ';
        $filds .= 'ELSE VCH_order_create_type END as create_type,';
        $filds .= 'VCH_functional_loc,';
        $filds .= 'VCH_equipment_id,';
        $filds .= 'VCH_equpment_description,';
        $filds .= 'VCH_sold_toparty,';
        $filds .= 'DAT_basic_date_start,';
        $filds .= 'DAT_basic_date_end,';
        $filds .= 'staus,';
        $filds .= '"" as enc_order_spk_id,';
        $filds .= 'IFNULL(VCH_pkb_number,"-") as pkb_number,';
        $filds .= 'flag';

        if($custom_filter !='on'){
            $filter['VCH_order_id'] = $filter_id;
        }else{
            if($custom_periode == 'rb_date'){
                $Mdate=date_create($filter_tgl);
                $date_new = date_format($Mdate,"Y-m-d");

                $filter['DATE_FORMAT(last_insert, "%Y-%m-%d") = "'.$date_new.'"']= NULL;
            }
            else{
                $filter['MONTH(last_insert)']= $filter_moth;
                $filter['YEAR(last_insert)']= $filter_year;
            }

            if($filter_user != '-1')
                $filter['VCH_user_create']= $filter_user;
        }


        $res = $this->mod_order->get_order_spk_data($filds,$filter);
        if(count($res) > 0){
            foreach($res as $key=>$row){
                $enc_id = $this->encrypt->encrypt($row->id); 

                $res[$key]->enc_order_spk_id = 'report'.'/'.$this->url_save($enc_id);
            }
        }

        echo json_encode(array("data"=>$res));
    }

    function itemdetail(){
        $this->load->model('order/order_model','mod_order');
        $order_id = $this->uri->segment(4);
        $filds = '*';
        $filter['VCH_order_id'] = $order_id;

        $data['order_header'] = current($this->mod_order->get_order_spk_data($filds,$filter));

        $this->load->view('order/order_rpt_detail',$data);
    }

    function url_save($value){
        $dirty = array("+", "/", "=");
        $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
        return str_replace($dirty, $clean, $value);
    }
} 