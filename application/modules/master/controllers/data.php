<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data extends MY_Controller {
	var $tbl_hider 				= 'TF_BKPF';
	var $tbl_detail 			= 'TF_BSEG';

	var $fild_comp_code 		= 'CHR_BUKRS'; 
	var $fild_bisnis_area		= 'VCH_GSBER';
	var $fild_cost_center		= 'VCH_KOSTL';
	var $fild_gl_account		= 'VCH_HKONT';
	var $fild_curency				= 'CHR_CURENCY';
	var $fild_amount				= 'INT_WRBTR';
	var $fild_posting_date	= 'DAT_BUDAT';
	var $fild_doc_date			= 'DAT_BLDAT';
	var $fild_reverence			= 'CHR_XBLNR';
	var $fild_doc_type			= 'ENM_BLART';
	var $fild_acounting_no	= 'VCH_BELNR';
	var $fild_fiscal_year		= 'DYR_GJAHR';
	var $fild_text					= 'TEX_SGTXT';
	var $fild_internal_order	= 'VCH_AUFNR';
	var $fild_line_item			= 'VCH_BUZEI';
	var $fild_posting_key		= 'VCH_BSCHL';
	var $fild_indicator_saldo	= 'ENM_SHKZG';
	var $fild_group_id			= 'CHR_GROUP';
	var $fild_flag					= 'CHR_FLAG';	
	var $cfg_Clearing			= array();
	var $fild_doc_hider			= 'CHR_BKTXT';
	var $fild_clearing_date		= 'DAT_AUGCP';
	var $fild_clearing_doc_no	= 'VCH_AUGBL'; 
	var $fild_doc_number_alias 	= 'VCH_REBZG';
	var $fild_doc_yer_alias 	= 'DYR_REBZJ';
	var $fild_doc_item_alias 	= 'INT_REBZZ';
	var $fild_customer 			= 'VCH_KUNNR';
	var $fild_special			= 'VCH_IND_JURNAL';
	var $fild_billing_doc			= 'CHR_AWKEY';
	
	public function __construct(){
        parent::__construct();
		$this->cfg_Clearing = json_decode(json_encode($this->config->item('ar_clearing')));
    }

	function add_item_cheked($post_id){
		$this->load->model('master/master_model','mod_mstr');
		$doc_type = explode(",",$this->cfg_Clearing->type_doc_tag);
		$out['data'] = array();

	
		$fild  = "h.".$this->fild_comp_code." as company_code,";
		$fild .= "h.".$this->fild_acounting_no." as doc_number,";
		$fild .= "h.".$this->fild_fiscal_year." as doc_yer,";
		$fild .= "h.".$this->fild_doc_type." as doc_type,";
		$fild .= "d.".$this->fild_posting_key." as post_key,";
		$fild .= "d.".$this->fild_amount." as amount,";
		$fild .= "d.".$this->fild_line_item." as item,";
		$fild .= "d.".$this->fild_customer." as customer,";
		$fild .= "d.".$this->fild_bisnis_area." as bisnis_area,";
		$fild .= "h.".$this->fild_billing_doc." as billing_doc,";

		$filter['CONCAT(h.'.$this->fild_doc_hider.' IN ("'.implode('","',$post_id).'")'] = NULL;
		$res = $this->mod_mstr->get_join($fild,$filter);
		
		
		$limitOut = $this->mod_mstr->get_tmp(
			array(
				'tmp_for'=> $this->cfg_Clearing->initial
			)
		);
		if(count($limitOut) > 0){
		 	foreach($res as $key => $row){
				foreach($limitOut as $keylimit=>$ar_limit){
		 			if(($ar_limit->company_code == $row->company_code) && ($ar_limit->doc_number ==$row->doc_number) &&($ar_limit->doc_yer == $row->doc_yer) && ($ar_limit->item ==$row->item))
						unset($res[$key]);
		 		}
		 	}

			$res_out = array();
			foreach ($res as $key => $value){
				$res_outx['company_code'] 	= $value->company_code;
				$res_outx['doc_number'] 	= $value->doc_number;
				$res_outx['doc_yer'] 		= $value->doc_yer;
				$res_outx['doc_type'] 		= $value->doc_type;
				$res_outx['post_key'] 		= $value->post_key;
				$res_outx['amount'] 		= $value->amount;
				$res_outx['item'] 			= $value->item;
				$res_outx['text'] 			= $value->text;
				$res_outx['customer'] 		= $value->customer;
				$res_outx['ar_status'] 		= $value->ar_status;
				$res_outx['bp_status'] 		= $value->bp_status;
				$res_outx['bisnis_area'] = $value->bisnis_area;
				if(!$jsonOut){
					$res_outx['posting_date'] = $value->posting_date;
					$res_outx['doc_date'] = $value->doc_date;
				}
				array_push($res_out,$res_outx);
			}
			$res = json_decode(json_encode($res_out));
		}


		$out['data'] = $res;
		if($jsonOut)
			if($text =='' && $custon_doc_num=='' && $customer=='')
				echo json_encode(array('data'=>array()));
			else
				echo json_encode($out);
		else
			return $out;
	}
	
}