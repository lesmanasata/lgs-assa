<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master extends MY_Controller
{
    public $tbl_company 			= "MF_COMPANY";
    public $tbl_bisnis_area 		= "MF_AREA";
    public $tbl_cost_center 		= "MF_COST_CENTER";
    public $tbl_gl 					= "MF_GLACCOUNT";
	public $tbl_vendor				= "MF_VENDOR";
    public $tbl_payment             = "MF_PAYMENT_TERMS";
    public $tbl_wht_head            = "MF_WHT_HEADER";
    public $tbl_wht_detail          = "MF_WHT_DETAIL";
    public $tbl_gl_alter            = "MF_ALTERNATIF_GL";
    public $tbl_mf_internal         = "MF_INTERNAL_ORDER";
    public $tbl_printJv_header      = "TF_PRINT_JV_HEADER";

    public function __construct()
    {
        parent::__construct();
        $this->load->module('app');
        $this->app->cek_permit();
    }

    public function cmb_company()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_CODE as id, CONCAT(CHR_CODE,' - ',CHR_NAME) as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_company);

        echo json_encode(array('data'=>$res));
    }
    public function cmb_docnum()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_no_document as id, CHR_no_document as text";
        $filter['INT_document_print'] ='1';
        $res = $this->mod_app->get_cmb($fild, $this->tbl_printJv_header,$filter);

        echo json_encode(array('data'=>$res));
    }
    public function wht()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "VCH_Wht_type as id, CONCAT(VCH_Wht_type,' - ',VCH_Description) as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_wht_head);

        echo json_encode(array('data'=>$res));
    }
    public function wht_code()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "VCH_WHTCode as id, CONCAT(VCH_WHTCode,' - ',VCH_Description) as text";
        $filter['VCH_WHTType']= $this->input->post('id');
        $res = $this->mod_app->get_cmb($fild, $this->tbl_wht_detail, $filter);

        echo json_encode(array('data'=>$res));
    }
    public function cmb_bisnis_area()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_AREAID as id, CONCAT(CHR_AREAID,' - ',VCH_AREANAME) as text";
        $filter['CHR_COMPANY_CODE']= $this->input->post('id');
        $res = $this->mod_app->get_cmb($fild, $this->tbl_bisnis_area, $filter);

        echo json_encode(array('data'=>$res));
    }

    public function cmb_vendor()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CONCAT(VCH_VendordID,'|',VCH_ControllAccount) as id, CONCAT(VCH_VendordID,' - ',VCH_Name) as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_vendor);

        echo json_encode(array('data'=>$res));
    }
    public function cmb_vendor_gl()
    {
        $filter = array();
        $test = $this->input->post('id');
        $id = explode("|",$test);

        $this->load->model("jv/jv_model", "jv_mod");
        $fild = "VCH_AlternatifAccount as id, VCH_AlternatifAccount as text";
        $filter["VCH_Account"]= (isset($id[1]))?$id[1]:'';
        
        $res = $this->jv_mod->get_cmb($fild, $this->tbl_gl_alter, $filter);

        echo json_encode(array('data'=>$res));
    }
    public function cmb_vendor_detail()
    {
        $test = $this->input->post('id');
        $id = explode("|",$test);
        $this->load->model("app/app_model", "mod_app");
        $fild = "VCH_Name, VCH_City, VCH_Telephone, VCH_Stceg, VCH_Address,VCH_ControllAccount, CHR_PostCode";
        $filter['VCH_VendordID']= $id[0];
        $res = $this->mod_app->get_cmb($fild, $this->tbl_vendor, $filter);

        echo json_encode(array('data'=>$res));
    }
	public function cmb_payment()
	{
		$this->load->model("app/app_model", "mod_app");
		$fild = "INT_Terms as id, CHR_Type as text";
		$res = $this->mod_app->get_cmb($fild, $this->tbl_payment);

		echo json_encode(array('data'=>$res));
	}
    public function internal_order()
    {
        $calback=$_GET['callback'];
        $this->load->model("app/app_model", "mod_app");
        $fild = "CONCAT(VCH_OrderID,'-',VCH_OrderName) as id, CONCAT(VCH_OrderID,'-',VCH_OrderName) as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_mf_internal);

        echo $calback."(".json_encode($res).")";
    }
    public function cmb_cost_center2()
    {
        $this->load->model("app/app_model", "mod_app");
        $filter 	= array();
        $calback 	= $_GET['callback'];
        $fild = "CONCAT (CHR_CODE,'-',VCH_DESCRIPTION) as id, CONCAT (CHR_CODE,'-',VCH_DESCRIPTION) as text";
        if (isset($_GET['id'])) {
            $filter['VCH_BUSINESS_CODE']= $_GET['id'];
        }
        $res = $this->mod_app->get_cmb($fild, $this->tbl_cost_center, $filter);
        echo $calback."(".json_encode($res).")";
    }
	   public function cmb_gl_acc2()
    {
        $calback=$_GET['callback'];
        $this->load->model("app/app_model", "mod_app");
        $fild = "CONCAT(CHR_CODE,'-',VCH_DESCRIPTION) as id, CONCAT(CHR_CODE,'-',VCH_DESCRIPTION) as text";

        $res = $this->mod_app->get_cmb($fild, $this->tbl_gl);
        echo $calback."(".json_encode($res).")";
    }

    public function cmb_cost_center()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_CODE as id, VCH_DESCRIPTION as text";
        $filter['VCH_BUSINESS_CODE']= $this->input->post('id');
        $res = $this->mod_app->get_cmb($fild, $this->tbl_cost_center, $filter);

        echo json_encode(array('data'=>$res));
    }

    public function cmb_gl_acc()
    {
        $this->load->model("app/app_model", "mod_app");
        $fild = "CHR_CODE as id, VCH_DESCRIPTION as text";
        $res = $this->mod_app->get_cmb($fild, $this->tbl_gl);
        echo json_encode(array('data'=>$res));
    }

    public function get_bisnis_area()
    {
        $this->db_main = $this->load->database('default', true);

        $calback= $_GET['callback'];
        $text ='';
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];

            if (isset($_GET['filter']['filters'][0]['value'])) {
                $text = $filter['filters'][0]['value'];
            }
        }
        $res=array();
        $this->db_main->select('CHR_AREAID as id, CONCAT(CHR_AREAID," (", VCH_AREANAME,")") as text', false)
            ->from('MF_AREA');
        if ($text !='') {
            $this->db_main->like('CHR_AREAID', $text);
            $this->db_main->or_like('VCH_AREANAME', $text);
        }
        $this->db_main->where('CHR_COMPANY_CODE', $_GET['company_code']);
        $this->db_main->order_by('VCH_AREANAME', 'ASC');
        $resx = $this->db_main->get();
        $res = $resx->result();
        echo $calback."(".json_encode($res).")";
    }

    public function clear_all_tmp()
    {
        $this->db_main = $this->load->database('default', true);
        $user = $this->session->userdata('user');
        $this->db_main->where('user_create', $user);
        $this->db_main->delete('tmp_item_select');
    }
}
