<?php
class Master_model extends CI_Model
{
    public $tbl_hider = 'TF_BKPF';
    public $tbl_detail = 'TF_BSEG';
    public $tbl_vecor = 'MF_VENDOR_CUST';
    public $tbl_tmp = 'tmp_item_select';
    public $tbl_sap = 'TF_SINC';
    public $tbl_customer = 'MF_CUSTOMER';
    public $tbl_vendor = 'MF_VENDOR';
    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    public function get_hider($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    function clearing_bank_is_used($docid,$docyer,$doccp){
        $this->db_main->select('VCH_BELNR,DYR_GJAHR,VCH_AUGBL')
            ->from($this->tbl_detail)
            ->where('CHR_BUKRS',$doccp)
            ->where('VCH_BELNR',$docid)
            ->where('DYR_GJAHR',$docyer)
            ->where('VCH_AUGBL IS NOT NULL',NULL);
        $res=$this->db_main->get();
        return $res->result();
    }

    public function get_tmp($filter=array())
    {
        $this->db_main->select('*', false)
            ->from($this->tbl_tmp);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }
    public function reportBS($custom_fild, $filter=array(), $group=false)
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider.' bk')
            ->join($this->tbl_detail.' bs', 'bk.CHR_BUKRS=bs.CHR_BUKRS and bk.VCH_BELNR=bs.VCH_BELNR and bk.DYR_GJAHR=bs.DYR_GJAHR', 'left')
            ->join($this->tbl_sap.' si', 'bs.VCH_BELNR=si.VCH_BELNR AND bs.CHR_BUKRS=si.CHR_BUKRS AND bs.DYR_GJAHR=si.DYR_GJAHR', 'left');

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }
    public function getBK($custom_fild, $filter=array(), $group=false)
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider.' h')
            ->join($this->tbl_sap.' bs', 'h.CHR_BUKRS=bs.CHR_BUKRS and h.VCH_BELNR=bs.VCH_BELNR and h.DYR_GJAHR=bs.DYR_GJAHR', 'left');
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter, false);
        }
        if ($group !=false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        return $res->result();
    }
    public function get_join($custom_fild, $filter=array(), $group=false)
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider.' h')
            ->join($this->tbl_detail.' d', 'h.CHR_BUKRS=d.CHR_BUKRS and h.VCH_BELNR=d.VCH_BELNR and h.DYR_GJAHR=d.DYR_GJAHR');

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        if ($group !=false) {
            $this->db_main->group_by($group);
        }
        $res=$this->db_main->get();
        //echo $this->db_main->last_query();
        return $res->result();
    }

    public function get_detail($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_detail);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function del_hider($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_hider);
        return $res;
    }

    public function del_detail($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_detail);
        return $res;
    }

    public function join($filds, $filter=array())
    {
        $this->db_main->select($filds)
        ->from($this->tbl_hider." h")
        ->join($this->tbl_detail." d", "h.CHR_BUKRS=d.CHR_BUKRS and h.VCH_BELNR=d.VCH_BELNR and h.DYR_GJAHR=d.DYR_GJAHR");

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function add_hider($data)
    {
        $this->db_main->insert(
            $this->tbl_hider,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_tmp($data)
    {
        $this->db_main->insert(
            $this->tbl_tmp,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_detail($data)
    {
        $this->db_main->insert_batch(
            $this->tbl_detail,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    public function tmp_delete($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_tmp);
        return $res;
    }

    public function delete_hider($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_hider);
        return $res;
    }

    public function delete_detail($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_detail);
        return $res;
    }

    public function get_vecus($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_vecor);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();
        return $res->result();
    }

    public function update_hider($data, $filter=array())
    {
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
            $res = $this->db_main->update($this->tbl_hider, $data);
            return $res;
        } else {
            return false;
        }
    }

    public function update_detail($data, $filter=array())
    {
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
            $res = $this->db_main->update($this->tbl_detail, $data);
            return $res;
        } else {
            return false;
        }
    }

    public function update_hider_no_prefix($data, $filter=array())
    {
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);

            foreach ($data as $key=>$val) {
                $this->db_main->set($key, $val, false);
            }

            $res = $this->db_main->update($this->tbl_hider);
            return $res;
        } else {
            return false;
        }
    }

    public function update_detail_no_prefix($data, $filter=array())
    {
        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);

            foreach ($data as $key=>$val) {
                $this->db_main->set($key, $val, false);
            }

            $res = $this->db_main->update($this->tbl_detail);
            return $res;
        } else {
            return false;
        }
    }

    public function getCustomer($filter=array())
    {
        $this->db_main->select('*', false)
            ->from($this->tbl_customer);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $this->db_main->limit(20);
        $res=$this->db_main->get();

        return $res->result();
    }
    public function getVendor($filter=array())
    {
        $this->db_main->select('*', false)
            ->from($this->tbl_vendor);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        //$this->db_main->limit(20);
        $res=$this->db_main->get();

        return $res->result();
    }

}
