<?php
class App_model extends CI_Model
{
    public $tbl_hider = 'TF_BKPF';
    public $tbl_customer = 'MF_CUSTOMER';
    public $tbl_detail = 'TF_BSEG';
    public $tbl_date = 'MF_TIME_PERIOD';
    public $tbl_tmp	= 'tmp_item_select';
    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    public function get_hider($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_hider);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function get_date($filter=array()){
        $this->db_main->select('*', false)
            ->from($this->tbl_date);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function get_detail($custom_fild, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($this->tbl_detail);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function del_hider($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_hider);
        return $res;
    }

    public function del_detail($filter)
    {
        $this->db_main->where($filter);
        $res = $this->db_main->delete($this->tbl_detail);
        return $res;
    }

    public function join($filds, $filter=array())
    {
        $this->db_main->select($filds)
        ->from($this->tbl_hider." h")
        ->join($this->tbl_detail." d", "h.BUKRS=d.BUKRS and h.BELNR=d.BELNR and h.GJAHR=d.GJAHR");

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function get_cmb($custom_fild, $tbl, $filter=array())
    {
        $this->db_main->select($custom_fild, false)
            ->from($tbl);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $res=$this->db_main->get();

        return $res->result();
    }

    public function getCustomerBytmp($filter=array())
    {
        $this->db_main->select('d.customer AS VCH_KUNNR, IF(h.VCH_Name1 IS NULL,"Anonym",h.VCH_Name1) AS VCH_CSNAME', false)
            ->from($this->tbl_customer." h")
            ->join($this->tbl_tmp." d", "h.VCH_CustomerId=d.customer", 'RIGHT');

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }
        $this->db_main->group_by('d.customer');
        $this->db_main->limit(20);
        $res=$this->db_main->get();
        return $res->result();
    }

    function update_tmp($filter,$upvalue){
        $this->db_main->where($filter);
        $this->db_main->update(
            $this->tbl_tmp,$upvalue
        );
    }
}
