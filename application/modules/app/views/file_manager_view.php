<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jquery-ui/css/base/jquery-ui.css'); ?>" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/theme.css'); ?>" /> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/elfinder.min.css'); ?>" /> 
  <script type="text/javascript" src="<?php echo base_url('assets/jquery-1.7.2.min.js'); ?>"></script> 
  <script type="text/javascript" src="<?php echo base_url('assets/jquery-ui/js/jquery-ui.min.js'); ?>"></script> 
  <script type="text/javascript" src="<?php echo base_url('assets/elfinder/js/elfinder.min.js'); ?>"></script> 
   
  <script type="text/javascript"> 
    jQuery(document).ready(function(){ 
      jQuery('#elfinder-tag').elfinder({ 
        url: '<?php echo site_url('app/filemanager/elfinder'); ?>', 
 
      }).elfinder('instance'); 
 
      // fit to window.height on window.resize 
      var resizeTimer = null; 
      $(window).resize(function() { 
          resizeTimer && clearTimeout(resizeTimer); 
          resizeTimer = setTimeout(function() { 
              var h = parseInt($(window).height()) - 20; 
              if (h != parseInt($('#elfinder').height())) { 
                  elfinderInstance.resize('100%', h); 
              } 
          }, 200); 
      }); 
    }); 
  </script> 
 
<h1 class="page-title">File Manager</h1> 
<ol class="breadcrumb breadcrumb-2"> 
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li> 
  <li><a href="#">Master Data</a></li> 
  <li class="active"><strong>File Manager</strong></li> 
</ol> 
<div style="padding-right: 15px;"> 
  <div id="elfinder-tag" ></div> 
</div>