<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class App extends MY_Controller
{
    public $cust_id = 'VCH_CustomerId';//"VCH_KUNNR";
    public $cust_name = 'VCH_Name1';//"VCH_CSNAME";

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
        $this->lang->load('language', 'english');
    }

    public function index()
    {
        redirect('dashboard/index');
    }

    public function dropdown_kendo($data)
    {
        $res['cmbRequest'] = json_decode(json_encode($data));
        return $this->load->view('app/dropdown_js', $res, true);
    }

    public function get_date(){
        $this->load->model('app_model','mod_app');
        $company = $this->input->post('company');

        $filter['CHR_BUKRS'] = $company;
        $res= $this->mod_app->get_date($filter);

        echo json_encode(array('data'=>$res));
    }

    public function get_cmb_ppn()
    {
        $calback=$_GET['callback'];
        $data = array(
            array('id' =>'1','text'=>'1. Normal'),
            array('id'=>'2','text'=>'2. Wapu / PPN Dibebaskan')
        );
        echo $calback."(".json_encode($data).")";
    }

    public function get_cmb_pph()
    {
        $calback=$_GET['callback'];
        $data = array(
            array('id' =>'10','text'=>'10%'),
            array('id'=>'2','text'=>'2%'),
            array('id'=>'0','text'=>'No PPH')
        );
        echo $calback."(".json_encode($data).")";
    }
    public function get_cmb_ppntype()
    {
        $calback=$_GET['callback'];
        $data = array(
            array('id'=>'M0','text'=>'0'),
            array('id'=>'M4','text'=>'10%')
        );
        echo $calback."(".json_encode($data).")";
    }
    public function get_cmb_balance()
    {
        $calback=$_GET['callback'];
        $list_titipan = $this->config->item('titipan');
        $data = explode(",", $list_titipan['type_for_ar']);
        foreach ($data as $key => $value) {
            $res[$key]['id'] = $value;
            $res[$key]['text'] = $value;
        }
        if($_GET['cmp_code'] =='4000'){
            $res[$key]['id'] = 'BY Adm Lelang';
            $res[$key]['text'] = 'BY Adm Lelang';
        }
        echo $calback."(".json_encode($res).")";
    }

    public function get_cmb_customer()
    {
        $this->load->model('master/master_model', 'mod_mstr');
        $calback=$_GET['callback'];
        $filter = array();

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter[$this->cust_id.' LIKE  "%'.$keyword.'%" OR '.$this->cust_name.' LIKE "%'.$keyword.'%"']=null;
        }

        $data = $this->mod_mstr->getCustomer($filter);

        $res[0]['id'] = "-1";
        $res[0]['text'] = lang("cmb_select_cust");

        foreach ($data as $key => $value) {
            $res[$key+1]['id'] = $value->VCH_CustomerId;
            $res[$key+1]['text'] = $value->VCH_CustomerId." - ".$value->VCH_Name1;
        }
        echo $calback."(".json_encode($res).")";
    }

    public function get_cmb_vendor()
    {
        $this->load->model('master/master_model', 'mod_mstr');
        $calback=$_GET['callback'];
        $filter = array();

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['CONCAT(VCH_VendordID,VCH_Name) LIKE "%'.$keyword.'%"']=null;
        }

        $data = $this->mod_mstr->getVendor($filter);

        $res[0]['id'] = "-1";
        $res[0]['text'] = 'Select Vendor';

        foreach ($data as $key => $value) {
            $res[$key+1]['id'] = $value->VCH_VendordID;
            $res[$key+1]['text'] = $value->VCH_VendordID." - ".$value->VCH_Name;
        }
        echo $calback."(".json_encode($res).")";
    }
    public function get_cmb_customer_bytmp()
    {
        $this->load->model('app/app_model', 'mod_app');
        $calback=$_GET['callback'];
        $filter = array();

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['h.'.$this->cust_id.' LIKE  "%'.$keyword.'%" OR d.customer LIKE "%'.$keyword.'%"']=null;
        }
        $filter['d.user_create'] = $this->session->userdata('user');
        $data = $this->mod_app->getCustomerBytmp($filter);

        $res[0]['id'] = "-1";
        $res[0]['text'] = lang("cmb_select_cust");

        foreach ($data as $key => $value) {
            $res[$key+1]['id'] = $value->VCH_KUNNR;
            $res[$key+1]['text'] = $value->VCH_KUNNR." - ".$value->VCH_CSNAME;
        }
        echo $calback."(".json_encode($res).")";
    }

    public function get_cmb_petugas()
    {
        $this->load->model('user/user_model', 'mod_user');
        $calback=$_GET['callback'];
        $filter = array();

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['p.id_petugas LIKE  "%'.$keyword.'%" OR p.nama_petugas LIKE "%'.$keyword.'%"']=null;
        }

        $data = $this->mod_user->get_all($filter);

        $res[0]['id'] = "-1";
        $res[0]['text'] = lang("cmb_select_ptg");

        foreach ($data as $key => $value) {
            $res[$key+1]['id'] = $value->id;
            $res[$key+1]['text'] = $value->id." - ".str_replace("|", " ", $value->nama_petugas);
        }
        echo $calback."(".json_encode($res).")";
    }

    public function get_cmb_balance_bp()
    {
        $calback=$_GET['callback'];
        $list_titipan = $this->config->item('titipan');
        $data = explode(",", $list_titipan['type_for_bp']);
        foreach ($data as $key => $value) {
            $res[$key]['id'] = $value;
            $res[$key]['text'] = $value;
        }
        echo $calback."(".json_encode($res).")";
    }

    public function getcmbBulan()
    {
        $calback=$_GET['callback'];
        $data = array();
        for ($i=1; $i<=12; $i++) {
            $data_x['id'] 		= (strlen($i)>=2)?$i:"0".$i;
            $data_x['text'] 	= date('F', strtotime('2016-'.$i.'-01'));
            array_push($data, $data_x);
        }
        echo $calback."(".json_encode($data).")";
    }


    public function getcmbTrx()
    {
        $callback	= $_GET['callback'];
        $cs_center 	= json_decode(json_encode($this->config->item('cost_center')));
        $bp_int 	= json_decode(json_encode($this->config->item('bp_clearing_int')));
        $ar_int 	= json_decode(json_encode($this->config->item('ar_clearing_int')));
        $ar_local 	= json_decode(json_encode($this->config->item('ar_clearing')));
        $bp_local   = json_decode(json_encode($this->config->item('bp_clearing')));
        $ar_sspt    = json_decode(json_encode($this->config->item('ar_sspt')));
        $ar_sspt_int 	= json_decode(json_encode($this->config->item('ar_sspt_int')));

        $data = array(
            array('id'=> $cs_center->initial, 'text'=>'Cost Distribution'),
            array('id'=> $ar_local->initial, 'text'=>'AR Clearing'),
            array('id'=> $bp_local->initial, 'text'=>'BP Clearing'),
            array('id'=> $bp_int->initial_ar_clearing, 'text'=>'BP Clearing Interbranch'),
            array('id'=> $ar_int->initial_ar_clearing, 'text'=>'AR Clearing Interbranch'),
            array('id'=> 'ARB','text'=>'ARB Clearing' ),
            array('id'=> $ar_sspt->initial,'text'=>'PPN SPT Clearing' ),
            array('id'=> $ar_sspt_int->initial_ar_clearing,'text'=>'PPN SPT Clearing Inter Branch' )
        );
        echo $callback."(".json_encode($data).")";
    }

    public function country()
    {
        $data=array(
            array('id'=>'AD', 'text'=>'Andorran'),
            array('id'=>'AE', 'text'=>'Utd.Arab Emir.'),
            array('id'=>'AF', 'text'=>'Afghanistan'),
            array('id'=>'AG', 'text'=>'Antigua/Barbuda'),
            array('id'=>'AI', 'text'=>'Anguilla'),
            array('id'=>'AL', 'text'=>'Albania'),
            array('id'=>'AM', 'text'=>'Armenia'),
            array('id'=>'AN', 'text'=>'Dutch Antilles'),
            array('id'=>'AO', 'text'=>'Angola'),
            array('id'=>'AQ', 'text'=>'Antarctica'),
            array('id'=>'AR', 'text'=>'Argentina'),
            array('id'=>'AS', 'text'=>'Samoa, America'),
            array('id'=>'AT', 'text'=>'Austria'),
            array('id'=>'AU', 'text'=>'Australia'),
            array('id'=>'AW', 'text'=>'Aruba'),
            array('id'=>'AZ', 'text'=>'Azerbaijan'),
            array('id'=>'BA', 'text'=>'Bosnia-Herz.'),
            array('id'=>'BB', 'text'=>'Barbados'),
            array('id'=>'BD', 'text'=>'Bangladesh'),
            array('id'=>'BE', 'text'=>'Belgium'),
            array('id'=>'BF', 'text'=>'Burkina Faso'),
            array('id'=>'BG', 'text'=>'Bulgaria'),
            array('id'=>'BH', 'text'=>'Bahrain'),
            array('id'=>'BI', 'text'=>'Burundi'),
            array('id'=>'BJ', 'text'=>'Benin'),
            array('id'=>'BL', 'text'=>'Blue'),
            array('id'=>'BM', 'text'=>'Bermuda'),
            array('id'=>'BN', 'text'=>'Brunei Daruss.'),
            array('id'=>'BO', 'text'=>'Bolivia'),
            array('id'=>'BR', 'text'=>'Brazil'),
            array('id'=>'BS', 'text'=>'Bahamas'),
            array('id'=>'BT', 'text'=>'Bhutan'),
            array('id'=>'BV', 'text'=>'Bouvet Islands'),
            array('id'=>'BW', 'text'=>'Botswana'),
            array('id'=>'BY', 'text'=>'Belarus'),
            array('id'=>'BZ', 'text'=>'Belize'),
            array('id'=>'CA', 'text'=>'Canada'),
            array('id'=>'CC', 'text'=>'Coconut Islands'),
            array('id'=>'CD', 'text'=>'Dem. Rep. Congo'),
            array('id'=>'CF', 'text'=>'CAR'),
            array('id'=>'CG', 'text'=>'Rep.of Congo'),
            array('id'=>'CH', 'text'=>'Switzerland'),
            array('id'=>'CI', 'text'=>"Cote d'Ivoire"),
            array('id'=>'CK', 'text'=>'Cook Islands'),
            array('id'=>'CL', 'text'=>'Chile'),
            array('id'=>'CM', 'text'=>'Cameroon'),
            array('id'=>'CN', 'text'=>'China'),
            array('id'=>'CO', 'text'=>'Colombia'),
            array('id'=>'CR', 'text'=>'Costa Rica'),
            array('id'=>'CS', 'text'=>'Serbia/Monten.'),
            array('id'=>'CU', 'text'=>'Cuba'),
            array('id'=>'CV', 'text'=>'Cape Verde'),
            array('id'=>'CX', 'text'=>'Christmas Islnd'),
            array('id'=>'CY', 'text'=>'Cyprus'),
            array('id'=>'CZ', 'text'=>'Czech Republic'),
            array('id'=>'DE', 'text'=>'Germany'),
            array('id'=>'DJ', 'text'=>'Djibouti'),
            array('id'=>'DK', 'text'=>'Denmark'),
            array('id'=>'DM', 'text'=>'Dominica'),
            array('id'=>'DO', 'text'=>'Dominican Rep.'),
            array('id'=>'DZ', 'text'=>'Algeria'),
            array('id'=>'EC', 'text'=>'Ecuador'),
            array('id'=>'EE', 'text'=>'Estonia'),
            array('id'=>'EG', 'text'=>'Egypt'),
            array('id'=>'EH', 'text'=>'West Sahara'),
            array('id'=>'ER', 'text'=>'Eritrea'),
            array('id'=>'ES', 'text'=>'Spain'),
            array('id'=>'ET', 'text'=>'Ethiopia'),
            array('id'=>'EU', 'text'=>'European Union'),
            array('id'=>'FI', 'text'=>'Finland'),
            array('id'=>'FJ', 'text'=>'Fiji'),
            array('id'=>'FK', 'text'=>'Falkland Islnds'),
            array('id'=>'FM', 'text'=>'Micronesia'),
            array('id'=>'FO', 'text'=>'Faroe Islands'),
            array('id'=>'FR', 'text'=>'France'),
            array('id'=>'GA', 'text'=>'Gabon'),
            array('id'=>'GB', 'text'=>'United Kingdom'),
            array('id'=>'GD', 'text'=>'Grenada'),
            array('id'=>'GE', 'text'=>'Georgia'),
            array('id'=>'GF', 'text'=>'French Guayana'),
            array('id'=>'GH', 'text'=>'Ghana'),
            array('id'=>'GI', 'text'=>'Gibraltar'),
            array('id'=>'GL', 'text'=>'Greenland'),
            array('id'=>'GM', 'text'=>'Gambia'),
            array('id'=>'GN', 'text'=>'Guinea'),
            array('id'=>'GP', 'text'=>'Guadeloupe'),
            array('id'=>'GQ', 'text'=>'Equatorial Guin'),
            array('id'=>'GR', 'text'=>'Greece'),
            array('id'=>'GS', 'text'=>'S. Sandwich Ins'),
            array('id'=>'GT', 'text'=>'Guatemala'),
            array('id'=>'GU', 'text'=>'Guam'),
            array('id'=>'GW', 'text'=>'Guinea-Bissau'),
            array('id'=>'GY', 'text'=>'Guyana'),
            array('id'=>'HK', 'text'=>'Hong Kong'),
            array('id'=>'HM', 'text'=>'Heard/McDon.Isl'),
            array('id'=>'HN', 'text'=>'Honduras'),
            array('id'=>'HR', 'text'=>'Croatia'),
            array('id'=>'HT', 'text'=>'Haiti'),
            array('id'=>'HU', 'text'=>'Hungary'),
            array('id'=>'ID', 'text'=>'Indonesia'),
            array('id'=>'IE', 'text'=>'Ireland'),
            array('id'=>'IL', 'text'=>'Israel'),
            array('id'=>'IN', 'text'=>'India'),
            array('id'=>'IO', 'text'=>'Brit.Ind.Oc.Ter'),
            array('id'=>'IQ', 'text'=>'Iraq'),
            array('id'=>'IR', 'text'=>'Iran'),
            array('id'=>'IS', 'text'=>'Iceland'),
            array('id'=>'IT', 'text'=>'Italy'),
            array('id'=>'JM', 'text'=>'Jamaica'),
            array('id'=>'JO', 'text'=>'Jordan'),
            array('id'=>'JP', 'text'=>'Japan'),
            array('id'=>'KE', 'text'=>'Kenya'),
            array('id'=>'KG', 'text'=>'Kyrgyzstan'),
            array('id'=>'KH', 'text'=>'Cambodia'),
            array('id'=>'KI', 'text'=>'Kiribati'),
            array('id'=>'KM', 'text'=>'Comoros'),
            array('id'=>'KN', 'text'=>'St Kitts&Nevis'),
            array('id'=>'KP', 'text'=>'North Korea'),
            array('id'=>'KR', 'text'=>'South Korea'),
            array('id'=>'KW', 'text'=>'Kuwait'),
            array('id'=>'KY', 'text'=>'Cayman Islands'),
            array('id'=>'KZ', 'text'=>'Kazakhstan'),
            array('id'=>'LA', 'text'=>'Laos'),
            array('id'=>'LB', 'text'=>'Lebanon'),
            array('id'=>'LC', 'text'=>'St. Lucia'),
            array('id'=>'LI', 'text'=>'Liechtenstein'),
            array('id'=>'LK', 'text'=>'Sri Lanka'),
            array('id'=>'LR', 'text'=>'Liberia'),
            array('id'=>'LS', 'text'=>'Lesotho'),
            array('id'=>'LT', 'text'=>'Lithuania'),
            array('id'=>'LU', 'text'=>'Luxembourg'),
            array('id'=>'LV', 'text'=>'Latvia'),
            array('id'=>'LY', 'text'=>'Libya'),
            array('id'=>'MA', 'text'=>'Morocco'),
            array('id'=>'MC', 'text'=>'Monaco'),
            array('id'=>'MD', 'text'=>'Moldova'),
            array('id'=>'MG', 'text'=>'Madagascar'),
            array('id'=>'MH', 'text'=>'Marshall Islnds'),
            array('id'=>'MK', 'text'=>'Macedonia'),
            array('id'=>'ML', 'text'=>'Mali'),
            array('id'=>'MM', 'text'=>'Burma'),
            array('id'=>'MN', 'text'=>'Mongolia'),
            array('id'=>'MO', 'text'=>'Macau'),
            array('id'=>'MP', 'text'=>'N.Mariana Islnd'),
            array('id'=>'MQ', 'text'=>'Martinique'),
            array('id'=>'MR', 'text'=>'Mauretania'),
            array('id'=>'MS', 'text'=>'Montserrat'),
            array('id'=>'MT', 'text'=>'Malta'),
            array('id'=>'MU', 'text'=>'Mauritius'),
            array('id'=>'MV', 'text'=>'Maldives'),
            array('id'=>'MW', 'text'=>'Malawi'),
            array('id'=>'MX', 'text'=>'Mexico'),
            array('id'=>'MY', 'text'=>'Malaysia'),
            array('id'=>'MZ', 'text'=>'Mozambique'),
            array('id'=>'NA', 'text'=>'Namibia'),
            array('id'=>'NC', 'text'=>'New Caledonia'),
            array('id'=>'NE', 'text'=>'Niger'),
            array('id'=>'NF', 'text'=>'Norfolk Islands'),
            array('id'=>'NG', 'text'=>'Nigeria'),
            array('id'=>'NI', 'text'=>'Nicaragua'),
            array('id'=>'NL', 'text'=>'Netherlands'),
            array('id'=>'NO', 'text'=>'Norway'),
            array('id'=>'NP', 'text'=>'Nepal'),
            array('id'=>'NR', 'text'=>'Nauru'),
            array('id'=>'NT', 'text'=>'NATO'),
            array('id'=>'NU', 'text'=>'Niue'),
            array('id'=>'NZ', 'text'=>'New Zealand'),
            array('id'=>'OM', 'text'=>'Oman'),
            array('id'=>'OR', 'text'=>'Orange'),
            array('id'=>'PA', 'text'=>'Panama'),
            array('id'=>'PE', 'text'=>'Peru'),
            array('id'=>'PF', 'text'=>'Frenc.Polynesia'),
            array('id'=>'PG', 'text'=>'Pap. New Guinea'),
            array('id'=>'PH', 'text'=>'Philippines'),
            array('id'=>'PK', 'text'=>'Pakistan'),
            array('id'=>'PL', 'text'=>'Poland'),
            array('id'=>'PM', 'text'=>'St.Pier,Miquel.'),
            array('id'=>'PN', 'text'=>'Pitcairn Islnds'),
            array('id'=>'PR', 'text'=>'Puerto Rico'),
            array('id'=>'PS', 'text'=>'Palestine'),
            array('id'=>'PT', 'text'=>'Portugal'),
            array('id'=>'PW', 'text'=>'Palau'),
            array('id'=>'PY', 'text'=>'Paraguay'),
            array('id'=>'QA', 'text'=>'Qatar'),
            array('id'=>'RE', 'text'=>'Reunion'),
            array('id'=>'RO', 'text'=>'Romania'),
            array('id'=>'RU', 'text'=>'Russian Fed.'),
            array('id'=>'RW', 'text'=>'Rwanda'),
            array('id'=>'SA', 'text'=>'Saudi Arabia'),
            array('id'=>'SB', 'text'=>'Solomon Islands'),
            array('id'=>'SC', 'text'=>'Seychelles'),
            array('id'=>'SD', 'text'=>'Sudan'),
            array('id'=>'SE', 'text'=>'Sweden'),
            array('id'=>'SG', 'text'=>'Singapore'),
            array('id'=>'SH', 'text'=>'Saint Helena'),
            array('id'=>'SI', 'text'=>'Slovenia'),
            array('id'=>'SJ', 'text'=>'Svalbard'),
            array('id'=>'SK', 'text'=>'Slovakia'),
            array('id'=>'SL', 'text'=>'Sierra Leone'),
            array('id'=>'SM', 'text'=>'San Marino'),
            array('id'=>'SN', 'text'=>'Senegal'),
            array('id'=>'SO', 'text'=>'Somalia'),
            array('id'=>'SR', 'text'=>'Suriname'),
            array('id'=>'ST', 'text'=>'S.Tome,Principe'),
            array('id'=>'SV', 'text'=>'El Salvador'),
            array('id'=>'SY', 'text'=>'Syria'),
            array('id'=>'SZ', 'text'=>'Swaziland'),
            array('id'=>'TC', 'text'=>'Turksh Caicosin'),
            array('id'=>'TD', 'text'=>'Chad'),
            array('id'=>'TF', 'text'=>'French S.Territ'),
            array('id'=>'TG', 'text'=>'Togo'),
            array('id'=>'TH', 'text'=>'Thailand'),
            array('id'=>'TJ', 'text'=>'Tajikistan'),
            array('id'=>'TK', 'text'=>'Tokelau Islands'),
            array('id'=>'TL', 'text'=>'East Timor'),
            array('id'=>'TM', 'text'=>'Turkmenistan'),
            array('id'=>'TN', 'text'=>'Tunisia'),
            array('id'=>'TO', 'text'=>'Tonga'),
            array('id'=>'TP', 'text'=>'East Timor'),
            array('id'=>'TR', 'text'=>'Turkey'),
            array('id'=>'TT', 'text'=>'Trinidad,Tobago'),
            array('id'=>'TV', 'text'=>'Tuvalu'),
            array('id'=>'TW', 'text'=>'Taiwan'),
            array('id'=>'TZ', 'text'=>'Tanzania'),
            array('id'=>'UA', 'text'=>'Ukraine'),
            array('id'=>'UG', 'text'=>'Uganda'),
            array('id'=>'UM', 'text'=>'Minor Outl.Isl.'),
            array('id'=>'UN', 'text'=>'United Nations'),
            array('id'=>'US', 'text'=>'USA'),
            array('id'=>'UY', 'text'=>'Uruguay'),
            array('id'=>'UZ', 'text'=>'Uzbekistan'),
            array('id'=>'VA', 'text'=>'Vatican City'),
            array('id'=>'VC', 'text'=>'St. Vincent'),
            array('id'=>'VE', 'text'=>'Venezuela'),
            array('id'=>'VG', 'text'=>'Brit.Virgin Is.'),
            array('id'=>'VI', 'text'=>'Amer.Virgin Is.'),
            array('id'=>'VN', 'text'=>'Vietnam'),
            array('id'=>'VU', 'text'=>'Vanuatu'),
            array('id'=>'WF', 'text'=>'Wallis,Futuna'),
            array('id'=>'WS', 'text'=>'Samoa'),
            array('id'=>'YE', 'text'=>'Yemen'),
            array('id'=>'YT', 'text'=>'Mayotte'),
            array('id'=>'ZA', 'text'=>'South Africa'),
            array('id'=>'ZM', 'text'=>'Zambia'),
            array('id'=>'ZW', 'text'=>'Zimbabwe'),
        );

        echo json_encode(array('data'=>$data));
    }
    public function getAutoId_print($fields, $table)
    {
        $query 		= $this->db_main->query('SELECT SUBSTRING(MAX('.$fields.'),6,10) as max from '.$table);
        $result		= current($query->result());
        $number 	= 0;
        $imax		= 10;
        $tmp  		= "";

        if ($result->max !='') {
            $number = $result->max;
        }

        $number ++;
        $number = strval($number);
        for ($i=1; $i<=($imax-strlen($number)); $i++) {
            $tmp=$tmp."0";
        }

        return $tmp.$number;
    }

    public function getAutoId($fields, $table, $inisial)
    {
        $leng_inisial = strlen($inisial);
        $query 		= $this->db_main->query('SELECT MAX('.$fields.') as max from '.$table.' WHERE left('.$fields.','.$leng_inisial.')="'.$inisial.'"');
        $result		= current($query->result());
        $number 	= 0;
        $imax			= 10;
        $tmp  		= "";

        if ($result->max !='') {
            $number = substr($result->max, strlen($inisial));
        }

        $number ++;
        $number = strval($number);
        for ($i=1; $i<=($imax-strlen($inisial)-strlen($number)); $i++) {
            $tmp=$tmp."0";
        }

        return $inisial.$tmp.$number;
    }

    public function getAutoIdbyth($fields, $table, $inisial)
    {
        $leng_inisial = strlen($inisial);
        $query      = $this->db_main->query('SELECT MAX('.$fields.') as max from '.$table.' WHERE left('.$fields.','.$leng_inisial.')="'.$inisial.'" and DYR_NotifTh="'.date('Y').'"');
        $result     = current($query->result());
        $number     = 0;
        $imax           = 10;
        $tmp        = "";

        if ($result->max !='') {
            $number = substr($result->max, strlen($inisial));
        }

        $number ++;
        $number = strval($number);
        for ($i=1; $i<=($imax-strlen($inisial)-strlen($number)); $i++) {
            $tmp=$tmp."0";
        }

        return $inisial.$tmp.$number;
    }

    public function group_id($fields, $table)
    {
        $alfabet = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        $alfabet = explode(',', $alfabet);
        $size = 4;
        $current_set = array('');
        $initial_str = '';
        $number 	= 0;
        $imax			= 15; //99999999999999
        $tmp  		= "";
        $max_nub	= 99999999999;

        for ($i = 0; $i < $size; $i++) {
            $tmp_set = array();
            foreach ($current_set as $curr_elem) {
                foreach ($alfabet as $new_elem) {
                    $tmp_set[] = $curr_elem . $new_elem;
                }
            }
            $current_set = $tmp_set;
        }

        $inisial=strtoupper($current_set[0]);
        $leng_inisial = strlen($inisial);

        $query 		= $this->db_main->query('SELECT MAX('.$fields.') as max from '.$table.' WHERE left('.$fields.','.$leng_inisial.')="'.$inisial.'"');
        $result 	= current($query->result());

        if ($result->max !='') {
            $number = substr($result->max, strlen($inisial));
            $initial_str = substr($result->max, 0, $size);
        }
        $number ++;

        if ($number > $max_nub) {
            foreach ($current_set as $key => $value) {
                if ($value==$initial_str) {
                    $next_key = $key;
                }
            }
            $inisial = $$current_set[$next_key+1];
            $number = $number-$max_nub;
        }

        $number = strval($number);
        for ($i=1; $i<=($imax-strlen($inisial)-strlen($number)); $i++) {
            $tmp=$tmp."0";
        }

        return $inisial.$tmp.$number;
    }

    public function cek_permit()
    {
        $app = $this->config->item('app');
        $this->db_main->select('id_petugas,sessionid,UNIX_TIMESTAMP(NOW()) dtimenow,UNIX_TIMESTAMP(dtimeexpired) dtimeexpired');
        $this->db_main->where('id_petugas', $this->session->userdata('user').$app);
        $stmt = $this->db_main->get('session');

        if ($stmt->num_rows() <=0) {
            echo '<script type="text/javascript">
					alert("Sesi anda telah berakhir, Silakan Login kembali...");
					window.location.href = "'.site_url('user/log/out').'"; </script>';
        }
    }

    public function set_pdf($data=array(), $doc_num='', $date=false)
    {
        $this->load->library('fpdf_master');
        if ($date==false) {
            $date=date('d F Y');
        }
        $top=array(
            'logo'      => base_url().'assets/images/logo.png',
            'addres'    => 'PT Adi Sarana Armada, Tbk /n Jln. Walisongo KM 9 RT 01 RW02, Kel. Tambak Aji, Kec. Ngaliyan, /n Semarang Barat /n Tel: 024 7612333 Fax: 024 7611777',
            'title'     => 'PERMOHONAN PEMBAYARAN',
            'doc_desc'  =>array(
                'No._Dokumen'=>$doc_num,
                'Tanggal_Estimasi_Pembayaran'=>$date
            )
        );

        $this->fpdf->Header($top);

        $this->fpdf->Ln();
        $table  ='<table border="1">';
        $table .='<tr>';
        foreach ($data[0] as $key=>$row) {
            $table .='<td>'.strtoupper(str_replace('_', ' ', $key)).'</td>';
        }
        $table .='</tr>';
        foreach ($data as $key=>$row) {
            $table .='<tr>';
            foreach ($data[$key] as $x_row) {
                $table .='<td>'.$x_row.'</td>';
            }
            $table .='</tr>';
        }
        $table .='</table>';


        $this->fpdf->SetFont('Arial', '', 8);
        $this->fpdf->WriteHTML($table);
        $nomor = substr($doc_num, 6, 9);
        $this->fpdf->Output(BASEPATH."/../tmp/PRINT$nomor.pdf", "F");
    }

    function set_paste_tmp(){
        $data = $this->input->post('models');
        $type = ($this->input->post('paste_type'))?$this->input->post('paste_type'):'clear_amount';
       $this->load->model('app/app_model', 'app');
        foreach ($data as $key => $value) {
            $my_val = $value['clear_amount'];  //tgl_bukpot //numb_bukpot //clear_amount

            if($type=='tgl_bukpot'){
                $c_date =  $this->is_date($my_val);
                if(!$c_date)
                    $my_val = date('Y-m-d');
                else{
                    $d = explode('-', $my_val);
                    $my_val = $d[2].'-'.$d[1].'-'.$d[0]; 
                }
            }
            
            if($type=='clear_amount')
               $my_val =  str_replace(",", '',$my_val);

            $this->app->update_tmp(
                array('doc_number'=>$value['doc_number']),
                array($type =>$my_val)
            );
        }
    }

    function is_date($str) {
        try {
            $dt = new DateTime(trim($str));
        }catch( Exception $e ) {
            return false;
        }
        $month = $dt->format('m');
        $day = $dt->format('d');
        $year = $dt->format('Y');
        if( checkdate($month, $day, $year) ) {
            return true;
        }else {
            return false;
        }
    }

    function grid_blank(){
        echo json_encode(array());
    }

}
