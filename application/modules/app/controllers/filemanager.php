<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 
class Filemanager extends CI_Controller { 
   
  public function __construct() 
  { 
    parent::__construct(); 
    //$this->load->helper('url'); 
    $this->load->module('app'); 
    $this->app->cek_permit(); 
  } 
   
  public function index() 
  { 
    $this->template->build('app/file_manager_view'); 
  } 
   
  public function elfinder() 
  { 
    require_once './assets/elfinder/php/elFinderConnector.class.php'; 
    require_once './assets/elfinder/php/elFinder.class.php'; 
    require_once './assets/elfinder/php/elFinderVolumeDriver.class.php'; 
    require_once './assets/elfinder/php/elFinderVolumeLocalFileSystem.class.php'; 
     
    $conn = new elFinderConnector(new elFinder(array( 
      'roots'=>array( 
        array( 
          'driver'=>'LocalFileSystem', 
          'path'=>'./converter/file', 
          'URL'=>base_url('converter/file').'/', 
          'defaults'   => array('read' => true, 'write' => true),
          'disabled' => array('rename', 'rm') 
        ) 
      ) 
    ))); 
    $conn->run(); 
  } 
} 
 
/* End of file welcome.php */ 
/* Location: ./application/controllers/welcome.php */