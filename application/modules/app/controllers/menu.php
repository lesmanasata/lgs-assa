<?php if (! defined('BASEPATH')) { 
    exit('No direct script access allowed'); 
} 
 
class Menu extends MY_Controller 
{ 
    public function __construct() 
    { 
        parent::__construct(); 
        $this->lang->load('language', 'english'); 
    } 
 
    public function showMenu() 
    { 
        $cek_menu = $this->config->item('app_theme'); 
        $iclass=false; 
 
        if ($cek_menu == 'mouldifi') { 
            foreach ($this->config->item('list_menu') as $link=>$caption) { 
                if ($iclass==false && uri_string()==$link) { 
                    $iclass=true; 
                    $aclass='active'; 
                } else { 
                    $aclass=''; 
                } 
                $user = $this->session->userdata('level'); 
                if ($user == USER_ADMIN) { 
                    if (in_array($user, $caption[1])) { 
                        if (!isset($caption[2])) { 
                            echo '<li class="'.$aclass.'">'.anchor($link, '<i class="icon-gauge"></i><span class="title">'.lang($caption[0]).'</span>').'</li>'; 
                        } else { 
                            echo '<li class="has-sub"><a href="#"><i class="'.$caption[3].'"></i><span class="title">'.lang($caption[0]).'</span></a>'; 
                            echo '<ul class="nav collapse">'; 
                            foreach ($caption[2] as $slink=>$scaption) { 
                                if (in_array($user, $scaption[1])) { 
                                    echo '<li>'.anchor($slink, '<i class="fa fa-sign-in nav_icon"></i>'.lang($scaption[0]), array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                } 
                            } 
                            echo '</ul></li>'; 
                            if ($caption[0]=='') { 
                                foreach ($caption[2] as $slink=>$scaption) { 
                                    if (in_array($user, $scaption[1])) { 
                                        echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                    } 
                                } 
                            } 
                        } 
                    } 
                } else { 
                    if (in_array($user, $caption[1])) { 
                        if (!isset($caption[2])) { 
                            echo '<li class="'.$aclass.'">'.anchor($link, '<i class="icon-gauge"></i><span class="title">'.lang($caption[0]).'</span>').'</li>'; 
                        } else { 
                            $mod_ses =$this->session->userdata('module'); 
                            $ses_send = explode(",", $mod_ses); 
 
                            foreach ($ses_send as $key => $value) { 
                                if (array_key_exists($value, $caption[2])) { 
                                    echo '<li class="has-sub"><a href="#"><i class="'.$caption[3].'"></i><span class="title">'.lang($caption[0]).'</span></a>'; 
                                    echo '<ul class="nav collapse">'; 
                                    foreach ($caption[2] as $slink=>$scaption) { 
                                        if (in_array($user, $scaption[1])) { 
                                            echo '<li>'.anchor($slink, '<i class="fa fa-sign-in nav_icon"></i>'.lang($scaption[0]), array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                        } 
                                    } 
                                    echo '</ul></li>'; 
                                    if ($caption[0]=='') { 
                                        foreach ($caption[2] as $slink=>$scaption) { 
                                            if (in_array($user, $scaption[1])) { 
                                                echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                            } 
                                        } 
                                    } 
                                } 
                            } 
                        } 
                    } 
                } 
            } 
        } 
        else { 
            echo '<ul class="nav" id="side-menu">'; 
            foreach ($this->config->item('list_menu') as $link=>$caption) { 
                if ($iclass==false && uri_string()==$link) { 
                    $iclass=true; 
                    $aclass='active'; 
                } else { 
                    $aclass=''; 
                } 
                $user = $this->session->userdata('level'); 
                if ($user == USER_ADMIN) { 
                    if (in_array($user, $caption[1])) { 
                        if (!isset($caption[2])) { 
                            echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                        } else { 
                            echo '<li><a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">'.lang($caption[0]).'</span><span class="fa arrow"></span></a>'; 
                            echo '<ul class="nav nav-second-level">'; 
                            foreach ($caption[2] as $slink=>$scaption) { 
                                if (in_array($user, $scaption[1])) { 
                                    echo '<li>'.anchor($slink, '<i class="fa fa-sign-in nav_icon"></i>'.lang($scaption[0]), array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                } 
                            } 
                            echo '</ul></li>'; 
                            if ($caption[0]=='') { 
                                foreach ($caption[2] as $slink=>$scaption) { 
                                    if (in_array($user, $scaption[1])) { 
                                        echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                    } 
                                } 
                            } 
                        } 
                    } else { 
                        if (in_array($user, $caption[1])) { 
                            if (!isset($caption[2])) { 
                                echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                            } else { 
                                $mod_ses =$this->session->userdata('module'); 
                                $ses_send = explode(",", $mod_ses); 
 
                                foreach ($ses_send as $key => $value) { 
                                    if (array_key_exists($value, $caption[2])) { 
                                        echo '<li><a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">'.lang($caption[0]).'</span><span class="fa arrow"></span></a>'; 
                                        echo '<ul class="nav nav-second-level">'; 
                                        foreach ($caption[2] as $slink=>$scaption) { 
                                            if (in_array($user, $scaption[1])) { 
                                                echo '<li>'.anchor($slink, '<i class="fa fa-sign-in nav_icon"></i>'.lang($scaption[0]), array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                            } 
                                        } 
                                        echo '</ul></li>'; 
                                        if ($caption[0]=='') { 
                                            foreach ($caption[2] as $slink=>$scaption) { 
                                                if (in_array($user, $scaption[1])) { 
                                                    echo '<li class="'.$aclass.'">'.anchor($link, '<i class="fa fa-dashboard nav_icon "></i><span class="nav-label">'.lang($caption[0]).'</span>', array('class'=>'hvr-bounce-to-right')).'</li>'; 
                                                } 
                                            } 
                                        } 
                                    } 
                                } 
                            } 
                        } 
                    } 
                } 
            } 
            echo '</ul>'; 
        } 
    } 
} 