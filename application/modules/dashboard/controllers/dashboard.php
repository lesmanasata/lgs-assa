<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	var $fild_comp_code 		= 'CHR_BUKRS';
	var $fild_clearing_doc_no	= 'VCH_AUGBL'; 
	var $fild_posting_key		= 'VCH_BSCHL';
	var $fild_doc_type			= 'ENM_BLART';
	var $cfg_Clearing			= array();
	var $cfg_arClearing 		= array();

	public function __construct(){
        parent::__construct();
		$this->load->module('app');
		$this->app->cek_permit();
    }

	public function index()
	{
		$this->cek_build_access(explode(',',ALLOWED_USER));
		$userId 	= $this->session->userdata('limit_id');
		$customer	= array();
		$admin		= array();
		$data['auser'] = $this->get_aktif_user();
		$this->template->build('dashboard/dashboard',$data);
	}

	function get(){
		$this->load->model('master/master_model','mod_mst');
		$cfg_dash 	= array('ar','bp','arint','bpint');
		$fild='COUNT(d.'.$this->fild_comp_code.') as total';

		foreach ($cfg_dash as $key => $value) {
			if($value =='ar'){
				$this->cfg_Clearing = json_decode(json_encode($this->config->item('ar_clearing')));
				unset($filter);
				$filter['h.'.$this->fild_doc_type]= $this->cfg_Clearing->type_doc_in;
				$filter['d.'.$this->fild_clearing_doc_no.' IS NULL ']=NULL;
				$filter['d.'.$this->fild_posting_key]=$this->cfg_Clearing->get_postkey_in;
			}

			if($value =='bp'){
				$this->cfg_Clearing = json_decode(json_encode($this->config->item('bp_clearing')));
				unset($filter);
				$filter['h.'.$this->fild_doc_type]= $this->cfg_Clearing->type_doc_in;
				$filter['d.'.$this->fild_clearing_doc_no.' IS NULL ']=NULL;
				$filter['d.'.$this->fild_posting_key]=$this->cfg_Clearing->get_postkey_in;
			}

			if($value=='arint'){
				$this->cfg_Clearing = json_decode(json_encode($this->config->item('ar_clearing_int')));
				unset($filter);
				$filter['h.' . $this->fild_doc_type] = $this->cfg_Clearing->cf_bank_doc_type;
		        $filter['d.' . $this->fild_clearing_doc_no . ' IS NULL '] = NULL;
		        $filter['d.' . $this->fild_posting_key] = $this->cfg_Clearing->cf_bank_key;
			}

			if($value=='bpint'){
				unset($filter);
				$this->cfg_arClearing = json_decode(json_encode($this->config->item('bp_clearing_int')));
				$filter['h.' . $this->fild_doc_type] = $this->cfg_arClearing->cf_bank_doc_type;
		        $filter['d.' . $this->fild_posting_key] = $this->cfg_arClearing->cf_bank_key;
			}
					//$this->mod_mst->get_join($fild, $filter);
			$total = current($this->mod_mst->get_join($fild,$filter));

			$res[] = array('id'=>$value,'total'=>$total->total);
		}

		echo json_encode($res);
	}

	function get_aktif_user(){
		$res 	= array();
		$filter = array();
		$this->load->model('user/user_model','mod_user');
		
		if($this->input->post('id') !='')
			$filter['p.id_petugas IN("'.implode('","',explode(',',$this->input->post('id'))).'")']=NULL;
		
		$res = $this->mod_user->get_all($filter);
		
		return $res;
	}

}
