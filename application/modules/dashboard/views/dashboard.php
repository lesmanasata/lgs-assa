<?php

 
$pesan ='';
$time  = date('Hi');
if($time < 1200)
	$pesan ="Good morning";
else if($time<=1400)
	$pesan='Good Afternoon';
elseif($time<=1800)
	$pesan='A Good Afternoon';
elseif($time<=2400)
	$pesan='Good night';
?>
<div class="blank">
	<header class="jumbotron hero-spacer" style="padding: 15px;">
		<h1>Welcome... </h1>
		<p><?php echo $pesan." ".str_replace("|"," ",$this->session->userdata('real_name')); ?></p>
	</header>
</div>


<?php 
	$arcek= explode(',', $this->session->userdata('module'));
	if(in_array('clearing/ar', $arcek) || $this->session->userdata('level')==USER_ADMIN){ 
?>
<style type="text/css"> .huge{font-size:40px;} </style>
	<div class="blank">
	<?php 
		$cfg_color 	= array('panel-primary','panel-info','panel-warning','panel-danger');
		$cfg_dash 	= array('ar','bp','arint','bpint');
		foreach ($cfg_dash as $key => $value) {
	?>
		<div class="col-lg-3 col-md-6">
			<div class="panel <?php echo $cfg_color[$key]; ?>">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-tasks fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge value" id="value_get_<?php echo $value; ?>">0</div>
							<div><?php echo lang('titile_'.$value.'_form'); ?></div>
						</div>
					</div>
				</div>
				<a href="<?php echo site_url('clearing/'.$value); ?>">
					<div class="panel-footer">
						<span class="pull-left"><?php echo lang('dash_view_dtl'); ?></span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	<?php } ?>
	<div style="clear:both;"></div>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url:'<?php echo site_url('dashboard/get'); ?>',
			type:'POST',
			dataType:'json',tail:1,
			beforeSend:function(){
				$('.value').addClass('loader');
			},
			success:function(res){
				$(jQuery.parseJSON(JSON.stringify(res))).each(function() {  
					var id = this.id;
         			var total = this.total;
         			$("#value_get_"+id).html(total);
				});
			}
		}).done(function(){
			$('.value').removeClass('loader');
		});
	});
</script>

<?php } ?>