<?php
class Notif_model extends CI_Model
{

    public $tbl_notif           = 'TF_NOTIFICATION';
    public $tbl_maintenace_code = 'MF_MAINTENANCE_CODE';
    public $tbl_equipment       = 'MF_EQUIPMENT';
    public $tbl_loc             = 'MF_FUNCLOC';
    public $tbl_plane           = 'MF_PLANT_DESTINATION';

    public function __construct()
    {
        parent::__construct();
        $this->db_main = $this->load->database('default', true);
    }

    function notification_add($data)
    {
        $this->db_main->insert(
            $this->tbl_notif,
            $data
        );
        if ($this->db_main->affected_rows()> 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_maintenace_code($fild='*',$filter=array(),$group=false)
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_maintenace_code);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);

        $res=$this->db_main->get();

        return $res->result();
    }

    function get_equipment($fild='*',$filter=array(),$group=false) 
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_equipment);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);
        $this->db_main->limit(20);
        $res=$this->db_main->get();

        return $res->result();
    }

    function get_loc($fild='*',$filter=array(),$group=false) 
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_loc);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);
        $this->db_main->limit(20);
        $res=$this->db_main->get();

        return $res->result();
    }

    function get_plane_dest($fild='*',$filter=array(),$group=false) 
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_plane);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter);
        }

        if($group != false)
            $this->db_main->group_by($group);
        $this->db_main->limit(20);
        $res=$this->db_main->get();

        return $res->result();
    }

    function get_notif($fild='*',$filter=array(),$group=false) 
    {
        $this->db_main->select($fild, false)
            ->from($this->tbl_notif);

        if ($filter !=false && count($filter)>0) {
            $this->db_main->where($filter,FALSE);
        }

        if($group != false)
            $this->db_main->group_by($group);
        $this->db_main->limit(20);
        $res=$this->db_main->get();
       
        return $res->result();
    }
}