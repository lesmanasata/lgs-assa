<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notif extends MY_Controller {
    
    var $insert_disposal        = "VCH_Description,VCH_Equipment1,VCH_UserName,VCH_Position,VCH_Departement,VCH_PlantDestination,VCH_Type,VCH_Coding,VCH_LokasiFisikUnit,VCH_Created_by";
    var $insert_mutasi          = "DTM_Mutation,VCH_Description,FLT_Distance,VCH_FuncLoc,VCH_Equipment1,VCH_UserName,VCH_Position,VCH_Departement,VCH_DriverName,VCH_Address,VCH_TelephoneNo,VCH_Type,VCH_Coding";
    var $insert_ganti           = "VCH_Description,FLT_Distance,VCH_Equipment1,VCH_Equipment2,VCH_Type,VCH_Coding,VCH_Address,VCH_UserName,VCH_TelephoneNo,DTM_RequiredStart,DTM_RequiredEnd";
    var $insert_tugas           = "VCH_Description,FLT_Distance,VCH_Equipment1,VCH_Type,VCH_Coding,VCH_UserName,VCH_Position,VCH_Address,VCH_TelephoneNo,DTM_RequiredStart,DTM_RequiredEnd";
    var $insert_compliment      = "VCH_Description,FLT_Distance,VCH_Equipment1,VCH_Type,VCH_Coding,VCH_UserName,VCH_Position,VCH_Address,VCH_TelephoneNo,DTM_RequiredStart,DTM_RequiredEnd";
    var $insert_other           = "VCH_Description,FLT_Distance,VCH_Equipment1,VCH_Type,VCH_Coding,VCH_UserName,VCH_Position,VCH_Address,VCH_TelephoneNo,DTM_RequiredStart,DTM_RequiredEnd";
	
    var $cfg_notif		 		= array();
    var $tbl_maintenace_code    = 'MF_MAINTENANCE_CODING';
	var $tbl_notification   	= 'TF_NOTIFICATION';

	public function __construct(){
        parent::__construct();
		$this->load->module('app');
		$this->load->model('notif/notif_model','mod_ntf');
		$this->app->cek_permit();
		$this->cfg_notif = json_decode(json_encode($this->config->item('notif_cfg')));
    }

    function disposal(){
        
        $objek = 'type,'.$this->insert_disposal;
       	$data['type'] = 'disposal';
       	$data['form'] = explode(',',$objek);

    	$this->get_form($data);
    }

    function mutasi(){
        $objek = 'type,'.$this->insert_mutasi;
        $data['type'] = 'mutasi';

        $data['form'] = explode(',',$objek);

        $this->get_form($data);
    }

    function ganti(){
    	$objek = 'type,'.$this->insert_ganti;
        $data['type'] = 'replacement';

        $data['form'] = explode(',',$objek);

        $this->get_form($data);
    }

    function tugas(){
    	$objek = 'type,'.$this->insert_tugas;
        $data['type'] = 'tugas';

        $data['form'] = explode(',',$objek);

        $this->get_form($data);
    }

    function compli(){
    	$objek = 'type,'.$this->insert_mutasi;
        $data['type'] = 'compliment';

        $data['form'] = explode(',',$objek);

        $this->get_form($data);
    }

    function other(){
    	$objek = 'type,'.$this->insert_other;
        $data['type'] = 'other';

        $data['form'] = explode(',',$objek);

        $this->get_form($data);
    }

    function list(){
        $data['type']   = 'list';
        $token          = time();
        $get_name       = array('VCH_Type','VCH_Coding','find_bulan','find_year');

        
        $data['token']  = $token;
        foreach($get_name as $k=>$row)
            $data['enc_form'][$row] = md5($row.$token);

        $arCmb = array(
            array('name' => $data['enc_form']['VCH_Type'], 'url' => 'notif/cmb_jenis/'.$data['type'], 'flow' =>$data['enc_form']['VCH_Coding']),
            array('name' => $data['enc_form']['VCH_Coding'], 'url' => 'notif/cmb_jenis/'.$data['type'].'/coding', 'flow' =>''),
        );

        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);

    	$this->template->build('notif/list',$data);
    }

    function showdata(){
        $token  = $this->input->post('token');
        $jenis  = $this->input->post(md5('VCH_Type'.$token));
        $coding = $this->input->post(md5('VCH_Coding'.$token));
        $periode= $this->input->post(md5('find_year'.$token));
        $bln    = $this->input->post(md5('find_bulan'.$token));

        $data['res'] = array(
            'jenis'=>$jenis,
            'coding'=>$coding,
            'tahun'=>$periode,
            'bulan'=>$bln
        );
        $this->load->view('notif/list_tbl',$data);
    }

    function get_showdata(){
        $this->load->model("notif/notif_model", "mod_ntf");
        $calback=$_GET['callback'];

        $filds  = 'VCH_Notifid as doc_web,';
        $filds .= 'IFNULL(VCH_NotificationId,"-") as doc_sap,';
        $filds .= 'VCH_Type as doc_type,';
        $filds .= 'VCH_Coding as doc_coding,';
        $filds .= 'VCH_Description as description,';
        $filds .= 'created_at as create_date';

        $filter['DYR_NotifTh']= $this->input->post('tahun');

        if($this->input->post('jenis') !='')
            $filter['VCH_Type']= $this->input->post('jenis');

        if($this->input->post('coding') !='')
            $filter['VCH_Coding']= $this->input->post('coding');
        
        $filter['month(created_at)']= $this->input->post('bulan');

        $data = $this->mod_ntf->get_notif($filds,$filter);

        echo $calback."(".json_encode($data).")";
    }

    public function submit(){
    	$this->load->model("notif/notif_model", "mod_ntf");
        $out['status']  = FALSE;
        $token 			= $this->input->post('token');
        $type			= $this->input->post(md5('type'.$token));
        $filds_add		= array();
        $notifId        = $this->app->getAutoIdbyth('VCH_Notifid', $this->tbl_notification, 'NFT');

        if($type=='disposal')
          $filds_add = explode(",", $this->insert_disposal);

        if($type=='mutasi')
            $filds_add = explode(",", $this->insert_mutasi);

        if($type=='replacement')
            $filds_add = explode(",", $this->insert_ganti);

        if($type=='tugas')
            $filds_add = explode(",", $this->insert_tugas);

        if($type=='compliment')
            $filds_add = explode(",", $this->insert_compliment);

        if($type=='other')
            $filds_add = explode(",", $this->insert_other);


        $data=array();
       foreach($filds_add as $key => $value)
          $data[$value] = $this->input->post(md5($value.$token));

        if(in_array('DTM_RequiredStart', $filds_add))
          $data['DTM_RequiredStart']   = date('Y-m-d H:i:s');

        if(in_array('DTM_RequiredEnd', $filds_add))
          $data['DTM_RequiredEnd']   = date('Y-m-d H:i:s');
        
        
        if($type=='mutasi'){
            $data['VCH_User_Status']    = '10';
            $data['VCH_ProcessStatus']  = 'Being processed';
        }

        if($type=='disposal')
            $data['VCH_User_Status']    = '1';

        $data['VCH_Notifid']          = $notifId;
        $data['DYR_NotifTh']          = date('Y');
        $data['VCH_Created_by']       = $this->session->userdata('user');
        $data['updated_at']           = date('Y-m-d H:i:s');

       
        $res = $this->mod_ntf->notification_add($data);
        $out['status']  = $res;
        if($res)
        	$out['messages']  = lang('msg_success_save_record')."<b> Id ".$notifId."</b>";
        else
        	$out['messages']  = lang('msg_failed_save_record');

        echo json_encode($out);
    }


    private function get_form($data){
    	$this->cek_build_access(explode(',',ALLOWED_USER));
		$this->template->title('Notification');
        $get_name 	= $data['form'];
    	$token 		= time();

        $data['token'] = $token;
        foreach($get_name as $k=>$row)
            $data['enc_form'][$row] = md5($row.$token);

        $arCmb = array(
            array('name' => $data['enc_form']['VCH_Type'], 'url' => 'notif/cmb_jenis/'.$data['type'], 'flow' => $data['enc_form']['VCH_Coding']),
            array('name' => $data['enc_form']['VCH_Coding'], 'url' => 'notif/cmb_jenis/'.$data['type'].'/coding', 'flow' =>'')
        );

        if(array_key_exists('VCH_FuncLoc', $data['enc_form']))
            $arCmb[] = array('name' => $data['enc_form']['VCH_FuncLoc'], 'url' => 'notif/get_cmb_func_loc','flow' =>'');

        $data['js_cmb'] = $this->app->dropdown_kendo($arCmb);

		$this->template->build('notif/main',$data);
    }

    public function cmb_equipment()
    {
    	$this->load->model("notif/notif_model", "mod_ntf");
        $calback=$_GET['callback'];
        $filter = array();

        if (isset($_GET['filter']['filters'])) {
            $setFilter = $_GET['filter']['filters'];
            $keyword = $setFilter[0]['value'];
            $filter['VCH_EQUIPMENT LIKE  "%'.$keyword.'%" OR VCH_SortField LIKE "%'.$keyword.'%"']=null;
        }

        $fild 	= "VCH_EQUIPMENT as id, CONCAT(VCH_EQUIPMENT,' | ',VCH_SortField) as text";

        $res = $this->mod_ntf->get_equipment($fild,$filter);
        echo $calback."(".json_encode($res).")";
    }

    public function get_func_loc(){
        $this->load->model("notif/notif_model", "mod_ntf");
        $id = $this->input->post('id');

        $fild   = "VCH_Description as descrip, VCH_FuncLoc as loc";
        $filter['VCH_EQUIPMENT'] = $id;

        $res = $this->mod_ntf->get_equipment($fild,$filter);
        if($id=='')
            $res = array(array('id'=>'','text'=>''));
        echo json_encode($res);
    }

    public function get_cmb_func_loc(){
        $this->load->model("notif/notif_model", "mod_ntf");
        $res        = array();
        $fild   = "VCH_FunlocID as id, VCH_FunlocID as text";
        $res = $this->mod_ntf->get_loc($fild,array());
        echo json_encode(array('data'=>$res)); 
    }

    public function cmb_plane(){
        $this->load->model("notif/notif_model", "mod_ntf");
        $group = $this->input->post('group');
        
        if($group !='FALSE')

        echo json_encode(array('data'=>$res)); 
    }

    public function cmb_jenis()
    {
        $this->load->model("notif/notif_model", "mod_ntf");
        $type 		= $this->uri->segment(3);
        $type_item	= $this->uri->segment(4);
        $filter		= array(); 
        $group 		= FALSE;
        $id 		= $this->input->post('id');
        $res 		= array();


        if($type_item==''){
          $fild = "VCH_Type as id, VCH_CodingGroup as text";

          if($type=='disposal')
            $filter['VCH_CodingGroup'] = $this->cfg_notif->disposal;

            if($type=='mutasi')
                $filter['VCH_CodingGroup'] = $this->cfg_notif->mutasi;

            if($type=='replacement')
                $filter['VCH_CodingGroup'] = $this->cfg_notif->ganti_sementara;

            if($type=='tugas')
                $filter['VCH_CodingGroup'] = $this->cfg_notif->tugas_kantor;

            if($type=='compliment')
                $filter['VCH_CodingGroup'] = $this->cfg_notif->comliment;

            if($type=='other')
                $filter['VCH_CodingGroup'] = $this->cfg_notif->other;
          $group = 'VCH_CodingGroup';
        }


        if($type_item=='coding' && $id !=''){
        	$fild = "VCH_Coding as id, CONCAT(VCH_Coding,' | ',VCH_Desc) as text";
        	$filter['VCH_Type'] = $id;
        }

        if($fild !='')
       		$res = $this->mod_ntf->get_maintenace_code($fild, $filter,$group);
        echo json_encode(array('data'=>$res));
    }
}