<h1 class="page-title">Notification - <?php echo ucfirst(strtolower($type)); ?></h1>
<ol class="breadcrumb breadcrumb-2"> 
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li> 
  <li><a href="#">Notification</a></li> 
  <li class="active"><strong><?php echo $type; ?></strong></li> 
</ol>


<div class="grid-form">
  <div class="grid-form1">
      <form class="form-horizontal" id="Frm_notifikasi_<?php echo $token; ?>" method="POST">
        <input type="hidden" name='<?php echo $enc_form['type']; ?>' value="<?php echo $type; ?>">
        <div class="col-md-6">
          
 
          <?php if(in_array('VCH_Type', $form)){?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Jenis :</label>
              <div class="col-sm-4">
                <input type="text" class="kendodropdown form-control" name="<?php echo $enc_form['VCH_Type']; ?>" style="width: 100%">
              </div>
              <?php if(in_array('VCH_Coding', $form)) {?> 
                <div class="col-sm-5">
                    <input type="text" class="kendodropdown form-control" name="<?php echo $enc_form['VCH_Coding']; ?>" style="width: 100%">
                </div>
              <?php } ?>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_Equipment1', $form)) {?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> Equipment</label>
              <div class="col-sm-9">
                <input type="text" class="form-control equpment"  name="<?php echo $enc_form['VCH_Equipment1']; ?>" style="width: 100%">
              </div>
            </div>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> &nbsp; </label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  id="eq_desc" placeholder="Description" readonly="">
              </div>
            </div>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> &nbsp; </label> 
              <div class="col-sm-9">
                <input type="text" class="form-control" id="eq_loc"   placeholder="Functional Loc" readonly="">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_FuncLoc', $form)) { ?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> Functional Loc</label> 
              <div class="col-sm-9">
                <input type="text" class="kendodropdown form-control" name="<?php echo $enc_form['VCH_FuncLoc']; ?>" style="width: 100%" />
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_Description', $form)) { ?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> Description</label>
              <div class="col-sm-9">
                <textarea class="form-control" name="<?php echo $enc_form['VCH_Description']; ?>" placeholder="Description"></textarea>
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('FLT_Distance', $form)) { ?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> KM</label>
              <div class="col-sm-5">
                <input type="number" class="form-control" name="<?php echo $enc_form['FLT_Distance']; ?>" placeholder="Km"/>
              </div>
            </div>
          <?php } ?>
        </div>

        <div class="col-md-6">
          <?php if(in_array('VCH_Equipment2', $form)) {?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> Equipment</label>
              <div class="col-sm-9">
                <input type="text" class="form-control equpment2"  name="<?php echo $enc_form['VCH_Equipment2']; ?>" style="width: 100%">
              </div>
            </div>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> &nbsp; </label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  id="eq_desc2" placeholder="Description" readonly="">
              </div>
            </div>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> &nbsp; </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="eq_loc2"   placeholder="Functional Loc" readonly="">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_UserName', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Nama Penerima:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_UserName']; ?>" placeholder="Nama Penerima">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_Position', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Position Penerima:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_Position']; ?>" placeholder="Posisi">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_Departement', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Department:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_Departement']; ?>" placeholder="Departement">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_DriverName', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Nama Driver:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_DriverName']; ?>" placeholder="Driver">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_Address', $form)) { ?>
            <div class="form-group">
              <label  class="col-sm-3 control-label hor-form"> Alamat </label>
              <div class="col-sm-9">
                <textarea class="form-control" name="<?php echo $enc_form['VCH_Address']; ?>" placeholder="Alamat"></textarea>
              </div>
            </div>
          <?php } ?> 

          <?php if(in_array('VCH_TelephoneNo', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >No. Telp:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_TelephoneNo']; ?>" placeholder="No. Telp">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('VCH_LokasiFisikUnit', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Lok. Fisik unit:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"  name="<?php echo $enc_form['VCH_LokasiFisikUnit']; ?>" placeholder="Lokasi Fisik Unit">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('DTM_Mutation', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Tgl Mutasi:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control datepicker"  name="<?php echo $enc_form['DTM_Mutation']; ?>" placeholder="Tgl Mutasi">
              </div>
            </div>
          <?php } ?>

          <?php if(in_array('DTM_RequiredStart', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Tgl Mulai:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control datepicker"  name="<?php echo $enc_form['DTM_RequiredStart']; ?>" placeholder="Tgl Mulai">
              </div>
            </div>
          <?php } ?>
 
          <?php if(in_array('DTM_RequiredEnd', $form)) {?>
            <div class="form-group">
              <label class="control-label col-sm-3" >Tgl Selesai:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control datepicker"  name="<?php echo $enc_form['DTM_RequiredEnd']; ?>" placeholder="Tgl Selesai">
              </div>
            </div>
          <?php } ?>

        </div>

        <div style="clear:both; ">&nbsp;</div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
            <button type="submit" class="btn btn-primary"><?php echo lang('btn_save'); ?></button>
            <button type="button" class="btn btn-danger"><?php echo lang('btn_cancel'); ?></button>
            </div>
          </div>
        </div>
      </form>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  <?php echo $js_cmb; ?>



  $('.equpment').kendoDropDownList({
    filter: "contains",
    autoBind: true,
    dataTextField: "text",
    dataValueField: "id",
    optionLabel: "Please Select...",
    dataSource: {
      serverFiltering: true,
      transport: {
      read: {
        dataType: "jsonp",
        url: "<?php echo site_url('notif/cmb_equipment'); ?>",
      }
      }
    },
    select:function(e){
      var dataItem = this.dataItem(e.item);
      $.ajax({
        url:'<?php echo site_url('notif/get_func_loc'); ?>',
        type:'POST',
        dataType: 'json',tail:1,
        data:'id='+ dataItem.id,
        success: function(res) {
            $('#eq_desc').val(res[0].descrip);
            $('#eq_loc').val(res[0].loc);
        }
      });
    }
  });

   <?php if(in_array('VCH_Equipment2', $form)) {?>
 
    $('.equpment2').kendoDropDownList({
    filter: "contains",
    autoBind: false,
    dataTextField: "text",
    dataValueField: "id",
    optionLabel: "Please Select...",
    dataSource: {
      serverFiltering: true,
      transport: {
      read: {
        dataType: "jsonp",
        url: "<?php echo site_url('notif/cmb_equipment'); ?>",
      }
      }
    },
    select:function(e){
      var dataItem = this.dataItem(e.item);
      $.ajax({
        url:'<?php echo site_url('notif/get_func_loc'); ?>',
        type:'POST',
        dataType: 'json',tail:1,
        data:'id='+ dataItem.id,
        success: function(res) {
            $('#eq_desc2').val(res[0].descrip);
            $('#eq_loc2').val(res[0].loc);
        }
      });
    }
  });
  <?php } ?>
 

  $("#Frm_notifikasi_<?php echo $token; ?>").submit(function(e){
    e.preventDefault();
    $.ajax({
        url: '<?php echo site_url('notif/submit'); ?>',
        type: 'POST',
        data: $(this).serialize()+"&token=<?php echo $token; ?>",
        dataType: 'json',tail:1,
        beforeSend:function(){
          $('#loading-mask').addClass('loader-mask');
        },
        success: function(res) {
            if(res.status){
              <?php foreach($form as $row){ ?>
                <?php if($row =='type'){?>
                  $('#Frm_notifikasi_<?php echo $token; ?> input[name=<?php echo $enc_form[$row]; ?>]').val('<?php echo $type; ?>');
                <?php }else if($row =='VCH_Description'){ ?>
                  $('#Frm_notifikasi_<?php echo $token; ?> textarea[name=<?php echo $enc_form[$row]; ?>]').val('');
                <?php }else if($row=='VCH_Type'){ ?>   
                  cmb['<?php echo $enc_form[$row]; ?>'].value('');
                <?php }else if($row=='VCH_Coding'){ ?>
                  cmb['<?php echo $enc_form[$row]; ?>'].value('');
                <?php }else if($row=='VCH_FuncLoc'){ ?>
                  cmb['<?php echo $enc_form[$row]; ?>'].value('');
                <?php }else{ ?>
                  $('#Frm_notifikasi_<?php echo $token; ?> input[name=<?php echo $enc_form[$row]; ?>]').val('');
                <?php } ?>
              <?php } ?>
              $('#eq_desc').val('');
              $('#eq_loc').val('');

              <?php if(in_array('VCH_Equipment1', $form)) {?>
                
                $('#eq_desc').val('');
                $('#eq_loc').val('');
              <?php } ?>

              <?php if(in_array('VCH_Equipment2', $form)) {?>
                $('#eq_desc2').val('');
                $('#eq_loc2').val('');
              <?php } ?>
            }
            msg_box(res.messages, ['btnOK'], 'Info!');
        }
    }).done(function() {
       $('#loading-mask').removeClass('loader-mask');
    });
  });



});
</script>