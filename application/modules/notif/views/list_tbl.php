<?php $timestamp = time();?>
<div id="notif_list<?php echo $timestamp; ?>"></div>

<script type="text/javascript">
	var no_urut=0;
	$(document).ready(function () {

		var ds_notif = new kendo.data.DataSource({
			serverFiltering: true,
			transport: {
				read: {
					type:'POST',
					dataType: "jsonp",
					data:<?php echo json_encode($res); ?>,
					url: '<?php echo site_url('notif/get_showdata'); ?>',
				}
			},
			/*schema: {
				parse: function(response){
					return response.data;
				},
				model: {
					fields: {
						 amount_item: { type: "number"},
					 }
				}
			},*/
		});

		$("#notif_list<?php echo $timestamp; ?>").kendoGrid({
			dataSource: ds_notif,
			resizable: true,
			scrollable: true,
			pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
			columns: [ 
				{field:"doc_web",width: 150, title:"DOC Web",filterable: false},
				{field:"doc_sap",width: 150, title:"DOC SAP",filterable: false},
				{field:"doc_type",width: 150, title:"DOC Type",filterable: false},
				{field:"doc_coding",width: 150, title:"Coding",filterable: false},
				{field:"description",width: 150, title:"Description",filterable: false},
				{field:"create_date",width: 150, title:"Date Create",filterable: false}

			]
		});

	});
</script>
