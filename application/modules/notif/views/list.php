<h1 class="page-title">Notification - <?php echo ucfirst(strtolower($type)); ?></h1>
<ol class="breadcrumb breadcrumb-2"> 
  <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i>Dashboard</a></li> 
  <li><a href="#">Notification</a></li> 
  <li class="active"><strong><?php echo $type; ?></strong></li> 
</ol>

<div class="grid-form">
  	<div class="grid-form1">
      	<form class="form-horizontal" id="list_notifikasi_<?php echo $token; ?>" method="POST">
       		<div class="col-md-6">
       			<div class="form-group">
              		<label class="control-label col-sm-3" >Jenis :</label>
              		<div class="col-sm-9">
                		<input type="text" class="kendodropdown form-control" name="<?php echo $enc_form['VCH_Type']; ?>" style="width: 100%">
              		</div>
              	</div>
              	<div class="form-group">
              		<label class="control-label col-sm-3" >Coding :</label>
              		<div class="col-sm-9">
                		<input type="text" class="kendodropdown form-control" name="<?php echo $enc_form['VCH_Coding']; ?>" style="width: 100%">
              		</div>
              	</div>

              	<div class="form-group" >
              		<label class="control-label col-lg-3" >Periode :</label>
			    	<div class="col-sm-4">
			        	<input type="number" onkeypress='numberOnly(event)' name="<?php echo $enc_form['find_year']; ?>" class="form-control" value="<?php echo date('Y'); ?>" placeholder="<?php echo lang('rpt_find_yer'); ?>" >
			      	</div>
			      	<div class="col-sm-5">
			        	<input type="text"  name="<?php echo $enc_form['find_bulan']; ?>" style="width: 100%" class=" form-control"  placeholder="<?php echo lang('rpt_find_month'); ?>" >
			      	</div>
			    </div>

              	<div class="form-group">
              		<label class="control-label col-sm-3" ></label>
              		<div class="col-sm-9">
                		 <button type="submit" class="btn btn-primary"><?php echo lang('btn_proses'); ?></button>
              		</div>
              	</div>
        	</div>
       	</form>
  	</div>
</div>
<div style="clear:both;">&nbsp;</div>
<div class="row">
  <div class="col-sm-12">
    <div class="padding_tab_body">
      <div id="item_row<?php echo $token; ?>"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	<?php echo $js_cmb; ?>

	var cmbBulan = $('#list_notifikasi_<?php echo $token; ?> input[name=<?php echo $enc_form['find_bulan']; ?>]')
      .kendoDropDownList({
        autoBind: true,
        dataTextField: "text",
        dataValueField: "id",
        dataSource: {
          serverFiltering: true,
          transport: {
            read: {
              dataType: "jsonp",
              url: "<?php echo site_url('app/getcmbBulan'); ?>",
            }
          }
        }
    });

    $('#list_notifikasi_<?php echo $token; ?>').submit(function(e){
    	e.preventDefault();
        $.ajax({
          url:'<?php echo site_url('notif/showdata'); ?>',
          type:'POST',
          data:$(this).serialize()+'&token=<?php echo $token; ?>',
          dataType: 'html',
          beforeSend:function(){
            $('#item_row<?php echo $token; ?>').addClass('loader');
          },success:function(res){
            $('#item_row<?php echo $token; ?>').html(res);
          }
        }).done(function(){
          $('#item_row<?php echo $token; ?>').removeClass('loader');
        });
    });


});
</script>
