<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Area_model extends CI_Model {
	var $tbl_area ="MF_AREA";

	public function __construct(){
        parent::__construct();
		$this->db_main = $this->load->database('default',TRUE);
    }

	function get_area($filds, $filter=array()){
		$this->db_main->select($filds,FALSE)
			->from($this->tbl_area);
		if(count($filter)> 0)
			$this->db_main->where($filter);
		$res = $this->db_main->get();

		if($res->num_rows() > 0)
			return $res->result();
		else
			return array();
	}
}