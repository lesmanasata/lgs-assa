<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends MY_Controller {

	var $filds_id = "CHR_AREAID";
	var $filds_company_code = "CHR_COMPANY_CODE";
	var $filds_area = "VCH_AREANAME";

	function get(){
		$this->load->model('area/area_model','mod_area');
		$filds = $this->filds_id." as id,";
		$filds .= $this->filds_company_code." as comp_code,";

		$filds .= $this->filds_area." as text";

		$res = $this->mod_area->get_area($filds);
		foreach ($res as $key => $value) {
			$res[$key]->text=$value->id.'-'.$value->text;
		}
		echo json_encode(array('data'=>$res));

	}
}
