#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'Vendor',
		'table'  	=> 'MF_VENDOR',
		'fild' 		=> 'VCH_VendordID,VCH_CompanyCode,VCH_Name,VCH_Telephone,VCH_City,VCH_Address,VCH_PaymentMethod,VCH_Stceg,VCH_ControllAccount,VCH_MatchCode,CHR_PostCode'
	),
	array(
		'initial' 	=> 'INTODR',
		'table'  	=> 'MF_INTERNAL_ORDER',
		'fild' 		=> 'VCH_OrderID,VCH_Ordertype,VCH_Person_Resp,VCH_Ordername,VCH_Companycode,VCH_bis_area,created_at,VCH_Sys_status'
	),
	array(
		'initial' 	=> 'CUST',
		'table'  	=> 'MF_CUSTOMER',
		'fild' 		=> 'VCH_CustomerId,VCH_Title,VCH_Name1,VCH_Name2,VCH_Address,VCH_City,DAT_CreatedOn,VCH_CreatedBy,VCH_AccountGroup,VCH_TaxClassification,VCH_VatRegistrationNo,VCH_OrderBlock,VCH_BillingBlock,VCH_DeliveryBlock,VCH_PostingBlock,VCH_SalesBlock,SIN_Deleted,SIN_DeletedBlock'
	),
	array(
		'initial' 	=> 'MAT',
		'table'  	=> 'MF_MATERIAL',
		'fild' 		=> 'CHR_MaterialNo,CHR_Description,CHR_MaterialGroup,CHR_UnitMeasure,CHR_MaterialType,DAT_CreatedOn,CHR_CreatedBy'
	),
	array(
		'initial' 	=> 'EQUI',
		'table'  	=> 'MF_EQUIPMENT',
		'fild' 		=> 'VCH_EQUIPMENT,VCH_ObjectType,VCH_Inventory,DAT_Acquisition,VCH_Manufacture,VCH_CountryManufacture,VCH_ManufactureSerialNumber,VCH_ManufactureModelNumber,VCH_YearConstruction,VCH_MonthConstruction,VCH_StartFrom,VCH_WorkCenter,VCH_Description,VCH_SortField,VCH_MaintenancePlat,VCH_AssetLocation,VCH_PlantSection,VCH_CostCenter,VCH_CompanyCode,VCH_AssetNo,VCH_SubNumber,VCH_SettlementOrder,DAT_CreatedAt,VCH_CreatedBy,DAT_UpdatedAt,DAT_UpdatedBy,VCH_AddressNumber,VCH_ObjectNumber,VCH_MaterialNumber,VCH_EquipmentCategory,VCH_FuncLoc,VCH_MeasuringPoint,DAT_ValidFrom,VCH_VehicleNo,VCH_ChasisNo,VCH_LicensePlatNumber,DAT_ExpiredAt,VCH_UsageIndicator,VCH_EngineType,VCH_EngineCapacity,VCH_UnitCap,VCH_EngineNo,VCH_SystemStatusCode,VCH_SystemStatus,VCH_SystemStatusDescription,VCH_UserStatusCode,VCH_UserStatus,VCH_UserStatusDescription'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");

