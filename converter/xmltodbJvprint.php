#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'JVPRINT',
		'table'  	=> 'TF_PRINT_JV_ITEM',
		'fild' 		=> 'CHR_company_code,CHR_id,DAT_year,CHR_vendor,DAT_posting_date,CHR_no_faktur,CHR_ref,CHR_no_web,CHR_bisnis_area,INT_service_none,INT_service,INT_ppn,INT_nilai_invoice,INT_PPh,INT_amount'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");