#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'BSEG',
		'table'  	=> 'TF_BSEG',
		'fild' 		=> 'CHR_BUKRS,VCH_BELNR,DYR_GJAHR,VCH_BUZEI,VCH_BSCHL,VCH_IND_JURNAL,ENM_SHKZG,INT_WRBTR,VCH_SAKNR,VCH_HKONT,VCH_KUNNR,VCH_GSBER,VCH_REBZG,DYR_REBZJ,INT_REBZZ,TEX_SGTXT,sspt_ppnind,INT_TERMS,DATE_BASE'
	),
	array(
		'initial' 	=> 'BKPF',
		'table'  	=> 'TF_BKPF',
		'fild' 		=> 'CHR_BUKRS,VCH_BELNR,DYR_GJAHR,ENM_BLART,DAT_BUDAT,CHR_XBLNR,CHR_BKTXT,CHR_AWKEY,CHR_BILWEB'
	),array(
		'initial' 	=> 'TBSEG',
		'table'  	=> 'TF_BSEG',
		'fild' 		=> 'CHR_BUKRS,VCH_BELNR,DYR_GJAHR,VCH_BUZEI,VCH_BSCHL,VCH_IND_JURNAL,ENM_SHKZG,INT_WRBTR,VCH_SAKNR,VCH_HKONT,VCH_KUNNR,VCH_GSBER,VCH_REBZG,DYR_REBZJ,INT_REBZZ,TEX_SGTXT,DAT_AUGCP,VCH_AUGBL'
	),
	array(
		'initial' 	=> 'TBKPF',
		'table'  	=> 'TF_BKPF',
		'fild' 		=> 'CHR_BUKRS,VCH_BELNR,DYR_GJAHR,ENM_BLART,DAT_BUDAT,CHR_XBLNR,CHR_BKTXT,CHR_AWKEY,CHR_BILWEB'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");