<?php

class Mysistem
{
    public function setXML($berkas, $file_name)
    {
        $doc = new DOMDocument("1.0", "UTF-8");
        $doc->formatOutput = true;

        $xml = $doc->createElement("xml");
        $doc->appendChild($xml);

        $trans = $doc->createElement("TransactionData");
        $id = $doc->createElement("ID");
        $id->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($id);

        $guid = $doc->createElement("TransGUID");
        $guid->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($guid);

        $docNum = $doc->createElement("DocumentNumber");
        $docNum->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($docNum);

        $file = $doc->createElement("FileType");
        $file->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($file);

        $IP = $doc->createElement("IPAddress");
        $IP->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($IP);

        $desUser = $doc->createElement("DestinationUser");
        $desUser->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($desUser);

        $key0 = $doc->createElement("Key1");
        $key0->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key0);

        $key1 = $doc->createElement("Key2");
        $key1->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key1);

        $key2 = $doc->createElement("Key3");
        $key2->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key2);

        $dataL= $doc->createElement("DataLength");
        $dataL->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($dataL);

        $data= $doc->createElement("Data");
        $doc->createTextNode('');
        $trans->appendChild($data);

        foreach ($berkas as $key => $value) {
            $string= $doc->createElement("string");

            $string->appendChild(
                  $doc->createTextNode(implode($value, '|'))
                );
            $trans->appendChild($string);
        }
        $xml->appendChild($trans);
        $directoryName = dirname(__FILE__).'/file/output/';

        $res = $doc->save($directoryName.$file_name);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
	

    public function set_xml_data($berkas, $file_name)
    {
        $doc = new DOMDocument("1.0", "UTF-8");
        $doc->formatOutput = true;

        $xml = $doc->createElement("xml");
        $doc->appendChild($xml);

        $trans = $doc->createElement("TransactionData");
        $id = $doc->createElement("ID");
        $id->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($id);

        $guid = $doc->createElement("TransGUID");
        $guid->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($guid);

        $docNum = $doc->createElement("DocumentNumber");
        $docNum->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($docNum);

        $file = $doc->createElement("FileType");
        $file->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($file);

        $IP = $doc->createElement("IPAddress");
        $IP->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($IP);

        $desUser = $doc->createElement("DestinationUser");
        $desUser->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($desUser);

        $key0 = $doc->createElement("Key1");
        $key0->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key0);

        $key1 = $doc->createElement("Key2");
        $key1->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key1);

        $key2 = $doc->createElement("Key3");
        $key2->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key2);

        $dataL= $doc->createElement("DataLength");
        $dataL->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($dataL);

        $data= $doc->createElement("Data");
        $doc->createTextNode('');
        $trans->appendChild($data);

        $no =1;
        foreach ($berkas as $key => $value) {
            if($no > 1)
                $string= $doc->createElement("item");
            else
                $string= $doc->createElement("Header");

            $string->appendChild(
                  $doc->createTextNode(implode($value, '|'))
                );
            $trans->appendChild($string);
            $no++;
        }
        $xml->appendChild($trans);
        $directoryName = dirname(__FILE__).'/file/output/';

        $res = $doc->save($directoryName.$file_name);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function setXML_SERCA($berkas, $file_name,$doc_num)
    {
        $doc = new DOMDocument("1.0", "UTF-8");
        $doc->formatOutput = true;

        $xml = $doc->createElement("xml");
        $doc->appendChild($xml);

        $trans = $doc->createElement("TransactionData");
        $id = $doc->createElement("ID");
        $id->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($id);

        $guid = $doc->createElement("TransGUID");
        $guid->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($guid);

        $docNum = $doc->createElement("DocumentNumber");
        $docNum->appendChild(
            $doc->createTextNode($doc_num)
          );
        $trans->appendChild($docNum);

        $file = $doc->createElement("FileType");
        $file->appendChild(
            $doc->createTextNode('SCPM001')
          );
        $trans->appendChild($file);

        $IP = $doc->createElement("IPAddress");
        $IP->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($IP);

        $desUser = $doc->createElement("DestinationUser");
        $desUser->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($desUser);

        $key0 = $doc->createElement("Key1");
        $key0->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key0);

        $key1 = $doc->createElement("Key2");
        $key1->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key1);

        $key2 = $doc->createElement("Key3");
        $key2->appendChild(
            $doc->createTextNode('')
          );
        $trans->appendChild($key2);

        $dataL= $doc->createElement("DataLength");
        $dataL->appendChild(
            $doc->createTextNode(count($berkas))
          );
        $trans->appendChild($dataL);

        $data= $doc->createElement("Data");
        $doc->createTextNode('');
        $trans->appendChild($data);

        foreach ($berkas as $key => $value) {
            $string= $doc->createElement("string");

            $string->appendChild(
                  $doc->createTextNode(implode($value, '|'))
                );
            $trans->appendChild($string);
        }
        $xml->appendChild($trans);
        $directoryName = dirname(__FILE__).'/file/output/';

        $res = $doc->save($directoryName.$file_name);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

}
