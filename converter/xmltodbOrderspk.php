#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'NOTIF',
		'table'  	=> 'MF_ORDER',
		'fild' 		=> 'VCH_notif_id,VCH_notif_type,VCH_equipment,VCH_description,DAT_reported_date,VCH_reported_by,VCH_customer_code,VCH_voc_number,VCH_serca_number,VCH_functional_lock,CHR_bisnis_area,VCH_system_status'
	),
	array(
		'initial' 	=> 'ZBAK',
		'table'  	=> 'MF_ORDER_ZBAK',
		'fild' 		=> 'VCH_order_no,TXT_desc,VCH_create_date,CHR_bisnis_area,VCH_customer,VCH_equipment,VCH_functional_lock,VCH_pm_act_type,VCH_system_status,VCH_po,VCH_po_date,VCH_priority,VCH_status'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");