#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'PERIOD',
		'table'  	=> 'MF_TIME_PERIOD',
		'fild' 		=> 'CHR_BUKRS,DYR_FROM_THN_ONE,VCH_FROM_BLN_ONE,DYR_TO_THN_ONE,VCH_TO_THN_ONE,DYR_FROM_THN_TWO,VCH_FROM_BLN_TWO,DYR_TO_THN_TWO,VCH_TO_BLN_TWO'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");