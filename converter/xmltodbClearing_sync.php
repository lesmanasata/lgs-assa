#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'CLEAR',
		'table'  	=> 'TF_SINC',
		'fild' 		=> 'VCH_BELNR,CHR_BUKRS,DYR_GJAHR,VCH_BELNR_SAP,VCH_SAPJV,VCH_SAPPAYMENT,VCH_SAPREKON,VCH_msgtext'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");	