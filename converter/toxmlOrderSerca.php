#!/usr/bin/php
<?php 

require_once('connection.php');
require_once('ftp_conn.php');
include 'ftp_arl.php';
require_once('mysistem.php');
require_once('vendor/query.php');



$qbuilder		= new Myquery();
$ftpObj			= new FTPClient();
$FTP_HOST		= '192.168.1.11';
$FTP_USER		= 'vendor';
$FTP_PASS		= 'jangkrik.1';
$tbl_header 	= 'TF_ORDER_HEADER';
$tbl_component 	= 'TF_ORDER_COMPONENTS';
$tbl_operation 	= 'TF_ORDER_OPERATION';
$tbl_operation_detail 	= 'TF_ORDER_OPERATION_ITEM';
$tbl_order 		= 'MF_ORDER';
$tbl_actytype	= 'MF_ACTIVITY_TYPE';
$tbl_equip		= 'MF_EQUIPMENT';
$data 			= array();
$h_where		= array();
$ar_xml 		= array();
$h_filds 		= '';
$h_from		 	= '';
$destination	= '/SERCA/Upload/';
$xml_out		= '/file/output/';
$basepath		= dirname(__FILE__);
$ar_file		= array();

if (!is_dir($basepath.$xml_out)) {
    mkdir($basepath.$xml_out, 0777, true);
}

$h_filds	.= '"HS" as initial_header,'; 
$h_filds	.= 'h.VCH_notif_id,'; 
$h_filds	.= 'o.VCH_voc_number,';
$h_filds	.= 'o.VCH_serca_number,';
$h_filds	.= 'h.VCH_order_id,';
$h_filds	.= 'h.VCH_order_create_type,';
$h_filds	.= 'h.TXT_description,';
$h_filds	.= 'h.VCH_pm_act_type,';
$h_filds	.= 'a.VCH_Description,';
$h_filds	.= 'h.VCH_system_status,';
$h_filds	.= 'h.VCH_equipment_id,';
$h_filds	.= 'e.VCH_LicensePlatNumber,';
$h_filds	.= 'h.VCH_workcenter_plan';

$h_from 	.= 'TF_ORDER_HEADER h ';
$h_from 	.= 'LEFT JOIN MF_ORDER o ON h.VCH_notif_id = o.VCH_notif_id ';
$h_from 	.= 'JOIN MF_ACTIVITY_TYPE a ON h.VCH_order_create_type=a.VCH_mode AND h.VCH_pm_act_type=a.VCH_Type ';
$h_from 	.= 'LEFT JOIN MF_EQUIPMENT e ON e.VCH_EQUIPMENT = h.VCH_equipment_id+0 ';

$h_where['h.flag_serca']='0';
$h_where['h.VCH_notif_id !=""']=NULL;
$h_where['o.VCH_serca_number !=""']=NULL;

$arr_header = array('select'=>$h_filds, 'from'=> $h_from, 'where'=>$h_where);
$res_header= mysqli_query($conn, $qbuilder->build_query($arr_header));
if(mysqli_num_rows($res_header)>0){
  while ($row_header = mysqli_fetch_assoc($res_header)){
  	$key = $row_header['VCH_order_id'];
  	$ar_xml[$key][] = $row_header;
  }
}


if(count($ar_xml)> 0){
	foreach ($ar_xml as $xkey => $value){
		$os_filds  ='"OS" as initial_os,';
		$os_filds .='VCH_operation_type,';
		$os_filds .='TXT_description,';
		$os_filds .= "'".$value[0]['VCH_workcenter_plan']."' as plant";
		$key_os = $value[0]['VCH_order_id'];

		$arr_os = array('select'=>$os_filds, 'from'=> 'TF_ORDER_OPERATION', 
			'where'=>array('VCH_order_id'=>$value[0]['VCH_order_id'])
		);

		$res_os= mysqli_query($conn, $qbuilder->build_query($arr_os));
		if(mysqli_num_rows($res_os)>0){
		  while ($row_header = mysqli_fetch_assoc($res_os)){
		  	$ar_xml[$key_os][] = $row_header;
		  }
		}

		$cs_filds  = '"CS" as initial_cs,';
		$cs_filds .= 'VCH_part_description,';
		$cs_filds .= 'BIG_qty,';
		$cs_filds .= 'VCH_uom,';
		$cs_filds .= 'CHR_component_type';

		$arr_cs = array('select'=>$cs_filds, 'from'=> 'TF_ORDER_COMPONENTS', 
			'where'=>array('VCH_order_id'=>$value[0]['VCH_order_id'])
		);

		$res_cs= mysqli_query($conn, $qbuilder->build_query($arr_cs));
		if(mysqli_num_rows($res_cs)>0){
		  while ($row_header = mysqli_fetch_assoc($res_cs)){
		  	$ar_xml[$key_os][] = $row_header;
		  }
		}
	}
}


if(count($ar_xml) > 0){
  foreach($ar_xml as $key =>$row){
    $file_name = 'SCPM001_'.$ar_xml[$key][0]['VCH_notif_id'].'.xml';
    $doc_num = 'SCPM001_'.$ar_xml[$key][0]['VCH_notif_id'];

    $ar_file[] = array(
        'file_name'=>$file_name,
        'key_id'   => $ar_xml[$key][0]['VCH_order_id'],
        'date'     => date('Ymd')
    );

    $sistem->setXML_SERCA($ar_xml[$key], $file_name,$doc_num);
  }
}


if(count($ar_file)>0){
  $ftpObj->connect($FTP_HOST, $FTP_USER, $FTP_PASS);
  foreach($ar_file as $row){
    $file_from = $basepath.$xml_out.$row['file_name'];
    $file_to   = $destination.$row['file_name'];
    $key       = array(
      'id'    => $row['key_id'],
      'date'  => $row['date'],
      'table' => 'TF_ORDER_HEADER',
      'fild'  => '`VCH_order_id`'
    );
    $ftpObj->upload_serca($file_from, $file_to,$key);
  }
}