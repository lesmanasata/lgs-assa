#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'LOG',
		'table'  	=> 'TF_LOG_CLEARING',
		'fild' 		=> 'INT_id,DAT_createon,TME_createat,VCH_reference,CHR_BUKRS,DAT_postingdate,CHR_year,VCH_msgtext'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");