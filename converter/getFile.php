<?php
require_once('ftp_conn.php');
$dir = dirname(__FILE__)."/file/input/";
if (!is_dir($dir)) {
    mkdir($dir, 0777, true);
}

$ftp_conn = ftp_connect(FTP_HOST) or die("Could not connect to $ftp_server");
$login = ftp_login($ftp_conn,FTP_USER, FTP_PASS);
ftp_pasv($ftp_conn, true);
$file_list = ftp_nlist($ftp_conn, "/Upload");

$initial_file = array('Vendor','INTODR','CUST','MAT');
foreach ($file_list as $key => $value) {
	$file_parts = pathinfo($value);
	$extension = (isset($file_parts['extension']))?$file_parts['extension']:"";
	if($extension=='xml'){
		foreach($initial_file as $f_initial){
			$length = strlen($f_initial);
			if(substr($file_parts['basename'],0,$length)==$f_initial){
				$time = date("His");
				if (ftp_get($ftp_conn, $dir.$file_parts['basename'],$value, FTP_BINARY)) {
				   ftp_rename($ftp_conn, $value, "/Upload/SUCCESS/".$time.$file_parts['basename']);
				} else {
				   ftp_rename($ftp_conn, $value, "/Upload/FAILED/".$time.$file_parts['basename']);
				}
			}
		}
	}
}

ftp_close($ftp_conn);
