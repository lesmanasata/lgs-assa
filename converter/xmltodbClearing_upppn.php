#!/usr/bin/php
<?php
require_once('connection.php');

$que1 = "UPDATE  TF_BSEG bs JOIN TF_BKPF bk 
		ON bs.CHR_BUKRS = bk.CHR_BUKRS 
		AND bs.VCH_BELNR = bk.VCH_BELNR
		AND bs.DYR_GJAHR = bk.DYR_GJAHR
		SET bs.pph_indicator = '2'
		WHERE LEFT(bk.CHR_XBLNR,3) IN('020','030')";
mysqli_query($conn, $que1);