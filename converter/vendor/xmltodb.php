<?php
require_once(dirname(__FILE__).'/query.php');

class XmlToDb{
	private $db_host 		= '127.0.0.1';//'192.168.1.235';
	private $db_uname 		= 'lamsolusi';
	private $db_pass 		= 'dev_lamsolusi';//'4rfv!@#$';
	private $db_name 		= 'assarent_lamjaya';
	private $file_source	= '/../file/input/';
	private $qbuilder;
	protected static $conn;
	protected $_input_path;
	protected $_input_failed = 'failed/';
	protected $_input_sukses = 'success/';
	protected $_enabled		 = TRUE;

	public function __construct(){
		$this->qbuilder		= new Myquery();
		$this->_input_path	= dirname(__FILE__).'/../file/input/';

		if (! is_dir($this->_input_path) OR  !is_really_writable($this->_input_path))
			$this->_enabled = FALSE;
	}

	private function connect(){
		if(!self::$conn){
			$conn = mysqli_connect($this->db_host, $this->db_uname, $this->db_pass, $this->db_name); 
			if (mysqli_connect_errno()) {
			    printf("Connect failed: %s\n", mysqli_connect_error()); 
			    exit(); 
			}
		}
		return $conn;
	}

	private function connect_close($con){
		mysqli_close($con);
	}

	public function parse_to_tbl($table, $file_name,$fild_contract_header,$initial=""){
		$this->cekDir();

	    $is_faild   = false;
	    $dom        = new DOMDocument();
	    $dir_func   = dirname(__FILE__).$this->file_source;
	    $cdom       = $dom->loadXML(file_get_contents($dir_func.$file_name));
	    $data       = $dom->getElementsByTagName('string');
		$tf 		='';
	    if($cdom){
	    	$mcon = $this->connect();
	        foreach ($data as $row){
	            $datas      = $row->nodeValue;
	            $array      = (explode('|', $datas));
	            $koma       = str_replace("'","\'",$array);
	            $json       = json_encode($koma);
	            $ins        = json_decode($json, true);
	            $colomns    = "`".implode("`, `", explode(",", $fild_contract_header))."`";
	            $es_value   = array_values($ins);
	            $values     = "'".implode("', '", $es_value)."'";

	            if($initial=='BSEG'){
	           		$ar_tocek 	= explode('|', $datas);
	           		$ppnInd		= $ar_tocek[16];
	           		$ppnIndv 	= (substr($ppnInd,0,3)=='020' || substr($ppnInd,0,3)=='030')?'2':'1'; 
					
					foreach ($ar_tocek as $key => $value){
						$ar_tocek[16] = $ppnIndv;
						
						$ar_tocek[17] = ($ar_tocek[17] !='')?str_replace('T','0',$ar_tocek[17]):'0';

						if($ar_tocek[18]=="00000000" || $ar_tocek[18] ==' ')
		            		$ar_tocek[18] = 'NULL';
					}
					$koma       = str_replace("'","\'",$ar_tocek);
		            $json       = json_encode($koma);
		            $ins        = json_decode($json, true);
		            $colomns    = "`".implode("`, `", explode(",", $fild_contract_header))."`";
		            $es_value   = array_values($ins);
		            $values_b   = "'".implode("', '", $es_value)."'";
		            $values     = str_replace("'NULL'", "NULL", $values_b);

	           	}

	            if($table=='TF_CLEARING_RESET'){
	            	$values .=", '".date('Y-m-d H:i:s')."'";
	            	$clearing_no = trim($es_value[0]);
	            	$this->reset_clearing($clearing_no);
	            }

	            if($table=='TF_ORDER_HEADER'){
	            	$ar_tocek 		= explode(',', $values);
	            	$colomns .=", `TXT_keluhan`";
	            	$values  .=", ".$ar_tocek[2];
	            }

	            if($table=='MF_MAINTENANCE_PLAN_ITEM'){
	            	$ar_tocek 		= explode(',', $values);
	            	foreach ($ar_tocek as $key => $value) {
	            		if((str_replace("'", "", $ar_tocek[3])=="00000000") || str_replace("'", "", $ar_tocek[3]) ==' ')
		            		$ar_tocek[3] = 'NULL';
	            	}

	            	$values     = implode(", ", $ar_tocek);
	            }

	            if($initial=='BSEG'){
	            	$ar_tocek 		= explode(',', $values);
	            	foreach ($ar_tocek as $key => $value) {
	            		if((str_replace("'", "", $ar_tocek[18])=="00000000") || str_replace("'", "", $ar_tocek[18]) ==' ')
		            		$ar_tocek[18] = 'NULL';
	            	}

	            	$values     = implode(", ", $ar_tocek);
	            }

	            if($table=='MF_TIME_PERIOD'){
	            	$ar_tocek 		= explode(',', $values);
	            	$bln_one_from	= str_replace("'", "", $ar_tocek[2]); 
	            	$bln_one_to		= str_replace("'", "", $ar_tocek[4]); 
	            	$bln_two_from	= str_replace("'", "", $ar_tocek[6]); 
	            	$bln_two_to		= str_replace("'", "", $ar_tocek[8]); 

	            	foreach ($ar_tocek as $key => $value) {
	            		
						$ar_tocek[2] = (($bln_one_from+0) > 12) ? "'12'":"'".substr($bln_one_from, 2,2)."'";  
						$ar_tocek[4] = (($bln_one_to+0) > 12) ? "'12'":"'".substr($bln_one_to, 2,2)."'";  
						$ar_tocek[6] = (($bln_two_from+0) > 12) ? "'12'":"'".substr($bln_two_from, 2,2)."'";  
						$ar_tocek[8] = (($bln_two_to+0) > 12) ? "'12'":"'".substr($bln_two_to, 2,2)."'";  
					}
	            	$values     = implode(", ", $ar_tocek);
	            }

	           	if($table=='MF_CONTRACT_BILLING_PLAN'){
	           		$ar_tocek 	= explode(',', $values);
					$from 		= str_replace("'", "", $ar_tocek[5]); 	           		
					$to 		= str_replace("'", "", $ar_tocek[4]);

					if($from > $to){
						foreach ($ar_tocek as $key => $value) {
							$ar_tocek[4] = "'".$from."'";  
							$ar_tocek[5] = "'".$to."'";  
						}
						$values     = implode(", ", $ar_tocek);
					}
	           	}

	            $query      = "REPLACE INTO ".$table." ($colomns) VALUES ($values)";

	            $sql= mysqli_query($mcon, $query);
	            if (! $sql) {
	                 $is_faild = true;
	                 $tf .= $query."\n";
	            }
	        }
	        $this->connect_close($mcon);
	    }else
	        $is_faild   = true;

	    if($is_faild){
	        if (!is_dir($dir_func.'failed/')) 
	            mkdir($dir_func.'failed/', 0777, true);

	        $mdir = $dir_func.'failed/'.date('Ymd').'/';
	        $this->cre_log_dir($mdir);

	        if(!is_file($mdir."index.html")){
	            $content = "<html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
	            file_put_contents($mdir."index.html", $content);

	            file_put_contents($mdir.'log_'.$file_name, $tf);
	        }

	        rename($dir_func.$file_name, $mdir.$file_name);
	    }else{
	        if(!is_dir($dir_func.'success/')) 
	            mkdir($dir_func.'success/', 0777, true);

	        $mdir = $dir_func.'success/'.date('Ymd').'/';
	        $this->cre_log_dir($mdir);

	        if(!is_file($mdir."index.html")){
	            $content = "<html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body></html>";
	            file_put_contents($mdir."index.html", $content);
	        }

	        rename($dir_func.$file_name, $mdir.'old_'.$file_name);
    	}
    }

    private function cekDir(){
		$dir = dirname(__FILE__).$this->file_source;
		if (!is_dir($dir)) {
		    mkdir($dir, 0777, true);
		}
	}


	private function reset_clearing($clearing_no){
		$doc_reset  = array();
		$initial_ar = array('ARL','ARI');
		$initial_bp = array('BPL','BPI','ARB');
		$initial_sp = array('SPL','SPI');

		$ar_find_doc = array(
			'select'=>'CONCAT(CHR_BUKRS,VCH_REBZG,DYR_REBZJ,INT_REBZZ) AS doc_to_reset',
			'from'=> 'TF_BSEG',
			'where'=>array(
				'VCH_BELNR'=> $clearing_no
			)
		);

		$q_find_doc = $this->qbuilder->build_query($ar_find_doc);

		$res_find_doc= mysqli_query($this->connect(), $q_find_doc);
		if(mysqli_num_rows($res_find_doc)>0){
				while ($row_find_doc = mysqli_fetch_array($res_find_doc)){
				$doc_reset[] = $row_find_doc['doc_to_reset'];
			}
		}

		if(count($doc_reset) > 0){
			$q_reset  = "UPDATE `TF_BSEG` SET `DAT_AUGCP` =NULL, `VCH_AUGBL` =NULL ";
			
			if(in_array(substr($clearing_no, 0,3), $initial_ar))
				$q_reset  .=", `ar_status`='0' ";

			if(in_array(substr($clearing_no, 0,3), $initial_bp))
				$q_reset  .=", `bp_status`='0' ";

			if(in_array(substr($clearing_no, 0,3), $initial_sp))
				$q_reset  .=", `ssp_status`='0' ";


			$q_reset .= "WHERE CONCAT(CHR_BUKRS,VCH_BELNR,DYR_GJAHR,VCH_BUZEI) IN('".implode("','", $doc_reset)."')";

			$res_reset= mysqli_query($this->connect(), $q_reset);
			if($res_reset){
				$q_reset_m = mysqli_query($this->connect(),'UPDATE `TF_BSEG` SET VCH_REBZG = NULL, DYR_REBZJ=NULL,INT_REBZZ=NULL WHERE VCH_BELNR="'.$clearing_no.'"');
			}
			unset($doc_reset);

		}
		
	}

	function cre_log_dir($dir_res){
		if ($this->_enabled === FALSE)
			return FALSE;
		
		$cdir = $dir_res;
		$udir = $cdir;


		if(!file_exists($udir) and !is_dir($udir)){
			if($cdir!=$udir and !file_exists($cdir) and !is_dir($cdir)){
				mkdir($cdir,DIR_READ_MODE);
			}
			mkdir($udir,DIR_READ_MODE);
		}
	}
}

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('FOPEN_READ',                            'rb');
define('FOPEN_READ_WRITE',                        'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',        'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',    'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',                    'ab');
define('FOPEN_READ_WRITE_CREATE',                'a+b');
define('FOPEN_WRITE_CREATE_STRICT',                'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',        'x+b');

if ( ! function_exists('is_really_writable'))
{
	function is_really_writable($file)
	{
		// If we're on a Unix server with safe_mode off we call is_writable
		if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE)
		{
			return is_writable($file);
		}

		// For windows servers and safe_mode "on" installations we'll actually
		// write a file then read it.  Bah...
		if (is_dir($file))
		{
			$file = rtrim($file, '/').'/'.md5(mt_rand(1,100).mt_rand(1,100));

			if (($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
			{
				return FALSE;
			}

			fclose($fp);
			@chmod($file, DIR_WRITE_MODE);
			@unlink($file);
			return TRUE;
		}
		elseif ( ! is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE)
		{
			return FALSE;
		}

		fclose($fp);
		return TRUE;
	}
}