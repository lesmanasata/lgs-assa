<?php

class Myquery{
	function has_compare_operator($str){
		$str = trim($str);
		if(! preg_match("/(\s|<|>|!|=|is null|is not null)/i", $str))
			return FALSE;
		return TRUE;
	}

	function has_logic_operator($str){
		$str = trim($str);
		if(! preg_match("/$(and|or)\s/i", $str))
			return FALSE;
		return TRUE;
	}
		
	function build_condition($clause){
		if(! is_array($clause))
			return $clause;
		else{
			$s = '';
			foreach($clause as $k=>$v){
				if (! $this->has_compare_operator($k))
					$k .= ' = ';
				if($s!='' && !$this->has_logic_operator($k))
					$s .= ' AND ';
				else
					$s .= ' ';
				$s.=$k.(is_string($v)?'"'.htmlspecialchars($v).'"':$v);
			}
			return $s;
		}
	}

	function build_query($attr){
		$sql = 'SELECT ';
		if(! isset($attr['select']))
			$sql.= '*';
		else
			$sql.= $attr['select'];
			
		$sql.= "\nFROM $attr[from]";

		if(isset($attr['where']))
			$sql.= "\nWHERE ".$this->build_condition($attr['where']);
			
		if(isset($attr['group']))
			$sql.= "\nGROUP BY $attr[group]";
			
		if(isset($attr['have']))
			$sql.= "\nHAVING ".$this->build_condition($attr['have']);
			
		if(isset($attr['order']))
			$sql.= "\nORDER BY $attr[order]";
			
		if(isset($attr['limit']))
			$sql.= "\nLIMIT $attr[limit]";
		return $sql;
	}

	function aasort (&$array, $key) {
	    $sorter=array();
	    $ret=array();
	    reset($array);
	    foreach ($array as $ii => $va) {
	        $sorter[$ii]=$va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va) {
	        $ret[$ii]=$array[$ii];
	    }
	    $array=$ret;
	}
}