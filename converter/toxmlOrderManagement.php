<?php
include 'ftp_arl.php';
include 'connection.php';
include 'ftp_conn.php';
require_once('mysistem.php');
require_once('vendor/query.php');


$ftpObj               = new FTPClient();
$sistem               = new Mysistem();
$qbuilder             = new Myquery();
$basepath             = dirname(__FILE__);
$ar_xml               = array();
$ar_xmln              = array();
$use_table            = 'TF_MAINTENANCE_PLAN';
$destination          = '/Download/MAIN_PLAN/';
$xml_out              = '/file/output/';



$filter['flag'] = '0';
$query_array = array(
  'select'=> 'VCH_Plan_Number,VCH_Plan_Item,VCH_Action,INT_count_reading,DAT_start_date,VCH_Plan_id',
  'from'  => $use_table,
  'where' => $filter
);

$query = $qbuilder->build_query($query_array);

$res= mysqli_query($conn, $query);
if(mysqli_num_rows($res)>0){
  while ($row = mysqli_fetch_array($res)){
    $data['MAINTENANCE_PLAN'] = $row['VCH_Plan_Number'];

    if($row['VCH_Action']=='release')
      $data['PROCESS_TYPE'] = '1';
    elseif($row['VCH_Action']=='restart')
      $data['PROCESS_TYPE'] = '2';
    else
      $data['PROCESS_TYPE'] = '3';

    $data['START_DATE']       = $row['DAT_start_date'];
    $data['START_COUNTER']    = $row['INT_count_reading'];
    $data['CALL_NUMBER']      = ($row['VCH_Plan_Item']==0)?'':$row['VCH_Plan_Item'];
    $data['KEEP']             = "Y";

    $file_name = 'IP10_'.$row['VCH_Plan_id'].'-'.date('his').'.xml';
    $xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><MP></MP>',LIBXML_NOEMPTYTAG);
    array_to_xml($data,$xml_data);
    unset($data);

    $result = $xml_data->asXML($basepath.$xml_out.$file_name);


    if($result){
        $file_from = $basepath.$xml_out.$file_name;
        $file_to   = $destination.$file_name;
        $key       = array(
          'id'    => $row['VCH_Plan_Number'].$row['VCH_Plan_Item'],
          'date'  => date('Ymd'),
          'table' => $use_table,
          'fild'  => 'CONCAT(VCH_Plan_Number,VCH_Plan_Item)'
        );
      $ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);
      $ftpObj->upload($file_from, $file_to,$key);
    }
  }
}


function array_to_xml( $data, &$xml_data,$skey='' ) {
    foreach( $data as $key => $value ) {

        if( is_numeric($key) ){
            $key = $skey.'_DETAIL'; 
        }
        if( is_array($value) ) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode,$key);
        } else {
            if($value=='')
              $xml_data->$key='';
            else
              $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}





