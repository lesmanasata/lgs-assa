#!/usr/bin/php
<?php
require_once('ftp_arl.php');
require_once('connection.php');
require_once('ftp_conn.php');
require_once('mysistem.php');

    $sistem = new Mysistem();
    $ftpObj = new FTPClient();
    $ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);

    $date = date('YmdHi');
    $biscode='';
    $doc_type='';

    $point = "SELECT `VCH_BELNR`
             FROM `TF_BKPF`
             WHERE `CHR_FLAG`= 0 AND `ENM_BLART`='IB' AND LEFT(VCH_BELNR,3)='COS'";
             //AND DATE(`last_insert`)= CURDATE()";
    $points = mysqli_query($conn, $point);
    if (mysqli_num_rows($points)>0) {
        $no=1;
        while ($row = mysqli_fetch_array($points, MYSQLI_ASSOC)) {
            $doc_type = $row['VCH_BELNR'];
            $query = "SELECT REPLACE (`bk`.`DAT_BLDAT`,'-','') AS DAT_BLDAT,
                     REPLACE (`bk`.`DAT_BUDAT`,'-','') AS DAT_BUDAT,
                     `bk`.`ENM_BLART`,
                     `bk`.`CHR_BUKRS`,
                     `bk`.`CHR_CURENCY`,
                     `bk`.`VCH_BELNR`,
                     `bk`.`CHR_XBLNR`,
                     `bs`.`VCH_BSCHL`,
                     `bs`.`VCH_HKONT`,
                     `bs`.`VCH_GSBER`,
                     `bs`.`VCH_KOSTL`,
                     `bs`.`VCH_AUFNR`,
                     IF(`bs`.`ENM_SHKZG` = 'H',concat('-',bs.INT_WRBTR),bs.INT_WRBTR) as new_INT_WRBTR,
                     `bs`.`TEX_SGTXT`
            FROM     `TF_BKPF` `bk`
            LEFT JOIN `TF_BSEG` `bs`
                ON `bk`.`VCH_BELNR`= `bs`.`VCH_BELNR`
                WHERE `bs`.`VCH_BELNR`= '$doc_type'";

            //echo $query;
            $result = mysqli_query($conn, $query);
            if (mysqli_num_rows($result)>0) {
                $cost = array();
                while ($result_arraycle = mysqli_fetch_assoc($result)) {
                    $cost[] = $result_arraycle;
                }
                if ($no == 1) {
                    $biscode = $cost[0]['VCH_GSBER'];
                }
                $no++;
                $sistem->setXML($cost, 'cost_'.$doc_type.'bis'.$biscode.$date.'.xml');

                $fileFrom = dirname(__FILE__).'/file/output/cost_'.$doc_type.'bis'.$biscode.$date.'.xml';
                $fileTo = '/Download/COST_DIST/cost_'.$doc_type.'bis'.$biscode.$date.'.xml';

                if ($ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS)) {
                    $ftpObj->uploadFile2($fileFrom, $fileTo, $doc_type, $biscode, $date);
                } else {
                    echo 'Failed to connect';
                }
            }
        }
    }
    mysqli_free_result($points);
