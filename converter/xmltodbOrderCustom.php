#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'HEAD',
		'table'  	=> 'TF_ORDER_HEADER',
		'fild' 		=> 'VCH_order_id,VCH_order_create_type,TXT_description,VCH_functional_loc,VCH_equipment_id,VCH_pm_act_type,VCH_workcenter_main,VCH_workcenter_plan,VCH_system_status,VCH_user_status,DAT_basic_date_start,DAT_basic_date_end,VCH_user_create,VCH_sold_toparty'
	),
	array(
		'initial' 	=> 'OPER',
		'table'  	=> 'TF_ORDER_OPERATION',
		'fild' 		=> 'VCH_order_id,CHR_operation_id,VCH_operation_type,TXT_description'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");