#!/usr/bin/php
<?php 
require_once('connection.php');
require_once('vendor/query.php');

$qbuilder   = new Myquery();
$cur_mth 	= date('Y-m-').'1';
$cur_lop 	= 3;
$lmt_mth 	= array();
$flt_doc_tag= array('RV','LL','DN','DP');
$tbl_header = 'TF_BKPF';
$tbl_detail = 'TF_BSEG';


for($i=0; $i < $cur_lop; $i++){
	$lmt_mth[] = date('Ym', strtotime('-'.$i.' month', strtotime($cur_mth)));
}

$ar_doc_cek = array(
	'select'=>'h.VCH_BELNR,concat(h.CHR_BUKRS,h.VCH_BELNR,h.DYR_GJAHR) as doc_cek, SUM(d.INT_WRBTR) as t_amount',
	'from'=> $tbl_header.' h JOIN '.$tbl_detail.' d ON h.CHR_BUKRS = d.CHR_BUKRS and h.VCH_BELNR = d.VCH_BELNR and h.DYR_GJAHR = d.DYR_GJAHR',
	'where'=>array(
		"h.ENM_BLART IN('".implode("','", $flt_doc_tag)."')"=> NULL,
		"DATE_FORMAT(h.last_insert, '%Y%m') IN('".implode("','", $lmt_mth)."')"=> NULL,
		"d.VCH_AUGBL IS NULL"=> NULL,
		"d.VCH_BSCHL"=>'01'
	),
	'group' => 'h.CHR_BUKRS,h.VCH_BELNR,h.DYR_GJAHR'
);
$q_doc_cek = $qbuilder->build_query($ar_doc_cek);



$res_doc_cek= mysqli_query($conn, $q_doc_cek);
if(mysqli_num_rows($res_doc_cek)>0){
  while ($row_doc_cek = mysqli_fetch_assoc($res_doc_cek)){
  	$key_doc_tag = $row_doc_cek['doc_cek'];
  	$ar_doc_compare = array(
  		'select' => 'VCH_BELNR,DYR_GJAHR,SUM(INT_WRBTR) as t_amount_compare',
  		'from'	 => $tbl_detail,
  		'where'  => array(
  			'concat(CHR_BUKRS,VCH_REBZG,DYR_REBZJ)' => $key_doc_tag
  		)
  	);
  	$q_doc_compare  = $qbuilder->build_query($ar_doc_compare);
  	$res_doc_compare= mysqli_query($conn, $q_doc_compare);
	echo $q_doc_compare.'<br>';
  	if(mysqli_num_rows($res_doc_compare)>0){
  		while ($row_doc_compare = mysqli_fetch_assoc($res_doc_compare)){
  			if($row_doc_compare['t_amount_compare']==$row_doc_cek['t_amount']){
  				$q = "UPDATE `".$tbl_detail."` SET `DAT_AUGCP`='".date('Y-m-d')."', `VCH_AUGBL`='".$row_doc_compare['VCH_BELNR']."' ";
  				$q .= "WHERE concat(CHR_BUKRS,VCH_BELNR,DYR_GJAHR)='".$key_doc_tag."' AND `VCH_BSCHL`='01'";
  				mysqli_query($conn, $q);
  			}
  		}
  	}
  }
}



?>