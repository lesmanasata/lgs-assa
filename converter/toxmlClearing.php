#!/usr/bin/php
<?php
require_once('ftp_arl.php');
require_once('connection.php');
require_once('ftp_conn.php');
require_once('mysistem.php');


$ftpObj = new FTPClient();
$sistem = new Mysistem();
$ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);

$flag = '';
$vch_belnr = '';
$chr_bukrs = '';
$dyr_gjahr = '';
$gsbr1 ='';
$date = date('YmdHi');
$destAR='/Download/AR_CLEAR/';
$destBP='/Download/BP_CLEAR/';
$destSP='/Download/BP_CLEAR/';

$que1 = "SELECT bk.VCH_BELNR, bk.CHR_BUKRS, bk.DYR_GJAHR, bs.VCH_GSBER
            FROM TF_BKPF bk
            inner JOIN TF_BSEG bs
            on bk.VCH_BELNR=bs.VCH_BELNR
            and bk.CHR_BUKRS=bs.CHR_BUKRS
            and bk.DYR_GJAHR=bs.DYR_GJAHR
            WHERE LEFT(bk.VCH_BELNR,2) in('AR','BP','SP')
            AND bk.CHR_FLAG = 0
            AND DATE(bk.last_insert)= CURDATE()";
            $q_que1 = mysqli_query($conn, $que1);

if (mysqli_num_rows($q_que1)>0) {
    $flag=1;
    while ($row = mysqli_fetch_array($q_que1, MYSQLI_ASSOC)) {
        $chr_bukrs = $row['CHR_BUKRS'];
        $dyr_gjahr = $row['DYR_GJAHR'];
        $vch_belnr = $row['VCH_BELNR'];

        if ($flag == 1) {
            $gsbr1 = $row['VCH_GSBER'];
            $vch_belnr = $row['VCH_BELNR'];
        }
        $flag++;
        $que3 = "SELECT
			                        REPLACE (`bk`.`DAT_BUDAT`,'-','') AS DAT_BLDAT
			                      , REPLACE (`bk`.`DAT_BUDAT`,'-','') AS DAT_BUDAT
			                      , `bk`.`ENM_BLART`
			                      , `bk`.`CHR_BUKRS`
			                      , `bk`.`CHR_CURENCY`
			                      , `bk`.`VCH_BELNR`
			                      , `bk`.`CHR_XBLNR`
			                      , `bk`.`CHR_BKTXT`
                                  , CASE
                                       WHEN bs.VCH_IND_JURNAL='T'
                                       THEN `bs`.`VCH_KUNNR`
                                       WHEN `bs`.`VCH_BSCHL` IN('40','50')
                                       THEN `bs`.`VCH_HKONT`
                                       WHEN `bs`.`VCH_BSCHL` = '15'
                                       THEN `bs`.`VCH_KUNNR`
                                       ELSE `bs`.`VCH_KUNNR`
                                  END as POSITION
                                  , CASE
                                  WHEN `bs`.`VCH_BSCHL` = '40' and bs.VCH_IND_JURNAL='T'
                                  THEN 'D'
                                  WHEN bs.VCH_BSCHL = '40'
                                  THEN 'S'
                                  WHEN bs.VCH_BSCHL = '15'
                                  THEN 'D'
                                  WHEN bs.VCH_BSCHL IS NULL AND LEFT(bk.VCH_BELNR,3) ='SPL'
                                  THEN 'D'
                                  WHEN bs.VCH_IND_JURNAL='T' AND bs.VCH_REBZG IS NOT NULL THEN 'D'
                                  ELSE ''
                                  END as INDIKATOR
			                      , bs.VCH_IND_JURNAL
			                      , bs.VCH_REBZG
			                      , bs.DYR_REBZJ
                                  , bs.INT_REBZZ
			                      ,CASE
                                      WHEN bs.VCH_BSCHL ='40' AND bs.VCH_REBZG IS NOT NULL
			                          THEN concat('-',bs.INT_WRBTR)
                                      WHEN bs.VCH_BSCHL ='40' AND bs.VCH_REBZG IS NULL
                                      THEN bs.INT_WRBTR
			                          ELSE bs.INT_WRBTR
			                      END as newINT_WRBTR
			                      ,CASE
			                          WHEN bs.VCH_REBZG is null
			                          THEN bs.VCH_BSCHL
			                          ELSE ''
			                      END AS post
			                      ,CASE
			                          WHEN bs.VCH_REBZG is null 
			                          THEN bs.VCH_GSBER
			                          ELSE ''
			                      END AS bisarea
			                      ,CASE
									WHEN bs.VCH_HKONT IN('4200999999')
										THEN ''
									ELSE
										bs.VCH_KOSTL
									END AS VCH_KOSTL
			                      ,CASE
                                        WHEN bs.VCH_HKONT IN('6102000006','6102000033')
                                        THEN 'M0'
                                        WHEN bs.VCH_HKONT ='4200999999'
                                        THEN 'K0'
                                        ELSE ''
                                    END AS TAX
			                      ,CASE
                                        WHEN LEFT('$vch_belnr',2) in('BP') AND bs.VCH_REBZG IS NOT NULL
                                        THEN CONCAT(bs.VCH_NUMBPOT,';',DATE_FORMAT(bs.DAT_BUKPOT,'%d/%m/%Y'))
                                        WHEN LEFT('$vch_belnr',2) in('SP') AND bs.VCH_REBZG IS NOT NULL
                                        THEN bs.VCH_NUMBPOT_SSP
                                        ELSE bs.TEX_SGTXT
                                        END AS TEXT_DATA
			                            FROM TF_BKPF bk
			                            INNER JOIN TF_BSEG bs
			                             ON `bk`.`VCH_BELNR` = `bs`.`VCH_BELNR`
			                            AND `bk`.`CHR_BUKRS` = `bs`.`CHR_BUKRS`
			                            AND `bk`.`DYR_GJAHR` = `bs`.`DYR_GJAHR`
										WHERE bs.VCH_BELNR = '$vch_belnr'
                                        AND bs.CHR_BUKRS= '$chr_bukrs'
                                        AND bs.DYR_GJAHR='$dyr_gjahr'";
        $q_que3 = mysqli_query($conn, $que3);


        if (mysqli_num_rows($q_que3)>0) {
            $arrclear = array();
            while ($result_array = mysqli_fetch_assoc($q_que3)) {
                $arrclear[] = $result_array;
            }
            $sistem->setXML($arrclear, ''.$vch_belnr.'bis'.$gsbr1.$date.'.xml');
            $key = $arrclear[0]['VCH_BELNR'];

            $fileFrom = dirname(__FILE__).'/file/output/'.$vch_belnr.'bis'.$gsbr1.$date.'.xml';
            if (substr($vch_belnr, 0, 2)=='AR') {
                $fileTo = $destAR.$vch_belnr.'bis'.$gsbr1.$date.'.xml';
            } elseif (substr($vch_belnr, 0, 2)=='BP') {
                $fileTo = $destBP.$vch_belnr.'bis'.$gsbr1.$date.'.xml';
            }elseif (substr($vch_belnr, 0, 2)=='SP') {
                $fileTo = $destSP.$vch_belnr.'bis'.$gsbr1.$date.'.xml';
            }

            if ($ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS)) {
                $ftpObj->uploadFile1($fileFrom, $fileTo, $key, $vch_belnr, $gsbr1, $date);
            } else {
                echo 'Failed to connect';
            }
        }
    }
}
