#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'MP_HEADER',
		'table'  	=> 'MF_MAINTENANCE_PLAN',
		'fild' 		=> 'VCH_Plan_Number,VCH_Plan_Description,VCH_Func_Loc,VCH_Equipment,VCH_Equipment_Desc,DAT_Created,VCH_Created_By,INT_Total_Counter,VCH_Uom,VCH_Plat_Number,INT_indicator,VCH_sortfild'
	),
	array(
		'initial' 	=> 'MP_DETAIL',
		'table'  	=> 'MF_MAINTENANCE_PLAN_ITEM',
		'fild' 		=> 'VCH_Plan_Number,VCH_Call_No,DAT_Plane_Date,DAT_Call_Date,VCH_Due_Package,VCH_Scaduling,VCH_Counter,VCH_Next_Planed,VCH_Order'
	)
);	

require_once(dirname(__FILE__)."/vendor/helper.php");

