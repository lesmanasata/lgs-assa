<?php
include 'ftp_arl.php';
include 'connection.php';
include 'ftp_conn.php';
require_once('mysistem.php');
require_once('vendor/query.php');


$ftpObj               = new FTPClient();
$sistem               = new Mysistem();
$qbuilder             = new Myquery();
$query                = '';
$cfg_ontime           = '0000500000';
$basepath             = dirname(__FILE__);
$tbl_hider            = 'TF_JVHEADER';
$tbl_item             = 'TF_JVITEM';
$tbl_vendor             = 'MF_VENDOR';
$destination          = '/Download/JV/';
$xml_out              = '/file/output/';
$fild_initialkey      = "JV";

$fild_hider           = "CHR_bisnis_area,CHR_jv_id,concat(CHR_jv_th,REPLACE(CHR_jv_id,'".$fild_initialkey."','')) as jvid,date_format(DAT_date_posting,'%Y%m%d') as DAT_date_posting,";
$fild_hider          .= "date_format(DAT_invoice_date,'%Y%m%d') as DAT_invoice_date,CHR_company_code,";
$fild_hider          .= "CHR_invoice_number,'IDR' as curency,CHR_vendor_id,";
$fild_hider          .= "CHR_vendor_name,CHR_vendor_addres,CHR_vendor_kota,CHR_country, '' as kodepos,'' as blank,CHR_vendor_npwp,";
$fild_hider          .= "CONCAT('-',INT_amount) as INT_amount,";
$fild_hider          .= "INT_tax,CHR_faktur_pajak,date_format(DAT_date_base_line,'%Y%m%d') as DAT_date_base_line,";
$fild_hider          .= "CHR_payment_code,CHR_gl_account,CHR_country as country,date_format(DAT_faktur_date,'%Y%m%d') as DAT_faktur_date,";
$fild_hider          .= "CHR_wht_type,CHR_wht_code,INT_wht_base_amount";

$fild_item            = "concat(i.CHR_jv_th,REPLACE(i.CHR_jv_id,'".$fild_initialkey."','')) as jvid,i.CHR_gl_account,i.INT_amount,i.CHR_ppn_type,i.INT_ppn_amount,substr(i.CHR_cost_center,5,20) as CHR_cost_center,i.VCH_internal_order,i.VCH_text_desc";
$ar_row               = array();
$filter               = array();
$filter2              = array();
$ar_xml               = array();
$ar_file              = array();
$otv_id               = array();

$type = explode(',', $tbl_hider.','.$tbl_item);
foreach($type as $table){
  $filds ='';
  if($table == $tbl_hider){
    $filds = $fild_hider;
    
    $filter['parking']  = '0';
    $filter['flag']     = '0';
  }else{
    $filds = $fild_item;
    $table = $tbl_item.' i join '.$tbl_hider.' h ON h.CHR_jv_id = i.CHR_jv_id AND h.CHR_jv_th and i.CHR_jv_th';
    $filter['h.CHR_jv_id IN ("'.implode('","', $filter2).'")'] = NULL; 
  }


  $query= $qbuilder->build_query(
      array(
        'select'=> $filds,
        'from'  => $table,
        'where' => $filter
      )
  );

  $res= mysqli_query($conn, $query);
  if( mysqli_num_rows($res) > 0 ){
    while ($row = mysqli_fetch_assoc($res)){
      array_push($ar_row,$row);
      if($table == $tbl_hider)
        $filter2[] = $row['CHR_jv_id'];
    }
  }else{
    exit();
  }
  unset($filter['parking']);
  unset($filter['flag']);
}

$qbuilder->aasort($ar_row,"jvid");

$query_otv= $qbuilder->build_query(
    array(
      'select'=> 'VCH_VendordID',
      'from'  => $tbl_vendor,
      'where' => array('CHR_IsOtv'=>'1')
    )
);
$res_otv= mysqli_query($conn, $query_otv);
while ($row_otv = mysqli_fetch_assoc($res_otv)){
  $otv_id[] = $row_otv['VCH_VendordID'];
}


foreach ($ar_row as  $row){
  $key = $row['jvid'];
  unset($row['jvid']);
  $ar_xml[$key][] = $row;
}

if(count($ar_xml) > 0){
  foreach($ar_xml as $key =>$row){
    $date = date('Ymdhis');
    $bisname  = 'BIS'.$ar_xml[$key][0]['CHR_bisnis_area'].'-';
    $file_name = 'JV'.substr($key, 4,50).$bisname.$date.'.xml';

    if(!in_array($ar_xml[$key][0]['CHR_vendor_id'],$otv_id)){
      $ar_xml[$key][0]['CHR_vendor_name']='';
      $ar_xml[$key][0]['CHR_vendor_addres']='';
      $ar_xml[$key][0]['CHR_vendor_kota']='';
      $ar_xml[$key][0]['CHR_country']='';
      $ar_xml[$key][0]['kodepos']='';
      $ar_xml[$key][0]['blank']='';
      $ar_xml[$key][0]['CHR_vendor_npwp']='';
    }

    //unset($ar_xml[$key][0]['CHR_bisnis_area']);

    $ar_file[] = array(
        'file_name'=>$file_name,
        'key_id'   => 'JV'.substr($key, 4,50),
        'date'     => $date
    );

    $sistem->set_xml_data($ar_xml[$key], $file_name);
  }
}

if(count($ar_file)>0){
  $ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);
  foreach($ar_file as $row){
    $file_from = $basepath.$xml_out.$row['file_name'];
    $file_to   = $destination.$row['file_name'];
    $key       = array(
      'id'    => $row['key_id'],
      'date'  => $row['date'],
      'table' => $tbl_hider,
      'fild'  => "`CHR_jv_id`"
    );
    $ftpObj->upload($file_from, $file_to,$key);
  }
}
