<?php
include 'ftp_arl.php';
include 'connection.php';
include 'ftp_conn.php';
require_once('mysistem.php');
require_once('vendor/query.php');


$ftpObj               = new FTPClient();
$sistem               = new Mysistem();
$qbuilder             = new Myquery();
$type_billing         = 'ZP01';
$type_billing_materai = 'ZM02';
$query                = '';
$basepath             = dirname(__FILE__);
$ar_file              = array();
$ar_xml               = array();
$ar_xmln              = array();
$ar_key               = array();
$tbl_hider            = 'TF_BILLING_HEADER';
$tbl_item             = 'TF_BILLING_ITEM';
$destination          = '/Download/BILLING/';
$xml_out              = '/file/output/';


$type = explode(',', $type_billing.','.$type_billing_materai);
foreach($type as $row){
  $filds  = "h.VCH_SalesOrg,i.CHR_SalesDocument,i.CHR_ItemSD, date_format(h.DAT_BillingDate,'%Y%m%d') as DAT_BillingDate,date_format(i.DAT_BillingDateIndex,'%Y%m%d') as DAT_SettlemetDateDeadline,h.VCH_TaxClassCustomer,'' as blank, h.VCH_BillingDocumentWeb,h.VCH_Uraian,";
 
  if($row==$type_billing_materai)
    $filds .= "h.VCH_Keterangan, '".$row."' as condition_type,";
  else
    $filds .= "h.VCH_Keterangan, IF(CHR_AccountAssigmentGroup='Z1' ,'".$row."','ZP02') as condition_type,";
  
  $filds .= ($row ==$type_billing_materai)? "h.BIG_MateraiFee as ammount" : "i.INT_amount as ammount";

  $from   = $tbl_hider." h JOIN ".$tbl_item." i ON h.VCH_BillingDocumentWeb=i.BIG_BillingId";

  $filter['h.flag'] = '0';

  if($row==$type_billing_materai)
    $filter['h.BIG_MateraiFee IS NOT NULL'] = NULL;

  $query_array = array(
    'select'=> $filds,
    'from'  => $from,
    'where' => $filter
  );

  $query .= ($row ==$type_billing)? $qbuilder->build_query($query_array):' UNION ALL ('.$qbuilder->build_query($query_array).' GROUP BY  VCH_BillingDocumentWeb)';
}
$query .=" order by VCH_BillingDocumentWeb ASC,condition_type DESC";



$res= mysqli_query($conn, $query);
if(mysqli_num_rows($res)>0){
  while ($row = mysqli_fetch_assoc($res)){
    $key = $row['VCH_BillingDocumentWeb'];
    $org = 'BIS'.$row['VCH_SalesOrg'];


   unset($row['VCH_SalesOrg']);
    $ar_xml[$key][] = $row;
    $ar_xmln[$key]['nama'] = $org;
  }
}

if(count($ar_xml) > 0){
  foreach($ar_xml as $key =>$row){
    $file_name = $key.$ar_xmln[$key]['nama'].date('Ymd').'.xml';

    $ar_file[] = array(
        'file_name'=>$file_name,
        'key_id'   => $key,
        'date'     => date('Ymd')
    );

    $sistem->setXML($ar_xml[$key], $file_name);
  }
}


if(count($ar_file)>0){
  $ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);
  foreach($ar_file as $row){
    $file_from = $basepath.$xml_out.$row['file_name'];
    $file_to   = $destination.$row['file_name'];
    $key       = array(
      'id'    => $row['key_id'],
      'date'  => $row['date'],
      'table' => $tbl_hider,
      'fild'  => '`VCH_BillingDocumentWeb`'
    );
    $ftpObj->upload($file_from, $file_to,$key);
  }
}





