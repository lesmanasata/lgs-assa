#!/usr/bin/php
<?php 

require_once('connection.php');
require_once('ftp_conn.php');
include 'ftp_arl.php';
require_once('vendor/query.php');

$qbuilder	= new Myquery();
$ftpObj		= new FTPClient();

$tbl_header 	= 'TF_ORDER_HEADER';
$tbl_component 	= 'TF_ORDER_COMPONENTS';
$tbl_operation 	= 'TF_ORDER_OPERATION';
$tbl_operation_detail 	= 'TF_ORDER_OPERATION_ITEM';
$data 			= array();
$order_id 		= '';
$destination	= '/Download/ORDER/';
$xml_out		= '/file/output/';
$basepath		= dirname(__FILE__);

if (!is_dir($basepath.$xml_out)) {
    mkdir($basepath.$xml_out, 0777, true);
}

$hfilds	 = 'IFNULL(VCH_order_create_type," ") as ORDER_TYPE,';
$hfilds .= 'IFNULL(VCH_zbak_priority," ") as PRIORITY,';
$hfilds .= 'IFNULL(VCH_notif_id," ") as NOTIFICATION,';
$hfilds .= 'IFNULL(VCH_workcenter_main," ") as WORK_CTR,';
$hfilds .= 'IFNULL(VCH_workcenter_plan," ") as PLANT,';
$hfilds .= 'IFNULL(VCH_sys_cond," ") as SYST_COND,';
$hfilds .= 'IFNULL(VCH_po_number," ") as PURCH_ORDER,';
$hfilds .= 'IFNULL(date_format(VCH_po_date,"%Y%m%d")," ") as PURCH_ORDER_DATE,';
$hfilds .= 'IFNULL(VCH_pm_act_type," ") as ACTIVITY_TYPE,';
$hfilds .= 'IFNULL(date_format(DAT_basic_date_start,"%Y%m%d")," ") as DAT_basic_date_start,';
$hfilds .= 'IFNULL(date_format(DAT_basic_date_end,"%Y%m%d")," ") as DAT_basic_date_end,';
$hfilds .= 'IFNULL(VCH_user_create," ") as VCH_user_create,';
$hfilds .= 'IFNULL(INT_km," ") as INT_km,';
$hfilds .= 'VCH_order_id,';
$hfilds .= 'VCH_payment_terms,';
$hfilds .= 'VCH_ppntype,';
$hfilds .= 'VCH_vendor_addres,';
$hfilds .= 'VCH_vendor_post_code,';
$hfilds .= 'VCH_vendor_telpon,';
$hfilds .= 'VCH_vendor_kota,';
$hfilds .= 'VCH_order_id,';
$hfilds .= 'VCH_vendor_id,';
$hfilds .= 'TXT_description,';
$hfilds .= 'INT_cost,';
$hfilds .= 'VCH_equipment_id,';
$hfilds .= 'VCH_sold_toparty';

$arr_header = array(
	'select'=>$hfilds,
	'from'=> $tbl_header,
	'where'=>array(
		'staus'=>'1',
		'flag'=>'0',
		'LEFT(VCH_order_id,3)'=>'ORD'
	)
);

$query_header = $qbuilder->build_query($arr_header);
$res_header= mysqli_query($conn, $query_header);
if(mysqli_num_rows($res_header)>0){
  while ($row_header = mysqli_fetch_array($res_header)){
  	$data['ORDER_NO'] = $row_header['VCH_order_id'];
  	$data['ORDER_TYPE'] = $row_header['ORDER_TYPE'];
  	$data['DESCRIPTION'] = $row_header['TXT_description'];
  	$data['PRIORITY'] = $row_header['PRIORITY'];
  	$data['NOTIFICATION'] = ($row_header['ORDER_TYPE'] =='OR01')?' ':$row_header['NOTIFICATION'];
  	$data['SUPERIOR_ORDER'] = ($row_header['ORDER_TYPE'] =='OR01')?$row_header['NOTIFICATION']:' ';
  	$data['WORK_CTR'] = $row_header['WORK_CTR'];
  	$data['PLANT'] = $row_header['PLANT'];
  	$data['ACTIVITY_TYPE'] = $row_header['ACTIVITY_TYPE'];
  	$data['SYST_COND'] = $row_header['SYST_COND'];
  	$data['PURCH_ORDER'] = $row_header['PURCH_ORDER'];
  	$data['PURCH_ORDER_DATE'] = $row_header['PURCH_ORDER_DATE'];
  	$data['ESTIMATED_COSTS'] = $row_header['INT_cost'];
  	$data['CURRENCY'] = 'IDR';
  	$data['START_DATE'] = $row_header['DAT_basic_date_start'];
  	$data['FINISH_DATE'] = $row_header['DAT_basic_date_end'];
  	$data['KEY_PERSON'] = $row_header['VCH_user_create'];
  	$data['KM'] = $row_header['INT_km'];
  	$data['EQUIPMENT'] = $row_header['VCH_equipment_id'];

  	$data['PO']['PAYTERM'] = $row_header['VCH_payment_terms'];
  	$data['PO']['TAXCD'] = $row_header['VCH_ppntype'];
  	$data['PO']['STREET'] = $row_header['VCH_vendor_addres'];
  	$data['PO']['HOUSENO'] = ' ';
  	$data['PO']['POSTAL'] = $row_header['VCH_vendor_post_code'];
  	$data['PO']['CITY'] = $row_header['VCH_vendor_kota'];
  	$data['PO']['COUNTRY'] ='IDN';
  	$data['PO']['TELP'] = $row_header['VCH_vendor_telpon'];
  	$data['PO']['FAX'] = $row_header['INT_km'];

  	$data['PARTNER']['PARTNER_DETAIL']['PARTN_ROLE']= "AG";
  	$data['PARTNER']['PARTNER_DETAIL']['PARTNER']	= $row_header['VCH_sold_toparty'];

  	$order_id = $row_header['VCH_order_id'];
  	$order_plant = $row_header['PLANT'];
  	$arr_operation= array(
  		'select'=>'*',
		'from'=> $tbl_operation,
		'where'=>array(
			'VCH_order_id'=> $order_id
		)
  	);
  	
  	$query_operation = $qbuilder->build_query($arr_operation);
  	$res_operation= mysqli_query($conn, $query_operation);
	if(mysqli_num_rows($res_operation)>0){
		$no_op = 0;
		while ($row_operation = mysqli_fetch_array($res_operation)){
			$data['OPERATION'][$no_op]['OPERATION_NO'] 	= $row_operation['CHR_operation_id'];
			$data['OPERATION'][$no_op]['WORK_CTR']		= $row_header['WORK_CTR'];
			$data['OPERATION'][$no_op]['PLANT']			= $row_header['PLANT'];
			$data['OPERATION'][$no_op]['CTR_KEY']		= $row_operation['VCH_operation_type'];
			$data['OPERATION'][$no_op]['SHORT_TEXT']	= $row_operation['TXT_description'];
			$data['OPERATION'][$no_op]['WORK_ACTIVITY']	= '';
			$data['OPERATION'][$no_op]['WORK_UNIT']		= '';
			$data['OPERATION'][$no_op]['PURCH_ORG']		= '1000';
			$data['OPERATION'][$no_op]['PURCH_GROUP']	= $row_operation['VCH_purc_group'];
			$data['OPERATION'][$no_op]['REQUISITIONER']	= 'ASO';
			$data['OPERATION'][$no_op]['RECIPIENT']		= 'ASO';
			$data['OPERATION'][$no_op]['VENDOR']		= $row_header['VCH_vendor_id'];
			$data['OPERATION'][$no_op]['MATERIAL_GROUP']= '7200';

			$arr_operation_ex= array(
		  		'select'=>'*',
				'from'=> $tbl_operation_detail,
				'where'=>array(
					'VCH_order_id'=> $order_id,
					'VCH_operation_id'=>  $row_operation['CHR_operation_id']
				)
		  	);
		  	$query_operation_ex = $qbuilder->build_query($arr_operation_ex);
		  	$res_operation_ex= mysqli_query($conn, $query_operation_ex);
			if(mysqli_num_rows($res_operation_ex)>0){
				$no_op_ex = 0;
				while ($row_operation_ex = mysqli_fetch_array($res_operation_ex)){
					
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['ITEM_NO']	= $row_operation_ex['VCH_operation_item_no'];
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['SHORT_TEXT']= $row_operation_ex['VCH_purch'];
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['QTY']		= $row_operation_ex['BIG_qty'];
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['UNIT']		= 'EA';
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['PRICE']		= $row_operation_ex['BIG_price'];
					$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'][$no_op_ex]['CURRENCY']	= 'IDR';
					$no_op_ex ++;
				}
			}else{
				$data['OPERATION'][$no_op]['EXTERNAL_SERVICES'] = " ";
			}
			$no_op++;
		}
	}else{
		$data['OPERATION'] = " ";
	}
	
	
	$arr_component= array(
  		'select'=>'*',
		'from'=> $tbl_component,
		'where'=>array(
			'VCH_order_id'=> $order_id
		)
  	);
	$query_component = $qbuilder->build_query($arr_component);
	$res_component= mysqli_query($conn, $query_component);
	if(mysqli_num_rows($res_component)>0){
		$no_cp=0;
		while ($row_component = mysqli_fetch_array($res_component)){
			$data['COMPONENT'][$no_cp]['ITEM_NO'] 		= $row_component['VHC_component_id'];
			$data['COMPONENT'][$no_cp]['ITEM_CAT'] 		= $row_component['CHR_component_type'];
			$data['COMPONENT'][$no_cp]['COMPONENT'] 	= $row_component['VCH_part_number'];
			$data['COMPONENT'][$no_cp]['PLANT'] 		= $row_header['PLANT'];
			$data['COMPONENT'][$no_cp]['REQ_QTY'] 		= $row_component['BIG_qty'];
			$data['COMPONENT'][$no_cp]['UNIT'] 			= $row_component['VCH_uom'];
			$data['COMPONENT'][$no_cp]['OPERATION_NO'] 	= $row_component['VCH_operation_number'];
			$data['COMPONENT'][$no_cp]['PURCH_GROUP'] 	= $row_component['CHR_purch_group'];
			$data['COMPONENT'][$no_cp]['REQUISITIONER'] = "ASO";
			$data['COMPONENT'][$no_cp]['RECIPIENT'] 	= "ASO";
			$data['COMPONENT'][$no_cp]['VENDOR'] 		= $row_header['VCH_vendor_id'];
			$data['COMPONENT'][$no_cp]['PRICE'] 		= $row_component['BIG_price'];
			$data['COMPONENT'][$no_cp]['CURRENCY'] 		= "IDR";
			$no_cp++;

		}
	}else
		$data['COMPONENT']=" ";

	//print_r($data);

	$xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><WORK_ORDER></WORK_ORDER>');
	array_to_xml($data,$xml_data);
	unset($data);
	$file_name ='ORD_'.$order_id.'BIS'.$order_plant.'.xml';
	$result = $xml_data->asXML(dirname(__FILE__).'/file/output/'.$file_name);
	
	 if($result){
		$file_from = $basepath.$xml_out.$file_name;
	    $file_to   = $destination.$file_name;
	    $key       = array(
	      'id'    => $order_id,
	      'date'  => date('Ymd'),
	      'table' => $tbl_header,
	      'fild'  => 'VCH_order_id'
	    );
		$ftpObj->connect(FTP_HOST, FTP_USER, FTP_PASS);
	    $ftpObj->upload($file_from, $file_to,$key);
	}
  }
}



function array_to_xml( $data, &$xml_data,$skey='' ) {
    foreach( $data as $key => $value ) {

        if( is_numeric($key) ){
            $key = $skey.'_DETAIL'; 
        }
        if( is_array($value) ) {
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode,$key);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}