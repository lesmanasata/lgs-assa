#!usr/bin/php
<?php
require_once(dirname(__FILE__)."/vendor/ftp.php");
require_once(dirname(__FILE__)."/vendor/xmltodb.php");

$ftp 	= new MyFtp();
$xmldb  = new XmlToDb();
$eksec 	= array(
	array(
		'initial' 	=> 'CANC',
		'table'  	=> 'TF_CLEARING_RESET',
		'fild' 		=> 'VCH_clearing_id,VCH_user_proses,DAT_tgl_proses'
	)
);

require_once(dirname(__FILE__)."/vendor/helper.php");