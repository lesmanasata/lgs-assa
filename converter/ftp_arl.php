<?php
require_once('mysistem.php');


$sistem = new Mysistem();
class FTPClient
{
    // *** Class variables

    public function __construct()
    {
    }
    private $connectionId;
    private $loginOk = false;
    private $messageArray = array();

    private function logMessage($message)
    {
        $this->messageArray[] = $message;
    }
    public function getMessages()
    {
        return $this->messageArray;
    }

    public function connect($server, $ftpUser, $ftpPassword, $isPassive=true)
    {
        //Set up basic connection
        $this->connectionId = ftp_connect($server);

        //Login with username and password
        $loginResult = ftp_login($this->connectionId, $ftpUser, $ftpPassword);

        //Sets passive mode on/off (default off)
        ftp_pasv($this->connectionId, $isPassive);

        //Check connection
        if ((!$this->connectionId) || (!$loginResult)) {
            $this->logMessage('FTP connection has failed!');
            $this->logMessage('Attempted to connect to ' . $server . ' for user ' . $ftpUser, true);
            return false;
        } else {
            $this->logMessage('Connected to ' . $server . ', for user ' . $ftpUser);
            $this->loginOk = true;
            return true;
        }
    }

    public function uploadFile1($fileFrom, $fileTo, $key, $doc_type, $biscode, $date)
    {
        // *** Upload the file
        $upload = ftp_put($this->connectionId, $fileTo, $fileFrom, FTP_BINARY);

        // *** Check upload status
        if (!$upload) {
            $this->logMessage('FTP upload has failed!');
            rename(''.$fileFrom.'', dirname(__FILE__).'/file/output/failed/fail_'.$doc_type.'bis'.$biscode.$date.'.xml');
            return false;
        } else {
            $this->logMessage('Uploaded "' . $fileFrom . '" as "' . $fileTo);
            $this->ArltoOne1($key);
            rename(''.$fileFrom.'', dirname(__FILE__).'/file/output/success/up_'.$doc_type.'bis'.$biscode.$date.'.xml');
            return true;
        }
    }
    public function uploadFile2($fileFrom, $fileTo, $doc_type, $biscode, $date)
    {
        // *** Upload the file
        $upload = ftp_put($this->connectionId, $fileTo, $fileFrom, FTP_BINARY);

        // *** Check upload status
        if (!$upload) {
            $this->logMessage('FTP upload has failed!');
            rename(''.$fileFrom.'', dirname(__FILE__).'/file/output/failed/fail_cost_'.$doc_type.'bis'.$biscode.$date.'.xml');
            return false;
        } else {
            $this->logMessage('Uploaded "' . $fileFrom . '" as "' . $fileTo);
            $this->ArltoOne1($doc_type);
            rename(''.$fileFrom.'', dirname(__FILE__).'/file/output/success/up_cost_'.$doc_type.'bis'.$biscode.$date.'.xml');
            return true;
        }
    }

    public function ArltoOne1($keyUp)
    {
            include 'connection.php';
            $query = "UPDATE `TF_BKPF` SET `CHR_FLAG` = 1 WHERE `VCH_BELNR`='$keyUp'";
            $sql = mysqli_query($conn, $query);
    }

    public function __deconstruct()
    {
        if ($this->connectionId) {
            ftp_close($this->connectionId);
        }
    }

    public function upload($fileFrom, $fileTo, $key=array())
    {
        include 'connection.php';

        $upload = ftp_put($this->connectionId, $fileTo, $fileFrom, FTP_BINARY);
        $directoryName = dirname(__FILE__).'/file/output/';
        if (!$upload) {
            rename($fileFrom, $directoryName.'failed/fail_BIL'.$key.$date.'.xml');
            return false;
        } else {
            $query = "UPDATE `".$key['table']."` SET `flag` = 1 WHERE ".$key['fild']."='".$key['id']."'";
            $sql = mysqli_query($conn, $query);

            rename($fileFrom, $directoryName.'success/up_BIL'.$key['id'].$key['date'].'.xml');
            return true;
        }
    }

    public function upload_serca($fileFrom, $fileTo, $key=array())
    {
        include 'connection.php';

        $upload = ftp_put($this->connectionId, $fileTo, $fileFrom, FTP_BINARY);
        $directoryName = dirname(__FILE__).'/file/output/';
        if (!$upload) {
            rename($fileFrom, $directoryName.'failed/fail_'.$key.$date.'.xml');
            return false;
        } else {
            $query = "UPDATE `".$key['table']."` SET `flag_serca` = 1 WHERE ".$key['fild']."='".$key['id']."'";
            $sql = mysqli_query($conn, $query);

            rename($fileFrom, $directoryName.'success/up_'.$key['id'].$key['date'].'.xml');
            return true;
        }
    }


}
