<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $template['title']; ?></title>
<?php echo $template['metadata']; ?>
<!-- Custom Theme files -->
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>themes/default/css/bootstrap.min.css"  />
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>themes/default/css/style.css" />
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>themes/default/css/font-awesome.css" > 
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>assets/styles/kendo.common.min.css" />
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>assets/styles/kendo.material.min.css" />
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>themes/default/css/custom.css" >

<!--<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/jquesry.min.js"> </script>-->

<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/jquery.nicescroll.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/scripts.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/custom.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/screenfull.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/jquery.metisMenu.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/jquery.slimscroll.min.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/kendo.all.min.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>themes/default/js/bootstrap.min.js"> </script>
<script 	type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/msg_box.js"></script> 

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<script>
	$(function () {
		$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

		if (!screenfull.enabled) {
			return false;
		}

		$('#toggle').click(function () {
			screenfull.toggle($('#container')[0]);
		});
		
		 $('.staticParent').on('keydown', '.numberOnly', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
		
	});
</script>
<style type="text/css">
	#padding_tab_link{padding:10px 15px;}
	.padding_tab_body{padding-left:15px;}
</style>


</head>
<body>
<div id="wrapper">
       <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="#"><?php echo $this->config->item('app'); ?></a></h1>         
			</div>
			
			<div class=" border-bottom">
				<div class="full-left">
					<section class="full-top"> <button id="toggle"><i class="fa fa-arrows-alt"></i></button>	</section>
					<div class="clearfix"> </div>
				</div>
 
            <!-- Brand and toggle get grouped for better mobile display -->
		 
			   <!-- Collect the nav links, forms, and other content for toggling -->
				<div class="drop-men" >
					<ul class=" nav_1">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown">
								<span class=" name-caret"><?php echo str_replace("|"," ",$this->session->userdata('real_name')); ?><i class="caret"></i></span>
								<img src="<?php echo base_url()."themes/default/images/wo.jpg"; ?>">
							</a>
						  <ul class="dropdown-menu " role="menu">
								<li><?php echo anchor('user/password','Ganti Password'); ?></li>
								<li><?php echo anchor('user/log/out','Log Out'); ?></li>
						  </ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			<div class="clearfix"></div>
	  
		    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
					<?php echo Modules::run('app/menu/showMenu'); ?>
				</div>
			</div>
        </nav>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			   <div class="content-main">
					<?php
							$alert_message = $this->session->flashdata('alert_message');
							$alert_type = $this->session->flashdata('alert_type');
							echo $alert_message == '' ? '' : '<div class="typo-1" ><div class="alert alert-'.$alert_type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
							<p>'.$alert_message.'</p>
							</div></div><script>$(".alert").bind("close", function(){$(this).addClass("fade out");});$(".alert").alert();</script>';
					?>
					<?php echo $template['body']; ?>
				</div>
		</div>
		</div>
		<div class="clearfix"> </div>
       </div>
     
<!---->
<!--scrolling js-->

	<!--//scrolling js-->
</body>
</html>
<script>

	kendo.culture("de-DE");
	 var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
	var datepicker=$(".datepicker").kendoDatePicker({
		format: "yyyy-MM-dd",
		//value:todayDate
	});

	
	
	function numberWithCommas(x) {
		x = x.toString();
		var pattern = /(-?\d+)(\d{3})/;
		while (pattern.test(x))
			x = x.replace(pattern, "$1,$2");
		return x;
	}
	
	//use onkeypress='numberOnly(event)'
	function numberOnly(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}
</script>
