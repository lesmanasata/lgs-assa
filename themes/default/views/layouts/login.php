<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $template['title']; ?></title>
<?php echo $template['metadata']; ?>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="<?php echo $this->config->base_url(); ?>themes/default/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="<?php echo $this->config->base_url(); ?>themes/default/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo $this->config->base_url(); ?>themes/default/css/font-awesome.css" rel="stylesheet"> 
<script src="<?php echo $this->config->base_url(); ?>themes/default/js/jquery.min.js"> </script>
<script src="<?php echo $this->config->base_url(); ?>themes/default/js/bootstrap.min.js"> </script>
</head>
<body>
	<div class="login">
		<h1><a href="index.html"><?php echo strtoupper($this->config->item('app')); ?> </a></h1>
		<div class="login-bottom">
			<?php 
					$alert_message = $this->session->flashdata('alert_message');
					$alert_type = $this->session->flashdata('alert_type');
					echo $alert_message == '' ? '' : '<div class="alert alert-'.$alert_type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
					<p>'.$alert_message.'</p>
					</div><script>$(".alert").bind("close", function(){$(this).addClass("fade out");});$(".alert").alert();</script>';
					?>
					<?php echo $template['body']; ?>
		</div>
	</div>
		<!---->
<div class="copy-right">
            <p> &copy; 2017 Payment. All Rights Reserved  </div>  
<!---->
<!--scrolling js-->
	<script src="<?php echo $this->config->base_url(); ?>themes/default/js/jquery.nicescroll.js"></script>
	<script src="<?php echo $this->config->base_url(); ?>themes/default/js/scripts.js"></script>
	<!--//scrolling js-->
</body>
</html>

