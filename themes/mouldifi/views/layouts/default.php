<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $template['title']; ?></title>
<?php echo $template['metadata']; ?>
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='<?php echo $this->config->base_url(); ?>themes/mouldifi/images/favicon.ico' />
<!-- /site favicon -->

<!-- Entypo font stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/entypo.css" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/font-awesome.min.css" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/plugins/css3-animate-it-plugin/animations.css" rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/bootstrap.min.css" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/mouldifi-core.css" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/mouldifi-forms.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/html5shiv.min.js"></script>
      <script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>assets/styles/kendo.common.min.css" />
<link rel="stylesheet" type='text/css' href="<?php echo $this->config->base_url(); ?>assets/styles/kendo.material.min.css" />

<!--Load JQuery-->
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/jquery.min.js"></script>
<script src="<?php echo $this->config->base_url(); ?>assets/js/select2.min.js"></script>
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/bootstrap.min.js"></script>
<script 	type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/msg_box.js"></script>
<script  type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/kendo.all.min.js"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<?php if($this->uri->segment(2) !='filemanager'){ ?>
	<style type="text/css">
		*{
		    -webkit-box-sizing:border-box;
		    -moz-box-sizing:border-box;
		    box-sizing:border-box
		}
	</style>
<?php } ?>
<script>

    $(document).ready(function(){

     $("#example").kendoGrid({
                        height: 550,
                        sortable: true
                    });

        $("#comboBox").kendoComboBox();
		$("#cust_name").width(500).kendoComboBox();
		$("#doc_type").width(500).kendoComboBox();
		$("#sales_org").width(500).kendoComboBox();
    });

	$(function () {
		/*$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

		if (!screenfull.enabled) {
			return false;
		}

		$('#toggle').click(function () {
			screenfull.toggle($('#container')[0]);
		});*/

		 $('.staticParent').on('keydown', '.numberOnly', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
		$('.select').select2();
	});
</script>
<style type="text/css">
	.padding_tab_body{padding-top:15px;}
	.loader{
		background-image:url('<?php echo $this->config->base_url(); ?>themes/mouldifi/images/loader_1.gif') !important;
		background-position:center center;
		background-repeat:no-repeat;
		min-height:50px;
		min-width:50px;
	}

	.loader-mask {
	  position: absolute;
	  height: 100%;
	  width: 100%;
	  background-color: #fff;
	  bottom: 0;
	  left: 0;
	  right: 0;
	  top: 0;
	  z-index: 9999;
	  opacity: 0.4;
	  background-image:url('<?php echo $this->config->base_url(); ?>themes/mouldifi/images/loader_1.gif') !important;
	  background-position:center center;
	  background-repeat:no-repeat;
	}
</style>
</head>
<body>
<div id="loading-mask"></div>
<!-- Page container -->
<div class="page-container">

  <!-- Page Sidebar -->
  <div class="page-sidebar">

  		<!-- Site header  -->
		<header class="site-header">
		  <div class="site-logo"><a href="#"><img src="<?php echo $this->config->base_url(); ?>themes/mouldifi/images/logo.png" alt="<?php echo $this->config->item('app'); ?>" title="<?php echo $this->config->item('app'); ?>"></a></div>
		  <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
		  <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
		</header>
		<!-- /site header -->

		<!-- Main navigation -->
		<ul id="side-nav" class="main-menu navbar-collapse collapse">
			<?php echo Modules::run('app/menu/showMenu'); ?>
		</ul>
		<!-- /main navigation -->
  </div>
  <!-- /page sidebar -->

  <!-- Main container -->
  <div class="main-container">

	<!-- Main header -->
    <div class="main-header row">
      <div class="col-sm-12 col-xs-12">

		  <div class="pull-right">
			<!-- User info -->
	        <ul class="user-info ">
	          <li class="profile-info dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
	          	<img width="44" class="img-circle avatar" alt="" src="<?php echo base_url()."themes/default/images/wo.jpg"; ?>"><?php echo str_replace("|"," ",$this->session->userdata('real_name')); ?> <span class="caret"></span></a>

				<!-- User action menu -->
	            <ul class="dropdown-menu">
				  <li><?php echo anchor('user/password','<i class="icon-cog"></i> Ganti Password'); ?></li>
				  <li class="divider"></li>
				  <li><?php echo anchor('user/log/out','<i class="icon-logout"></i> Log Out'); ?></li>
	            </ul>
				<!-- /user action menu -->

	          </li>
	        </ul>
			<!-- /user info -->

	      </div>
      </div>

    </div>
	<!-- /main header -->

	<!-- Main content -->
	<div class="main-content">

		<div class="row">
			<div class="col-lg-12">
		<?php
			$alert_message = $this->session->flashdata('alert_message');
			$alert_type = $this->session->flashdata('alert_type');
			echo $alert_message == '' ? '' : '<div class="typo-1" ><div class="alert alert-'.$alert_type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>'.$alert_message.'</p>
			</div></div><script>$(".alert").bind("close", function(){$(this).addClass("fade out");});$(".alert").alert();</script>';
		?>
		<?php echo $template['body']; ?>
			</div>
		</div>

	  </div>
	  <!-- /main content -->

  </div>
  <!-- /main container -->

</div>
<!-- /page container -->

<!-- Load CSS3 Animate It Plugin JS -->
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/plugins/css3-animate-it-plugin/css3-animate-it.js"></script>

<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/plugins/metismenu/jquery.metisMenu.js"></script>
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/functions.js"></script>

</body>
</html>
<script>

	kendo.culture("de-DE");
	 var todayDate = kendo.toString(kendo.parseDate(new Date()), 'yyyy-MM-dd');
	$(".datepicker").kendoDatePicker({
		format: "dd-MM-yyyy",
		//value:todayDate
	});



	function numberWithCommas(x) {
		x = x.toString();
		var pattern = /(-?\d+)(\d{3})/;
		while (pattern.test(x))
			x = x.replace(pattern, "$1,$2");
		return x;
	}

	//use onkeypress='numberOnly(event)'
	function numberOnly(evt) {
	  var theEvent = evt || window.event;
	  var key = theEvent.keyCode || theEvent.which;
	  key = String.fromCharCode( key );
	  var regex = /[0-9]|\./;
	  if( !regex.test(key) ) {
		theEvent.returnValue = false;
		if(theEvent.preventDefault) theEvent.preventDefault();
	  }
	}
</script>
