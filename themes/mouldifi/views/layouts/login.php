<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $template['title']; ?></title>
<?php echo $template['metadata']; ?>

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='<?php echo $this->config->base_url(); ?>themes/mouldifi/images/favicon.ico' />
<!-- /site favicon -->

<!-- Entypo font stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/entypo.css" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/font-awesome.min.css" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/plugins/css3-animate-it-plugin/animations.css" rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/bootstrap.min.css" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/mouldifi-core.css" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="<?php echo $this->config->base_url(); ?>themes/mouldifi/css/mouldifi-forms.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
<![endif]-->

<style type="text/css">
	.padTop{padding-top:15px;}
	*{
	    -webkit-box-sizing:border-box;
	    -moz-box-sizing:border-box;
	    box-sizing:border-box
	}
</style>

</head>
<body class="login-page">
	<div class="login-pag-inner">
		<div class="animatedParent animateOnce z-index-50">
			<div class="login-container animated growIn slower">
				<div class="login-branding">
					<a href="#"><img src="<?php echo $this->config->base_url(); ?>themes/mouldifi/images/logo.png" alt="<?php echo strtoupper($this->config->item('app')); ?>" title="Mouldifi"></a>
				</div>
				<div class="login-content">
					<!--<form method="post" action="index.html">                        
						<div class="form-group">
							<input type="text" placeholder="Username" class="form-control">
						</div>                        
						<div class="form-group">
							<input type="password" placeholder="Password" class="form-control">
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-block">Login</button>
						</div>                     
					</form>-->
					<?php echo $template['body']; ?>
					<?php 
						$alert_message = $this->session->flashdata('alert_message');
						$alert_type = $this->session->flashdata('alert_type');
						echo $alert_message == '' ? '' : '<div class="alert alert-'.$alert_type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>
						<p>'.$alert_message.'</p>
						</div><script>$(".alert").bind("close", function(){$(this).addClass("fade out");});$(".alert").alert();</script>';
					?>
					
				</div>
			</div>
		</div>
	</div>
<!--Load JQuery-->
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/jquery.min.js"></script>
<!-- Load CSS3 Animate It Plugin JS -->
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/plugins/css3-animate-it-plugin/css3-animate-it.js"></script>
<script src="<?php echo $this->config->base_url(); ?>themes/mouldifi/js/bootstrap.min.js"></script>
</body>
</html>
